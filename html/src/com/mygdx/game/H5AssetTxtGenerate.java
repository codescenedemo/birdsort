package com.mygdx.game;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;

public class H5AssetTxtGenerate {
    //public static String[] assets = {"assets0", "loadingScreen", "map1", "map2"};
    public static String assetPath = "war/assets/assets.txt";
    public static String importPath = "../h5assets/";
    public static String exportPath = "build/gwt/draftOut/assets/"; //"war/assets/";
    public static String deployPath = "../h5assets/out/"; //"war/assets/";

    public static void main(String[] args) throws Exception
    {
        ArrayList<String> assets = getAllTxtFile();
        try {
            for(int i=0;i<assets.size();i++) {
                String name = assets.get(i);
                ArrayList<String> assetList = readArrayText(assetPath);
                generateAssetTxt(assetList, name);
            }
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    public static ArrayList<String> getAllTxtFile(){
        ArrayList<String> txtList = new ArrayList<>();
        File directoryPath = new File(importPath);
        String contents[] = directoryPath.list();
        System.out.println("List of files and directories in the specified directory:");
        for(int i=0; i<contents.length; i++) {
            if(contents[i].contains(".txt"))
                txtList.add(contents[i]);
        }
        return txtList;
    }


    public static void generateAssetTxt(ArrayList<String> assetList, String name) throws Exception{
        System.out.println("generate asset " + name);
        ArrayList<String> inputList = readArrayText(importPath + name);

        ArrayList<String> result = new ArrayList<>();
        for (String line : assetList) {
            if (isArrayContainString(inputList, line)) {
                result.add(line);
            }
        }
        String output = exportPath  + "assets-" + name;
        String deploy = deployPath + "assets-" + name;

        if(name.contains("assets0")) {
            output = exportPath + "assets.txt";
            deploy = deployPath + "assets.txt";
        }
        try {
            writeToFile(output, result);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
        writeToFile(deploy, result);
    }
    public static boolean isArrayContainString(ArrayList<String> array, String str){
        for (String line:array) {
            if(str.contains(line))
                return true;
        }
        return false;
    }

    public static ArrayList<String> readArrayText(String filename) throws  Exception{
        File file = new File(filename);
        ArrayList<String> r = new ArrayList<String>();
        BufferedReader br  = new BufferedReader(new FileReader(file));
        String st;
        while ((st = br.readLine()) != null) {
            if(!st.equals("")) {
                r.add(st);
                //System.out.println(st);
            }
        }
        return r;
    }

    public static void writeToFile(String fileName, ArrayList<String> lines) throws Exception{
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        for (String line: lines) {
            bw.write(line + "\n");
        }
        bw.close();
    }
}
