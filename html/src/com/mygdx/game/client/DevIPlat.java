package com.mygdx.game.client;

import com.core.utils.PrefSLoader;
import com.core.utils.SessionLoader;
import com.platform.AdsClose;

public class DevIPlat extends FBIPlat{
    public DevIPlat(HtmlLauncher launcher) {
        super(launcher);
    }

    @Override
    public SessionLoader getSessionLoader() {
        return new PrefSLoader("com.zs.supermoria");//todo build superDev
    }

    @Override
    public void ShowFullscreen(AdsClose onshow, AdsClose ondone) {
        onshow.OnEvent(true);
        ondone.OnEvent(true);
    }

    @Override
    public boolean isVideoRewardReady() {
        return true;
    }

    @Override
    public void ShowVideoReward(AdsClose callback) {
        callback.OnEvent(true);
    }

    @Override
    public void ShowBanner(boolean visible) {

    }
}
