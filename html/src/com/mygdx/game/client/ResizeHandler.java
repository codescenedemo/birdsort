package com.mygdx.game.client;

import com.google.gwt.event.logical.shared.ResizeEvent;
import com.google.gwt.event.logical.shared.ResizeHandler;

class ResizeListener implements ResizeHandler {
  HtmlLauncher launcher;
  public ResizeListener(HtmlLauncher launcher) {
    this.launcher = launcher;
  }
  @Override
  public void onResize(ResizeEvent event) {
    launcher.resizeCanvas();
  }
}
