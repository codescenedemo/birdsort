package com.mygdx.game.client;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.GwtApplicationConfiguration;
import com.badlogic.gdx.backends.gwt.preloader.Preloader;
import com.badlogic.gdx.files.FileHandle;
import com.game.GMain;
import com.google.gwt.dom.client.CanvasElement;
import com.google.gwt.dom.client.Style;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Panel;
import com.platform.IPlat;

public class HtmlLauncher extends GwtApplication {
  int PADDING = 0;
  boolean isHandleResize = false;

  @Override
  public GwtApplicationConfiguration getConfig () {

    int height = com.google.gwt.user.client.Window.getClientHeight() - PADDING;
    int width = com.google.gwt.user.client.Window.getClientWidth()- PADDING;

    GwtApplicationConfiguration cfg = new GwtApplicationConfiguration(width, height);
    Window.enableScrolling(false);
    Window.setMargin("0");
    isHandleResize = true;
    Window.addResizeHandler(new ResizeListener(this));

    cfg.disableAudio = false;
    return cfg;
  }

  @Override
  protected void adjustMeterPanel(Panel meterPanel, Style meterStyle) {
    meterPanel.setStyleName("gdx-meter");
    meterStyle.setProperty("backgroundColor","#0068ED");
    meterStyle.setProperty("backgroundImage","none");
    super.adjustMeterPanel(meterPanel, meterStyle);
  }

  @Override
  public Preloader.PreloaderCallback getPreloaderCallback() {
    return new Preloader.PreloaderCallback() {
      public void error(String file) {
        consoleLog("error: " + file);
      }

      public void update(Preloader.PreloaderState state) {
        double percent = 100.0F * state.getProgress();
        FBInstant.LoadingProgress((int)percent);

        if(state.hasEnded()) {
          FBInstant.LoadingFinished();
        }
      }
    };
  }

  @Override
  public ApplicationListener createApplicationListener () {
    GwtApplication.consoleLog("dev mode: " + FBInstant.isDev());
    return new GMain(FBInstant.isDev() ? new DevIPlat(this) : new FBIPlat(this));
  }

  public void setUpCanvas(){

    CanvasElement canvas = getCanvasElement();
    int w = canvas.getWidth();
    int h = canvas.getHeight();
    double dpr  = DOMServices.devicePixelRatio();
    int cw = (int)(w*dpr);
    int ch = (int)(h*dpr);


    if(isHandleResize){
      w =   DOMServices.getInnerWidth();
      h =   DOMServices.getInnerHeight();
      cw =  DOMServices.getWidthPx();
      ch =  DOMServices.getHeightPx();
    }

    canvas.getStyle().setWidth(w, Style.Unit.PX);
    canvas.getStyle().setHeight(h, Style.Unit.PX);
    canvas.setWidth(cw);
    canvas.setHeight(ch);
  }

  public void resizeCanvas(){
    CanvasElement canvas = getCanvasElement();
    int fw = DOMServices.getInnerWidth();
    int fh = DOMServices.getInnerHeight();
    canvas.getStyle().setWidth(fw, Style.Unit.PX);
    canvas.getStyle().setHeight(fh, Style.Unit.PX);
    canvas.setWidth(DOMServices.getWidthPx());
    canvas.setHeight(DOMServices.getHeightPx());
  }


  @Override
  public void onModuleLoad() {
    super.onModuleLoad();
  }

  public void preloadAssets(String assetTxt, IPlat.PreloaderCallback callback) {
    //final Preloader.PreloaderCallback callback = getPreloaderCallback();
    Preloader preloader = this.getPreloader();
    preloader.preload(assetTxt, new Preloader.PreloaderCallback() {
      @Override
      public void error (String file) {
        //callback.error(file);
        GwtApplication.consoleLog(file);
        callback.error(file);
      }

      @Override
      public void update (Preloader.PreloaderState state) {
//        callback.update(state);
        GwtApplication.consoleLog("preloadAssets progress " + state.getProgress());

        if (state.hasEnded()) {
          GwtApplication.consoleLog("preloadAssets done");
        }
        callback.update(state.getProgress(), state.hasEnded());
      }
    });
  }
}
