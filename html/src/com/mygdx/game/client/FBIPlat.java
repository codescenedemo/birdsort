package com.mygdx.game.client;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.gwt.GwtApplication;
import com.badlogic.gdx.backends.gwt.preloader.Preloader;
import com.badlogic.gdx.utils.ObjectMap;
import com.core.utils.SessionLoader;
import com.gameservice.PlayService;
import com.platform.AdsClose;
import com.platform.IPlat;

public class FBIPlat implements IPlat {
  boolean isCanvasSetup = false;
  HtmlLauncher launcher;
  long lastFullScreenTime;

  public FBIPlat(HtmlLauncher launcher) {
    this.launcher = launcher;
  }

  @Override
  public void log(String str) {
    GwtApplication.consoleLog(str);
  }

  @Override
  public String getMd5PathName(String filename) {
    Preloader preloader = ((com.badlogic.gdx.backends.gwt.GwtApplication) Gdx.app).getPreloader();
    return preloader.assetNames.get(filename);
  }

  @Override
  public void preloadAssets(String assetTxt, PreloaderCallback callback) {
    launcher.preloadAssets(assetTxt, callback);
  }

  @Override
  public SessionLoader getSessionLoader() {
//    return new PrefSLoader("com.zs.supermoria");//todo build superDev
    return new FBSLoader("com.zs.supermoria");//todo build Dist
  }

  @Override
  public String getPlatform() {
    String p = FBInstant.getPlatform();
    if (p == null)
      p = "WEB";
    return p;
  }

  @Override
  public PlayService getPlayService() {
    return new FBPlayService();
  }

  @Override
  public String GetDefaultLanguage() {
    return FBInstant.getLocale().split("_")[0];
  }

  @Override
  public boolean isVideoRewardReady() {
    return FBInstant.IsVideoRewardReady();
  }

  @Override
  public void ShowVideoReward(AdsClose callback) {
    FBInstant.ShowVideoReward(callback);
  }

  @Override
  public void ShowRewardInterstitial(AdsClose callback) {

  }

  @Override
  public void ShowFullscreen(AdsClose onshow, AdsClose ondone) {
    long curMs = System.currentTimeMillis();
    if ((curMs - lastFullScreenTime) >= GetConfigIntValue("fullScreenTime", 40000)) {
      lastFullScreenTime = curMs;
      FBInstant.ShowFullscreen(onshow, ondone);
    }
    else {
      FBInstant.blankFullscreen(onshow, ondone);
    }
  }

  @Override
  public void ShowUIFullscreen() {

  }

  @Override
  public void ShowNative(boolean visible) {

  }

  @Override
  public boolean IsNativeAdvancedReady() {
    return false;
  }

  @Override
  public boolean isFullScreenReady() {
    return FBInstant.IsFullscreenReady();
  }

  @Override
  public void ShowBanner(boolean visible) {
    if (visible)
      FBInstant.showBanner();
    else
      FBInstant.hideBanner();
  }

  @Override
  public int GetConfigIntValue(String name, int defaultValue) {
    return defaultValue;
  }

  @Override
  public String GetConfigStringValue(String name, String defaultValue) {
    return defaultValue;
  }

  @Override
  public void CrashKey(String key, String value) {

  }

  @Override
  public void CrashLog(String log) {

  }

  @Override
  public int GetBannerHeight() {
    return 0;
  }

  @Override
  public void OpenStore() {

  }

  @Override
  public void TrackScreen(String screen) {

  }

  @Override
  public void TrackNotification(int notifyId, int dailyNotificationCount) {

  }

  @Override
  public void TrackCustomEvent(String event) {

  }

  @Override
  public void TrackGameStart(int serverVersion) {

  }

  @Override
  public void TrackEvaluate(String eventname, int hitCount, String evaluate) {

  }

  @Override
  public void TrackLevelInfo(String event, int level, float time) {

  }

  @Override
  public void TrackLevelInfo(String event, ObjectMap<String, Object> data) {

  }

  @Override
  public void TrackPlayerInfo(String event, int mode, int level) {

  }

  @Override
  public void TrackPlaneInfo(String event, int planeid, int level) {

  }

  @Override
  public void TrackVideoReward(String type) {

  }

  @Override
  public void TrackPlayerDead(String event, int mode, int difficult, int level, int parentModel, int shooterModel, boolean isBoss) {

  }

  @Override
  public int GetNotifyId() {
    return 0;
  }

  @Override
  public void SetDailyNotification(int id, String header, String content, int days, int hours) {

  }

  @Override
  public void CancelDailyNotification(int id) {

  }

  @Override
  public void TrackRating(int rateid, int rateFromLevel) {

  }

  @Override
  public void Restart() {

  }

  @Override
  public void ShowAdsTestSuit() {

  }


  @Override
  public void onShow() {
    if(!isCanvasSetup){
      launcher.setUpCanvas();
      isCanvasSetup = true;
    }
  }

  @Override
  public int dayDiff(long t1, long t2) {
    return DOMServices.dayDiff(t1, t2);
  }
}
