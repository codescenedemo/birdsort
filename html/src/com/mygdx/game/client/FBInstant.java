package com.mygdx.game.client;

import com.core.utils.RawResult;
import com.platform.AdsClose;

public class FBInstant {
  public static native void ShowFullscreen(AdsClose start, AdsClose end) /*-{
            var s = function(D) {
              start.@com.platform.AdsClose::OnEvent(*)(D);
            };
            var e = function(E) {
              end.@com.platform.AdsClose::OnEvent(*)(E);
            }
            $wnd.showFullscreen(s, e);
        }-*/;

  public static native void blankFullscreen(AdsClose start, AdsClose end) /*-{
            var s = function(D) {
              start.@com.platform.AdsClose::OnEvent(*)(D);
            };
            var e = function(E) {
              end.@com.platform.AdsClose::OnEvent(*)(E);
            }
            s(true);
            e(true);
        }-*/;


  public static native boolean IsFullscreenReady()/*-{
            return $wnd.isFullscreenReady();
        }-*/;

  public static native void ShowVideoReward(AdsClose callback) /*-{
            $wnd.showVideoReward(function(D){
                callback.@com.platform.AdsClose::OnEvent(*)(D);
            });
        }-*/;

  public static native void LoadingFinished() /*-{
            $wnd.FBInstant.startGameAsync().then(function() {
                $wnd.startGame();
              });
    }-*/;

  public static native boolean IsVideoRewardReady()/*-{
            return $wnd.isVideoRewardReady();
        }-*/;
  public static native void LoadingProgress(int percent) /*-{
            $wnd.FBInstant.setLoadingProgress(percent);
    }-*/;
  public static native void setPlayerData(String key, String data) /*-{
            $wnd.setPlayerData(key, data);
    }-*/;

  public static native void getPlayerData(String key, RawResult callback) /*-{
          $wnd.getPlayerData(key, function(D) {
             callback.@com.core.utils.RawResult::onResult(*)(D);
//             $wnd.console.log(D);
          });
    }-*/;

  public static native String getLocale() /*-{
          return $wnd.FBInstant.getLocale();
    }-*/;
  public static native String getPlatform() /*-{
          return $wnd.FBInstant.getPlatform();
    }-*/;

  public static native boolean isDev() /*-{
    return ($wnd.getPlayerData === undefined)
  }-*/;
  public static native void showBanner() /*-{
          $wnd.showBanner();
      }-*/;

  public static native void hideBanner() /*-{
          $wnd.hideBanner();
      }-*/;
}
