package com.mygdx.game.client;

import com.gameservice.PlayHandler;
import com.gameservice.PlayService;

public class FBPlayService implements PlayService {
  @Override
  public void loginWithDeviceID() {

  }

  @Override
  public boolean loggedIn() {
    return false;
  }

  @Override
  public String getId() {
    return "";
  }

  @Override
  public String getDisplayName() {
    return "";
  }

  @Override
  public void updateDisplayName(String displayName) {

  }

  @Override
  public void setHandler(PlayHandler handler) {

  }

  @Override
  public void updateScore(String boardName, long score) {

  }

  @Override
  public void getLeaderBoard(String boardName, int start, int count) {

  }
}
