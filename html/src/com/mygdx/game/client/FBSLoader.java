package com.mygdx.game.client;

import com.core.utils.PrefSLoader;
import com.core.model.Session;
import com.game.GMain;
import com.google.gwt.core.client.GWT;

public class FBSLoader extends PrefSLoader {

  public FBSLoader(String key){
    super(key);
  }

  @Override
  public void load(LoadResult res){
    FBInstant.getPlayerData(key, rawData -> {
      try {
        if (rawData == null) {
          session = Session.ofDefault();
        }
        else {
          session = serializer.fromJson(Session.class, rawData);
        }
      }
      catch (Exception e) {
        session = Session.ofDefault();
      }
      session.reBalance();
      res.onResult(session);
    });
  }

  @Override
  public void sync(){
    String val = serializer.toJson(session, Session.class);
    FBInstant.setPlayerData(key, val);
  }
}
