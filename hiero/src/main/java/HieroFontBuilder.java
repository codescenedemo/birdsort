import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.PixmapPacker;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.tools.bmfont.BitmapFontWriter;
import com.badlogic.gdx.tools.hiero.BMFontUtil;
import com.badlogic.gdx.tools.hiero.HieroSettings;
import com.badlogic.gdx.tools.hiero.unicodefont.UnicodeFont;
import com.badlogic.gdx.tools.hiero.unicodefont.effects.ColorEffect;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontFormatException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Set;

public class HieroFontBuilder {

  public static void main(String[] args) {
//    Lwjgl3ApplicationConfiguration.disableAudio = true;

    Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
//    config.allowSoftwareMode = true;
//    config.samples = 2;
//    config.forceExit = false;

//    new Lwjgl3Application(new BMFont(), config);

    new Lwjgl3Application(new ApplicationAdapter() {

      public void create() {
//        String filename = "font_50";//size 50
        String filename = "birdsort_eff60";//size 40
//        String filename = "font_unstroke";//size 40
//        String filename = "font_wheel";// size 25
        BitmapFontWriter.FontInfo info = new BitmapFontWriter.FontInfo();
        info.padding = new BitmapFontWriter.Padding(1, 1, 1, 1);
        BitmapFontWriter.Spacing space = new BitmapFontWriter.Spacing();
        space.horizontal = -2;
        space.vertical = -28;
        info.spacing = space;

        FreeTypeFontGenerator.FreeTypeFontParameter param = new FreeTypeFontGenerator.FreeTypeFontParameter();
        param.size = 60;
        param.gamma = 2f;
        param.renderCount = 3;
        param.padBottom = 4;
        param.padLeft = 4;
        param.padRight = 4;
        param.padTop = 4;
//        param.shadowOffsetY = -4;
//        param.shadowColor = new com.badlogic.gdx.graphics.Color(0, 0, 0, .2f);
//        param.borderWidth = 2;
//        param.borderColor = new com.badlogic.gdx.graphics.Color(4/255f, 143/255f, 266/255f, .6f);
//        param.borderColor = new com.badlogic.gdx.graphics.Color(0,0,0, .2f);
        param.characters = DEFAULT_CHARS;
        param.packer = new PixmapPacker(1024, 1024, Pixmap.Format.RGBA8888, 2, false, new PixmapPacker.SkylineStrategy());

        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.absolute("./hiero/font/ICIELCADENA.OTF"));
        FreeTypeFontGenerator.FreeTypeBitmapFontData data = generator.generateData(param);

        int size = param.packer.getPages().size;
        String[] fileNames;
        if (size == 1)
          fileNames = new String[]{filename + ".png"};
        else {
          fileNames= new String[size];
          for (int i = 0; i < size; i++) {
            fileNames[i] = filename + "_" + i + ".png";
          }
        }

        BitmapFontWriter.writeFont(data, fileNames,
            Gdx.files.absolute("./android/assets/bitmapFont/" + filename + ".fnt"), info, 1024, 1024);
        BitmapFontWriter.writePixmaps(param.packer.getPages(), Gdx.files.absolute("./android/assets/bitmapFont/"), filename);

        System.exit(0);
      }
    }, config);
  }

  static String DEFAULT_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz1234567890\"!`?'.,;:()[]{}<>|/@\\^$-%+=#_&~*\n" +
      "ÁÀẢÃẠÂẤẦẨẪẬĂẮẰẲẴẶÉÈẺẼẸÊẾỀỂỄỆÓÒỎÕỌÔỐỒỔỖỘƠỚỜỞỠỢÚÙỦŨỤƯỨỪỬỮỰÍÌỈĨỊÝỲỶỸỴĐáàảãạâấầẩẫậăắằẳẵặéèẻẽẹêếềểễệóòỏõọôốồổỗộơớờởỡợúùủũụưứừửữựíìỉĩịýỳỷỹỵđçÇñÑ";

  static String NEHE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ\n" //
      + "abcdefghijklmnopqrstuvwxyz\n1234567890 \n" //
      + "\"!`?'.,;:()[]{}<>|/@\\^$-%+=#_&~*\u0000\u007F";

  static String RU_CHARS = "ABCČĆDĐEFGHIJKLMNOPQRSŠTUVWXYZŽabcčćdđefghijklmnopqrsštuvwxyzžАБВГҐДЂЕЁЄЖЗЅИІЇЙЈКЛЉМНЊОПРСТЋУЎФХЦЧЏШЩЪЫЬЭЮЯабвгґдђеёєжзѕиіїйјклљмнњопрстћуўфхцчџшщъыьэюяΑΒΓΔΕΖΗΘΙΚΛΜΝΞΟΠΡΣΤΥΦΧΨΩαβγδεζηθικλμνξοπρστυφχψωάΆέΈέΉίϊΐΊόΌύΰϋΎΫΏĂÂÊÔƠƯăâêôơư1234567890‘?’“!”(%)[#]{@}/&\\<-+÷×=>®©$€£¥¢:;,.*";
}

