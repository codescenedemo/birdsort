import com.badlogic.gdx.tools.hiero.HieroSettings;
import com.badlogic.gdx.tools.hiero.unicodefont.UnicodeFont;
import com.badlogic.gdx.tools.hiero.unicodefont.effects.ColorEffect;

import java.awt.Color;

public class FontSetting {
  String inputFont;
  String fileName;
  HieroSettings settings;

  public void init(String text, String inputFont, String fileName, int fontSize, boolean bold) {
    System.out.println("font init");
    this.inputFont = inputFont;
    this.fileName = fileName;
    this.settings = new HieroSettings();
    settings.setBold(bold);
    settings.setFontSize(fontSize);
    settings.setGlyphText(text);
    settings.setPaddingLeft(1);
    settings.setPaddingRight(1);
    settings.setPaddingBottom(4);
    settings.setPaddingTop(4);
    settings.setPaddingAdvanceX(-2);
    settings.setPaddingAdvanceY(-18);
    settings.setRenderType(UnicodeFont.RenderType.Java.ordinal());
//      ShadowEffect shadowEffect = new ShadowEffect(new Color(0, 184, 148), 0, 2, 1);
//      settings.getEffects().add(shadowEffect);
//    settings.getEffects().add(new ColorEffect());
    System.out.println("init done");
//      settings.getEffects().add(new OutlineEffect(1, Color.GRAY));
  }
}
