package com.game;

import android.app.Activity;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;

import com.badlogic.gdx.utils.Timer;
//import com.google.android.ads.nativetemplates.NativeTemplateStyle;
//import com.google.android.ads.nativetemplates.TemplateView;
import com.google.android.gms.ads.AdError;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdLoader;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.FullScreenContentCallback;
import com.google.android.gms.ads.LoadAdError;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.RequestConfiguration;
import com.google.android.gms.ads.interstitial.InterstitialAd;
import com.google.android.gms.ads.interstitial.InterstitialAdLoadCallback;
import com.google.android.gms.ads.nativead.NativeAdOptions;
import com.google.android.gms.ads.nativead.NativeAdView;
import com.google.android.gms.ads.rewarded.RewardedAd;
import com.google.android.gms.ads.rewarded.RewardedAdLoadCallback;
import com.platform.IPlat;
import com.zs.unrollball.R;

import java.util.Arrays;

//import zs.tilematch.R;

//import com.google.android.ads.mediationtestsuite.MediationTestSuite;


public class AdmobMediation implements IMediation {
  private static final String TAG = "ZenAds AdmobMediation";
  Activity android;
  IPlat platform;
  FrameLayout rootView;


  //test id
  private static final String ADMOB_BANNER_ID = "ca-app-pub-3940256099942544/6300978111";
  private static final String ADMOB_FULLSCREEN_ID = "ca-app-pub-3940256099942544/1033173712";
  private static final String ADMOB_VIDEO_ID = "ca-app-pub-3940256099942544/5224354917";
//  private static final String ADMOB_AD_UNIT_ID = "";

//  private static final String ADMOB_BANNER_ID     = "ca-app-pub-8137503353572003/5552981836";
//  private static final String ADMOB_FULLSCREEN_ID = "ca-app-pub-8137503353572003/3295603712";
//  private static final String ADMOB_VIDEO_ID      = "ca-app-pub-8137503353572003/4417113691";

  private static String[] ADMOB_BANNER_IDS = {};
  private static final String[] DEFAULT_ADMOB_BANNER_IDS = {
      ADMOB_BANNER_ID

  };
  private static String[] ADMOB_FULLSCREEN_IDS = {};
  private static final String[] DEFAULT_ADMOB_FULLSCREEN_IDS = {
      ADMOB_FULLSCREEN_ID
  };
  private static String[] ADMOB_VIDEO_IDS = {};
  private static final String[] DEFAULT_ADMOB_VIDEO_IDS = {
      ADMOB_VIDEO_ID
  };

  boolean bannerVisible = false;
  boolean nativeAdVisible = false;
  AdSize bannerAdSize;
  //    AdView bannerView;
//
//
//    InterstitialAd interstitialView;
//    boolean isInitInterstitialLow = false;
//    RewardedAd videoRewardedView;
//    boolean isVideoLoad = false;
//    int videoFailedToLoadedCount = 0;
  boolean hasBanner = false;
  ISAdListenerEvent videoRewardCallback = null;


  @Override
  public void InitMediation(Activity android, IPlat platform, FrameLayout rootView, boolean hasBanner) {
    this.android = android;
    this.platform = platform;
    this.rootView = rootView;
    this.hasBanner = hasBanner;

    ADMOB_BANNER_IDS = DEFAULT_ADMOB_BANNER_IDS;
    ADMOB_FULLSCREEN_IDS = DEFAULT_ADMOB_FULLSCREEN_IDS;
    ADMOB_VIDEO_IDS = DEFAULT_ADMOB_VIDEO_IDS;

    String banners = platform.GetConfigStringValue("ADMOB_BANNER_IDS", "default");
    if (!banners.equals("default")) {
      Log.d(TAG, "New Banners Config" + banners);
      ADMOB_BANNER_IDS = banners.split(",");
    }

    String inters = platform.GetConfigStringValue("ADMOB_FULLSCREEN_IDS", "default");
    if (!inters.equals("default")) {
      Log.d(TAG, "New Inter Config" + inters);
      ADMOB_FULLSCREEN_IDS = inters.split(",");
    }

    String videos = platform.GetConfigStringValue("ADMOB_VIDEO_IDS", "default");
    if (!videos.equals("default")) {
      Log.d(TAG, "New Video Config" + videos);
      ADMOB_VIDEO_IDS = videos.split(",");
    }

    RequestConfiguration configuration =
        new RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("256CF234F13996A141F344E52F23625D", "1CD687A3D9CA00814F9BC9C58EDC2B3E")).build();
    MobileAds.setRequestConfiguration(configuration);
    MobileAds.initialize(android, initializationStatus -> {
      if (hasBanner) {
//        System.out.println("HAS BANNER");
        bannerVisible = true;
        InitBanner();
      }
      InitInterstitial();
      InitVideoReward();
//      InitNativeAd();
    });

  }


  @Override
  public void ShowAdsTestSuit() {
//     RequestConfiguration.Builder()
//          new RequestConfiguration.Builder().setTestDeviceIds(Arrays.asList("3006DB48FE2B95B46B680876EB6D25CB", "1CD687A3D9CA00814F9BC9C58EDC2B3E"));
//     MediationTestSuite.setAdRequest();
//        MediationTestSuite.launch(android);
  }

  @Override
  public void onResume() {

  }

  @Override
  public void onPause() {

  }

  public int GetBannerHeightInPixel() {
    if (bannerAdSize != null)
      return bannerAdSize.getHeightInPixels(android);
    return 0;
  }

  public void ShowBanner(boolean visible) {
    bannerVisible = visible;
    if (admobBannerAds == null) return;
    AdmobBannerAd curAdmobBannerAd = admobBannerAds[curAdmobBannerAdId];
    if (curAdmobBannerAd != null) {
      curAdmobBannerAd.showBanner();
    }
  }

  AdSize getAdSize() {
    // Step 2 - Determine the screen width (less decorations) to use for the ad width.
    Display display = android.getWindowManager().getDefaultDisplay();
    DisplayMetrics outMetrics = new DisplayMetrics();
    display.getMetrics(outMetrics);
    float widthPixels = outMetrics.widthPixels;
    float density = outMetrics.density;
    int adWidth = (int) (widthPixels / density);
    // Step 3 - Get adaptive ad size and return for setting on the ad view.
    return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(android, adWidth);
  }

  int curAdmobBannerAdId = 0;
  AdmobBannerAd[] admobBannerAds;

  void InitBanner() {
    Log.d(TAG, "InitBanner");

    admobBannerAds = new AdmobBannerAd[ADMOB_BANNER_IDS.length];
    for (int i = 0; i < ADMOB_BANNER_IDS.length; i++) {
      admobBannerAds[i] = new AdmobBannerAd(ADMOB_BANNER_IDS[i]);
    }
    curAdmobBannerAdId = 0;
    InitNextBannerAd();
  }

  void InitNextBannerAd() {
    AdmobBannerAd curAdmobBannerAd = admobBannerAds[curAdmobBannerAdId];
    if (curAdmobBannerAd != null) {
      curAdmobBannerAd.loadAd(() -> {
        if (curAdmobBannerAdId >= admobBannerAds.length - 1) {
          Log.d(TAG, "No banner ad to load. Try to reload previous banner ad id");
        } else {
          curAdmobBannerAd.removeFromRootLayout();
          curAdmobBannerAdId++;
          InitNextBannerAd();
        }
      });
    }
  }


  int curAdmobInterstitialAdId = 0;
  AdmobInterstitialAd[] admobInterstitalAds;

  void InitInterstitial() {
    Log.d(TAG, "InitInterstitial");
    admobInterstitalAds = new AdmobInterstitialAd[ADMOB_FULLSCREEN_IDS.length];
    for (int i = 0; i < ADMOB_FULLSCREEN_IDS.length; i++) {
      admobInterstitalAds[i] = new AdmobInterstitialAd(ADMOB_FULLSCREEN_IDS[i], i == ADMOB_FULLSCREEN_IDS.length - 1);
    }
    curAdmobInterstitialAdId = 0;
    InitNextInterstitialAd();
  }

  void InitNextInterstitialAd() {
    AdmobInterstitialAd curAdmobInterstitialAd = admobInterstitalAds[curAdmobInterstitialAdId];
    if (curAdmobInterstitialAd != null) {
      curAdmobInterstitialAd.loadAd(() -> {
        if (curAdmobInterstitialAdId >= admobInterstitalAds.length - 1) {
          Log.d(TAG, "No inter ad to load. Try to reload previous inter ad id");
        } else {
          curAdmobInterstitialAdId++;
          InitNextInterstitialAd();
        }
      });
    }
  }

  @Override
  public boolean IsInterstitialReady() {
    if (admobInterstitalAds == null) {
      return false;
    }
    for (AdmobInterstitialAd admobInterstitalAd : admobInterstitalAds) {
      if (admobInterstitalAd != null && admobInterstitalAd.isInterstitialLoad()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void ShowInterstitial(ISAdListenerEvent callback) {
    if (admobInterstitalAds == null) {
      callback.OnEvent(false, false);
      return;
    }
    ;
    for (AdmobInterstitialAd admobInterstitalAd : admobInterstitalAds) {
      if (admobInterstitalAd != null && admobInterstitalAd.isInterstitialLoad()) {
        admobInterstitalAd.showInterstitial();
        callback.OnEvent(true, false);
        return;
      }
    }

    callback.OnEvent(false, false);
  }


  int curAdmobRewardedAdId = 0;
  AdmobRewardedAd[] admobRewardedAds;

  void InitVideoReward() {
    Log.d(TAG, "InitVideoReward");
    admobRewardedAds = new AdmobRewardedAd[ADMOB_VIDEO_IDS.length];
    for (int i = 0; i < ADMOB_VIDEO_IDS.length; i++) {
      admobRewardedAds[i] = new AdmobRewardedAd(ADMOB_VIDEO_IDS[i], i == ADMOB_VIDEO_IDS.length - 1);
    }
    curAdmobRewardedAdId = 0;
    InitNextRewardedAd();
  }


  void InitNextRewardedAd() {
    AdmobRewardedAd curAdmobRewardedAd = admobRewardedAds[curAdmobRewardedAdId];
    if (curAdmobRewardedAd != null) {
      curAdmobRewardedAd.loadAd(() -> {
        if (curAdmobRewardedAdId >= admobRewardedAds.length - 1) {
          Log.d(TAG, "No reward ad to load. Try to reload previous reward ad id");
        } else {
          curAdmobRewardedAdId++;
          InitNextRewardedAd();
        }
      });
    }
  }

  AdmobNativeAdvancedAd admobNativeAd;

//  void InitNativeAd() {
//    Log.d(TAG, "InitNative");
//    admobNativeAd = new AdmobNativeAdvancedAd(ADMOB_AD_UNIT_ID);
////    InitNextNativeAd();
//  }

//  void InitNextNativeAd() {
//    AdmobNativeAdvancedAd curNativeAd = admobNativeAd;
//    if (curNativeAd != null) {
//      curNativeAd.loadAd(() -> {
//        curNativeAd.removeFromRootLayout();
//        InitNextNativeAd();
//      });
//    }
//  }

  @Override
  public void ShowNative(boolean visible) {
    nativeAdVisible = visible;
    if (admobNativeAd == null) return;
    admobNativeAd.showNative();
  }

  public boolean IsNativeAdvancedReady() {
    return admobNativeAd.isNativeLoaded();
  }

  @Override
  public boolean IsVideoRewardReady() {
    if (admobRewardedAds == null) {
      return false;
    }

    for (AdmobRewardedAd admobRewardedAd : admobRewardedAds) {
      if (admobRewardedAd != null && admobRewardedAd.isRewardedLoad()) {
        return true;
      }
    }
    return false;
  }

  @Override
  public void ShowVideoReward(ISAdListenerEvent callback) {
    if (admobRewardedAds == null) {
      callback.OnEvent(false, false);
      return;
    }
    ;

    for (AdmobRewardedAd admobRewardedAd : admobRewardedAds) {
      if (admobRewardedAd != null && admobRewardedAd.isRewardedLoad()) {
        admobRewardedAd.showRewarded(callback);
        return;
      }
    }
    callback.OnEvent(false, false);
  }


  public interface AdmobAdListenerEvent {
    void onAdFailedToLoad();
  }

  public class AdmobInterstitialAd {
    String interstitialId;
    InterstitialAd interstitialView;
    AdmobAdListenerEvent callback;
    boolean reload;
    int reloadFailedCount = 10;

    public AdmobInterstitialAd(String interstitialId, boolean reload) {
      this.interstitialId = interstitialId;
      this.reload = reload;
      this.interstitialView = null;

    }

    public boolean isInterstitialLoad() {
      return (interstitialView != null);
    }

    public void showInterstitial() {
      if (interstitialView != null) {
        android.runOnUiThread(() -> {
          interstitialView.show(android);
          Log.d(TAG, "(Inter)" + interstitialId + " show");
          // if(callback) callback.OnEvent(true, false);
        });
      }
    }

    public void loadAd(AdmobAdListenerEvent callback) {
      this.callback = callback;
      Log.d(TAG, "(Inter)" + interstitialId + " request");
      AdRequest adRequest = new AdRequest.Builder().build();
      InterstitialAd.load(android, interstitialId, adRequest, new InterstitialAdLoadCallback() {
        @Override
        public void onAdLoaded(@NonNull InterstitialAd interstitialAd) {
          Log.d(TAG, "(Inter)" + interstitialId + " loaded");
          interstitialView = interstitialAd;
          interstitialView.setFullScreenContentCallback(new FullScreenContentCallback() {
            @Override
            public void onAdDismissedFullScreenContent() {
              Log.d(TAG, "(Inter)" + interstitialId + " onAdDismissedFullScreenContent");
              loadAd(callback);
            }

            @Override
            public void onAdFailedToShowFullScreenContent(AdError adError) {
              Log.d(TAG, "(Inter)" + interstitialId + " onAdFailedToShowFullScreenContent");
            }

            @Override
            public void onAdShowedFullScreenContent() {
              Log.d(TAG, "(Inter)" + interstitialId + " onAdShowedFullScreenContent");
              interstitialView = null;
            }
          });
        }

        @Override
        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
          Log.d(TAG, "(Inter)" + interstitialId + " Error  " + loadAdError.getMessage());
          interstitialView = null;
          if (callback != null) callback.onAdFailedToLoad();

          if (reload) {
            reloadFailedCount--;
            if (reloadFailedCount > 0)
              Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                  android.runOnUiThread(() -> loadAd(callback));
                }
              }, 10);
          }
        }
      });
    }
  }


  public class AdmobRewardedAd {
    String rewardedId;
    RewardedAd rewardedView;
    AdmobAdListenerEvent callback;
    boolean reload;
    int reloadFailedCount = 10;

    public AdmobRewardedAd(String rewardedId, boolean reload) {
      this.rewardedId = rewardedId;
      this.reload = reload;
      this.rewardedView = null;

    }

    public boolean isRewardedLoad() {
      return (rewardedView != null);
    }

    public void showRewarded(ISAdListenerEvent cb) {
      android.runOnUiThread(() -> {
        if (rewardedView != null) {
          Log.d(TAG, "(Rewarded)" + rewardedId + " show");
          videoRewardCallback = cb;
          rewardedView.show(android, rewardItem -> {
            Log.d(TAG, "(Rewarded)" + rewardedId + " onReward");
            if (videoRewardCallback != null) {
              videoRewardCallback.OnEvent(true, false);
              videoRewardCallback = null;
            }
          });
        } else {
          Log.d(TAG, "(Rewarded)" + rewardedId + " not ready");
          videoRewardCallback.OnEvent(false, false);
          videoRewardCallback = null;
        }
      });
    }

    public void loadAd(AdmobAdListenerEvent callback) {
      this.callback = callback;
      Log.d(TAG, "(Rewarded)" + rewardedId + " request");
      AdRequest adRequest = new AdRequest.Builder().build();

      RewardedAd.load(android, rewardedId, adRequest, new RewardedAdLoadCallback() {
        @Override
        public void onAdFailedToLoad(@NonNull LoadAdError loadAdError) {
          Log.d(TAG, "(Rewarded)" + rewardedId + " Error " + loadAdError.getMessage());
          rewardedView = null;

          if (callback != null) callback.onAdFailedToLoad();

          if (reload) {
            reloadFailedCount--;
            if (reloadFailedCount > 0)
              Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                  android.runOnUiThread(() -> loadAd(callback));
                }
              }, 10);
          }

        }

        @Override
        public void onAdLoaded(@NonNull RewardedAd rewardedAd) {
          Log.d(TAG, "(Rewarded)" + rewardedId + " loaded");

          rewardedView = rewardedAd;
          rewardedView.setFullScreenContentCallback(new FullScreenContentCallback() {
            @Override
            public void onAdShowedFullScreenContent() {
              Log.d(TAG, "(Rewarded)" + rewardedId + " onAdShowedFullScreenContent");
              rewardedView = null;
            }

            @Override
            public void onAdFailedToShowFullScreenContent(AdError adError) {
              Log.d(TAG, "(Rewarded)" + rewardedId + " onAdFailedToShowFullScreenContent");
            }

            @Override
            public void onAdDismissedFullScreenContent() {
              Log.d(TAG, "(Rewarded)" + rewardedId + " onAdDismissedFullScreenContent");
              if (videoRewardCallback != null) {
                videoRewardCallback.OnEvent(false, false);
                videoRewardCallback = null;
              }
              loadAd(callback);
            }
          });
        }
      });

    }
  }

  public class AdmobBannerAd {
    String bannerId;
    AdView bannerView;

    RelativeLayout rootRelativeLayout = null;

    public AdmobBannerAd(String bannerId) {
      this.bannerId = bannerId;

      this.bannerView = null;

    }

    public void showBanner() {
      if (bannerView != null) {
        android.runOnUiThread(() -> {
          if (bannerVisible)
            bannerView.setVisibility(View.VISIBLE);
          else
            bannerView.setVisibility(View.GONE);
        });
      }
    }

    public void removeFromRootLayout() {
      rootView.removeView(rootRelativeLayout);
    }

    public void loadAd(AdmobAdListenerEvent callback) {
      Log.d(TAG, "(Banner)" + bannerId + " request");
      bannerView = new AdView(android);
      bannerView.setAdUnitId(bannerId);
      if (bannerAdSize == null) bannerAdSize = getAdSize();

      bannerView.setAdSize(bannerAdSize);
      bannerView.setBackgroundColor(Color.BLACK);
      rootRelativeLayout = new RelativeLayout(android);
      rootView.addView(rootRelativeLayout);

      RelativeLayout.LayoutParams adViewParams = new RelativeLayout.LayoutParams(AdView.LayoutParams.WRAP_CONTENT, AdView.LayoutParams.WRAP_CONTENT);
      adViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
      adViewParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
      adViewParams.height = bannerAdSize.getHeightInPixels(android);
      rootRelativeLayout.addView(bannerView, adViewParams);

//            bannerView.loadAd(new AdRequest.Builder().build());
      bannerView.setAdListener(new AdListener() {
        @Override
        public void onAdLoaded() {
          Log.d(TAG, "(Banner)" + bannerId + " loaded");
          super.onAdLoaded();
          bannerView.setVisibility(View.GONE);
          showBanner();
        }

        @Override
        public void onAdFailedToLoad(LoadAdError adError) {
          Log.d(TAG, "(Banner)" + bannerId + " Error " + adError.getMessage());
          super.onAdFailedToLoad(adError);
          if (callback != null) callback.onAdFailedToLoad();
          //bannerView.setVisibility(View.INVISIBLE);
        }

        @Override
        public void onAdOpened() {
          // Code to be executed when an ad opens an overlay that
          // covers the screen.
        }

        @Override
        public void onAdClicked() {
          // Code to be executed when the user clicks on an ad.
        }

        @Override
        public void onAdClosed() {
          // Code to be executed when the user is about to return
          // to the app after tapping on an ad.
        }
      });
      AdRequest adRequest = new AdRequest.Builder().build();

      bannerView.loadAd(adRequest);

    }
  }

  public class AdmobNativeAdvancedAd {
    String nativeAdId;

    NativeAdView adView = null;

    public AdmobNativeAdvancedAd(String nativeAdId) {
      this.nativeAdId = nativeAdId;

    }

    public boolean isNativeLoaded(){
      return adView != null;
    }

    public void showNative() {
      if (adView != null) {
        android.runOnUiThread(() -> {
          if (nativeAdVisible)
            adView.setVisibility(View.VISIBLE);
          else
            adView.setVisibility(View.GONE);
        });
      }
    }

    public void removeFromRootLayout() {
      rootView.removeView(adView);
    }

//    public void loadAd(AdmobAdListenerEvent callback) {
//
//
//      AdLoader adLoader = new AdLoader.Builder(android, ADMOB_AD_UNIT_ID)
//          .forNativeAd(nativeAd -> {
//            // Show the ad.
//            Log.d(TAG, "(NativeAd)" + nativeAdId + " loaded");
//
//            adView = (NativeAdView) android.getLayoutInflater()
//                .inflate(R.layout.ad_unified, null);
////            adView.setVisibility(View.GONE);
//            // This method sets the text, images and the native ad, etc into the ad
//            // view.
//
//            RelativeLayout.LayoutParams adParams =
//                new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//                    RelativeLayout.LayoutParams.WRAP_CONTENT);
//            adParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
//            adParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);
//
//            ((AndroidLauncher) android).rootView.addView(adView);
////            frameLayout.addView(adView);
//
////            NativeTemplateStyle styles = new
////                NativeTemplateStyle.Builder().withMainBackgroundColor(new ColorDrawable()).build();
////            TemplateView template = android.findViewById(R.id.my_template);
////            template.setStyles(styles);
////            template.setNativeAd(nativeAd);
//          })
//          .withAdListener(new AdListener() {
//            @Override
//            public void onAdFailedToLoad(LoadAdError adError) {
//              Log.d(TAG, "(NativeAd)" + nativeAdId + " Error " + adError.getMessage());
//              if (callback != null) callback.onAdFailedToLoad();
//              // Handle the failure by logging, altering the UI, and so on.
//            }
//          })
//          .withNativeAdOptions(new NativeAdOptions.Builder()
//              // Methods in the NativeAdOptions.Builder class can be
//              // used here to specify individual options settings.
//              .build())
//          .build();
//
//      adLoader.loadAd(new AdRequest.Builder().build());
//    }
  }
}