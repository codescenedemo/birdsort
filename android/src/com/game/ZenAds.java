package com.game;

import android.app.Activity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.widget.FrameLayout;

import com.google.android.gms.ads.AdSize;
import com.platform.AdsClose;
import com.platform.IPlat;

public class ZenAds {
  private static final String TAG = "ZenAds";

  Activity android;
  IPlat platform;
  FrameLayout rootView;

  long lastInterstitialTime = 0;
  IMediation mediation = null;

  public void InitAd(Activity android, IPlat platform, FrameLayout rootView) {
    Log.i(TAG, "InitAd");
    this.android = android;
    this.platform = platform;
    this.rootView = rootView;

    //dùng mediation nào thì đổi sang cái đó
    // if(platform.GetConfigStringValue("mediationType", "MAX").equals("MAX")){
//       mediation = new ApplovinMediation();
//        }
//        else
    mediation = new AdmobMediation();

    mediation.InitMediation(android, platform, rootView, true);

  }

  public void ShowInterstitial(AdsClose ondone) {
    if (!mediation.IsInterstitialReady()) {
      ondone.OnEvent(false);
      return;
    }
    long fullscreenTime = platform.GetConfigIntValue("fullscreenTime", 20000);
    if (System.currentTimeMillis() - lastInterstitialTime > fullscreenTime) {
      mediation.ShowInterstitial((success, capped) -> ondone.OnEvent(true));
      lastInterstitialTime = System.currentTimeMillis();
    }
    else ondone.OnEvent(false);

  }

  public void ShowInterstitial() {
    if (!mediation.IsInterstitialReady()) return;
    long fullscreenTime = platform.GetConfigIntValue("fullscreenTime", 80000);
    if (System.currentTimeMillis() - lastInterstitialTime > fullscreenTime) {
      mediation.ShowInterstitial((success, capped) -> {

      });
      lastInterstitialTime = System.currentTimeMillis();
    }

  }

  public void ShowUIInterstitial() {
    if (!mediation.IsInterstitialReady()) return;
    long fullscreenTime = platform.GetConfigIntValue("uiFullScreenTime", 80000);
    if (System.currentTimeMillis() - lastInterstitialTime > fullscreenTime) {
      mediation.ShowInterstitial((success, capped) -> {

      });
      lastInterstitialTime = System.currentTimeMillis();
    }

  }

  public void ShowVideoReward(final AdsClose callback) {
    mediation.ShowVideoReward((success, capped) -> callback.OnEvent(success));
  }

  private AdSize getAdSize() {
    // Step 2 - Determine the screen width (less decorations) to use for the ad width.
    Display display = android.getWindowManager().getDefaultDisplay();
    DisplayMetrics outMetrics = new DisplayMetrics();
    display.getMetrics(outMetrics);
    float widthPixels = outMetrics.widthPixels;
    float density = outMetrics.density;
    int adWidth = (int) (widthPixels / density);
    // Step 3 - Get adaptive ad size and return for setting on the ad view.
    return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(android, adWidth);
  }

  AdSize bannerAdSize = null;

  public int GetBannerHeightInPixel() {
    if (mediation != null) {
      if (bannerAdSize == null)
        bannerAdSize = getAdSize();
      return bannerAdSize.getHeightInPixels(android);
    }
    return 0;
  }

  public boolean IsVideoRewardReady() {
    return mediation.IsVideoRewardReady();
  }

  public boolean IsInterstitialReady() {
    return mediation.IsInterstitialReady();
  }

  public void ShowBanner(boolean visible) {
    mediation.ShowBanner(visible);
  }

  public void ShowNative(boolean visible){
    mediation.ShowNative(visible);
  }

  public boolean IsNativeAdvancedReady(){
    return mediation.IsNativeAdvancedReady();
  }

  public void ShowAdsTestSuit() {
    mediation.ShowAdsTestSuit();
  }

  public void onResume() {
    if (mediation != null)
      mediation.onResume();
  }

  public void onPause() {
    if (mediation != null)
      mediation.onPause();
  }
}

