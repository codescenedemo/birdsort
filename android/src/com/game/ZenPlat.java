package com.game;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;

import com.badlogic.gdx.utils.ObjectMap;
import com.core.utils.PrefSLoader;
import com.core.utils.SessionLoader;
import com.gameservice.PlayService;
import com.gameservice.PlayfabPlayService;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.platform.AdsClose;
import com.platform.IPlat;

import java.util.Calendar;
import java.util.Locale;

public class ZenPlat implements IPlat {
//  android.zenAds android.zenAds;
//  FirebaseRemoteConfig android.mFirebaseRemoteConfig;
//  FirebaseAnalytics android.mFirebaseAnalytics;
  AndroidLauncher android;
  PlayService playService;
  public ZenPlat(ZenAds zenAds, FirebaseRemoteConfig mFirebaseRemoteConfig, FirebaseAnalytics mFirebaseAnalytics, AndroidLauncher androidLauncher) {
//    this.android.zenAds = android.zenAds;
//    this.android.mFirebaseRemoteConfig = android.mFirebaseRemoteConfig;
//    this.android.mFirebaseAnalytics = android.mFirebaseAnalytics;
    this.android = androidLauncher;
    playService = new PlayfabPlayService("E0265", Settings.Secure.getString(android.getApplicationContext().getContentResolver(), Settings.Secure.ANDROID_ID));
  }

  @Override
  public void log(String str) {
    Log.i("Game", str);
  }

  @Override
  public String GetDefaultLanguage() {
    //Log.i(TAG, "lang : " + lang);
    //return "en";
    return Locale.getDefault().getLanguage();
  }

  @Override
  public boolean isVideoRewardReady() {
    if (android.zenAds == null) return false;
    return android.zenAds.IsVideoRewardReady();
  }

  @Override
  public void ShowVideoReward(AdsClose callback) {
    if (android.zenAds != null) android.zenAds.ShowVideoReward(callback);
  }

  @Override
  public void ShowRewardInterstitial(AdsClose callback) {
    callback.OnEvent(false);
  }

  @Override
  public void ShowFullscreen(AdsClose onshow, AdsClose ondone) {
    if (android.zenAds != null)
      android.zenAds.ShowInterstitial(ondone);
  }

  @Override
  public void ShowUIFullscreen() {
    if (android.zenAds != null)
      android.zenAds.ShowUIInterstitial();
  }

  @Override
  public void ShowNative(boolean visible){
    if (android.zenAds != null)
      android.zenAds.ShowNative(visible);
  }

  @Override
  public boolean IsNativeAdvancedReady() {
    if (android.zenAds != null)
      return android.zenAds.IsNativeAdvancedReady();
    return false;
  }

  @Override
  public boolean isFullScreenReady() {
    if (android.zenAds != null)
      return android.zenAds.IsInterstitialReady();
    return false;
  }


  @Override
  public void ShowBanner(boolean visible) {
    if (android.zenAds != null)
      android.zenAds.ShowBanner(visible);
    //android.zenAds.setBannerVisible(visible);
  }

  @Override
  public int GetBannerHeight() {
    if (android.zenAds == null) return 0;
    return android.zenAds.GetBannerHeightInPixel();
  }

  @Override
  public void OpenStore() {
    try {
      android.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(AndroidLauncher.PLAYSTORE_LINK + android.getPackageName())));
    } catch (Exception ignored) {
    }
  }

  @Override
  public void TrackScreen(String screen) {

  }

  @Override
  public void ShowAdsTestSuit() {
    if (android.zenAds != null) android.zenAds.ShowAdsTestSuit();
}

  @Override
  public void onShow() {

  }

  @Override
  public int dayDiff(long t1, long t2) {
    Calendar cal = Calendar.getInstance();
    cal.setTimeInMillis(t1);
    int d1 = cal.get(Calendar.DAY_OF_YEAR);
    int y1 = cal.get(Calendar.YEAR);
    cal.setTimeInMillis(t2);
    int d2 = cal.get(Calendar.DAY_OF_YEAR);
    int y2 = cal.get(Calendar.YEAR);

    if (y2 > y1)
      return 1;

    return Math.abs(d2 - d1);
  }

  @Override
  public String getMd5PathName(String filename) {
    return null;
  }

  @Override
  public void preloadAssets(String assetTxt, PreloaderCallback callback) {

  }

  @Override
  public SessionLoader getSessionLoader() {
    return new PrefSLoader("com.billyadventure");
  }

  @Override
  public String getPlatform() {
    return "ANDROID";
  }

  @Override
  public PlayService getPlayService() {
    return playService;
  }

  @Override
  public int GetConfigIntValue(String name, int defaultValue) {
    try {
      String r = GetConfigStringValue(name, "" + defaultValue);
      return Integer.parseInt(r);
    } catch (Exception e) {
      return defaultValue;
    }
  }


  @Override
  public String GetConfigStringValue(String name, String defaultValue) {
    try {
      String v = android.mFirebaseRemoteConfig.getString(name);
      //Log.i("remoteConfig", "name=" + name + " v=" + v);
      if (v.equals(""))
        return defaultValue;
      return v;
    } catch (Exception ignored) {
      return defaultValue;
    }
  }

  @Override
  public void TrackNotification(int notifyId, int dailyNotificationCount) {
    try {
      Bundle bundle = new Bundle();
      bundle.putInt("notifyId", notifyId);
      bundle.putInt("dailyNotificationCount", dailyNotificationCount);
      android.mFirebaseAnalytics.logEvent("Notifiation", bundle);


    } catch (Exception ignored) {
    }
  }


  @Override
  public void TrackCustomEvent(String event) {
    try {
      Bundle bundle = new Bundle();
      android.mFirebaseAnalytics.logEvent(event, bundle);
    } catch (Exception ignored) {
    }
  }

  @Override
  public void TrackGameStart(int serverVersion) {
    try {
      Bundle bundle = new Bundle();
      bundle.putInt("serverVersion", serverVersion);
      android.mFirebaseAnalytics.logEvent("GameStart", bundle);
    } catch (Exception ignored) {
    }
  }

  @Override
  public void TrackEvaluate(String eventname, int hitCount, String evaluate) {
    try {
      Bundle bundle = new Bundle();
      bundle.putInt("hitCount", hitCount);
      bundle.putString("evaluate", evaluate);
      android.mFirebaseAnalytics.logEvent(eventname, bundle);


    } catch (Exception ignored) {
    }
  }

  @Override
  public void TrackLevelInfo(String event, int level, float time) {
    try {
      Bundle bundle2 = new Bundle();
      bundle2.putInt("level", level);
      bundle2.putInt("time", (int) time);
      android.mFirebaseAnalytics.logEvent(event, bundle2);

    } catch (Exception ignored) {

    }
  }

  @Override
  public void TrackLevelInfo(String event, ObjectMap<String, Object> data) {
    try {
      Bundle bundle2 = new Bundle();
      for (ObjectMap.Entry<String, Object> d : data) {
        Object v = d.value;
        if (v instanceof Integer)
          bundle2.putInt(d.key, (Integer) v);
        else if (v instanceof Boolean)
          bundle2.putBoolean(d.key, (Boolean) v);
        else
          bundle2.putString(d.key, (String) v);
      }
      android.mFirebaseAnalytics.logEvent(event, bundle2);

    } catch (Exception ignored) {

    }
  }

  @Override
  public void TrackPlayerInfo(String event, int mode, int level) {
    try {
      Bundle bundle = new Bundle();
      bundle.putInt("mode", mode);
      //bundle.putInt("difficult", difficult);
      bundle.putInt("level", level);
      android.mFirebaseAnalytics.logEvent(event, bundle);
    } catch (Exception ignored) {
    }
  }

  @Override
  public void TrackPlaneInfo(String event, int planeid, int level) {
    try {
      Bundle bundle = new Bundle();
      bundle.putInt("planeid", planeid);
      bundle.putInt("level", level);
      android.mFirebaseAnalytics.logEvent(event, bundle);
    } catch (Exception ignored) {
    }
  }

  @Override
  public void TrackVideoReward(String type) {
    try {
      Bundle bundle = new Bundle();
      //bundle.putString("type", type);
      android.mFirebaseAnalytics.logEvent(type, bundle);
    } catch (Exception ignored) {
    }

  }

  @Override
  public void TrackPlayerDead(String event, int mode, int difficult, int level, int parentModel, int shooterModel, boolean isBoss) {

  }

  @Override
  public void TrackRating(int rateid, int rateFromLevel) {
    Bundle bundle = new Bundle();
    bundle.putInt("rateid", rateid);
    bundle.putInt("rateFromLevel", rateFromLevel);
    android.mFirebaseAnalytics.logEvent("Rating", bundle);

  }

  @Override
  public void Restart() {
    System.exit(0);
  }

  @Override
  public int GetNotifyId() {
    return 0;
  }

  @Override
  public void SetDailyNotification(int id, String header, String content, int days, int hours) {
//        DailyNotification.SetDailyNotification(AndroidLauncher.this, id, header, content, days, hours);
  }

  @Override
  public void CancelDailyNotification(int id) {
//        DailyNotification.CancelDailyNotification(AndroidLauncher.this, id);
  }

  @Override
  public void CrashKey(String key, String value) {
    //Crashlytics.setString(key, value);
  }

  @Override
  public void CrashLog(String log) {

    //Crashlytics.log(log);
  }
}
