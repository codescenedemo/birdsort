package com.game;

import android.app.Activity;
import android.widget.FrameLayout;

import com.platform.IPlat;

public interface IMediation {
    void InitMediation(Activity android, IPlat platform, FrameLayout rootView, boolean hasBanner);

    void ShowVideoReward(final ISAdListenerEvent callback);
    void ShowInterstitial(ISAdListenerEvent callback);
    void ShowBanner(boolean visible);

    int GetBannerHeightInPixel();
    boolean IsInterstitialReady();
    boolean IsVideoRewardReady();
    void ShowNative(boolean visible);
    boolean IsNativeAdvancedReady();
    void ShowAdsTestSuit();
    void onResume();
    void onPause();

    interface ISAdListenerEvent {
        void OnEvent(boolean success, boolean capped);
    }

}
