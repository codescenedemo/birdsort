package com.game;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.FrameLayout;

import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.remoteconfig.FirebaseRemoteConfig;
import com.google.firebase.remoteconfig.FirebaseRemoteConfigSettings;
import com.platform.IPlat;
import com.zs.unrollball.R;
//import zs.tilematch.R;

public class AndroidLauncher extends AndroidApplication {
  private static final String TAG = "Zen Launcher";
  public static final String PLAYSTORE_LINK = "https://play.google.com/store/apps/details?id=";
  FrameLayout rootView;

   FirebaseAnalytics mFirebaseAnalytics;
   FirebaseRemoteConfig mFirebaseRemoteConfig;

  int notifyId = -1;
  IPlat platform;

  ZenAds zenAds = null;//new ZenAds();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
    config.numSamples = 3;
    config.useImmersiveMode = true;

    platform = new ZenPlat(zenAds,mFirebaseRemoteConfig, mFirebaseAnalytics,this);

    //initialize(new GMain(plat), config);
    View libgdxview = initializeForView(new GMain(platform), config);
    rootView = new FrameLayout(this);
    rootView.addView(libgdxview);
    setContentView(rootView);


    InitGA();

    InitRemoteConfig(task -> {
      if (task.isSuccessful()) {
        boolean updated = task.getResult();
        Log.i(TAG, "remoteconfig ok");

      } else {
        Log.i(TAG, "remoteconfig false");
      }
      zenAds = new ZenAds();
      zenAds.InitAd(AndroidLauncher.this, platform, rootView);
    });


    //AlarmReceiver.setAlarm(true, this);
    Intent appLinkIntent = getIntent();
    String appLinkAction = appLinkIntent.getAction();
    Uri appLinkData = appLinkIntent.getData();

    initNotificationData();
  }


  public static final int RESTART_ID = 1999;

  void initNotificationData() {
    notifyId = this.getIntent().getIntExtra("id", -1);
    if (notifyId == RESTART_ID)
      notifyId = -1;

  }

  public void sendFirstOpen() {
    Preferences pref = Gdx.app.getPreferences("Android");
    boolean firstOpen = pref.getBoolean("firstOpen2", true);
    if (firstOpen) {
      Bundle bundle = new Bundle();
      mFirebaseAnalytics.logEvent("firstOpen2", bundle);
      pref.putBoolean("firstOpen2", false);
      pref.flush();
      Gdx.app.log("AndroidLauncher", "firstOpen2");
    }
  }


  public void InitRemoteConfig(OnCompleteListener<Boolean> callback) {
    Log.d(TAG, "InitRemoteConfig");
    try {
      mFirebaseRemoteConfig = FirebaseRemoteConfig.getInstance();
      FirebaseRemoteConfigSettings configSettings = new FirebaseRemoteConfigSettings.Builder()
          .setMinimumFetchIntervalInSeconds(3600)
          .build();
      mFirebaseRemoteConfig.setDefaultsAsync(R.xml.remote_config_defaults);
      mFirebaseRemoteConfig.setConfigSettingsAsync(configSettings);
      mFirebaseRemoteConfig.fetchAndActivate()
          .addOnCompleteListener(this, callback);
    } catch (Exception ignored) {

    }

  }

  public void RestartApp() {
//    Intent mStartActivity = new Intent(this, AndroidLauncher.class);
//    DailyNotification.PendingRestartApplication(mStartActivity, this, RESTART_ID);
    System.exit(0);
  }


  int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 0;

  void requestPermision() {
    if (ContextCompat.checkSelfPermission(this,
        "com.google.android.c2dm.permission.SEND")
        != PackageManager.PERMISSION_GRANTED) {

      // Permission is not granted
      // Should we show an explanation?
      if (ActivityCompat.shouldShowRequestPermissionRationale(this,
          "com.google.android.c2dm.permission.SEND")) {
        // Show an explanation to the user *asynchronously* -- don't block
        // this thread waiting for the user's response! After the user
        // sees the explanation, try again to request the permission.
        Log.d("Notification", "ShowExplain");
      } else {
        // No explanation needed; request the permission
        ActivityCompat.requestPermissions(this,
            new String[]{"com.google.android.c2dm.permission.SEND"},
            MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        Log.d("Notification", "NoExplain");
        // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
        // app-defined int constant. The callback method gets the
        // result of the request.
      }
    } else {
      // Permission has already been granted
      Log.d("Notification", "AlradyGranted");
    }
  }

  void InitGA() {
    try {
      mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    if (zenAds != null) zenAds.onResume();

  }

  @Override
  public void onPause() {
    if (zenAds != null) zenAds.onPause();
    super.onPause();
  }

  @Override
  public void onDestroy() {
    super.onDestroy();
  }


}
