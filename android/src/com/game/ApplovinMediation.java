package com.game;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Point;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import com.applovin.mediation.MaxAd;
import com.applovin.mediation.MaxAdListener;
import com.applovin.mediation.MaxAdViewAdListener;
import com.applovin.mediation.MaxError;
import com.applovin.mediation.MaxReward;
import com.applovin.mediation.MaxRewardedAdListener;
import com.applovin.mediation.ads.MaxAdView;
import com.applovin.mediation.ads.MaxInterstitialAd;
import com.applovin.mediation.ads.MaxRewardedAd;
//import com.applovin.mediation.nativeAds.MaxNativeAdListener;
//import com.applovin.mediation.nativeAds.MaxNativeAdLoader;

import com.applovin.mediation.nativeAds.MaxNativeAdView;
//import com.applovin.mediation.nativeAds.MaxNativeAdViewBinder;
import com.applovin.sdk.AppLovinSdk;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Timer;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.platform.IPlat;
import com.zs.unrollball.R;

import java.util.concurrent.TimeUnit;



public class ApplovinMediation implements IMediation {
  private static final String TAG = "ZenAds MAX";
  Activity android;
  IPlat platform;
  FrameLayout rootView;

  private MaxInterstitialAd interstitialAd;
  private int retryFullscreenAttempt;

  private MaxRewardedAd rewardedAd;
  private int retryVideoAttempt;

//  private MaxNativeAdLoader nativeAdLoader;
  private MaxAd nativeAd;
  private MaxAdView adView;
  private MaxNativeAdView maxNativeAdView;

  public int width = 0;
  public int height = 0;

  boolean bannerVisible = false;
  boolean nativeVisible = false;

  ISAdListenerEvent videoRewardCallback = null;
  ISAdListenerEvent fullscreenCallback = null;
//
//  private static final String MAX_BANNER_ID       = "0f05aef73f55e4fa";
//  private static final String MAX_FULLSCREEN_ID   = "84ac5348392eecdc";
//  private static final String MAX_VIDEO_ID        = "e9ceca0bfc8f75d4";
  private static final String MAX_NATIVE_ID       = "";


  private static final String MAX_BANNER_ID = "";
  private static final String MAX_FULLSCREEN_ID = "84ac5348392eecdc";
  private static final String MAX_VIDEO_ID = "e9ceca0bfc8f75d4";


  @Override
  public void InitMediation(Activity android, IPlat platform, FrameLayout rootView, boolean hasBanner) {
    Log.d(TAG, "InitMediation");
    this.android = android;
    this.platform = platform;
    this.rootView = rootView;

    AppLovinSdk.getInstance(android).setMediationProvider("max");
    AppLovinSdk.initializeSdk(android, config -> {
      InitMAXVideoReward();
      InitMAXInterstitial();
//      initMaxNativeAd();
      if (hasBanner) {
        InitMAXBanner();
      }
    });
  }

  AdSize getAdSize() {
    // Step 2 - Determine the screen width (less decorations) to use for the ad width.
    Display display = android.getWindowManager().getDefaultDisplay();
    DisplayMetrics outMetrics = new DisplayMetrics();
    display.getMetrics(outMetrics);
    float widthPixels = outMetrics.widthPixels;
    float density = outMetrics.density;
    int adWidth = (int) (widthPixels / density);
    // Step 3 - Get adaptive ad size and return for setting on the ad view.
    return AdSize.getCurrentOrientationAnchoredAdaptiveBannerAdSize(android, adWidth);
  }

  @SuppressLint({"ResourceAsColor"})
  void InitMAXBanner() {
    AdSize bannerAdSize = getAdSize();
    RelativeLayout bannerContainer = new RelativeLayout(android);
    rootView.addView(bannerContainer);

    RelativeLayout.LayoutParams adViewParams = new RelativeLayout.LayoutParams(AdView.LayoutParams.WRAP_CONTENT, AdView.LayoutParams.WRAP_CONTENT);
    adViewParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
    adViewParams.addRule(RelativeLayout.CENTER_IN_PARENT, RelativeLayout.TRUE);

    adView = new MaxAdView(MAX_BANNER_ID, android);
    adView.setListener(new MaxAdViewAdListener() {
      @Override
      public void onAdExpanded(MaxAd ad) {

      }

      @Override
      public void onAdCollapsed(MaxAd ad) {

      }

      @Override
      public void onAdLoaded(MaxAd ad) {
        //bo code nay luôn
      }

      @Override
      public void onAdDisplayed(MaxAd ad) {
        android.runOnUiThread(() -> {
          if (!ApplovinMediation.this.bannerVisible) {
            adView.setVisibility(View.GONE);
            adView.stopAutoRefresh();
            adView.setBackgroundColor(Color.TRANSPARENT);

          } else {
            adView.setBackgroundColor(R.color.ic_background_color);
          }
        });
      }

      @Override
      public void onAdHidden(MaxAd ad) {

      }

      @Override
      public void onAdClicked(MaxAd ad) {

      }

      @Override
      public void onAdLoadFailed(String adUnitId, MaxError error) {

      }

      @Override
      public void onAdDisplayFailed(MaxAd ad, MaxError error) {

      }
    });
    adView.setBackgroundColor(Color.TRANSPARENT);

    FrameLayout.LayoutParams layoutParams = new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, FrameLayout.LayoutParams.WRAP_CONTENT);
    adViewParams.height = android.getResources().getDimensionPixelSize(R.dimen.banner_height);
    bannerContainer.addView(adView, adViewParams);

    // Load the ad
    adView.loadAd();
  }

  void InitMAXInterstitial() {
    interstitialAd = new MaxInterstitialAd(MAX_FULLSCREEN_ID, android);
    interstitialAd.setListener(new MaxAdListener() {
      @Override
      public void onAdLoaded(MaxAd ad) {
        // Interstitial ad is ready to be shown. interstitialAd.isReady() will now return 'true'
        // Reset retry attempt
        retryFullscreenAttempt = 0;
      }

      @Override
      public void onAdDisplayed(MaxAd ad) {
        if (fullscreenCallback != null) {
          fullscreenCallback.OnEvent(true, false);
          fullscreenCallback = null;
        }
      }

      @Override
      public void onAdHidden(MaxAd ad) {
        // Interstitial ad is hidden. Pre-load the next ad
        interstitialAd.loadAd();
      }

      @Override
      public void onAdClicked(MaxAd ad) {

      }

      @Override
      public void onAdLoadFailed(String adUnitId, MaxError error) {
        // Interstitial ad failed to load
        // We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds)
        retryFullscreenAttempt++;
        long delayMillis = TimeUnit.SECONDS.toMillis((long) Math.pow(2, Math.min(6, retryFullscreenAttempt)));

        new Handler().postDelayed(() -> interstitialAd.loadAd(), delayMillis);
      }

      @Override
      public void onAdDisplayFailed(MaxAd ad, MaxError error) {
        // Interstitial ad failed to display. We recommend loading the next ad
        interstitialAd.loadAd();
        if (fullscreenCallback != null) {
          fullscreenCallback.OnEvent(false, false);
          fullscreenCallback = null;
        }
      }
    });

    interstitialAd.loadAd();
  }

  void InitMAXVideoReward() {
    rewardedAd = MaxRewardedAd.getInstance(MAX_VIDEO_ID, android);
    rewardedAd.setListener(new MaxRewardedAdListener() {
      @Override
      public void onRewardedVideoStarted(MaxAd ad) {
        // Rewarded ad is ready to be shown. rewardedAd.isReady() will now return 'true'
        // Reset retry attempt
        retryVideoAttempt = 0;
      }

      @Override
      public void onRewardedVideoCompleted(MaxAd ad) {

      }

      @Override
      public void onUserRewarded(MaxAd ad, MaxReward reward) {
        // Rewarded ad was displayed and user should receive the reward
        if (videoRewardCallback != null)
          videoRewardCallback.OnEvent(true, false);

        videoRewardCallback = null;
      }

      @Override
      public void onAdLoaded(MaxAd ad) {

      }

      @Override
      public void onAdDisplayed(MaxAd ad) {

      }

      @Override
      public void onAdHidden(MaxAd ad) {
        // rewarded ad is hidden. Pre-load the next ad
        rewardedAd.loadAd();
      }

      @Override
      public void onAdClicked(MaxAd ad) {

      }

      @Override
      public void onAdLoadFailed(String adUnitId, MaxError error) {
        // Rewarded ad failed to load
        // We recommend retrying with exponentially higher delays up to a maximum delay (in this case 64 seconds)
        retryVideoAttempt++;
        long delayMillis = TimeUnit.SECONDS.toMillis((long) Math.pow(2, Math.min(6, retryVideoAttempt)));

        new Handler().postDelayed(() -> rewardedAd.loadAd(), delayMillis);
      }

      @Override
      public void onAdDisplayFailed(MaxAd ad, MaxError error) {
        // Rewarded ad failed to display. We recommend loading the next ad
        rewardedAd.loadAd();
        if (videoRewardCallback != null) {
          videoRewardCallback.OnEvent(false, false);
        }
        videoRewardCallback = null;
      }
    });

    rewardedAd.loadAd();
  }

//  public void initMaxNativeAd() {
//    nativeAdLoader = new MaxNativeAdLoader(MAX_NATIVE_ID, android);
//    nativeAdLoader.setNativeAdListener(new MaxNativeAdListener() {
//      @Override
//      public void onNativeAdLoaded(final MaxNativeAdView nativeAdView, final MaxAd ad) {
//        // Clean up any pre-existing native ad to prevent memory leaks.
//        if (nativeAd != null) {
//          nativeAdLoader.destroy(nativeAd);
//        }
//
//        // Save ad for cleanup.
//        nativeAd = ad;
//
//        // Add ad view to view.
//        Log.d(TAG, "(NativeAd)" + MAX_NATIVE_ID + " loaded");
//        ((AndroidLauncher) android).rootView.removeView(maxNativeAdView);
//        maxNativeAdView = nativeAdView;
//        maxNativeAdView.setVisibility(View.GONE);
//
//        RelativeLayout.LayoutParams adParams =
//            new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.WRAP_CONTENT,
//                RelativeLayout.LayoutParams.WRAP_CONTENT);
//        adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
//        adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
//
//        ((AndroidLauncher) android).rootView.addView(nativeAdView, adParams);
////        maxNativeAdView.getLayoutParams().width = GetNativeAdWidth();
////        maxNativeAdView.getLayoutParams().height = GetNativeAdHeight();
//
//
//        width = getViewWidth(maxNativeAdView);     //in pixel
//        height = getViewHeight(maxNativeAdView);   //in pixel
//
//      }
//
//      @Override
//      public void onNativeAdLoadFailed(final String adUnitId, final MaxError error) {
//        // We recommend retrying with exponentially higher delays up to a maximum delay
//        Log.d(TAG, "(NativeAd)" + MAX_NATIVE_ID + " Error " + error.getMessage());
//      }
//
//      @Override
//      public void onNativeAdClicked(final MaxAd ad) {
//        // Optional click callback
//      }
//    });
//
////    nativeAdLoader.loadAd(maxNativeAdView);
//    nativeAdLoader.loadAd();
////    int nativeTime = platform.GetConfigIntValue("NativeTime", 40);
////    Timer.schedule(new Timer.Task() {
////      @Override
////      public void run() {
////        android.runOnUiThread(() -> {
////          Log.d(TAG, "Reload native ad");
////          nativeAdLoader.loadAd();
////        });
////      }
////    }, 0, nativeTime);
//  }

  public void setNativeMargin(float marginRight, float marginTop) {
    if (maxNativeAdView == null) return;


    RelativeLayout.LayoutParams adParams =
        new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
            RelativeLayout.LayoutParams.MATCH_PARENT);

    adParams.addRule(RelativeLayout.ALIGN_PARENT_TOP);
    adParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);

    Log.d("setMargin", "" + marginRight + " " + marginTop);
    android.runOnUiThread(() -> {

      ((AndroidLauncher) android).rootView.removeView(maxNativeAdView);
      ((AndroidLauncher) android).rootView.addView(maxNativeAdView, adParams);
//      maxNativeAdView.getLayoutParams().width = GetNativeAdWidth();
//      maxNativeAdView.getLayoutParams().height = GetNativeAdHeight();
//      maxNativeAdView.setX(Gdx.graphics.getWidth() - marginRight - GetNativeAdWidth());
      maxNativeAdView.setY(marginTop);
    });


  }

  @Override
  public void ShowVideoReward(ISAdListenerEvent callback) {
    Log.d(TAG, "MAXVideo show");
    if (rewardedAd != null && rewardedAd.isReady()) {
      videoRewardCallback = callback;
      rewardedAd.showAd();
    } else {
      callback.OnEvent(false, false);
    }
  }

  @Override
  public void ShowInterstitial(ISAdListenerEvent callback) {
    Log.d(TAG, "MAXFullscreen show");
    if (interstitialAd.isReady()) {
      fullscreenCallback = callback;
      interstitialAd.showAd();
    }
  }

  @SuppressLint("ResourceAsColor")
  @Override
  public void ShowBanner(boolean visible) {
    Log.d(TAG, "ShowBanner " + visible);
    ApplovinMediation.this.bannerVisible = visible;
    if (adView != null) {
      android.runOnUiThread(() -> {
        if (ApplovinMediation.this.bannerVisible) {
          adView.setVisibility(View.VISIBLE);
          adView.startAutoRefresh();
          adView.setBackgroundColor(R.color.ic_background_color);
        } else {
          adView.setVisibility(View.GONE);
          adView.stopAutoRefresh();
          adView.setBackgroundColor(Color.TRANSPARENT);
        }


      });
    }
  }

  @Override
  public int GetBannerHeightInPixel() {
    return 0;
  }

  @Override
  public boolean IsInterstitialReady() {
    if (interstitialAd != null)
      return interstitialAd.isReady();
    else return false;
  }

  public boolean IsVideoRewardReady() {
    if (rewardedAd != null)
      return rewardedAd.isReady();
    else return false;
  }

  @Override
  public void ShowNative(boolean visible) {
    Log.d(TAG, "ShowNative " + visible);
    ApplovinMediation.this.nativeVisible = visible;
    if (maxNativeAdView != null) {
      android.runOnUiThread(() -> {
        if (ApplovinMediation.this.nativeVisible) {
          maxNativeAdView.setVisibility(View.VISIBLE);
//          maxNativeAdView.startAutoRefresh();
          maxNativeAdView.setBackgroundColor(android.getResources().getColor(R.color.ic_background_color));
        } else {
          maxNativeAdView.setVisibility(View.GONE);
//          maxNativeAdView.stopAutoRefresh();
          maxNativeAdView.setBackgroundColor(Color.TRANSPARENT);
        }
      });
    }
  }

//  @Override
//  public void ReloadNative() {
//    Log.d(TAG, "Reload native ad");
//    android.runOnUiThread(() -> {
//      if (nativeAdLoader != null)
//        nativeAdLoader.loadAd();
//    });
//  }

  @Override
  public boolean IsNativeAdvancedReady() {
    return nativeAd != null;
  }

  @Override
  public void onResume() {

  }

  @Override
  public void onPause() {

  }

//  @Override
//  public int GetNativeAdWidth() {
//    return android.getResources().getDimensionPixelSize(R.dimen.native_ad_width);
//  }

//  @Override
//  public int GetNativeAdHeight() {
//    return android.getResources().getDimensionPixelSize(R.dimen.native_ad_height);
//  }

//  @Override
//  public void SetNativeAdMargin(float marginRight, float marginTop) {
//    setNativeMargin(marginRight, marginTop);
//  }

  public void ShowAdsTestSuit() {
    AppLovinSdk.getInstance(android).showMediationDebugger();
  }

  public static int getViewHeight(View view) {
    WindowManager wm =
        (WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    DisplayMetrics outMetrics = new DisplayMetrics();
    display.getMetrics(outMetrics);
    float density = 1;//outMetrics.density;

    int deviceWidth;

    Point size = new Point();
    display.getSize(size);
    deviceWidth = size.x;

    int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
    int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    view.measure(widthMeasureSpec, heightMeasureSpec);
    return (int) (view.getMeasuredHeight() / density); //        view.getMeasuredWidth();
  }

  public static int getViewWidth(View view) {
    WindowManager wm =
        (WindowManager) view.getContext().getSystemService(Context.WINDOW_SERVICE);
    Display display = wm.getDefaultDisplay();
    DisplayMetrics outMetrics = new DisplayMetrics();
    display.getMetrics(outMetrics);
    float density = 1;//outMetrics.density;
    int deviceWidth;

    Point size = new Point();
    display.getSize(size);
    deviceWidth = size.x;

    int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(deviceWidth, View.MeasureSpec.AT_MOST);
    int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
    view.measure(widthMeasureSpec, heightMeasureSpec);
    return (int) (view.getMeasuredWidth() / density); //        view.getMeasuredWidth();
  }
}

