package com.game;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.NotificationCompat;
import androidx.work.OneTimeWorkRequest;
import androidx.work.WorkManager;
import androidx.work.Worker;
import androidx.work.WorkerParameters;

import com.badlogic.gdx.Gdx;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.zs.unrollball.R;

//import zs.tilematch.R;

public class ZenFirebaseMessagingService extends FirebaseMessagingService {
  private static final String TAG = "ZenFirebaseMsgService";

  @Override
  public void onMessageReceived(RemoteMessage remoteMessage) {
    Log.d(TAG, "From: " + remoteMessage.getFrom());

    // Check if message contains a data payload.
    if (remoteMessage.getData().size() > 0) {
      if (true) {
        // For long-running tasks (10 seconds or more) use WorkManager.
        scheduleJob();
      } else {
        // Handle message within 10 seconds
        handleNow();
      }

    }

    // Check if message contains a notification payload.
    if (remoteMessage.getNotification() != null) {
      Gdx.app.log(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
    }

    sendNotification(remoteMessage.getNotification().getBody(), remoteMessage.getNotification().getTitle());
  }


  @Override
  public void onNewToken(String token) {
    Log.d(TAG, "Refreshed token: " + token);

    sendRegistrationToServer(token);
  }

  private void scheduleJob() {
    OneTimeWorkRequest work = new OneTimeWorkRequest.Builder(ZenWorker.class).build();
    WorkManager.getInstance().beginWith(work).enqueue();

  }


  private void handleNow() {

  }


  private void sendRegistrationToServer(String token) {

  }

  private void sendNotification(String messageBody, String messageTitle) {
    Intent intent = new Intent(this, AndroidLauncher.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
    PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
        PendingIntent.FLAG_ONE_SHOT);

    String channelId = getString(R.string.default_notification_channel_id);
    Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
    NotificationCompat.Builder notificationBuilder =
        new NotificationCompat.Builder(this, channelId)
            .setSmallIcon(R.drawable.ic_stat_notification)
            .setContentTitle(messageTitle)
            .setContentText(messageBody)
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(pendingIntent);

    NotificationManager notificationManager =
        (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    // Since android Oreo notification channel is needed.
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
      NotificationChannel channel = new NotificationChannel(channelId,
          "Channel human readable title",
          NotificationManager.IMPORTANCE_DEFAULT);
      notificationManager.createNotificationChannel(channel);
    }

    notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
  }

  public static class ZenWorker extends Worker {
    private static final String TAG = "MyWorker";
    public ZenWorker(@NonNull Context appContext, @NonNull WorkerParameters workerParams) {
      super(appContext, workerParams);
    }

    @NonNull
    @Override
    public Result doWork() {
      Log.d(TAG, "Performing long running task in scheduled job");
      // TODO(developer): add long running task here.
      return Result.success();
    }
  }
}