rm -rf ./fi_deploy/assets
rm -rf ./fi_deploy/html
#rm -rf ./fi_deploy/scripts

cp -rf ./html/build/dist/assets ./fi_deploy/assets
cp -rf ./html/build/dist/html ./fi_deploy/html
#cp ./h5assets/out/* ./fi_deploy/assets
#cp -rf ./html/build/dist/scripts ./fi_deploy/scripts

pushd ./fi_deploy || exit 1
http-server --ssl -c-1 -p 8080 -a 127.0.0.1
popd || exit 1
#/usr/bin/open -a "/Applications/Brave Browser.app" "https://www.google.com/"

#https://www.facebook.com/embed/instantgames/704045440891725/player?game_url=https://localhost:8080