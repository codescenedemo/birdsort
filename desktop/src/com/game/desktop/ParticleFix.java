package com.game.desktop;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.ui.ParticleEffectActor;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;

public class ParticleFix extends Game {
  final String atlasPath = "textureAtlas/";
  final String particlePath = "particle/";
  final String outPutPath = "./particle/";
  final java.util.List<String> particles = Arrays.asList(
      "hammer_hit.p"
  );



  public static void main(String[] args) {
    Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
//    config.setWindowedMode((int) (720 * 0.6f), (int) (1280 * 0.6f));
    new Lwjgl3Application(new ParticleFix(), config);
  }

  @Override
  public void create() {
    AssetManager am = new AssetManager();
    am.load(atlasPath + "particle.atlas", TextureAtlas.class);
    am.finishLoading();
    TextureAtlas ta = am.get(atlasPath + "particle.atlas", TextureAtlas.class);

    for (String particle : particles) {
      try {
        ParticleEffectActor a = new ParticleEffectActor(Gdx.files.internal(particlePath + particle), ta);
        BufferedWriter writer = new BufferedWriter(new FileWriter(outPutPath + particle));
        a.getEffect().save(writer);
        writer.close();
        System.out.println("save particle done: " + particle);
      }
      catch (IOException ignored) {
        System.out.println("error" + ignored.getMessage());
      }
    }

    System.exit(0);
  }
}
