package com.game.desktop;

import com.badlogic.gdx.backends.lwjgl3.Lwjgl3Application;
import com.badlogic.gdx.backends.lwjgl3.Lwjgl3ApplicationConfiguration;
import com.badlogic.gdx.utils.ObjectMap;
import com.core.utils.PrefSLoader;
import com.core.utils.SessionLoader;
import com.game.GMain;
import com.gameservice.PlayService;
import com.gameservice.PlayfabPlayService;
import com.platform.AdsClose;
import com.platform.IPlat;

import java.util.Calendar;

public class DesktopLauncher {
  public static DesktopLauncher me;
  public static void main(String[] arg) {

    Lwjgl3ApplicationConfiguration config = new Lwjgl3ApplicationConfiguration();
    config.setWindowedMode(720 / 2 , 1280 / 2);
    config.setForegroundFPS(400);

    new Lwjgl3Application(new GMain(new IPlat() {
      PlayService playService = new PlayfabPlayService("E0265", "THUC");


      @Override
      public String getMd5PathName(String filename) {
        return null;
      }

      @Override
      public void preloadAssets(String assetTxt, PreloaderCallback callback) {

      }

      @Override
      public SessionLoader getSessionLoader() {
        return new PrefSLoader("com.billyadventure");
      }

      @Override
      public String getPlatform() {
        return "DESKTOP";
      }

      @Override
      public PlayService getPlayService() {
        return playService;
      }

      @Override
      public void log(String str) {
        System.out.println(str);
      }

      @Override
      public String GetDefaultLanguage() {
        return "en";
      }

      @Override
      public boolean isVideoRewardReady() {
        return true;
      }

      @Override
      public void ShowVideoReward(AdsClose callback) {
        callback.OnEvent(true);
      }

      @Override
      public void ShowRewardInterstitial(AdsClose callback) {
        callback.OnEvent(true);
      }

      @Override
      public void ShowFullscreen(AdsClose onshow, AdsClose ondone) {
        onshow.OnEvent(true);
        ondone.OnEvent(true);
      }

      @Override
      public void ShowUIFullscreen() {

      }

      @Override
      public void ShowNative(boolean visible) {

      }

      @Override
      public boolean IsNativeAdvancedReady() {
        return true;
      }

      @Override
      public boolean isFullScreenReady() {
        return false;
      }


      @Override
      public void ShowBanner(boolean visible) {

      }

      @Override
      public int GetConfigIntValue(String name, int defaultValue) {
        return defaultValue;
      }

      @Override
      public String GetConfigStringValue(String name, String defaultValue) {
        return defaultValue;
      }

      @Override
      public void CrashKey(String key, String value) {

      }

      @Override
      public void CrashLog(String log) {

      }

      @Override
      public int GetBannerHeight() {
        return 0;
      }

      @Override
      public void OpenStore() {

      }

      @Override
      public void TrackScreen(String screen) {

      }

      @Override
      public void TrackNotification(int notifyId, int dailyNotificationCount) {

      }

      @Override
      public void TrackCustomEvent(String event) {

      }

      @Override
      public void TrackGameStart(int serverVersion) {

      }

      @Override
      public void TrackEvaluate(String eventname, int hitCount, String evaluate) {

      }

      @Override
      public void TrackLevelInfo(String event, int level, float time) {

      }

      @Override
      public void TrackLevelInfo(String event, ObjectMap<String, Object> data) {

      }

      @Override
      public void TrackPlayerInfo(String event, int mode, int level) {

      }

      @Override
      public void TrackPlaneInfo(String event, int planeid, int level) {

      }

      @Override
      public void TrackVideoReward(String type) {

      }

      @Override
      public void TrackPlayerDead(String event, int mode, int difficult, int level, int parentModel, int shooterModel, boolean isBoss) {

      }

      @Override
      public int GetNotifyId() {
        return 0;
      }

      @Override
      public void SetDailyNotification(int id, String header, String content, int days, int hours) {

      }

      @Override
      public void CancelDailyNotification(int id) {

      }

      @Override
      public void TrackRating(int rateid, int rateFromLevel) {

      }

      @Override
      public void Restart() {

      }

      @Override
      public void ShowAdsTestSuit() {

      }

      @Override
      public void onShow() {

      }

      @Override
      public int dayDiff(long t1, long t2) {
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(t1);
        int d1 = cal.get(Calendar.DAY_OF_YEAR);
        int y1 = cal.get(Calendar.YEAR);
        cal.setTimeInMillis(t2);
        int d2 = cal.get(Calendar.DAY_OF_YEAR);
        int y2 = cal.get(Calendar.YEAR);

        if (y2 > y1)
          return 1;

        return Math.abs(d2 - d1);
      }
    }), config);
  }
}
