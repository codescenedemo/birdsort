package com.game.desktop;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.StreamUtils;

import java.io.Closeable;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

public class TextureEncryptor {
    public static final int[] DECODE_KEYS = new int[]{2, 0, 4, 1, 3};
    public static final int[] ENCODE_KEYS = new int[]{1, 3, 0, 4, 2};

    public static final String HEADER_VALUE = ".tex";
    public static String assetsPath = "./android/assets/";
    public static String nonEncryptAssetsPath = "./non-encryptassets";

    public static String [] encryptPaths = {
            "textureAtlas", "spine", "level"

    };

    public static final boolean IsEncrypt = false;



    public static void main(String[] args) throws Exception
    {
        File nonEncryptAssetsPathFile = new File(nonEncryptAssetsPath);
        File assetsPathFile = new File(assetsPath);
        copyDirectory(nonEncryptAssetsPathFile, assetsPathFile);
        System.out.println("copy from " + nonEncryptAssetsPath + " to " + assetsPath);


        if(IsEncrypt) {
            ArrayList<String> assets = getAllTextureFile();
            try {
                for (int i = 0; i < assets.size(); i++) {
                    String name = assets.get(i);

                    System.out.println(name);
                    encodePngFile(name);

                    //return;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private static void copyDirectory(File sourceDirectory, File destinationDirectory) throws IOException {
        if (!destinationDirectory.exists()) {
            destinationDirectory.mkdir();
        }
        for (String f : sourceDirectory.list()) {
            copyDirectoryCompatibityMode(new File(sourceDirectory, f), new File(destinationDirectory, f));
        }
    }

    public static void copyDirectoryCompatibityMode(File source, File destination) throws IOException {
        if (source.isDirectory()) {
            copyDirectory(source, destination);
        } else {
            copyFile(source, destination);
        }
    }

    private static void copyFile(File sourceFile, File destinationFile)
            throws IOException {
        try (InputStream in = new FileInputStream(sourceFile);
             OutputStream out = new FileOutputStream(destinationFile)) {
            byte[] buf = new byte[1024];
            int length;
            while ((length = in.read(buf)) > 0) {
                out.write(buf, 0, length);
            }
        }
    }

    public static void encodePngFile(String filename){
        try {
            FileHandle file = new FileHandle(filename);
            InputStream inputStream = file.read();
            byte[] headerByteArray = new byte[4];
            inputStream.read(headerByteArray);
            String header = new String(headerByteArray);

            if(header.equals(HEADER_VALUE)){
                System.out.println(filename +" already encode");
                return;
            }
            StreamUtils.closeQuietly((Closeable) inputStream);
            inputStream = file.read();
            byte[] encodedData = readBytes((InputStream) inputStream, ENCODE_KEYS);
            StreamUtils.closeQuietly((Closeable) inputStream);

            byte[] ba = HEADER_VALUE.getBytes();
            file.writeBytes(ba, false);
            file.writeBytes(encodedData, true);
            System.out.println("encode " + filename + " done. Overrided");
        }
        catch (Exception e){
           // e.printStackTrace();
            System.out.println("encrypt file " + filename + " failed. Reason " + e.getMessage() );
        }
    }

    private static byte[] readBytes(InputStream inputStream, int[] keys) throws Exception {
        int fileSize = inputStream.available();
        byte[] encodeByteArray = new byte[fileSize];
        int blockSize = fileSize / keys.length;
        int blockIndex = 0;
        do {
            if (blockIndex >= keys.length) {
                int byteLeft = fileSize % keys.length;
                if (byteLeft <= 0) return encodeByteArray;
                inputStream.read(encodeByteArray, blockSize * keys.length, byteLeft);
                return encodeByteArray;
            }
            inputStream.read(encodeByteArray, blockSize * keys[blockIndex], blockSize);
            ++blockIndex;
        } while (true);
    }

    public static void getAllRecursiveFileInDirectory(File directory, ArrayList<String> result){
        if(!directory.isDirectory())
            return;

        File files[] = directory.listFiles();
        for(int i=0; i<files.length; i++) {
            if(files[i].isDirectory()){
                getAllRecursiveFileInDirectory(files[i], result);
            }
            else if(files[i].isFile() && files[i].getName().endsWith(".png") || directory.getName().endsWith("level")){
                result.add(files[i].getAbsolutePath());
            }

        }
    }

    public static ArrayList<String> getAllTextureFile(){
        ArrayList<String> result = new ArrayList<>();

        for(int i=0;i<encryptPaths.length;i++){
            File directory = new File(assetsPath + encryptPaths[i]);
            getAllRecursiveFileInDirectory(directory, result);
        }
        return result;
    }

}
