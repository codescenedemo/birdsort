package com.gameservice;

import com.badlogic.gdx.utils.JsonValue;

public interface PlayHandler {
  void handle(JsonValue resp);
}
