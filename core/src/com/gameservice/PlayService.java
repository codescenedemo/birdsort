package com.gameservice;

@SuppressWarnings("unused")
public interface PlayService {
  void    loginWithDeviceID(); // LoginWithAndroidDeviceID
  boolean loggedIn();
  String  getId();
  String  getDisplayName();
  void    updateDisplayName(String displayName); // UpdateUserTitleDisplayName
  void    setHandler(PlayHandler handler);
  void    updateScore(String boardName, long score); //UpdatePlayerStatistics
  void    getLeaderBoard(String boardName, int start, int count); //GetLeaderboard
}