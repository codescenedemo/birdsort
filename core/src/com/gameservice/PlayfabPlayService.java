package com.gameservice;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.utils.Json;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.badlogic.gdx.utils.JsonWriter;

public class PlayfabPlayService implements PlayService, Net.HttpResponseListener {
  Net.HttpRequest   req;
  String            titleId;
  String            deviceId;
  Json              json;
  String            sessionTicket = "";
  String            playfabId = "";
  boolean           loggedIn;
  String            cmd = "";
  JsonReader        jr;
  PlayHandler       handler;
  String            displayName;

  public PlayfabPlayService(String titleId, String deviceId) {
    req = new Net.HttpRequest();
    this.titleId = titleId;
    this.deviceId = deviceId;
    json = new Json();
    json.setIgnoreUnknownFields(true);
    json.setOutputType(JsonWriter.OutputType.json);
    jr = new JsonReader();
  }

  private void composeRequest(String url, String payload) {
    req.setUrl(url);
    req.setMethod("POST");
    req.setHeader("Content-Type", "application/json");
    req.setHeader("X-Authorization", sessionTicket);
    req.setContent(payload);
  }

  @Override
  public void loginWithDeviceID() {
    String url = "https://" + titleId + ".playfabapi.com/Client/LoginWithAndroidDeviceID";

    JsonValue payload = new JsonValue(JsonValue.ValueType.object);
    payload.addChild("TitleId", new JsonValue(titleId));
    payload.addChild("AndroidDeviceId", new JsonValue(deviceId));
    payload.addChild("CreateAccount", new JsonValue(true));

    JsonValue extra = new JsonValue(JsonValue.ValueType.object);
    extra.addChild("GetPlayerProfile", new JsonValue(true));
    extra.addChild("GetUserAccountInfo", new JsonValue(true));

    payload.addChild("InfoRequestParameters", extra);

    composeRequest(url, payload.toJson(JsonWriter.OutputType.javascript));
    Gdx.net.sendHttpRequest(req, this);
    cmd = "LoginWithAndroidDeviceID";
  }

  @Override
  public boolean loggedIn() {
    return loggedIn;
  }

  @Override
  public String getId() {
    return playfabId;
  }

  @Override
  public String getDisplayName() {
    return displayName;
  }

  @Override
  public void updateDisplayName(String displayName) {
    String url = "https://" + titleId + ".playfabapi.com/Client/UpdateUserTitleDisplayName";
    JsonValue payload = new JsonValue(JsonValue.ValueType.object);
    payload.addChild("DisplayName", new JsonValue(displayName));
    composeRequest(url, payload.toJson(JsonWriter.OutputType.javascript));
    Gdx.net.sendHttpRequest(req, this);
    cmd = "UpdateUserTitleDisplayName";
  }

  @Override
  public void setHandler(PlayHandler handler) {
    this.handler = handler;
  }

  @Override
  public void updateScore(String boardName, long score) {
    String url = "https://" + titleId + ".playfabapi.com/Client/UpdatePlayerStatistics";
    JsonValue payload = new JsonValue(JsonValue.ValueType.object);
    JsonValue stats = new JsonValue(JsonValue.ValueType.array);
    JsonValue scoreObj = new JsonValue(JsonValue.ValueType.object);
    scoreObj.addChild("StatisticName", new JsonValue(boardName));
    scoreObj.addChild("Value", new JsonValue(score));
    stats.addChild(scoreObj);
    payload.addChild("Statistics", stats);
    composeRequest(url, payload.toJson(JsonWriter.OutputType.javascript));
    Gdx.net.sendHttpRequest(req, this);
    cmd = "UpdatePlayerStatistics";
  }

  @Override
  public void getLeaderBoard(String boardName, int start, int count) {
    String url = "https://" + titleId + ".playfabapi.com/Client/GetLeaderboard";
    JsonValue payload = new JsonValue(JsonValue.ValueType.object);
    payload.addChild("StartPosition", new JsonValue(start));
    payload.addChild("MaxResultsCount", new JsonValue(count));
    payload.addChild("StatisticName", new JsonValue(boardName));
    composeRequest(url, payload.toJson(JsonWriter.OutputType.javascript));
    Gdx.net.sendHttpRequest(req, this);
    cmd = "GetLeaderboard";
  }

  @Override
  public void handleHttpResponse(Net.HttpResponse httpResponse) {
    String s = httpResponse.getResultAsString();
    JsonValue resp = jr.parse(s);
    if (resp.getInt("code") != 200) {
      failed(new Throwable(resp.getString("errorMessage")));
      return;
    }

    resp.addChild("cmd", new JsonValue(cmd));

    if ("LoginWithAndroidDeviceID".equals(cmd)) {
      sessionTicket = resp.get("Data").getString("SessionTicket");
      sessionTicket = sessionTicket == null ? "" : sessionTicket;
      if (!sessionTicket.equals("")) {
        try {
          loggedIn = true;
          playfabId = resp.get("Data").getString("PlayFabId");
          displayName = resp.get("Data").get("InfoResultPayload").get("AccountInfo").get("TitleInfo").getString("DisplayName", "");
        }
        catch (Exception e) {
          loggedIn = false;
          sessionTicket = "";
          displayName = "";
        }
        Gdx.app.log("PlayService", "LoginWithAndroidDeviceID Success");
      }
    }

    if (handler == null)
      return;
    Gdx.app.postRunnable(() -> handler.handle(resp));
  }

  @Override
  public void failed(Throwable t) {
    Gdx.app.error("[PlayService]", t.getMessage());
  }

  @Override
  public void cancelled() {

  }
}