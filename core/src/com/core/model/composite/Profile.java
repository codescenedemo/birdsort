package com.core.model.composite;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.MathUtils;


@SuppressWarnings("unused")
public class Profile {
  public double id;
  public String name;
  public int level;
  public int progress;
  public int choosenBgIdx;
  public int choosenBranchIdx;

  public static Profile ofDefault() {
    Profile p = new Profile();

    p.name = makeName();
    p.id = (System.currentTimeMillis() << 20) | (System.currentTimeMillis() & ~9223372036854251520L);
    p.level = 1;
    p.progress = 0;
    p.choosenBgIdx = 1;
    p.choosenBranchIdx = 1;
    return p;
  }

  public void reBalance() {
    if (id == 0)
      id = (System.currentTimeMillis() << 20) | (System.currentTimeMillis() & ~9223372036854251520L);

    if (name == null || name.isEmpty())
      name = makeName();

    if (level < 1){
      level = 1;
    }

    if (level > 399){
      level = 399;
    }

    if (progress >= 10){
      progress = 0;
    }

    if (choosenBgIdx == 0){
      choosenBgIdx = 1;
    }

    if (choosenBranchIdx == 0){
      choosenBranchIdx = 1;
    }
  }



  public static String makeName() {
    try {
      String[] contents = Gdx.files.internal("data/random_name.csv").readString().split("\n");
      int id = MathUtils.random(contents.length);
      String first = contents[id].split(",")[0];
      id = MathUtils.random(contents.length);
      String last = contents[id].split(",")[1];

      return first + " " + last;
    } catch (Exception e) {
      return "";
    }
  }






}