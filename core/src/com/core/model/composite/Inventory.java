package com.core.model.composite;

import com.badlogic.gdx.utils.IntMap;

public class Inventory {



  public int undo;
  public int branch;

  public int rewardIdx;
  public int unlockedIdx;

  public IntMap<Integer> unlockBgId;
  public IntMap<Integer> unlockBranchId;

  public static Inventory ofDefault(){
    Inventory inventory = new Inventory();
    inventory.undo = 3;
    inventory.branch = 0;
    inventory.rewardIdx = 1;
    inventory.unlockedIdx = 0;
    inventory.unlockBgId = new IntMap<>();
    inventory.unlockBgId.put(1, 1);
    inventory.unlockBranchId = new IntMap<>();
    inventory.unlockBranchId.put(1, 1);

    return inventory;
  }

  public void reBalance(){
    if (undo > 5){
      undo = 5;
    }

    if (branch > 3){
      branch = 3;
    }
    if (rewardIdx > 10){
      rewardIdx = 10;
    }

    if (rewardIdx == 0){
      rewardIdx = 1;
    }

    if (unlockedIdx > 10){
      unlockedIdx = 10;
    }

    if (unlockBgId == null){
      unlockBgId = new IntMap<>();
      unlockBgId.put(1, 1);
    }

    if (unlockBranchId == null){
      unlockBranchId = new IntMap<>();
      unlockBranchId.put(1, 1);
    }
  }



}
