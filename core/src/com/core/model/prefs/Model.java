package com.core.model.prefs;

import com.badlogic.gdx.utils.reflect.ReflectionException;

//this mean every Model have an ability of repair when data corrupt
public interface Model<A> {
  void reBalance();
}