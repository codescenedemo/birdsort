package com.core.model.prefs;

@SuppressWarnings("unused")
public interface SettingAdapter {
  boolean soundOn();
  boolean musicOn();
  boolean vibrateOn();
}