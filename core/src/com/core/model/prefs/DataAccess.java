package com.core.model.prefs;

/*
Abstract data io, K,V style
*/
public interface DataAccess<T extends Model> {
  void  sync(String key, T t);
  T     load(String key) throws Exception;
}