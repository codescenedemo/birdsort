package com.core.model;

import com.core.model.composite.Inventory;
import com.core.model.composite.PlayData;
import com.core.model.composite.Profile;
import com.core.model.composite.Setting;
import com.core.model.prefs.Model;


/*
Use this for all later game, simplify data persistent, serialization
 */
@SuppressWarnings("unused")
public class Session implements Model<Session>{


  public Profile profile;
  public PlayData playData;
  public Inventory inventory;
  public Setting setting;


  public static Session ofDefault() {
    Session session     = new Session();

    session.profile = Profile.ofDefault();
    session.playData = PlayData.ofDefault();
    session.inventory = Inventory.ofDefault();
    session.setting = Setting.ofDefault();

    return session;
  }

  @Override
  public void reBalance() {



    if (profile == null)
      profile = Profile.ofDefault();

    if (playData == null){
      playData = PlayData.ofDefault();
    }if (inventory == null)
      inventory = Inventory.ofDefault();
    if (setting == null){
      setting = Setting.ofDefault();
    }


    inventory.reBalance();
    profile.reBalance();
    playData.reBalance();
    setting.reBalance();


  }


}