package com.core.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;

import java.util.Comparator;

public enum GBox2DLayer {
    bottom("bottom", Touchable.childrenOnly),
    sprite("sprite", Touchable.childrenOnly);

    private Comparator comparator;
    private GLayerGroup group;
    private String name;
    private Touchable touchable;


    private GBox2DLayer(String name, Touchable touchable) {
        this.name = name;
        this.touchable = touchable;
    }

    private void createComparator() {
        Comparator<Actor> comparator = new Comparator<Actor>() {
            public int compare(Actor var1, Actor var2) {
                return var1.getY() < var2.getY() ? -1 : (var1.getY() > var2.getY() ? 1 : 0);
            }
        };
        switch (this) {
            case bottom:
            case sprite:
//      case effect:

                comparator = null;
            default:
                this.comparator = comparator;
        }
    }

    public Comparator getComparator() {
        return this.comparator;
    }

    public GLayerGroup getGroup() {
        return this.group;
    }

    public String getName() {
        return this.name;
    }

    public Touchable getTouchable() {
        return this.touchable;
    }

    public void init(GLayerGroup layerGroup) {
        this.group = layerGroup;
        layerGroup.setName(this.name);
        layerGroup.setTouchable(this.touchable);
        this.createComparator();
    }
}