package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.PolygonSpriteBatch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ScreenUtils;
import com.badlogic.gdx.utils.SnapshotArray;
import com.badlogic.gdx.utils.Sort;
import com.badlogic.gdx.utils.StringBuilder;
import com.badlogic.gdx.utils.viewport.ExtendViewport;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.FitViewport;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Vector;

public final class GStage {
   private static int avgFps = 60;
   private static SpriteBatch batch;
  // private static GStage.StageBorder border;
   private static StringBuilder buff = new StringBuilder();
   private static float cameraOffsetX;
   private static float cameraOffsetY;
   private static Color clearColor  = Color.BLACK;
   private static float delta;
   private static int fps = 60;
   private static int fpsCount;
   private static double gameTime;
   private static long javaHeap;
   private static long lastIndex = 0L;
   private static int maxSpritesInBatch;
   private static long nativeHeap;
   private static boolean pauseUpdateServices = false;
   private static int renderCalls;
   public static ShaderProgram shader;
   private static float sleepTime = 0.033333335F;
   private static Stage stage;
   private static int stageActorcount;
   private static int timeCount;
   private static int totalRenderCalls;

   private static ObjectMap updateServices = new ObjectMap();


   private static String actorPath(Actor actor) {
      StringBuffer strBuffer = new StringBuffer();
      strBuffer.append(actor);

      for(Group group = actor.getParent(); group != null; group = group.getParent()) {
         strBuffer.append("<---" + group);
      }

      return strBuffer.toString();
   }

   public static void addToLayer(GLayer layer, Actor actor) {
      getLayer(layer).addActor(actor);
   }

   public static void addToLayer(GLayer layer, Actor actor, float padX, float padY, int align) {
      getLayer(layer).addActor(actor);
      align(padX, padY, align, actor);
   }

   public static void addToLayer(GBox2DLayer layer, Actor actor) {
        getLayer(layer).addActor(actor);
   }

  public static void addToLayer(GBox2DLayer layer, Actor actor, float padX, float padY, int align) {
    getLayer(layer).addActor(actor);
    align(padX, padY, align, actor);
  }

   public static void align(float padX, float padY, int align, Actor child) {
     float posX  = 0;
     float posY  = 0;
     int   textAlign = 0;

     if ((align & Align.center) == Align.center) {
       textAlign |= Align.center;
       posX = getWorldWidth()/2 + padX;
       posY = getWorldHeight()/2 + padY;
     }

     if ((align & Align.left) == Align.left) {
       textAlign |= Align.left;
       posX = padX;
     }

     if ((align & Align.top) == Align.top) {
       textAlign |= Align.top;
       posY = getHeight() - padY;
     }

     if ((align & Align.bottom) == Align.bottom) {
       textAlign |= Align.bottom;
       posY = padY;
     }

     if ((align & Align.right) == Align.right) {
       textAlign |= Align.right;
       posX = getWidth() - padX;
     }

     if (child instanceof Label)
       ((Label) child).setAlignment(textAlign);
     child.setPosition(posX, posY, align);
   }


   public static void clearAllLayers() {
      GLayer[] layers = GLayer.values();

      for(int i = 0; i < layers.length; ++i) {
         clearLayer(layers[i]);
      }

    //  GPool.freeAll();
      System.gc();
   }

   public static void clearLayer(GLayer layer) {
      getLayer(layer).clear();
   }

   public static void clearListeners() {
      getStageLayer().clearListeners();
   }

   public static void clearStage() {
      clearAllLayers();
      stage.unfocusAll();
   }

   private static void createLayer(GLayer layer) {
      GLayerGroup group = new GLayerGroup();
      layer.init(group);
      stage.addActor(group);
   }

   public static void dispose() {
      stage.dispose();
//      batch.dispose();
   }

   public static Actor getActorAtLayer(GLayer layer, String nameOfActor) {
      return getLayer(layer).findActor(nameOfActor);
   }


   public static OrthographicCamera getCamera() {
      return (OrthographicCamera)stage.getCamera();
   }

   public static float getCameraOffsetX() {
      return cameraOffsetX;
   }

   public static float getCameraOffsetY() {
      return cameraOffsetY;
   }

   public static float getDelta() {
      return delta;
   }
   public static float getDeltaTimescale(){
      return Gdx.graphics.getDeltaTime()*timeScale;
   }
   public static int getHeight() {
      return (int)stage.getHeight();
   }

   public static int getIndex() {
      return (int)(gameTime / (double)sleepTime);
   }

   public static GLayerGroup getLayer(GLayer layer) {
      return layer.getGroup();
   }

    public static GLayerGroup getLayer(GBox2DLayer layer) {
        return layer.getGroup();
    }

   public static float getSleepTime() {
      return sleepTime;
   }

   public static Stage getStage() {
      return stage;
   }

   private static Vector getStageActor(Group group) {
      Vector list = new Vector();
      SnapshotArray childs = group.getChildren();
      childs.begin();

      for(int i = 0; i < childs.size; ++i) {
         Actor actor = (Actor)childs.get(i);
         if(actor instanceof Group) {
            list.addAll(getStageActor((Group)actor));
         } else if(actor.getName() == null || !actor.getName().equals("debugActor")) {
            list.add(actor);
         }
      }

      childs.end();
      return list;
   }

   public static float getStageHeight() {
      return stage.getHeight();
   }

   public static Group getStageLayer() {
      return stage.getRoot();
   }

   public static float getStageWidth() {
      return stage.getWidth();
   }

   public static int getWidth() {
      return (int)stage.getWidth();
   }


   static public ShaderProgram createDefaultShader () {
      String vertexShader = "attribute vec4 " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
              + "attribute vec4 " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
              + "attribute vec2 " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
              + "uniform mat4 u_projTrans;\n" //
              + "varying vec4 v_color;\n" //
              + "varying vec2 v_texCoords;\n" //
              + "\n" //
              + "void main()\n" //
              + "{\n" //
              + "   v_color = " + ShaderProgram.COLOR_ATTRIBUTE + ";\n" //
              + "   v_color.a = v_color.a * (255.0/254.0);\n" //
              + "   v_texCoords = " + ShaderProgram.TEXCOORD_ATTRIBUTE + "0;\n" //
              + "   gl_Position =  u_projTrans * " + ShaderProgram.POSITION_ATTRIBUTE + ";\n" //
              + "}\n";
      String fragmentShader = "#ifdef GL_ES\n" //
              + "#define LOWP lowp\n" //
              + "precision mediump float;\n" //
              + "#else\n" //
              + "#define LOWP \n" //
              + "#endif\n" //
              + "varying LOWP vec4 v_color;\n" //
              + "varying vec2 v_texCoords;\n" //
              + "uniform sampler2D u_texture;\n" //
              + "void main()\n"//
              + "{\n" //
              + "  gl_FragColor = v_color * texture2D(u_texture, v_texCoords);\n" //
              + "}";

      ShaderProgram shader = new ShaderProgram(vertexShader, fragmentShader);
      if (shader.isCompiled() == false) throw new IllegalArgumentException("Error compiling shader: " + shader.getLog());
      return shader;
   }

   public static void init(float width, float height, float cameraX, float cameraY) {
      //Mesh.forceVBO = true;
     // if(Gdx.graphics.isGL20Available()) {
      shader = createDefaultShader();//SpriteBatch.createDefaultShader();
      //}

//       width/=100;
//       height/=100;
      cameraOffsetX = cameraX;
      cameraOffsetY = cameraY;
      stage = new Stage( new FitViewport(width, height), new PolygonSpriteBatch());
      getCamera().setToOrtho(false, width, height);
      getCamera().translate(cameraX, cameraY);
      Gdx.input.setInputProcessor(stage);
      Gdx.input.setCatchBackKey(true);
      initLayers();
      initDebug();
   }


    private static void initDebug() {
   }

   private static void initLayers() {
      GLayer[] layers = GLayer.values();

      for(int i = 0; i < layers.length; ++i) {
         createLayer(layers[i]);
      }
   }


   public static boolean isUpdate() {
      boolean flag;
      if((long)getIndex() - lastIndex >= 1L) {
         flag = true;
      } else {
         flag = false;
      }

      if(flag) {
         lastIndex = (long)getIndex();
      }

      return flag;
   }

   public static boolean isUpdateServicesPause() {
      return pauseUpdateServices;
   }

   public static void registerUpdateService(String serviceName, GUpdateService serviceHandler) {
      if(serviceHandler != null) {
         updateServices.put(serviceName, serviceHandler);
      } else {
         updateServices.remove(serviceName);
      }
   }

   public static void removeActor(GLayer layer, Actor actor) {
      getLayer(layer).removeActor(actor);
   }

   public static float timeScale=1f;
   public static void render() {
     // stage.setDebugAll(true);

      float r = clearColor.r;
      float g = clearColor.g;
      float b = clearColor.b;
      float a = clearColor.a;
      Gdx.gl.glClearColor(r, g, b, a);
      Gdx.gl.glClear(16384);

      delta = Gdx.graphics.getDeltaTime()*timeScale;
      sortLayers();
      updateServices(delta);
      stage.act(delta);

      stage.draw();

      gameTime += (double)delta;
   }


    public static void clearColor() {
        float r = clearColor.r;
        float g = clearColor.g;
        float b = clearColor.b;
        float a = clearColor.a;
        Gdx.gl.glClearColor(r, g, b, a);
        Gdx.gl.glClear(16384);
    }
    public static void renderStage() {
        delta = Gdx.graphics.getDeltaTime()*timeScale;
        sortLayers();
        updateServices(delta);
        stage.act(delta);
        stage.draw();
        gameTime += (double)delta;
    }

   public static void setClearColor(Color color) {
      clearColor = color;
   }

   public static void setSleepTime(long sleepTime) {
      GStage.sleepTime = (float)sleepTime;
   }

   public static void setUpdateServicesPause(boolean isPause) {
      pauseUpdateServices = isPause;
   }

   /*protected static void setViewport(float var0, float var1) {
      stage.setViewport(var0, var1);
   }*/

//   private static void sleep(long var0) {
//   }

   public static void sort(Group group, Comparator comparator) {
      Sort.instance().sort((Array)group.getChildren(), comparator);
   }

   public static void sort(GLayer layer) {
      sort(getLayer(layer), layer.getComparator());
   }

   private static void sortLayers() {
      GLayer[] layers = GLayer.values();

      for(int i = 0; i < layers.length; ++i) {
         if(layers[i].getComparator() != null) {
            sort(layers[i]);
         }
      }

   }

   private static void updateServices(float delta) {
      if(!pauseUpdateServices) {
         Iterator it = updateServices.values().iterator();

         while(it.hasNext()) {
            GUpdateService service = (GUpdateService)it.next();
            if(service.update(delta)) {
               updateServices.remove((String)updateServices.findKey(service, true));
            }
         }
      }

   }

   public static float getWorldWidth(){
       return getStage().getViewport().getWorldWidth();
   }

   public static float getWorldHeight(){
        return getStage().getViewport().getWorldHeight();
    }
   public TextureRegion getScreenSnapshot() {
      return ScreenUtils.getFrameBufferTexture();
   }

   public interface GUpdateService {
      boolean update(float delta);
   }

//   public interface StageBorder {
//      void drawHorizontalBorder(Batch var1, float var2, float var3);
//
//      void drawVerticalBorder(Batch var1, float var2, float var3);
//   }
}
