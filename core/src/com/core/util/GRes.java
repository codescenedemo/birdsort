package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.StreamUtils;


import java.io.BufferedReader;
import java.io.DataInputStream;
import java.util.Iterator;

public abstract class GRes {
   public static final String FILE_PATH_ACTION = "action/";
   public static final String FILE_PATH_ANIMATION = "animation/";
   public static final String FILE_PATH_FONT = "bitmapFont/";
   public static final String FILE_PATH_I18N = "i18n/";
   public static final String FILE_PATH_SPINE = "spine/";
   public static final String FILE_PATH_TMX = "tilemap/";
   public static final String FILE_PATH_PARTICLE = "particle/";
   public static final String FILE_PATH_SCRIPT = "script/";
   public static final String FILE_PATH_SOUND = "sound/";
   public static final String FILE_PATH_STRING = "string/";
   public static final String FILE_PATH_TEXTURE = "texture/";
   public static final String FILE_PATH_LEVEL = "level/";
   public static final String FILE_PATH_TEXTURE_ATLAS = "textureAtlas/";
   public static final String FILE_PATH_SHADER = "shader/";
   private static BitmapFont defaultFont;
   public static Texture.TextureFilter magFilter;
   public static Texture.TextureFilter minFilter;

   static {
      minFilter = Texture.TextureFilter.Linear;
      magFilter = Texture.TextureFilter.Linear;
      defaultFont = null;
   }

   public static TextureAtlas.AtlasRegion createRegionFromAtlasRegion(TextureAtlas.AtlasRegion atlas, int var1, int var2, int var3, int var4) {
      int w;
      if(atlas.rotate) {
         w = atlas.getRegionWidth();
      } else {
         w = atlas.getRegionHeight();
      }

      int regionX = atlas.getRegionX();
      int regionY = atlas.getRegionY();
      int h;
      if(atlas.rotate) {
         h = var4;
      } else {
         h = var3;
      }

      if(!atlas.rotate) {
         var3 = var4;
      }

      TextureAtlas.AtlasRegion atlasRegion = new TextureAtlas.AtlasRegion(atlas.getTexture(), regionX + var1, regionY - w + var2, h, var3);
      atlasRegion.rotate = atlas.rotate;
      atlasRegion.flip(false, true);
      return atlasRegion;
   }

   public static TextureRegion createRegionFromTextureRegion(TextureRegion var0, int var1, int var2, int var3, int var4) {
      int var5 = var0.getRegionX();
      int var6 = var0.getRegionY();
      int var7 = var0.getRegionHeight();
      var0 = new TextureRegion(var0.getTexture(), var5 + var1, var6 - var7 + var2, var3, var4);
      var0.flip(false, true);
      return var0;
   }

   public static String getActionPath(String name) {
      return FILE_PATH_ACTION + name;
   }

   public static String getAnimationPath(String name) {
      return FILE_PATH_ANIMATION + name;
   }


   public static String getFontPath(String name) {
      return FILE_PATH_FONT + name;
   }

   public static String getI18NPath(String name) {
      return FILE_PATH_I18N + name;
   }

   public static String getSpinePath(String name) {
      return FILE_PATH_SPINE + name;
   }

   public static String getParticlePath(String name) {
      return FILE_PATH_PARTICLE + name;//GStrRes.getResName(var0);
   }

   public static String getShaderPath(String name) {
      return FILE_PATH_SHADER + name;
   }

   public static String getScriptPath(String name) {
      return FILE_PATH_SCRIPT + name;
   }


   public static String getSoundPath(String name) {
      return FILE_PATH_SOUND + name;
   }

   public static String getStringPath(String name) {
      return FILE_PATH_STRING + name;
   }

   public static String getTextureAtlasPath(String name) {
      return FILE_PATH_TEXTURE_ATLAS + name + ".atlas";//GStrRes.getResName(var0);
   }

   public static String getSkeletonDataPath(String name) {
      return FILE_PATH_SPINE + name + ".skel";
   }

   public static String getTmxPath(String name) {
      return FILE_PATH_TMX + name + ".tmx";
   }

   public static String getSkeletonDataAtlasPath(String name) {
      return FILE_PATH_SPINE + name  + ".atlas";
   }

   public static String getTexturePath(String name) {
      return FILE_PATH_TEXTURE + name;
   }

   public static String getLevelPath(String name) {
      return FILE_PATH_LEVEL + name + ".json";
   }

   public static void initTextureAtlas(TextureAtlas textureAtlas) {
      Iterator iterator = textureAtlas.getTextures().iterator();

      while(iterator.hasNext()) {
         setTextureFilter((Texture)iterator.next());
      }

   }

   public static BitmapFont loadDefaultFont() {
      if(defaultFont == null) {
         defaultFont = new BitmapFont(true);
         setTextureFilter(defaultFont.getRegion().getTexture());
      }

      return defaultFont;
   }

   public static BitmapFont loadFont(String name) {
      BitmapFont var1 = new BitmapFont(openFileHandle(getFontPath(name)), true);
      setTextureFilter(var1.getRegion().getTexture());
      return var1;
   }

   public static Pixmap loadPixmap(String name) {
      return new Pixmap(openFileHandle(getTexturePath(name)));
   }

   public static Music loadSBGMusic(String name) {
      return Gdx.audio.newMusic(openFileHandle(getSoundPath(name)));
   }

   public static Sound loadSoundEffect(String name) {
      return Gdx.audio.newSound(openFileHandle(getSoundPath(name)));
   }

   public static Texture loadTexture(String name) {
      Texture texture = new Texture(openFileHandle(getTexturePath(name)));
      setTextureFilter(texture);
      return texture;
   }

   public static TextureAtlas loadTextureAtlas(String name) {
      TextureAtlas textureAtlas = new TextureAtlas(openFileHandle(getTextureAtlasPath(name)), true);
      initTextureAtlas(textureAtlas);
      return textureAtlas;
   }

   public static TextureRegion loadTextureRegion(String name) {
      TextureRegion textureRegion = new TextureRegion(loadTexture(name));
      textureRegion.flip(false, true);
      setTextureFilter(textureRegion.getTexture());
      return textureRegion;
   }

   public static TextureRegion loadTextureRegion(String name,  int x, int y, int width, int height) {
      TextureRegion textureRegion = new TextureRegion(loadTexture(name), x, y, width, height);
      textureRegion.flip(false, true);
      setTextureFilter(textureRegion.getTexture());
      return textureRegion;
   }

   public static FileHandle openFileHandle(String name) {
      return Gdx.files.internal(name);
   }

   public static DataInputStream openInputStream(String name) {
      return new DataInputStream(openFileHandle(name).read());
   }

   public static String readTextFile(FileHandle filename) {
	   StringBuffer stringBuffer = new StringBuffer();
	   BufferedReader bufferedReader;
		try
		{
		    bufferedReader = new BufferedReader(filename.reader("UTF-8") );
		}catch(Exception e)
		{
			return "";
		}
		
		try
		{
		    while(true)
		    {
		        String v3 = bufferedReader.readLine();
		        if(v3==null)
		        {
		        	StreamUtils.closeQuietly(bufferedReader);
		            return stringBuffer.toString();
		        }
		
		        stringBuffer.append(v3);
		    }
		}
		catch(Exception e)
		{
			StreamUtils.closeQuietly(bufferedReader);
		    return stringBuffer.toString();
		}
   }

   public static String readTextFile(String name) {
      return readTextFile(openFileHandle(name));
   }

   public static void setTextureFilter(Texture texture) {
      texture.setFilter(minFilter, magFilter);
   }

   public static TextureRegion[][] textureSplit(Texture texture, int tileWidth, int tileHeight) {
      setTextureFilter(texture);
      TextureRegion[][] textureRegions = TextureRegion.split(texture, tileWidth, tileHeight);

      for(int i = 0; i < textureRegions.length; ++i) {
         for(int j = 0; j < textureRegions[i].length; ++j) {
            textureRegions[i][j].flip(false, true);
         }
      }
      return textureRegions;
   }
}
