package com.core.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetLoaderParameters;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.AsynchronousAssetLoader;
import com.badlogic.gdx.assets.loaders.BitmapFontLoader;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.ParticleEffectLoader;
import com.badlogic.gdx.assets.loaders.TextureAtlasLoader;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.ParticleEffect;
import com.badlogic.gdx.graphics.g2d.ParticleEmitter;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.I18NBundle;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.utils.SkeletonDataLoader;
import com.game.gamemodel.LevelModel;
//import com.core.utils.loaders.ShaderLoader;
import java.io.InputStream;
import java.util.Iterator;



public class GAssetsManager {
   private AssetManager assetManager;
   private boolean isFinished;
   public Array<String> tempResLog;

   public GAssetsManager() {
      assetManager = new AssetManager();
      assetManager.setLoader(Object.class, new GDataLoader(new InternalFileHandleResolver()));
      assetManager.setLoader(ParticleEffect.class, new ParticleEffectLoader(new InternalFileHandleResolver()));
      assetManager.setLoader(TiledMap.class, new TmxMapLoader(new InternalFileHandleResolver()));
      assetManager.setLoader(SkeletonData.class, new SkeletonDataLoader(new InternalFileHandleResolver()));
      assetManager.setLoader(LevelModel.class, new TMLevelLoader(new InternalFileHandleResolver()));
//      assetManager.setLoader(ShaderProgram.class, new ShaderLoader(new InternalFileHandleResolver()));

      if(Gdx.app.getType() != Application.ApplicationType.WebGL){
         assetManager.setLoader(Texture.class, new EncryptTextureLoader(new InternalFileHandleResolver()));
      }

      tempResLog = new Array<>();

   }

   public void addToLog(String log) {
      //Debug.Log(var0);
      tempResLog.add(log);

   }

   String asset_log="";
   private void saveAssetLog() {
      FileHandle asset_file = Gdx.files.local("assetData/assetlog.txt");
      asset_file.writeString(asset_log,false);
   }

   public void clear() {
      assetManager.clear();
      isFinished = false;
   }

   public void clearTempResLog() {
      tempResLog.clear();
   }

   public boolean containsAsset(Object var0) {
      return assetManager.containsAsset(var0);
   }

   public void finishLoading() {
      assetManager.finishLoading();
      isFinished = true;
   }

   public String getAssetKey(Object obj) {
      return assetManager.getAssetFileName(obj);
   }

   public AssetManager getAssetManager() {
      return assetManager;
   }

   public Array<String> getAssetNameList() {
      return assetManager.getAssetNames();
   }

   public TextureRegion getTextureRegion(String atlas, String region) {
      TextureAtlas textureAtlas = getTextureAtlas(atlas);


      if(textureAtlas == null) {
         return null;
      } else {
         TextureRegion rg = textureAtlas.findRegion(region);
         if (rg != null)
            rg.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
         return rg;
      }
   }

   public TextureAtlas.AtlasRegion getAtlasRegion(String atlas, String region) {
      TextureAtlas textureAtlas = getTextureAtlas(atlas);


      if(textureAtlas == null) {
         return null;
      } else {
         TextureAtlas.AtlasRegion rg = textureAtlas.findRegion(region);
         if (rg != null)
            rg.getTexture().setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
         return rg;
      }
   }

   public BitmapFont getBitmapFont(String name) {
      String path = GRes.getFontPath(name);
      BitmapFont bitmapFont = (BitmapFont)getRes(path, BitmapFont.class);
      BitmapFont bitmapFontFinal = bitmapFont;
      if(bitmapFont == null) {
         loadBitmapFont(name);
         finishLoading();
         BitmapFont var4 = (BitmapFont)getRes(path, BitmapFont.class);
         if(var4 != null) {
            addToLog(path + "---------" + "BitmapFont.class");
            var4.getData().markupEnabled = true;
            return var4;
         }

         bitmapFontFinal = null;
      }else {

         bitmapFontFinal.getData().markupEnabled = true;

      }

      return bitmapFontFinal;
   }

   public I18NBundle getI18NBundle(String name) {
      String path = GRes.getI18NPath(name);
      I18NBundle i18n = (I18NBundle)getRes(path, I18NBundle.class);
      I18NBundle i18nFinal = i18n;
      if(i18n == null) {
         loadI18N(name);
         finishLoading();
         I18NBundle var4 = (I18NBundle)getRes(path, I18NBundle.class);
         if(var4 != null) {
            addToLog(path + "---------" + "I18NBundle.class");
            return var4;
         }

         i18nFinal = null;
      }

      return i18nFinal;
   }

   public Object getGameData(String name, GDataAssetLoad assetLoad) {
      Object result = getRes(name, Object.class);
      Object resultFinal = result;
      if(result == null) {
         if(assetLoad != null) {
            loadGameData(name, assetLoad);
            finishLoading();
            Object result3 = getRes(name, Object.class);
            if(result3 == null) {
               return null;
            }
            return result3;
         }
         resultFinal = null;
      }
      return resultFinal;
   }

   public Music getMusic(String name) {
      String path = GRes.getSoundPath(name);
      Music music = (Music)getRes(path, Music.class);
      Music musicFinal = music;
      if(music == null) {
         loadMusic(name);
         finishLoading();
         Music var4 = (Music)getRes(path, Music.class);
         if(var4 != null) {
            addToLog(path + "---------" + "Music.class");
            return var4;
         }
         musicFinal = null;
      }

      return musicFinal;
   }

   public ParticleEffect getParticleEffect(String name) {
      String path = GRes.getParticlePath(name);
      ParticleEffect effect = (ParticleEffect)getRes(path, ParticleEffect.class);
      ParticleEffect effectFinal = effect;
      if(effect == null) {
         loadParticleEffect(name);
         finishLoading();
         ParticleEffect effect2 = (ParticleEffect)getRes(path, ParticleEffect.class);
         if(effect2 != null) {
            addToLog(path + "---------" + "NParticleEffect.class");
            initParticle(effect2);
            return effect2;
         }

         effectFinal = null;
      }

      return effectFinal;
   }


   public ShaderProgram getShaderProgram(String name) {
      String path = GRes.getShaderPath(name);
      ShaderProgram shader = (ShaderProgram)getRes(path, ShaderProgram.class);
      ShaderProgram shaderFinal = shader;
      if(shader == null) {
         loadShader(name);
         finishLoading();
         ShaderProgram shader2 = (ShaderProgram)getRes(path, ShaderProgram.class);
         if(shader2 != null) {
            addToLog(path + "---------" + "ShaderProgram.class");
            return shader2;
         }

         shaderFinal = null;
      }

      return shaderFinal;
   }

   public Pixmap getPixmap(String name) {
      String path = GRes.getTexturePath(name);
      Pixmap pixmap = (Pixmap)getRes(path, Pixmap.class);
      Pixmap pixmapFinal = pixmap;
      if(pixmap == null) {
         loadPixmap(name);
         finishLoading();
         Pixmap pixmap2 = (Pixmap)getRes(path, Pixmap.class);
         if(pixmap2 != null) {
            addToLog(path + "---------" + "Pixmap.class");
            return pixmap2;
         }
         pixmapFinal = null;
      }

      return pixmapFinal;
   }


   public float getProgress() {
      return 100.0F * assetManager.getProgress();
   }

   public int getReferenceCount(Object obj) {
      return assetManager.getReferenceCount(getAssetKey(obj));
   }

   public int getReferenceCount(String fileName) {
      return assetManager.getReferenceCount(fileName);
   }


   public static void setResOutput(String name){
      resOutput = name;
      resList.clear();
   }
   public static Array<String> resList = new Array<>();
   public static String resOutput = "output.txt";
   public static final boolean LOGRES = true;
   public void checkAddResName(String fileName){
      if(Gdx.app.getType() == Application.ApplicationType.WebGL || !LOGRES) return;

      if(!resList.contains(fileName, false)){
        // Logger.log("new Res " + fileName);

         resList.add(fileName);
         exportResFile();
         if(!fileName.contains("i18n") && !fileName.contains(".f3123nt")) {
            Array<String> list = assetManager.getDependencies(fileName);
            if(list == null){
               this.finishLoading();
               list = assetManager.getDependencies(fileName);
            }
            if(list!=null) {

               for (int i = 0; i < list.size; i++) {

               String name = list.get(i);

                  checkAddResName(name);

                  if(fileName.contains(".tmx")){
                     String newTsxFile = name.replace(".png", ".tsx");
                     checkAddResName(newTsxFile);
                  }
               }
            }
         }

      }
   }

   public void exportResFile(){
      FileHandle file = Gdx.files.local(resOutput);
      file.writeString("", false);
      for (String line:resList
           ) {
         file.writeString(line + "\n", true);
      }

   }

//   public void clearRes(){
//      resList.clear();
//      //assetManager.clear();
//   }

   public Object getRes(String fileName) {
      checkAddResName(fileName);
      return assetManager.get(fileName);
   }

   public Object getRes(String fileName, Class type) {
      checkAddResName(fileName);
      return !assetManager.isLoaded(fileName, type)?null:assetManager.get(fileName, type);
   }

   public Sound getSound(String name) {
      String path = GRes.getSoundPath(name);
      Sound sound = (Sound)getRes(path, Sound.class);
      Sound soundFinal = sound;
      if(sound == null) {
         loadSound(name);
         finishLoading();
         Sound sound2 = (Sound)getRes(path, Sound.class);
         if(sound2 != null) {
            addToLog(path + "---------" + "Sound.class");
            return sound2;
         }
         soundFinal = null;
      }

      return soundFinal;
   }

   public Array<String> getTempResLoadLog() {
      return tempResLog;
   }

   public TextureAtlas getTextureAtlas(String name) {
      String path = GRes.getTextureAtlasPath(name);

      TextureAtlas textureAtlas = (TextureAtlas)getRes(path, TextureAtlas.class);
      TextureAtlas result = textureAtlas;
      if(textureAtlas == null) {
         loadTextureAtlas(name);
         finishLoading();

         TextureAtlas textureAtlas2 = (TextureAtlas)getRes(path, TextureAtlas.class);
         if(textureAtlas2 != null) {
            Iterator<Texture> var5 = textureAtlas2.getTextures().iterator();
            while(var5.hasNext()) {
               ((Texture)var5.next()).setFilter(GRes.minFilter, GRes.magFilter);
            }

            addToLog(path + "---------" + "TextureAtlas.class");
            return textureAtlas2;
         }
         result = null;
      }

      return result;
   }

   public TiledMap getTmxMap(String name) {
      String path =  GRes.getTmxPath(name);
      TiledMap tileMap = (TiledMap)getRes(path, TiledMap.class);
      TiledMap result = tileMap;
      if(tileMap == null) {
         loadTmxMap(name);
         finishLoading();
         TiledMap tileMap2 = (TiledMap)getRes(path, TiledMap.class);
         if(tileMap2 != null) {
            addToLog(path + "---------" + "TiledMap.class");
            return tileMap2;
         }
         result = null;
      }

      return result;
   }

   public Texture getTexture(String name) {
      String path = GRes.getTexturePath(name);
      Texture texture = (Texture)getRes(path, Texture.class);
      Texture textureFinal = texture;
      if(texture == null) {
         loadTexture(name);
         finishLoading();
         textureFinal = (Texture)getRes(path, Texture.class);
         if(textureFinal == null) {
            //GMain.platform.log("无法加载纹理(getTexture) : " + var3);
            return null;
         }

         addToLog(path + "---------" + "Texture.class");
      }
      return textureFinal;
   }
   public TextureAtlas getTextureAtlasWithPath(String name) {
      String path = name;
      TextureAtlas atlas = (TextureAtlas)getRes(path, TextureAtlas.class);
      TextureAtlas atlasFinal = atlas;
      if(atlas == null) {
         load(name, TextureAtlas.class, new TextureAtlasLoader.TextureAtlasParameter(false));
         finishLoading();
         TextureAtlas atlas2 = (TextureAtlas)getRes(path, TextureAtlas.class);
         if(atlas2 != null) {
            Iterator<Texture> it = atlas2.getTextures().iterator();

            while(it.hasNext()) {
               ((Texture)it.next()).setFilter(GRes.minFilter, GRes.magFilter);
            }

            addToLog(path + "---------" + "TextureAtlas.class");
            return atlas2;
         }
         atlasFinal = null;
      }

      return atlasFinal;
   }

   public TextureRegion getTextureRegion(String name) {
      String path = GRes.getTexturePath(name);
      Texture texture = (Texture)getRes(path, Texture.class);
      Texture textureFinal = texture;
      if(texture == null) {
         loadTexture(name);
         finishLoading();
         textureFinal = (Texture)getRes(path, Texture.class);
         if(textureFinal == null) {
            //GMain.platform.log("无法加载纹理(getTexture) : " + var3);
            return null;
         }

         addToLog(path + "---------" + "Texture.class");
      }


      TextureRegion var4 = new TextureRegion(textureFinal);
      //var4.flip(false, true);
      return var4;
   }

   public LevelModel getLevelData(String name) {
      String path = GRes.getLevelPath(name);
      LevelModel level = (LevelModel)getRes(path, LevelModel.class);
      LevelModel levelFinal = level;
      if(level == null) {
         loadLevelData(name);
         finishLoading();
         levelFinal = (LevelModel)getRes(path, LevelModel.class);
         if(levelFinal == null) {
            //GMain.platform.log("无法加载纹理(getTexture) : " + var3);
            return null;
         }

         addToLog(path + "---------" + "LevelModel.class");
      }

      //var4.flip(false, true);
      return levelFinal;
   }

   public void initParticle(ParticleEffect particleEff) {
      Iterator<ParticleEmitter> iterator = particleEff.getEmitters().iterator();

      /*while(var2.hasNext()) {
         Sprite var1 = ((ParticleEmitter)var2.next()).getSprite();
         GRes.setTextureFilter(var1.getTexture());
         if(!var1.isFlipY()) {
            var1.flip(false, true);
         }
      }*/ //xxxxxxx



      while(iterator.hasNext()) {
         Sprite var1 = ((ParticleEmitter)iterator.next()).getSprites().get(0);  //xxx
         GRes.setTextureFilter(var1.getTexture());
         if(!var1.isFlipY()) {
           // var1.flip(false, true);
         }
      }

   }

   public boolean isFinished() {
      return isFinished;
   }

   public boolean isLoaded(String filename) {
      return assetManager.isLoaded(filename);
   }

   private void load(String fileName, Class type, AssetLoaderParameters parameter) {
       assetManager.load(fileName, type, parameter);
   }

   public String loadBitmapFont(String name) {
      name = GRes.getFontPath(name);
      BitmapFontLoader.BitmapFontParameter param = new BitmapFontLoader.BitmapFontParameter();
      param.flip = false;
      param.minFilter = GRes.minFilter;
      param.magFilter = GRes.magFilter;
      load(name, BitmapFont.class, param);
      return name;
   }

      public String loadI18N(String name){
      name = GRes.getI18NPath(name);
      load(name, I18NBundle.class, null);
      return name;
   }

   public String loadGameData(String name, GDataAssetLoad dataAssetLoad) {
      GameDataParameter param = new GameDataParameter((GameDataParameter)null);
      param.dataLoad = dataAssetLoad;
      load(name, Object.class, param);
      return name;
   }

   public String loadTmxMap(String name){
      String path = GRes.getTmxPath(name);
      load(path, TiledMap.class, new TmxMapLoader.Parameters());
      checkAddResName(path);
      return path;
   }

   public String loadMusic(String name) {
      if (GSound.isHowler) {
         GSound.initMusic(name);
         return name;
      }
      name = GRes.getSoundPath(name);
      load(name, Music.class, null);
      checkAddResName(name);
      return name;
   }

	public String loadParticleEffect(String name) {
      name = GRes.getParticlePath(name);
      load(name, ParticleEffect.class, null);
      return name;
   }

	public String loadShader(String name) {
      name = GRes.getShaderPath(name);
      load(name, ShaderProgram.class, null);
      return name;
   }

   public String loadParticleEffectAsImageDir(String name, FileHandle file) {
      name = GRes.getParticlePath(name);
      ParticleEffectLoader.ParticleEffectParameter param = new ParticleEffectLoader.ParticleEffectParameter();
      param.imagesDir = file;
      load(name, ParticleEffect.class, param);
      return name;
   }

   public String loadParticleEffectAsTextureAtlas(String name) {
      name = GRes.getParticlePath(name);
      ParticleEffectLoader.ParticleEffectParameter param = new ParticleEffectLoader.ParticleEffectParameter();
      param.atlasFile = name + "ack";
      load(name, ParticleEffect.class, param);
      return name;
   }

   public String loadParticleEffectAsTextureAtlas(String name, String atlasName) {
      name = GRes.getParticlePath(name);
      ParticleEffectLoader.ParticleEffectParameter param = new ParticleEffectLoader.ParticleEffectParameter();
      param.atlasFile = GRes.getTextureAtlasPath(atlasName);
      load(name, ParticleEffect.class, param);
      return name;
   }

   public String loadPixmap(String name) {
      name = GRes.getTexturePath(name);
      load(name, Pixmap.class, null);
      return name;
   }

   public String loadSound(String soundName) {
      if (GSound.isHowler) {
         GSound.initSound(soundName);
         return soundName;
      }
      soundName = GRes.getSoundPath(soundName);
      load(soundName, Sound.class, null);

      checkAddResName(soundName);
      return soundName;
   }

   public String loadTexture(String name) {
      name = GRes.getTexturePath(name);

     // DecodeTexture(name);
      //var0 = "dec/" + var0;
      TextureLoader.TextureParameter var1 = new TextureLoader.TextureParameter();
      var1.minFilter = GRes.minFilter;
      var1.magFilter = GRes.magFilter;
      load(name, Texture.class, var1);
      return name;
   }

   public String loadTextureAtlas(String name) {
      name = GRes.getTextureAtlasPath(name);


      load(name, TextureAtlas.class, new TextureAtlasLoader.TextureAtlasParameter(false));
      checkAddResName(name);
      return name;
   }

   public String loadLevelData(String name) {
      name = GRes.getLevelPath(name);
      load(name, LevelModel.class, null);
      checkAddResName(name);
      return name;
   }

   public void paintAssetReferenceList() {
      Iterator<String> iterator = assetManager.getAssetNames().iterator();

      while(iterator.hasNext()) {
         String name = (String)iterator.next();
         System.err.println(name + " ____ : " + getReferenceCount(name));
      }

   }

   public void paintAssetReferenceList(String matchStr) {
      Iterator<String> iterator = assetManager.getAssetNames().iterator();

      while(iterator.hasNext()) {
         String name = (String)iterator.next();
         if(name.toLowerCase().contains(matchStr.toLowerCase())) {
            System.err.println(name + " ____ : " + getReferenceCount(name));
         }
      }

   }

   public void unload(Object obj) {
      String name = getAssetKey(obj);
      if(name != null) {
         unload(name);
      }
   }

   public void unload(String name) {
      assetManager.unload(name);
   }

   public void unloadFont(String name) {
      unload(GRes.getFontPath(name));
   }

   public void unloadParticleEffect(String name) {
      unload(GRes.getParticlePath(name));
   }

   public void unloadPixmap(String name) {
      unload(GRes.getTexturePath(name));
   }

   public void unloadSound(String name) {
      unload(GRes.getSoundPath(name));
   }

   public void unloadTexture(String name) {
      unload(GRes.getTexturePath(name));
   }

   public void unloadTextureAtlas(String name) {
      unload(GRes.getTextureAtlasPath(name));
   }

   public void update() {
      isFinished = assetManager.update();
   }

   public interface GDataAssetLoad {
      Object load(String name, FileHandle file);
   }

   public class GDataLoader extends AsynchronousAssetLoader<Object, GameDataParameter> {
      private Object dat;

      public GDataLoader(FileHandleResolver fileHandleResolver) {
         super(fileHandleResolver);
      }

      public Array getDependencies(String fileName, FileHandle file, GameDataParameter parameter) {
         return null;
      }

      public void loadAsync(AssetManager manager, String fileName, FileHandle file, GameDataParameter parameter) {
         this.dat = parameter.dataLoad.load(fileName, file);
      }

      public Object loadSync(AssetManager manager, String fileName, FileHandle file, GameDataParameter parameter) {
         return this.dat;
      }
   }

   private class GameDataParameter extends AssetLoaderParameters<Object> {
      GDataAssetLoad dataLoad;

      private GameDataParameter() {
      }

      GameDataParameter(GameDataParameter gameDataParameter) {

      }
   }


   public void DecodeTexture(String fname) {
      try {

        /* Gdx.app.log("DECODE", fname);

         FileHandle fileHandle = new FileHandle( fname);

         int[] arrn = new int[5];
         arrn[0] = 2;
         arrn[2] = 4;
         arrn[3] = 1;
         arrn[4] = 3;
        // this.readOrder = arrn;
         byte[] arrby = new byte[4];


         InputStream inputStream = fileHandle.read();
         inputStream.read(arrby);
         byte[] arrby2 = new String(arrby).equals(".tex") ? readBytes((InputStream) inputStream, (int[]) arrn) : fileHandle.readBytes();
         Pixmap pixmap = new Pixmap(new Gdx2DPixmap(arrby2, 0, arrby2.length, 0));
         StreamUtils.closeQuietly((Closeable) inputStream);


         FileHandle fh;

         fh = new FileHandle(fname);



         PixmapIO.writePNG(fh, pixmap);*/
      }
      catch(Exception e ){

      }
   }


   private byte[] readBytes(InputStream inputStream, int[] arrn) throws Exception {
      int n = inputStream.available();
      byte[] arrby = new byte[n];
      int n2 = n / arrn.length;
      int n3 = 0;
      do {
         if (n3 >= arrn.length) {
            int n4 = n % arrn.length;
            if (n4 <= 0) return arrby;
            inputStream.read(arrby, n2 * arrn.length, n4);
            return arrby;
         }
         inputStream.read(arrby, n2 * arrn[n3], n2);
         ++n3;
      } while (true);
   }

   public SkeletonData getSkeletonData(String name) {
      String path = GRes.getSkeletonDataPath(name);
      SkeletonData skeData = (SkeletonData) getRes(path, SkeletonData.class);
      SkeletonData result = skeData;
      if (skeData == null) {
         loadSkeletonData(name);
         finishLoading();
         SkeletonData skeData2 = (SkeletonData) getRes(path, SkeletonData.class);
         if (skeData2 != null) {
            addToLog(path + "---------" + "SkeletonData.class");
            return skeData2;
         }
         result = null;
      }

      return result;
   }

   public String loadSkeletonData(String name) {
      String path = GRes.getSkeletonDataPath(name);

      String atlasPath = GRes.getSkeletonDataAtlasPath(name);

      load(path, SkeletonData.class, new SkeletonDataLoader.SkeletonDataParameter(atlasPath));
      checkAddResName(path);
      checkAddResName(atlasPath);
      return path;
   }

}


