package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;

public class GShapeTools {
   private static ShapeRenderer sRender = new ShapeRenderer(1000);

   private static void begin(Batch batch, ShapeRenderer.ShapeType shapeType, Color color) {
      batch.end();
      Gdx.gl.glEnable(3042);
      Gdx.gl.glBlendFunc(770, 771);
      sRender.setProjectionMatrix(batch.getProjectionMatrix());
      sRender.setTransformMatrix(batch.getTransformMatrix());
      sRender.setColor(color);
      sRender.begin(shapeType);
   }

   public static void drawARC(Batch batch, Color color, float x, float y, float radius, float start, float degrees, boolean isFilled) {
      ShapeRenderer.ShapeType type;
      if(isFilled) {
         type = ShapeRenderer.ShapeType.Filled;
      } else {
         type = ShapeRenderer.ShapeType.Line;
      }

      begin(batch, type, color);
      sRender.arc(x, y, radius, start, degrees);
      end(batch);
   }

   public static void drawBox(Batch batch, Color color, float x, float y, float z, float width, float height, float depth) {
      begin(batch, ShapeRenderer.ShapeType.Line, color);
      sRender.box(x, y, z, width, height, depth);
      end(batch);
   }

   public static void drawCircle(Batch batch, Color color, float x, float y, float radius, boolean isFilled) {
      ShapeRenderer.ShapeType shapeType;
      if(isFilled) {
         shapeType = ShapeRenderer.ShapeType.Filled;
      } else {
         shapeType = ShapeRenderer.ShapeType.Line;
      }

      begin(batch, shapeType, color);
      sRender.circle(x, y, radius);
      end(batch);
   }

   public static void drawCurve(Batch batch, Color color, float x1, float y1, float cx1, float cy1, float cx2, float cy2, float x2, float y2) {
      begin(batch, ShapeRenderer.ShapeType.Line, color);
      sRender.curve(x1, y1, cx1, cy1, cx2, cy2, x2, y2, 1);
      end(batch);
   }

   public static void drawEllipse(Batch batch, Color color, float x, float y, float width, float height, boolean isFilled) {
      ShapeRenderer.ShapeType var7;
      if(isFilled) {
         var7 = ShapeRenderer.ShapeType.Filled;
      } else {
         var7 = ShapeRenderer.ShapeType.Line;
      }

      begin(batch, var7, color);
      sRender.ellipse(x, y, width, height);
      end(batch);
   }

   public static void drawLine(Batch batch, Color color, float x, float y, float x2, float y2) {
      begin(batch, ShapeRenderer.ShapeType.Line, color);
      sRender.line(x, y, x2, y2);
      end(batch);
   }

   public static void drawPoint(Batch batch, Color color, float x, float y) {
      begin(batch, ShapeRenderer.ShapeType.Point, color);
      sRender.point(x, y, 0.0F);
      end(batch);
   }

   public static void drawPolygon(Batch batch, Color color, Polygon polygon) {
      begin(batch, ShapeRenderer.ShapeType.Line, color);
      sRender.polygon(polygon.getTransformedVertices());
      end(batch);
   }

   public static void drawRectangle(Batch batch, Color color, float x, float y, float originX, float originY, float width, float height, float scaleX, float scaleY, boolean isFilled) {
      ShapeRenderer.ShapeType shapeType;
      if(isFilled) {
         shapeType = ShapeRenderer.ShapeType.Filled;
      } else {
         shapeType = ShapeRenderer.ShapeType.Line;
      }

      begin(batch, shapeType, color);
      //sRender.rect(var2, var3, var4, var5, var6, var7, var8);
      sRender.rect(x, y, originX, originY, width, height, scaleX, scaleY, 0); //xxxx
      end(batch);
   }

   public static void drawRectangle(Batch batch, Color color, float x, float y, float width, float height, boolean isFilled) {
      ShapeRenderer.ShapeType shapeType;
      if(isFilled) {
         shapeType = ShapeRenderer.ShapeType.Filled;
      } else {
         shapeType = ShapeRenderer.ShapeType.Line;
      }

      begin(batch, shapeType, color);
      sRender.rect(x, y, width, height);
      end(batch);
   }

   public static void drawTriangle(Batch batch, Color color, float x1, float y1, float x2, float y2, float x3, float y3, boolean isFilled) {
      ShapeRenderer.ShapeType shapeType;
      if(isFilled) {
         shapeType = ShapeRenderer.ShapeType.Filled;
      } else {
         shapeType = ShapeRenderer.ShapeType.Line;
      }

      begin(batch, shapeType, color);
      sRender.triangle(x1, y1, x2, y2, x3, y3);
      end(batch);
   }

   public static void drawX(Batch batch, Color color, float x, float y, float size) {
      begin(batch, ShapeRenderer.ShapeType.Line, color);
      sRender.x(x, y, size);
      end(batch);
   }

   private static void end(Batch batch) {
      sRender.end();
      batch.begin();
   }

   public static ShapeRenderer getShapeRenderer() {
      return sRender;
   }
}
