package com.core.util;

public class GPoint2 {
  public int p1;
  public int p2;

  public GPoint2(int p1, int p2) {
    this.p1 = p1;
    this.p2 = p2;
  }

  public boolean isZero() {
    return p1 == 0 && p2 == 0;
  }

  @Override
  public String toString() {
    return "(" + p1 + "," + p2 + ")";
  }
}
