package com.core.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.utils.LongMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.core.utils.HowlerSound;
import com.game.GMain;


import java.util.ArrayList;
import java.util.Iterator;


public class GSound {
  private static final float SOUND_INTERVAL = 0.1F;
  private static LongMap<String> loopSound = new LongMap<>();
  public static Music music;
  static ObjectMap<String, Music> musicPool = new ObjectMap<>();
  private static boolean silence_music;
  private static boolean silence_sound = false;
  private static ObjectMap<String, Float> soundPlayList = new ObjectMap<>();
  private static ObjectMap<String, Sound> soundPool = new ObjectMap<>();
  private static float volume_music = 1f;
  private static float volume_sound = 1f;


  static boolean isHowler = false;
  static boolean isWebGl = false;
  static String curMusicName = "";
  private static ArrayList<String> soundList = new ArrayList<>();

  public static void setIsUseHowler() {
    isHowler = false;
    isWebGl = Gdx.app.getType() == Application.ApplicationType.WebGL;
    if (isWebGl)
      isHowler = true;
    else
      GMain.platform().log("can not use howler sound");
  }

  static {
    registerSoundListUpdate();
  }

  public static void clear() {
    stopMusic();
    stopSound();


    ObjectMap.Values<Music> values = musicPool.values();
    Iterator<Music> iterator = values.iterator();

    while (iterator.hasNext()) {
      Music next = (Music) iterator.next();
      GMain.getAssetManager().unload((Object) values);
    }

    Iterator<Sound> iterator1 = soundPool.values().iterator();

    while (iterator1.hasNext()) {
      GMain.getAssetManager().unload((Object) ((Sound) iterator1.next()));
    }
    musicPool.clear();
    soundPool.clear();
    loopSound.clear();
    soundPlayList.clear();

  }

  public static Sound getSound(String name) {
    if (isHowler)
      return howlerSounds.get(name, new HowlerSound(name));
    if (!soundPool.containsKey(name)) {
      initSound(name);
    }

    return soundPool.get(name);
  }

  public static void initMusic(String name) {
    stopMusic();
    if (isHowler) {
      if (!curMusicName.equals("")) {
        HowlerSound.pauseSound(curMusicName);
      }

      String md5name = "assets/" + GMain.platform().getMd5PathName("sound/" + name);
      HowlerSound.loadSound(name, md5name, false, false);

      curMusicName = name;
    } else if (!musicPool.containsKey(name)) {
      music = GMain.getAssetManager().getMusic(name);
      musicPool.put(name, music);
    }
    music = musicPool.get(name);
  }

  public static void initSound(String name) {
    if (isHowler) {
      GMain.platform().log(name);

      String md5name = "assets/" + GMain.platform().getMd5PathName("sound/" + name);
      GMain.platform().log(md5name);
      HowlerSound.loadSound("sound/" + name, md5name, false, false);
      soundList.add(name);
    } else
      soundPool.put(name, GMain.getAssetManager().getSound(name));
  }

  public static boolean isMusicPlaying() {
    if (isHowler) {
      return HowlerSound.isMusicPlaying(curMusicName);
    } else if (music != null) {
      return music.isPlaying();
    }
    return false;
  }

  public static void pause() {
    if (music != null) {
      music.pause();
    }
    pauseSound();
  }

  public static void pauseMusic() {
    if (music != null) {
      music.pause();
    }
  }

  public static void pauseSound() {
    Iterator<Sound> it = soundPool.values().iterator();

    while (it.hasNext()) {
      it.next().pause();
    }

  }

   /*public static void pauseSound(String var0) {
      getSound(var0).pause();
   }*/

  public static long playLoopSound(String name) {

    if (!silence_sound && !soundPlayList.containsKey(name)) {
      long volume = getSound(name).loop(volume_sound);
      loopSound.put(volume, name);
      soundPlayList.put(name, 0.0F);
      return volume;
    } else {
      return -1L;
    }
  }

  public static long playLoopSound(String name, float volume) {
    if (!silence_sound && !soundPlayList.containsKey(name)) {
      long var1 = getSound(name).loop(volume);
      loopSound.put(var1, name);
      soundPlayList.put(name, 0.0F);
      return var1;
    } else {
      return -1L;
    }
  }
  /* public static void playLoopSound() {

      Iterator var0 = loopSound.values().iterator();

      while(var0.hasNext()) {
         playLoopSound((String)var0.next());
      }

   }*/

  public static void playMusic(String name) {
    initMusic(name);
    playMusic();
  }

  public static void playMusic() {
    if (isHowler && !silence_music && !HowlerSound.isMusicPlaying(curMusicName)) {
      GMain.platform().log("curMusic: " + curMusicName);
      HowlerSound.setVolume(curMusicName, volume_music);
      HowlerSound.setLooping(curMusicName, true);
      HowlerSound.playMusic(curMusicName);
    } else if (music != null && !silence_music && !music.isPlaying()) {
      music.setVolume(volume_music);
      music.setLooping(true);
      music.play();
    }
  }

  public static long playSound(String name) {
    if (isHowler && !silence_sound) {
      HowlerSound.playSound(name);
    } else if (!silence_sound && !soundPlayList.containsKey(name)) {
      soundPlayList.put(name, 0.0F);
      Sound sound = getSound(name);
      if (sound != null) {
        return sound.play(volume_sound);
      }
    }
    return -1L;
  }


  public static long playSound(String name, float volume) {
    if (isHowler && !silence_sound) {
      HowlerSound.playSound(name);
    } else if (!silence_sound && !soundPlayList.containsKey(name)) {
      soundPlayList.put(name, 0.0F);
      Sound var1 = getSound(name);
      if (var1 != null) {
        return var1.play(volume);
      }
    }
    return -1L;
  }

  public static ObjectMap<String, Sound> howlerSounds = new ObjectMap<>();

  public static Sound playAndGetSound(String name) {
    if (isHowler && !silence_sound) {
      HowlerSound.playSound(name);
      return howlerSounds.get(name, new HowlerSound(name));
    } else if (!silence_sound && !soundPlayList.containsKey(name)) {
      soundPlayList.put(name, 0.0F);
      Sound sound = getSound(name);
      if (sound != null) {
        sound.play(volume_sound);
        return sound;
      }
    }
    return null;
  }

  private static void registerSoundListUpdate() {
    GStage.registerUpdateService("soundUpdate", delta -> {
      Iterator<String> it = GSound.soundPlayList.keys().iterator();

      while (it.hasNext()) {
        String soundName = it.next();
        if (soundName != null) {
          float time = (Float) GSound.soundPlayList.get(soundName) + delta;
          if (time >= SOUND_INTERVAL) {
            GSound.soundPlayList.remove(soundName);
          } else {
            GSound.soundPlayList.put(soundName, time);
          }
        }
      }
      return false;
    });
  }

  public static void resume() {
    playMusic();
    resumeSound();
  }

  public static void resumeSound() {
    Iterator<Sound> it = soundPool.values().iterator();

    while (it.hasNext()) {
      it.next().resume();
    }
  }

  public static void resumeSound(String name) {
    getSound(name).resume();
  }

  private static void setLoopSoundVolume(float volume) {
    Iterator<String> it = loopSound.values().iterator();

    while (it.hasNext()) {
      String name = it.next();
      ((Sound) GMain.getAssetManager().getRes(name)).setVolume(loopSound.findKey(name, true, -1L), volume);
    }
  }

  public static void setMusicSilence(boolean silence) {
    silence_music = silence;
    if (isHowler) {
      if (!curMusicName.equals("")) {
        if (silence_music)
          HowlerSound.pauseSound(curMusicName);
        else
          playMusic();
      }
    } else if (music != null) {
      if (silence_music) {
        music.pause();
      } else {
        music.setVolume(volume_music);
        music.setLooping(true);
        music.play();
      }
    }
  }

  /*public static float setMusicVolume(float var0) {
     volume_music = Math.max(0.0F, var0);
     volume_music = Math.min(volume_music, 1.0F);
     if(music != null) {
        music.setVolume(volume_music);
     }

     return volume_music;
  }
*/
  public static void setSilence(boolean isSilence) {
    setMusicSilence(isSilence);
    setSoundSilence(isSilence);
  }

  public static void setSoundSilence(boolean isSilence) {
    silence_sound = isSilence;
    if (silence_music) {
      stopSound();
    }
  }

  public static float setSoundVolume(float volume) {
    volume_sound = Math.max(0.0F, volume);
    volume_sound = Math.min(volume_sound, 1.0F);
    setLoopSoundVolume(volume_sound);
    return volume_music;
  }

  public static void stopMusic() {
    if (isHowler) {
      if (!curMusicName.equals("")) {
        HowlerSound.pauseSound(curMusicName);
      }
    } else if (music != null) {
      music.stop();
    }
  }

  public static void stopSound() {
    Iterator<String> iterator = soundPool.keys().iterator();
    while (iterator.hasNext()) {
      stopSound(iterator.next());
    }
  }

  public static void stopSound(String name) {
    if (isHowler) {
      HowlerSound.stopSound(name);
      return;
    }
    getSound(name).stop();
    soundPlayList.remove(name);
    loopSound.remove(loopSound.findKey(name, true, -1L));
  }

  public static void stopSound(String name, long id) {
    getSound(name).stop(id);
    soundPlayList.remove(name);
    loopSound.remove(id);
  }

  /* public static void unloadMusic(String var0) {
      Music var1 = (Music)musicPool.get(var0);
      if(var1.isPlaying()) {
         var1.stop();
      }

      musicPool.remove(var0);
      GMain.getAssetManager().unload((Object)var1);
   }

   public static void unloadSound(String var0) {
      stopSound(var0);
      GMain.getAssetManager().unload(var0);
      soundPool.remove(var0);
      soundPlayList.remove(var0);
      loopSound.remove(loopSound.findKey(var0, true, -1L));
   }

   public float getMusicVolume() {
      return volume_music;
   }

   public float getSoundVolume() {
      return volume_sound;
   }*/
}
