package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import java.util.Iterator;

public class GPool<T> extends Pool<T> {
    private static final ObjectMap<Class, GPool> typePools = new ObjectMap();
    private boolean autoFree;
    private int freeMin;
    Array objectList;
    private final Class<T> type;



    private GPool(Class cls, int freeMin, boolean autoFree) {
        super(freeMin, freeMin);
        this.autoFree = true;
        this.freeMin = 0;
        this.objectList = new Array();
        this.freeMin = freeMin;
        this.type = cls;
        this.autoFree = autoFree;
        typePools.put(cls, this);

        for(int i = 0; i < freeMin / 2; ++i) {
            this.free(this.newObject());
        }

    }

    private void addToList(Object obj) {
        if(this.autoFree) {
            this.objectList.add(obj);
        }

    }

    public static void freeAll() {
        Iterator it = typePools.values().iterator();

        while(it.hasNext()) {
            GPool pool = (GPool)it.next();
            Array arr = pool.objectList;

            for(int i = arr.size - 1; i >= 0; --i) {
                pool.free(arr.get(i));
            }
        }

    }

    public static <T> GPool<T> get(Class cls, int freeMin) {
        return get(cls, freeMin, false);
    }

    public static <T> GPool<T> get(Class cls, int freeMin, boolean autoFree) {
        GPool var4 = (GPool)typePools.get(cls);
        GPool var3 = var4;
        if(var4 == null) {
            System.err.println("Get GPool Is Null _______  " + cls.getName());
            var3 = new GPool(cls, freeMin, autoFree);
            typePools.put(cls, var3);
        }

        return var3;
    }

    public static void saveAllFreeMin() {
        FileHandle var0 = Gdx.files.local("GPoolInfo.txt");
        ObjectMap.Values var1 = typePools.values();
        var0.writeString("\r\n_________GPool__________\r\n\r\n", false);
        Iterator var3 = var1.iterator();

        while(var3.hasNext()) {
            GPool var2 = (GPool)var3.next();
            var0.writeString(((Class)typePools.findKey(var2, true)).getName() + "  ___  " + var2.freeMin + " / " + var2.max + "\r\n", true);
        }

    }

    public void free(T obj) {
        if(obj instanceof Actor) {
            Actor var2 = (Actor)obj;
            var2.clearActions();
            var2.clearListeners();
        }

        super.free(obj);
        this.objectList.removeValue(obj, true);
    }

    protected T newObject() {
        Object var7;
        try {
            var7 = ClassReflection.newInstance(type);
            //var7 = this.type.newInstance();
            this.addToList(var7);
            return (T)var7;

        /* var7 = this.type.newInstance();
         this.addToList(var7);
         return (T)var7;*/
        } catch (Exception var6) {
         /*Constructor var1;
         try {
            var1 = this.type.getConstructor((Class[])null);
         } catch (Exception var5) {
            try {
               var1 = this.type.getDeclaredConstructor((Class[])null);
               var1.setAccessible(true);
            } catch (NoSuchMethodException var4) {
               throw new RuntimeException("Class cannot be created (missing no-arg constructor): " + this.type.getName());
            }
         }

         try {
            var7 = var1.newInstance(new Object[0]);
            this.addToList(var7);
            return (T)var7;
         } catch (Exception var3) {
            throw new GdxRuntimeException("Unable to create new instance: " + this.type.getName(), var6);
         }*/
        }
        return null;
    }

    public T obtain() {
        int totalFree = this.getFree();
        if(totalFree == 0) {
            System.err.println("obtain  _______  " + this.type.getName());
        }

        this.freeMin = Math.min(totalFree, this.freeMin);
        Object obj = super.obtain();
        this.addToList(obj);
        return (T)obj;
    }
}
