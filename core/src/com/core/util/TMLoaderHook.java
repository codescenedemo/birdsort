package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.resolvers.InternalFileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.game.gamemodel.LevelModel;

@SuppressWarnings("unused")
public class TMLoaderHook implements LoaderHook<LevelModel> {
  private final String          levelPath       = "level/";
  private       AssetManager    am;
  @Override
  public void load(AssetManager am) {
    am.setLoader(LevelModel.class, new TMLevelLoader(new InternalFileHandleResolver()));

    Array<String> res = new Array<>();
    FileHandle dirHandle = Gdx.files.internal("level/");
    for (FileHandle file : dirHandle.list()) {
      if (!file.isDirectory() && !file.name().toLowerCase().equals(".ds_store"))
        am.load(levelPath + file.name(), LevelModel.class, new AbstractLoader.AbstractParameter<>());
    }
  }

  @Override
  public void finish(AssetManager am) {
    this.am = am;
  }

  @Override
  public LevelModel get(String key) {
    return am.get(levelPath + key, LevelModel.class);
  }
}
