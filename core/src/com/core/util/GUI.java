package com.core.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class GUI {
   public static Button creatButton(TextureRegion... textureRegions) {
      TextureRegionDrawable[] textureRegionDrawables = new TextureRegionDrawable[4];
      if(textureRegions != null) {
         for(int var1 = 0; var1 < Math.min(4, textureRegions.length); ++var1) {
            textureRegionDrawables[var1] = new TextureRegionDrawable(textureRegions[var1]);
            if(var1 == 0) {
               TextureRegionDrawable var3 = textureRegionDrawables[0];
               textureRegionDrawables[3] = var3;
               textureRegionDrawables[2] = var3;
               textureRegionDrawables[1] = var3;
            }
         }
      }

      Button.ButtonStyle buttonStyle = new Button.ButtonStyle();
      buttonStyle.up = textureRegionDrawables[0];
      buttonStyle.down = textureRegionDrawables[1];
      buttonStyle.checked = textureRegionDrawables[2];
      buttonStyle.disabled = textureRegionDrawables[3];
      final Button btn = new Button(buttonStyle);

      btn.addListener(new ClickListener(){
         @Override
         public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
//            GSound.playSound("click);
         }

         public boolean touchDown (InputEvent event, float x, float y, int pointer, int button){
            btn.setColor(Color.GRAY);
            return super.touchDown(event, x, y, pointer, button);
         }

         public void touchUp (InputEvent event, float x, float y, int pointer, int button){
            btn.setColor(Color.WHITE);
            super.touchUp(event, x, y, pointer, button);
         }
      });
      return btn;
   }

   public static Button creatButton(Group parent, float x, float y, int align, TextureRegion... textureRegions){
      Button r = creatButton(textureRegions);
      parent.addActor(r);
      r.setPosition(x, y, align);
      return r;
   }
   public static Label creatLabel(CharSequence charSequence, BitmapFont font, Color color) {
      font.setUseIntegerPositions(false);
      return new Label(charSequence, new Label.LabelStyle(font, color));
   }
   public static Label creatLabel(CharSequence charSequence, Color color) {
      BitmapFont font = new BitmapFont();
      font.setUseIntegerPositions(false);
      return new Label(charSequence, new Label.LabelStyle(font, color));
   }
}
