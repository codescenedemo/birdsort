package com.core.util;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.SnapshotArray;

public class GGroupEx extends Group {
   private final Matrix4 combinedMatrix = new Matrix4();
   private Rectangle cullingArea;
   private Matrix4 lastMatrix;
   private final Matrix4 oldMatrix = new Matrix4();
   private final Matrix4 originMatrix = new Matrix4();

   private void resetLastMatrix(Batch var1) {
   }

   private void setOriginMatrix(Batch batch) {
      this.lastMatrix = batch.getTransformMatrix();
      batch.getTransformMatrix().set(this.originMatrix);
      this.setupMatrices(batch);
   }

   private void setupMatrices(Batch batch) {
      Matrix4 var2 = batch.getProjectionMatrix();
      Matrix4 var3 = batch.getTransformMatrix();
      /*if(!Gdx.graphics.isGL20Available()) {
         GL10 var4 = Gdx.gl10;
         var4.glMatrixMode(5889);
         var4.glLoadMatrixf(var2.val, 0);
         var4.glMatrixMode(5888);
         var4.glLoadMatrixf(var3.val, 0);
      } else */{
         this.combinedMatrix.set(var2).mul(var3);
         GStage.shader.setUniformMatrix("u_projTrans", this.combinedMatrix);
         GStage.shader.setUniformi("u_texture", 0);
         batch.setShader(GStage.shader);
      }
   }

   public static void toParentCoordinate(Actor actor) {
      float var6 = actor.getScaleX();
      float var7 = actor.getScaleY();
      float var5 = actor.getRotation();
      float var2 = -actor.getOriginX();
      float var1 = -actor.getOriginY();
      float var3 = actor.getX() - var2;
      float var4 = actor.getY() - var1;
      if(var6 != 1.0F || var7 != 1.0F) {
         var1 *= var7;
         var2 *= var6;
      }

      if(var5 != 0.0F) {
         var6 = MathUtils.cosDeg(var5);
         var5 = MathUtils.sinDeg(var5);
         var3 += var2 * var6 - var1 * var5;
         var1 = var1 * var6 + var2 * var5 + var4;
         var2 = var3;
      } else {
         var2 += var3;
         var1 += var4;
      }

      actor.setPosition(var2, var1);
   }

   public static void toScreenCoordinate(Actor actor) {
      Group var4 = actor.getParent();
      float var1 = actor.getRotation();
      float var3 = actor.getScaleX();

      float var2;
      for(var2 = actor.getScaleY(); var4 != null; var4 = var4.getParent()) {
         toParentCoordinate(actor);
         actor.setRotation(var4.getRotation());
         actor.setScale(var4.getScaleX(), var4.getScaleY());
         var3 *= var4.getScaleX();
         var2 *= var4.getScaleY();
         var1 += var4.getRotation();
         actor.setOriginX(var4.getOriginX() - actor.getX());
         actor.setOriginY(var4.getOriginY() - actor.getY());
         actor.setX(actor.getX() + var4.getX());
         actor.setY(actor.getY() + var4.getY());
      }

      actor.setRotation(var1);
      actor.setScale(var3, var2);
      actor.setOrigin(0.0F, 0.0F);
   }

   protected void drawChildrenc(Batch batch, float parentAlpha) {
      byte var10 = 0;
      int var9 = 0;
      parentAlpha *= this.getColor().a;
      SnapshotArray var12 = this.getChildren();
      Actor[] var13 = (Actor[])var12.begin();
      Rectangle var14 = this.cullingArea;
      Actor var16;
      if(var14 != null) {
         float var3 = var14.x;
         float var4 = var14.width;
         float var5 = var14.y;
         float var6 = var14.height;

         for(int var15 = var12.size; var9 < var15; ++var9) {
            var16 = var13[var9];
            if(var16.isVisible()) {
               float var7 = var16.getX();
               float var8 = var16.getY();
               if(var7 <= var4 + var3 && var8 <= var6 + var5 && var7 + var16.getWidth() >= var3 && var16.getHeight() + var8 >= var5) {
                  var16.draw(batch, parentAlpha);
               }
            }
         }
      } else {
         int var11 = var12.size;

         for(var9 = var10; var9 < var11; ++var9) {
            var16 = var13[var9];
            if(var16.isVisible()) {
               var16.draw(batch, parentAlpha);
            }
         }
      }

      var12.end();
   }

   public void setCullingArea(Rectangle rectangle) {
      this.cullingArea = rectangle;
   }
}
