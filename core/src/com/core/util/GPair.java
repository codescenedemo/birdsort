package com.core.util;

import java.io.Serializable;

public class GPair<K, V> implements Serializable {

  private final K key;
  private final V value;

  public GPair(K key, V value) {
    this.key = key;
    this.value = value;
  }

  public K getKey() {
    return key;
  }

  public V getValue() {
    return value;
  }
}