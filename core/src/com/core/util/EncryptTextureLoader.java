package com.core.util;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.assets.loaders.TextureLoader;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.TextureData;

public class EncryptTextureLoader extends TextureLoader {
    public static final int[] DECODE_KEYS = new int[]{2, 0, 4, 1, 3};

    static public class TextureLoaderInfo {
        String filename;
        TextureData data;
        Texture texture;
    };

    TextureLoaderInfo info = new TextureLoaderInfo();

    public EncryptTextureLoader(FileHandleResolver resolver) {
        super(resolver);
    }

    @Override
    public void loadAsync (AssetManager manager, String fileName, FileHandle file, TextureParameter parameter) {
        super.loadAsync(manager, fileName, new EncryptFileHandler(file, DECODE_KEYS), parameter);
    }
}
