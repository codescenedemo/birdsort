package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.utils.ActorGestureListener;

import com.badlogic.gdx.utils.Null;
import com.core.transitions.GTransition;

public abstract class GScreen implements Screen {
   private Color color = new Color(0.0F, 0.0F, 0.0F, 1.0F);
   public GDirectedGame game;
   public boolean isDebug = false;

   public static int getX() {
      return Gdx.input.getX();
   }

   public static int getX(int pointer) {
      return Gdx.input.getX(pointer);
   }

   public static int getY() {
      return Gdx.input.getY();
   }

   public static int getY(int pointer) {
      return Gdx.input.getY(pointer);
   }

   private void initGestureProcessor() {
      GStage.getStage().addCaptureListener(new ActorGestureListener() {
         public void fling(InputEvent event, float velocityX, float velocityY, int button) {
            GScreen.this.gFling(velocityX, velocityY, button);
         }

         public boolean longPress(Actor actor, float x, float y) {
            return GScreen.this.gLongPress(x, y);
         }

         public void pan(InputEvent event, float x, float y, float deltaX, float deltaY) {
            GScreen.this.gPan(x, y, deltaX, deltaY);
         }

         public void pinch(InputEvent event, Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
            GScreen.this.gPinch(initialPointer1, initialPointer2, pointer1, pointer2);
         }

         public void tap(InputEvent event, float x, float y, int count, int button) {
            GScreen.this.gTap(x, y, count, button);
         }

         public void zoom(InputEvent event, float initialDistance, float distance) {
            GScreen.this.gZoom(initialDistance, distance);
         }
      });
   }

   private void initInputProcessor() {
      GStage.getStage().addListener(new InputListener() {
         public void enter(InputEvent event, float x, float y, int pointer, @Null Actor fromActor) {
            GScreen.this.gEnter(event, x, y, pointer, fromActor);
         }

         public void exit(InputEvent event, float x, float y, int pointer, @Null Actor toActor) {
            GScreen.this.gExit(event, x, y, pointer, toActor);
         }

         public boolean keyDown(InputEvent event, int keycode) {
            return GScreen.this.gKeyDown(keycode);
         }

         public boolean keyTyped(InputEvent event, char character) {
            return GScreen.this.gKeyTyped(character);
         }

         public boolean keyUp(InputEvent event, int keycode) {
            return GScreen.this.gKeyUp(keycode);
         }

         public boolean mouseMoved(InputEvent event, float x, float y) {
            return GScreen.this.gMouseMoved(event, x, y);
         }

         public boolean scrolled(InputEvent event, float x, float y, float amountX, float amountY) {
            return GScreen.this.gScrolled(event, x, y, amountX, amountY);
         }

         public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
            return GScreen.this.gTouchDown(x, y, pointer, button);
         }

         public void touchDragged(InputEvent event, float x, float y, int pointer) {
            GScreen.this.gTouchDragged((int)x, (int)y, pointer);
         }

         public void touchUp(InputEvent event, float x, float y, int pointer, int button) {
            GScreen.this.gTouchUp(x, y, pointer, button);
         }
      });
   }

   public static boolean isJustTouched() {
      return Gdx.input.justTouched();
   }

   public static boolean isKeyPressed(int var0) {
      return Gdx.input.isKeyPressed(var0);
   }

   public static boolean isTouched() {
      return Gdx.input.isTouched();
   }

   public static boolean isTouched(int var0) {
      return Gdx.input.isTouched(var0);
   }

   public void debugPaint(String var1) {
      if(this.isDebug) {
         //GMain.platform.log(var1);
      }

   }

   public abstract void dispose();

   public void gEnter(InputEvent event, float x, float y, int pointer, @Null Actor fromActor) {
   }

   public void gExit(InputEvent event, float x, float y, int pointer, @Null Actor toActor) {
   }

   public boolean gFling(float velocityX, float velocityY, int button) {
      //this.debugPaint("fling 翻页动作 .............");
      return false;
   }

   public boolean gKeyDown(int keyCode) {
      return false;
   }

   public boolean gKeyTyped(char character) {
      return false;
   }

   public boolean gKeyUp(int keycode) {
      return false;
   }

   public boolean gLongPress( float x, float y) {
      //this.debugPaint("longPress 长按 .............");
      return false;
   }

   public boolean gMouseMoved(InputEvent event, float x, float y) {
      return false;
   }

   public boolean gPan(float x, float y, float deltaX, float deltaY) {
      //this.debugPaint("pan 划屏动作.............");
      return false;
   }

   public boolean gPinch(Vector2 initialPointer1, Vector2 initialPointer2, Vector2 pointer1, Vector2 pointer2) {
      //this.debugPaint("pan 手指捏的动作.............");
      return false;
   }

   public boolean gScrolled(InputEvent event, float x, float y, float amountX, float amountY) {
      return false;
   }

   public boolean gTap(float x, float y, int count, int button) {
      //this.debugPaint("tap 快速点击动作............." + var3);
      return false;
   }

   public boolean gTouchDown(float x, float y, int pointer, int button) {
      return false;
   }

   public void gTouchDragged(float x, float y, int pointer) {
   }

   public void gTouchUp(float x, float y, int pointer, int button) {
   }

   public boolean gZoom(float initialDistance, float distance) {
      this.debugPaint("zoom.............");
      return false;
   }

   public void hide() {
      this.dispose();
      GStage.clearAllLayers();
   }

   public abstract void init();

   public boolean isTransitionEnd() {
      return this.game.isTransitionEnd();
   }

   public void pause() {
      GSound.pause();
   }

   public void render(float delta) {
      this.run();
      GStage.render();
   }

   public void resize(int w, int h) {
      //viewportW = Gdx.graphics.getWidth();
      //viewportH = Gdx.graphics.getHeight();

   }

   public void resume() {
      GSound.resume();
   }

   public abstract void run();

   public void setColor(Color color) {
      this.color = color;
   }

   protected void setGameInstance(GDirectedGame game) {
      this.game = game;
   }

   public void setScreen(GScreen screen) {
      this.game.setScreen(screen);
   }

   public void setScreen(GScreen screen, GTransition transition) {
      this.game.setScreen(screen, transition);
   }

   public void show() {
      GStage.setClearColor(this.color);
      GStage.clearListeners();
      this.initGestureProcessor();
      this.initInputProcessor();
      this.init();
   }


}
