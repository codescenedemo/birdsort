package com.core.util;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;

import com.core.transitions.GTransition;
//import ze.gamegdx.Debug;
//import ze.gamelogic.mvc.view.GamePlayView;
//import ze.gamelogic.screen.MenuScreen;
//import ze.gamelogic.ui.PlayUI;

public abstract class GDirectedGame implements ApplicationListener {
   private static boolean isUseTransition = true;
   private SpriteBatch batch;
   private FrameBuffer currFbo;



   private GScreen currScreen;
   private boolean isTransitionEnd;
   private FrameBuffer nextFbo;
   private GTransition screenTransition;
   private float time;

   public void dispose() {
      if(this.currScreen != null) {
         this.currScreen.hide();
      }

      if(this.batch != null) {
         this.currFbo.dispose();
         this.currScreen = null;
         this.nextFbo.dispose();
         this.batch.dispose();
         this.batch = null;
      }

   }

   public boolean isTransitionEnd() {
      return this.isTransitionEnd;
   }

   public void pause() {
      if(this.currScreen != null) {
         this.currScreen.pause();
      }
   }

   public void render() {
      if(!this.isTransitionEnd && isUseTransition && this.screenTransition != null) {
         float dt = Math.min(GStage.getDelta(), GStage.getSleepTime());
         float duration = this.screenTransition.duration;
         this.time = Math.min(this.time + dt, duration);
         if(this.time >= duration) {
            this.screenTransition = null;
            this.isTransitionEnd = true;
         } else {
            this.nextFbo.begin();
            this.currScreen.render(dt);
            this.nextFbo.end();
            dt = this.time / duration;
            this.screenTransition.render(this.batch, this.currFbo.getColorBufferTexture(), this.nextFbo.getColorBufferTexture(), dt);
         }
      } else {
            this.currScreen.render(0.0F);
      }
   }

   public void resize(int width, int height) {
      GStage.getStage().getViewport().update(width, height);
      if(this.currScreen != null) {
         this.currScreen.resize(width, height);
      }

   }

   public void resume() {
      if(this.currScreen != null) {
         this.currScreen.resume();
      }

   }

   public void setScreen(GScreen screen) {
      this.setScreen(screen, (GTransition)null);
   }

   public void setScreen(GScreen screen, GTransition transition) {
      System.gc();
      screen.setGameInstance(this);
      if(isUseTransition && transition != null) {
         int w =  Gdx.graphics.getWidth();
         int h =  Gdx.graphics.getHeight();
         this.isTransitionEnd = false;
         if(this.batch == null) {
            this.batch = new SpriteBatch(2);
            this.currFbo = new FrameBuffer(Pixmap.Format.RGB565, w, h, false);
            this.nextFbo = new FrameBuffer(Pixmap.Format.RGB565, w, h, false);
           // this.currFbo =  FrameBuffer.createFrameBuffer(Pixmap.Format.RGB565, var3, var4, false);
           // this.nextFbo =  FrameBuffer.createFrameBuffer(Pixmap.Format.RGB565, var3, var4, false);
         }

         if(this.currScreen != null) {
            this.currFbo.begin();
            this.currScreen.render(0.0F);
            this.currFbo.end();
            this.currScreen.hide();
         }

         this.currScreen = screen;
         this.currScreen.show();
        // this.currScreen.resize(GScreen.viewportW, GScreen.viewportH);
         this.screenTransition = transition;
         this.time = 0.0F;
      } else {
         if(this.currScreen != null) {
            this.currScreen.hide();
         }

         this.currScreen = screen;
         this.currScreen.show();
         this.isTransitionEnd = true;
      }
   }

   public GScreen getCurrScreen() {
      return currScreen;
   }

   public void setUseTransition(boolean useTransition) {
      isUseTransition = useTransition;
   }
}
