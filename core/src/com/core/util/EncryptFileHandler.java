package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

import java.io.InputStream;

public class EncryptFileHandler extends FileHandle {
  FileHandle file;
  int[] cryptKey;

  public EncryptFileHandler(FileHandle file, int[] cryptKey) {
    this.file = file;
    this.cryptKey = cryptKey;
  }

  @Override
  public byte[] readBytes() {
    try {
      byte[] header = new byte[4];
      InputStream inputStream = file.read();
      inputStream.read(header);
      byte[] encodedData = new String(header).equals(".tex") ? readBytes((InputStream) inputStream, cryptKey) : file.readBytes();
      return encodedData;
    } catch (Exception e) {
      Gdx.app.error("EncryptFileHandler", "load error file " + file.name());
      e.printStackTrace();
    }

    return super.readBytes();
  }

  private byte[] readBytes(InputStream inputStream, int[] keys) throws Exception {
    int fileSize = inputStream.available();
    byte[] encodeByteArray = new byte[fileSize];
    int blockSize = fileSize / keys.length;
    int blockIndex = 0;
    do {
      if (blockIndex >= keys.length) {
        int byteLeft = fileSize % keys.length;
        if (byteLeft <= 0) return encodeByteArray;
        inputStream.read(encodeByteArray, blockSize * keys.length, byteLeft);
        return encodeByteArray;
      }
      inputStream.read(encodeByteArray, blockSize * keys[blockIndex], blockSize);
      ++blockIndex;
    } while (true);
  }

  @Override
  public String name() {
    return file.name();
  }

  @Override
  public String nameWithoutExtension() {
    return file.nameWithoutExtension();
  }
}
