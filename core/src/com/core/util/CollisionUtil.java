package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class CollisionUtil {
    public static Polygon getPolygon(Rectangle bounds, float rotation){
        Polygon polygon = new Polygon(new float[]{0,0,bounds.width,0,bounds.width,bounds.height,0,bounds.height});
        polygon.setOrigin(bounds.width/2, bounds.height/2);
        polygon.setRotation(rotation);
        polygon.setPosition(bounds.x,bounds.y);
        return polygon;
    }

    public static Polygon getPolygon(Image bounds, float rotation, float scale){
        float w = bounds.getWidth()*scale;
        float h= bounds.getHeight()*scale;
        Polygon polygon = new Polygon(new float[]{0,0,w,0,w,h,0,h});
        polygon.setOrigin(w/2, h/2);
        polygon.setRotation(rotation);
        polygon.setPosition(bounds.getX(),bounds.getY());

        return polygon;
    }

    public static boolean checkCollide(Polygon polygon1, Polygon polygon2){
        return Intersector.overlapConvexPolygons(polygon1,polygon2);
    }

    public static boolean checkCollide(Shape2D shape1, Shape2D shape2){
        if (shape1 instanceof Rectangle && shape2 instanceof Rectangle){
            return ((Rectangle) shape1).overlaps((Rectangle) shape2);
        }
        Polygon polygon1=null,polygon2=null;
        if (shape1 instanceof Rectangle){
            polygon1=getPolygon((Rectangle) shape1,0);
        }
        if (shape2 instanceof  Rectangle){
            polygon2=getPolygon((Rectangle)shape2,0);
        }
        if (polygon1==null)
            polygon1=(Polygon)shape1;
        if (polygon2==null)
            polygon2=(Polygon)shape2;

//        if(polygon1 == null || polygon2 == null)
//        {
//            Gdx.app.log("", "Test");
//        }
        return Intersector.overlapConvexPolygons(polygon1,polygon2);
    }
}
