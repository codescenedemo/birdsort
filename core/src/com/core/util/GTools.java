package com.core.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;

public class GTools {
   public static Preferences getPreferences(String var0) {
      return Gdx.app.getPreferences(var0);
   }

   public static float[] getVertices(float[] var0, float var1, float var2, float var3, float var4, float var5, float var6, float var7, float var8, float var9) {
      float var12;
      float var13;
      label15: {
         var5 = -var5;
         var6 = -var6;
         float var10 = var5 + var3;
         float var11 = var6 + var4;
         var13 = var1 - var5;
         var12 = var2 - var6;
         if(var8 == 1.0F) {
            var3 = var11;
            var1 = var10;
            var2 = var6;
            var4 = var5;
            if(var9 == 1.0F) {
               break label15;
            }
         }

         var4 = var5 * var8;
         var2 = var6 * var9;
         var1 = var10 * var8;
         var3 = var11 * var9;
      }

      if(var7 != 0.0F) {
         var5 = MathUtils.cosDeg(var7);
         var6 = MathUtils.sinDeg(var7);
         var7 = var4 * var5;
         var8 = var4 * var6;
         var4 = var5 * var3;
         var9 = var3 * var6;
         var3 = var7 - var2 * var6 + var13;
         var2 = var2 * var5 + var8 + var12;
         var0[0] = var3;
         var0[1] = var2;
         var7 = var7 - var9 + var13;
         var8 = var8 + var4 + var12;
         var0[2] = var7;
         var0[3] = var8;
         var5 = var1 * var5 - var9 + var13;
         var1 = var1 * var6 + var4 + var12;
         var0[4] = var5;
         var0[5] = var1;
         var0[6] = var5 + var3 - var7;
         var0[7] = var1 - (var8 - var2);
         return var0;
      } else {
         var4 += var13;
         var2 += var12;
         var1 += var13;
         var3 += var12;
         var0[0] = var4;
         var0[1] = var2;
         var0[2] = var4;
         var0[3] = var3;
         var0[4] = var1;
         var0[5] = var3;
         var0[6] = var1;
         var0[7] = var2;
         return var0;
      }
   }

   public static String[] jsonValueToStringArray(JsonValue jsonValue) {
      if(jsonValue == null) {
         return null;
      } else {
         String[] strings = new String[jsonValue.size];

         for(int i = 0; i < strings.length; ++i) {
            strings[i] = jsonValue.getString(i);
         }

         return strings;
      }
   }

   public static String[] splitString(String str, String var1) {
      boolean var2 = false;
      if(str == null) {
         return null;
      } else {
         Array var6 = new Array();

         int var3;
         for(int var4 = 0; !var2; var4 = var1.length() + var3) {
            var3 = str.indexOf(var1, var4);
            if(var3 == -1) {
               var3 = str.length();
               var2 = true;
            }

            int var5;
            if(var3 > 0 && str.charAt(var3 - 1) == 13) {
               var5 = var3 - 1;
            } else {
               var5 = var3;
            }

            String var7 = str.substring(var4, var5).trim();
            if(!var7.equals("")) {
               var6.add(var7);
            }
         }

         return (String[])var6.toArray(String.class);
      }
   }

   public static void toParentCoordinate(Actor actor) {
      float scaleX = actor.getScaleX();
      float scaleY = actor.getScaleY();
      float rotation = actor.getRotation();
      float x = -actor.getOriginX();
      float y = -actor.getOriginY();
      float dx = actor.getX() - x;
      float dy = actor.getY() - y;
      if(scaleX != 1.0F || scaleY != 1.0F) {
         y *= scaleY;
         x *= scaleX;
      }

      if(rotation != 0.0F) {
         scaleX = MathUtils.cosDeg(rotation);
         rotation = MathUtils.sinDeg(rotation);
         dx += x * scaleX - y * rotation;
         y = y * scaleX + x * rotation + dy;
         x = dx;
      } else {
         x += dx;
         y += dy;
      }

      actor.setPosition(x, y);
   }

   public static void toScreenCoordinate(Actor actor) {
      Group parent = actor.getParent();
      float rotation = actor.getRotation();
      float scaleX = actor.getScaleX();

      float scaleY ;
      for(scaleY = actor.getScaleY(); parent != null; parent = parent.getParent()) {
         toParentCoordinate(actor);
         actor.setRotation(parent.getRotation());
         actor.setScale(parent.getScaleX(), parent.getScaleY());
         scaleX *= parent.getScaleX();
         scaleY *= parent.getScaleY();
         rotation += parent.getRotation();
         actor.setOriginX(parent.getOriginX() - actor.getX());
         actor.setOriginY(parent.getOriginY() - actor.getY());
         actor.setX(actor.getX() + parent.getX());
         actor.setY(actor.getY() + parent.getY());
      }

      actor.setRotation(rotation);
      actor.setScale(scaleX, scaleY);
      actor.setOrigin(0.0F, 0.0F);
   }

   public static float[] toScreenPoint(Actor actor) {
      float[] result = new float[]{actor.getX(), actor.getY()};

      for(Group group = actor.getParent(); group != null; group = group.getParent()) {
         if(group instanceof Group) {
            result[0] += group.getX();
            result[1] += group.getY();
         }
      }

      return result;
   }

}
