package com.core.util;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

public class ParallaxBackground extends Actor {

  private Vector2 scroll;
  private Array<Texture> layers;
  private final float LAYER_SPEED_DIFFERENCE = 1f;

  int srcX, srcY;
  boolean flipX, flipY;

  private Vector2 speed;

  public ParallaxBackground(Array<Texture> textures) {
    layers = textures;
    if (Gdx.app.getType() != Application.ApplicationType.WebGL)
      for (int i = 0; i < textures.size; i++) {
        layers.get(i).setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
      }
    scroll = new Vector2();
    speed = new Vector2();

    flipX = flipY = false;
  }

  public void setSpeed(Vector2 newSpeed) {
    this.speed = newSpeed;
  }

  public void setSpeed(float x, float y) {
    this.speed.set(x, y);
  }


  @Override
  public void draw(Batch batch, float parentAlpha) {
    batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a * parentAlpha);

//    scroll.y += speed.y;
    for (int i = 0; i < layers.size; i++) {
      srcX = (int) (scroll.x + i * this.LAYER_SPEED_DIFFERENCE * scroll.x);
//      srcY = (int) (scroll.y + i * this.LAYER_SPEED_DIFFERENCE * scroll.y);
      batch.draw(layers.get(i), getX(), getY(), getOriginX(), getOriginY(), getWidth(), getHeight(), getScaleX(), getScaleY(), getRotation(), srcX, srcY, layers.get(i).getWidth(), layers.get(i).getHeight(), flipX, flipY);
    }
  }

  @Override
  public void act(float delta) {
    super.act(delta);
    scroll.x += speed.x * 60f * delta;
  }
}
