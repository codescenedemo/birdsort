package com.core.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

@SuppressWarnings("unused")
public class GUtils {
  public static Image createOverlay(float w, float h, float alpha) {
    Color c = Color.BLACK;
    c.a = alpha;
    Texture ol = createSolid(c);
    Image res = new Image(ol);
    res.setSize(w, h);
    return res;
  }

  public static Image createOverlay(float w, float h, float alpha, Color c) {
    c.a = alpha;
    Texture ol = createSolid(c);
    Image res = new Image(ol);
    res.setSize(w, h);
    return res;
  }

  public static Texture createSolid(Color color) {
    return createSolid(1, 1, color);
  }

  public static Texture createSolid(int w, int h, Color color) {
    Pixmap pixmap = new Pixmap(w, h, Pixmap.Format.RGBA8888);
    pixmap.setColor(color);
    pixmap.fillRectangle(0, 0, w, h);
    Texture texture = new Texture(pixmap);
    pixmap.dispose();
    return texture;
  }

  public static Texture createTextSprite(BitmapFont font, String text) {
    Matrix4 projector = new Matrix4();
    Batch batch = new SpriteBatch();
    GlyphLayout gly = new GlyphLayout(font, text);

    projector.setToOrtho2D(0, 0, gly.width, gly.height);
    batch.setProjectionMatrix(projector);
    FrameBuffer currFbo = new FrameBuffer(Pixmap.Format.RGBA8888, (int) gly.width, (int) gly.height, false);
    currFbo.begin();
    batch.begin();

    font.draw(batch, text, 0, 0);
    batch.end();
    currFbo.end();

    return currFbo.getColorBufferTexture();
  }

  public static int dayDiff(long t1, long t2) {
    return 0;
  }

  public static long startOfDay(long milliSecond) {
    return System.currentTimeMillis();
  }

  public static long strDate2Mills(String strDate) {
    return 0;
  }

  public static String msToTime(long milliseconds) {
    int seconds = (int) (milliseconds / 1000) % 60 ,
        minutes = (int) ((milliseconds / (1000*60)) % 60),
        hours   = (int) ((milliseconds / (1000*60*60)) % 24);

    String h = (hours < 10) ? "0" + hours : hours + "";
    String m = (minutes < 10) ? "0" + minutes : minutes + "";
    String s = (seconds < 10) ? "0" + seconds : seconds + "";

    return h + ":" + m + ":" + s;
  }
}