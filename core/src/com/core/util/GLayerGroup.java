package com.core.util;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;

public class GLayerGroup extends Group {
   private boolean pause;

   public float getCurTimeScale() {
      return curTimeScale;
   }

   private float curTimeScale = 1f;
   private float startTimeScale = 0;
   private float toTimeScale = 1f;
   private float durationTimescale = 0f;
   private float maxDurationTimescale = 0f;

   public void act(float delta) {
      if(!this.pause) {
         if(durationTimescale > 0){
            durationTimescale -=delta;
            float percent = (1f - durationTimescale/ maxDurationTimescale);
            float dTimeScale = toTimeScale - startTimeScale;
            curTimeScale = startTimeScale + dTimeScale * percent;
         }
         super.act(delta * curTimeScale);
      }
   }

   public boolean isPause() {
      return this.pause;
   }

   public void setPause(boolean isPause) {
      this.pause = isPause;
         Actor[] actors = getChildren().begin();
         for (int i = 0, n = getChildren().size; i < n; i++) {
            Actor a = actors[i];
         }
         getChildren().end();
   }

   public void setTimeScale( float toTimeScale, float durationTimescale){
      this.toTimeScale = toTimeScale;
      this.durationTimescale = durationTimescale;
      this.maxDurationTimescale = this.durationTimescale;
      this.startTimeScale = curTimeScale;
   }
}
