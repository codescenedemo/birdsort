package com.core.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class GImage extends Actor {
   TextureRegion region;
   boolean rotate;

   public GImage(Texture texture) {
      this.region = new TextureRegion(texture, 0, 0, texture.getWidth(), texture.getHeight());
      this.setSize((float)this.region.getRegionWidth(), (float)this.region.getRegionHeight());
   }

   public GImage(TextureRegion textureRegion) {
      this.region = textureRegion;
      if(textureRegion.getClass() == TextureAtlas.AtlasRegion.class && ((TextureAtlas.AtlasRegion)textureRegion).rotate) {
         this.setSize((float)textureRegion.getRegionHeight(), (float)textureRegion.getRegionWidth());
         this.rotate = true;
      } else {
         this.setSize((float)textureRegion.getRegionWidth(), (float)textureRegion.getRegionHeight());
         this.rotate = false;
      }
   }

   public void draw(Batch batch, float parentAlpha) {
      Color color = this.getColor();
      batch.setColor(color.r, color.g, color.b, color.a * parentAlpha);
      float x = this.getX();
      float y = this.getY();
      float originX = this.getOriginX();
      float originY = this.getOriginY();
      float scaleX = this.getScaleX();
      float scaleY = this.getScaleY();
      float width = this.getWidth();
      float height = this.getHeight();
      float rotation = this.getRotation();
      if(this.rotate) {
         batch.draw(this.region, x, y, originX, originY, width, height, scaleX, scaleY, rotation, false);
      } else {
         batch.draw(this.region, x, y, originX, originY, width, height, scaleX, scaleY, rotation);
      }

      this.setColor(color);
   }
}
