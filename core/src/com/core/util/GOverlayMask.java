package com.core.util;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Circle;
import com.badlogic.gdx.math.Ellipse;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Shape2D;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Array;
import com.core.util.GStage;

public class GOverlayMask extends Image {
  Array<Rectangle> rectangles;
  Array<Circle> circles;
  Array<Shape2D> shape2DS;
  Array<Pixmap> pixmaps;
  Texture texture;
  int w, h;

  public GOverlayMask(int width, int height) {
//    super();
    this.w = width;
    this.h = height;
    this.setWidth(width);
    this.setHeight(height);
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    super.draw(batch, parentAlpha);
    batch.draw(texture, this.getX(), this.getY(), w, h);
  }

  public void initRectangles(Array<Rectangle> rectangles) {
    this.rectangles = rectangles;
    texture = new Texture(createRectangleMask(w, h, rectangles));
    this.setDrawable(new TextureRegionDrawable(texture));
  }

  public void initShape(Array<Shape2D> shape2DS) {
    this.shape2DS = shape2DS;
    texture = new Texture(createShapeMask(w, h, shape2DS));
//    this.setDrawable(new TextureRegionDrawable(texture));
  }

//  public void initEllipse(int w, int h) {
////    texture = new Texture(createEllipse(w, h));
//    texture = createEllipse(w, h);
//  }

  public void initCircle(Array<Circle> circles) {
    this.circles = circles;
    texture = new Texture(createCircleMask(w, h, circles));
    this.setDrawable(new TextureRegionDrawable(texture));
  }

  public void initPixmap(Array<Pixmap> pixmaps) {
    this.pixmaps = pixmaps;
    texture = new Texture(createRoundMask(w, h, pixmaps));
    this.setDrawable(new TextureRegionDrawable(texture));
  }

  @Override
  public Actor hit(float x, float y, boolean touchable) {
    int newy = (int) (GStage.getWorldHeight() - y);
    if (rectangles != null)
      if (isRectanglesContainPoint(rectangles, (int) x, newy)) {
        return null;
      }
    if (circles != null)
      if (isCircleContainPoint(circles, (int) x, newy)) {
        return null;
      }

    if (shape2DS != null)
      if (isShapeContainPoint(shape2DS, (int) x, newy)) {
        return null;
      }

    if (pixmaps != null)
      if (isRoundContainPoint(pixmaps, (int) x, newy)) {
        return null;
      }

    return super.hit(x, y, touchable);
  }

//  @Override
//  public void draw(Batch batch, float parentAlpha) {
//   // super.draw(batch, parentAlpha);
//
//    if (texture != null) batch.draw(texture, this.getX(), this.getY());
//  }

  public static boolean isRectanglesContainPoint(Array<Rectangle> rectangles, int x, int y) {
    for (int i = 0; i < rectangles.size; i++) {
      if (rectangles.get(i).contains(x, y))
        return true;
    }
    return false;
  }

  public static Pixmap createRectangleMask(int width, int height, Array<Rectangle> rectangles) {
    Pixmap result = new Pixmap(width, height, Pixmap.Format.RGBA8888);
    //Rectangle rec = new Rectangle(100, 100, 10, 300);
    for (int x = 0; x < result.getWidth(); x++) {
      for (int y = 0; y < result.getHeight(); y++) {
        //System.out.println("p " + source.getPixel(x, y));
        if (isRectanglesContainPoint(rectangles, x, y))
          result.drawPixel(x, y, 0x00000000);
        else result.drawPixel(x, y, 0x000000D9);
      }
    }

    return result;
  }

  public static boolean isCircleContainPoint(Array<Circle> circles, int x, int y) {
    for (int i = 0; i < circles.size; i++) {
      if (circles.get(i).contains(x, y))
        return true;
    }
    return false;
  }

  public static Pixmap createCircleMask(int width, int height, Array<Circle> circles) {
    Pixmap result = new Pixmap(width, height, Pixmap.Format.RGBA8888);
    for (int x = 0; x < result.getWidth(); x++) {
      for (int y = 0; y < result.getHeight(); y++) {
        if (isCircleContainPoint(circles, x, y)) {
          result.drawPixel(x, y, 0x00000000);
        } else result.drawPixel(x, y, 0x000000D9);
      }
    }
    return result;
  }

  public static boolean isShapeContainPoint(Array<Shape2D> shape2DS, int x, int y) {
    for (int i = 0; i < shape2DS.size; i++) {
      if (shape2DS.get(i).contains(x, y))
        return true;
    }
    return false;
  }

  public static Pixmap createShapeMask(int width, int height, Array<Shape2D> shape2DS) {
    Pixmap result = new Pixmap(width, height, Pixmap.Format.RGBA8888);

    result.setBlending(Pixmap.Blending.None);
    result.setColor(new Color(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, .8f));
    result.fill();
    result.setColor(1f, 1f, 1f, 0f);
    for (Shape2D shape : shape2DS) {
      if (shape instanceof Circle) {
        result.fillCircle((int) ((Circle) shape).x, (int) ((Circle) shape).y, (int) ((Circle) shape).radius);
      }
      if (shape instanceof Rectangle) {
        result.fillRectangle((int) ((Rectangle) shape).x, (int) ((Rectangle) shape).y, (int) ((Rectangle) shape).width, (int) ((Rectangle) shape).height);
      }
//      if (shape instanceof Ellipse) {
////        result.setColor(1f, 1f, 1f, .8f);
//        result.drawPixmap(createEllipse((int) ((Ellipse) shape).width, (int) ((Ellipse) shape).height), (int) ((Ellipse) shape).x, (int) ((Ellipse) shape).y);
//      }
    }
    return result;
  }

  public static boolean isRoundContainPoint(Array<Pixmap> pixmaps, int x, int y) {
    for (int i = 0; i < pixmaps.size; i++) {
      if (pixmaps.get(i).getPixel(x, y) != 0)
        return true;
    }
    return false;
  }

  public static Pixmap createRoundMask(int width, int height, Array<Pixmap> pixmaps) {
    Pixmap result = new Pixmap(width, height, Pixmap.Format.RGBA8888);
    for (int x = 0; x < result.getWidth(); x++) {
      for (int y = 0; y < result.getHeight(); y++) {
        if (isRoundContainPoint(pixmaps, x, y)) {
          result.drawPixel(x, y, 0x00000000);
        } else result.drawPixel(x, y, 0x000000D9);
      }
    }
    return result;
  }

  public static Pixmap createEllipse(int w, int h) {
    Matrix4 projector = new Matrix4();
    FrameBuffer fbo;

    projector.setToOrtho2D(0, 0, w, h);
    fbo = new FrameBuffer(Pixmap.Format.RGBA8888, w, h, false);

    ShapeRenderer shapeRenderer = new ShapeRenderer();
    shapeRenderer.setProjectionMatrix(projector);
    Color color = Color.BLACK;
    shapeRenderer.setColor(color.r, color.g, color.b, .1f);
    shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);

    fbo.begin();
    shapeRenderer.ellipse(0, 0, w, h);
    shapeRenderer.end();
    Pixmap pixmap = Pixmap.createFromFrameBuffer(0, 0, w, h);
    pixmap.setColor(new Color(Color.BLACK.r, Color.BLACK.g, Color.BLACK.b, .8f));
    fbo.end();
    return pixmap;
  }

}
