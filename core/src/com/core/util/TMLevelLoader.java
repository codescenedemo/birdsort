package com.core.util;

import com.badlogic.gdx.assets.loaders.FileHandleResolver;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Json;
import com.game.gamemodel.LevelModel;

import java.io.Reader;
import java.io.StringReader;

@SuppressWarnings("unused")
public class TMLevelLoader extends AbstractLoader<LevelModel> {
  public static final int[] DECODE_KEYS = new int[]{2, 0, 4, 1, 3};

  public TMLevelLoader(FileHandleResolver resolver) {
    super(resolver);
  }

  @Override
  public LevelModel parse(String fileName, FileHandle file, Json json) {
    FileHandle f = new EncryptFileHandler(file, DECODE_KEYS);
    byte[] bytes = f.readBytes();
    String readByte = new String(bytes);

    Reader targetReader = new StringReader(readByte);
    LevelModel outputJson = json.fromJson(LevelModel.class, targetReader);
    return outputJson;
  }

  public static Array<int[]> loadLevelData(int[][] list) {
    Array<int[]> array = new Array<>();
    for (int[] ints : list) {
      int i4 = ints.length;
      int[] iArr = new int[i4];
      System.arraycopy(ints, 0, iArr, 0, i4);
      array.add(iArr);
    }
    return array;
  }
}
