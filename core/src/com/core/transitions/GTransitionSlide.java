package com.core.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;

public class GTransitionSlide extends GTransition {
   public static final int DOWN = 4;
   public static final int LEFT = 1;
   public static final int RIGHT = 2;
   public static final int UP = 3;
   private static final GTransitionSlide instance = new GTransitionSlide();
   private int direction;
   private Interpolation easing;
   private boolean slideOut;

   public static GTransitionSlide init(float duration, int direction, boolean slideOut, Interpolation easing) {
      instance.duration = duration;
      instance.direction = direction;
      instance.slideOut = slideOut;
      instance.easing = easing;
      return instance;
   }

   public void render(Batch batch, Texture texture, Texture texture1, float alpha) {
      float var7 = (float) texture.getWidth();
      float var8 = (float) texture.getHeight();
      float var5 = alpha;
      if(this.easing != null) {
         var5 = this.easing.apply(alpha);
      }

      int direction = this.direction;
      float var6 = 0.0F;
      alpha = 0.0F;
      boolean var10;
      switch(direction) {
      case 1:
         var6 = var5 * -var7;
         var10 = this.slideOut;
         alpha = 0.0F;
         var5 = var6;
         if(!var10) {
            alpha = 0.0F;
            var5 = var6 + var7;
         }
         break;
      case 2:
         var6 = var7 * var5;
         var10 = this.slideOut;
         alpha = 0.0F;
         var5 = var6;
         if(!var10) {
            alpha = 0.0F;
            var5 = var6 - var7;
         }
         break;
      case 3:
         var6 = var8 * var5;
         var10 = this.slideOut;
         var5 = 0.0F;
         alpha = var6;
         if(!var10) {
            alpha = var6 - var8;
            var5 = 0.0F;
         }
         break;
      case 4:
         var6 = -var8 * var5;
         var10 = this.slideOut;
         var5 = 0.0F;
         alpha = var6;
         if(!var10) {
            alpha = var6 + var8;
            var5 = 0.0F;
         }
         break;
      default:
         var5 = var6;
      }

      Texture var11;
      if(this.slideOut) {
         var11 = texture1;
      } else {
         var11 = texture;
      }

      Texture var12;
      if(this.slideOut) {
         var12 = texture;
      } else {
         var12 = texture1;
      }

      Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      Gdx.gl.glClear(16384);
      batch.begin();
      batch.draw(var11, 0.0F, 0.0F, 0.0F, 0.0F, var7, var8, 1.0F, 1.0F, 0.0F, 0, 0, texture.getWidth(), texture.getHeight(), false, true);
      batch.draw(var12, var5, alpha, 0.0F, 0.0F, var7, var8, 1.0F, 1.0F, 0.0F, 0, 0, texture1.getWidth(), texture1.getHeight(), false, true);
      batch.end();
   }
}
