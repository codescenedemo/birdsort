package com.core.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.utils.Array;

public class GTransitionSlice extends GTransition {
   public static final int DOWN = 2;
   public static final int UP = 1;
   public static final int UP_DOWN = 3;
   private static final GTransitionSlice instance = new GTransitionSlice();
   private int direction;
   private Interpolation easing;
   private Array sliceIndex = new Array();

   public static GTransitionSlice init(float duration, int direction, int sliceNum, Interpolation easing) {
      instance.duration = duration;
      instance.direction = direction;
      instance.easing = easing;
      instance.sliceIndex.clear();

      for(int i = 0; i < sliceNum; ++i) {
         instance.sliceIndex.add(Integer.valueOf(i));
      }

      instance.sliceIndex.shuffle();
      return instance;
   }

   public void render(Batch batch, Texture texture, Texture texture1, float alpha) {
      float width = (float) texture.getWidth();
      float height = (float) texture.getHeight();
      int var10 = (int)(width / (float)this.sliceIndex.size);
      Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      Gdx.gl.glClear(16384);
      batch.begin();
      batch.draw(texture, 0.0F, 0.0F, 0.0F, 0.0F, width, height, 1.0F, 1.0F, 0.0F, 0, 0, texture.getWidth(), texture.getHeight(), false, true);
      width = alpha;
      if(this.easing != null) {
         width = this.easing.apply(alpha);
      }

      alpha = 0.0F;

      for(int var9 = 0; var9 < this.sliceIndex.size; ++var9) {
         float var7 = (float)(var9 * var10);
         float var8 = ((float)((Integer)this.sliceIndex.get(var9)).intValue() / (float)this.sliceIndex.size + 1.0F) * height;
         switch(this.direction) {
         case 1:
            alpha = -var8 + var8 * width;
            break;
         case 2:
            alpha = var8 - var8 * width;
            break;
         case 3:
            if(var9 % 2 == 0) {
               alpha = -var8 + var8 * width;
            } else {
               alpha = var8 - var8 * width;
            }
         }

         batch.draw(texture1, var7, alpha, 0.0F, 0.0F, (float)var10, height, 1.0F, 1.0F, 0.0F, var9 * var10, 0, var10, texture1.getHeight(), false, true);
      }

      batch.end();
   }
}
