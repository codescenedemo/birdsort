package com.core.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;

public class GTransitionRotationScale extends GTransition {
   public static final int ROTATION_SCALE = 1;
   public static final int SCALE = 0;
   private static final GTransitionRotationScale instance = new GTransitionRotationScale();
   private int type;

   public static GTransitionRotationScale init(float duration, int type) {
      instance.duration = duration;
      instance.type = type;
      return instance;
   }

   public void render(Batch batch, Texture texture, Texture texture1, float alpha) {
      float width = (float) texture.getWidth();
      float height = (float) texture.getHeight();
      float var5;
      if(this.type == 1) {
         var5 = Interpolation.sineIn.apply(alpha);
      } else {
         var5 = 0.0F;
      }


      alpha = Interpolation.fade.apply(alpha);
      Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      Gdx.gl.glClear(16384);
      batch.begin();
      batch.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      batch.draw(texture1, 0.0F, 0.0F, width / 2.0F, height / 2.0F, width, height, 1.0F, 1.0F, -360.0F * var5, 0, 0, texture1.getWidth(), texture1.getHeight(), false, true);
      batch.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      batch.draw(texture, 0.0F, 0.0F, width / 2.0F, height / 2.0F, width, height, 1.0F - alpha, 1.0F - alpha, 360.0F * var5, 0, 0, texture.getWidth(), texture.getHeight(), false, true);
      batch.end();
   }
}
