package com.core.transitions;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;

public abstract class GTransition {
   public float duration;

   public abstract void render(Batch batch, Texture texture, Texture texture1, float alpha);
}
