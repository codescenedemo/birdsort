package com.core.transitions;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.math.Interpolation;

public class GTransitionFade extends GTransition {
   private static final GTransitionFade instance = new GTransitionFade();

   public static GTransitionFade init(float duration) {
      instance.duration = duration;
      return instance;
   }

   public void render(Batch batch, Texture texture, Texture texture1, float alpha) {
      float var5 = (float) texture.getWidth();
      float var6 = (float) texture.getHeight();
      alpha = Interpolation.fade.apply(alpha);
      Gdx.gl.glClearColor(0.0F, 0.0F, 0.0F, 1.0F);
      Gdx.gl.glClear(16384);
      batch.begin();
      batch.setColor(1.0F, 1.0F, 1.0F, 1.0F);
      batch.draw(texture, 0.0F, 0.0F, 0.0F, 0.0F, var5, var6, 1.0F, 1.0F, 0.0F, 0, 0, texture.getWidth(), texture.getHeight(), false, true);
      batch.setColor(1.0F, 1.0F, 1.0F, alpha);
      batch.draw(texture1, 0.0F, 0.0F, 0.0F, 0.0F, var5, var6, 1.0F, 1.0F, 0.0F, 0, 0, texture1.getWidth(), texture1.getHeight(), false, true);
      batch.end();
   }
}
