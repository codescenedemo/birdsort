package com.core.action.exAction;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.ObjectMap;
import com.core.util.GLayer;

public class GScreenShakeAction extends Action {
  float curOffX;
  float curOffY;
  float duration;
  float time;
  float range;
  ObjectMap<Group, Vector2> layers = new ObjectMap<>();

  public static GScreenShakeAction screenShake(float duration, GLayer... var1) {
    GScreenShakeAction shakeAction = (GScreenShakeAction) Actions.action(GScreenShakeAction.class);
    shakeAction.duration = duration;

    for (GLayer gGroup : var1) {
      shakeAction.layers.put(gGroup.getGroup(), new Vector2(gGroup.getGroup().getX(), gGroup.getGroup().getY()));
    }

    shakeAction.time = 0.0F;
    shakeAction.range = 3;
    return shakeAction;
  }

  public boolean act(float var1) {
    if (this.time == 0.0F) {
      this.begin();
    }

    if (this.time >= this.duration) {
      end();
      return true;
    } else {
      float x = MathUtils.random(-range, range);
      float y = MathUtils.random(-range, range);
      this.translateLayer(x - this.curOffX, y - this.curOffY);
      this.curOffX = x;
      this.curOffY = y;
      this.time += var1;
      return false;
    }
  }

  public void begin() {
    this.curOffY = 0;
    this.curOffX = 0;
  }

  public void end() {
    for (ObjectMap.Entry<Group, Vector2> entry : layers.entries()) {
      entry.key.setPosition(entry.value.x, entry.value.y);
    }
  }

  public void translateLayer(float x, float y) {
    for (Group group : layers.keys()) {
      group.moveBy(x, y);
    }
  }
}
