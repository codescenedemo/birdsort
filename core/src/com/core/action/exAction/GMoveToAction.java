package com.core.action.exAction;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.MoveToAction;

public class GMoveToAction extends MoveToAction {
    private boolean isRotate;
    private float degree;
    private static Vector2 tmp = new Vector2();

    public boolean isRotate() {
        return isRotate;
    }

    public void setRotate(boolean rotate) {
        isRotate = rotate;
    }

    public static GMoveToAction moveTo(float x, float y, float duration, boolean isRotate, Interpolation interpolation) {
        GMoveToAction action = (GMoveToAction)Actions.action(GMoveToAction.class);
        action.setPosition(x, y);
        action.setDuration(duration);
        action.setInterpolation(interpolation);
        action.setRotate(isRotate);
        return action;
    }
    protected void begin() {
        super.begin();
        calculateDegree();
    }


    public void calculateDegree() {
        tmp.set(this.getActor().getX() - this.getX(), this.getActor().getY() - this.getY());
        this.degree = tmp.angle();
    }

    protected void update (float percent){
        super.update(percent);
        if(isRotate)
            this.getActor().setRotation(degree+90);
    }
}
