package com.core.action.exAction;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.RelativeTemporalAction;
import com.badlogic.gdx.utils.Pool;
import com.core.util.GPool;

public class GSpeedMoveAction extends RelativeTemporalAction {
    private static GPool<GSpeedMoveAction> pool = GPool.get((Class)GSpeedMoveAction.class, (int)800);
    protected float degree;
    protected float deltaDegree;
    protected float deltaSpeed;
    protected boolean deltaSpeedChange;
    protected float maxSpeed;
    protected float minSpeed;
    protected float speed;
    protected boolean speedLimit;

    static {
    }

    public GSpeedMoveAction() {
    }

    public static GSpeedMoveAction deltaSpeedMove(float f, float f2, float f3, float f4, boolean bl, float f5, float f6, Interpolation interpolation) {
        return GSpeedMoveAction.deltaSpeedMove((GSpeedMoveAction)((GSpeedMoveAction) Actions.action((Class)GSpeedMoveAction.class)), (float)f, (float)f2, (float)f3, (float)f4, (boolean)bl, (float)f5, (float)f6, (Interpolation)interpolation);
    }

    public static GSpeedMoveAction deltaSpeedMove(GSpeedMoveAction gSpeedMoveAction, float f, float f2, float f3, float f4, boolean bl, float f5, float f6, Interpolation interpolation) {
        gSpeedMoveAction.setSpeed(f);
        gSpeedMoveAction.setMinSpeed(f3);
        gSpeedMoveAction.setMaxSpeed(f4);
        gSpeedMoveAction.setDeltaSpeed(f2);
        gSpeedMoveAction.setDeltaSpeedChange(bl);
        gSpeedMoveAction.setDegree(f5);
        gSpeedMoveAction.setDuration(f6);
        gSpeedMoveAction.setInterpolation(interpolation);
        gSpeedMoveAction.setDeltaDegree(0.0f);
        gSpeedMoveAction.speedLimit = false;
        gSpeedMoveAction.setPool((Pool)pool);
        return gSpeedMoveAction;
    }

    public static GSpeedMoveAction speedMove(float speed, float deltaSpeed, float degree, float deltaDegree, float duration, Interpolation interpolation) {
        return GSpeedMoveAction.speedMove(pool.obtain(), speed, deltaSpeed,  degree, deltaDegree, duration, interpolation);
    }

    public static GSpeedMoveAction speedMove(float speed, float degree, float duration, Interpolation interpolation) {
        return GSpeedMoveAction.speedMove(speed, 0.0f, degree, 0.0f, duration, interpolation);
    }

    public static GSpeedMoveAction speedMove(GSpeedMoveAction gSpeedMoveAction, float speed, float deltaSpeed, float degree, float deltaDegree, float duration, Interpolation interpolation) {
        gSpeedMoveAction.setSpeed(speed);
        gSpeedMoveAction.setDeltaSpeed(deltaSpeed);
        gSpeedMoveAction.setDegree(degree);
        gSpeedMoveAction.setDeltaDegree(deltaDegree);
        gSpeedMoveAction.setDuration(duration);
        gSpeedMoveAction.setInterpolation(interpolation);
        gSpeedMoveAction.speedLimit = false;
        gSpeedMoveAction.setPool((Pool)pool);
        return gSpeedMoveAction;
    }

    public static GSpeedMoveAction speedMove(GSpeedMoveAction gSpeedMoveAction, float f, float f2, float f3, Interpolation interpolation) {
        return GSpeedMoveAction.speedMove((GSpeedMoveAction)gSpeedMoveAction, (float)f, (float)0.0f, (float)f2, (float)0.0f, (float)f3, (Interpolation)interpolation);
    }

    public float getDegree() {
        return this.degree;
    }

    public float getDeltaDegree() {
        return this.deltaDegree;
    }

    public float getDeltaSpeed() {
        return this.deltaSpeed;
    }

    public float getMaxSpeed() {
        return this.maxSpeed;
    }

    public float getMinSpeed() {
        return this.minSpeed;
    }

    public float getSpeed() {
        return this.speed;
    }

    public boolean isDeltaSpeedChange() {
        return this.deltaSpeedChange;
    }

    public void setDegree(float f) {
        this.degree = f;
    }

    public void setDeltaDegree(float f) {
        this.deltaDegree = f;
    }

    public void setDeltaSpeed(float f) {
        this.deltaSpeed = f;
    }

    public void setDeltaSpeedChange(boolean bl) {
        this.deltaSpeedChange = bl;
    }

    public void setMaxSpeed(float f) {
        this.maxSpeed = f;
    }

    public void setMinSpeed(float f) {
        this.minSpeed = f;
    }

    public void setSpeed(float f) {
        this.speed = f;
    }

    protected void updateRelative(float percentDelta) {
        //Gdx.app.log("f", "" + percentDelta );
        float f = this.getDuration() * percentDelta;
        float f2 = this.speed;
        float f3 = MathUtils.cosDeg((float)this.degree);
        float f4 =  this.speed;
        float f5 = MathUtils.sinDeg((float)this.degree);
        this.speed += this.deltaSpeed * f;
        if (this.speedLimit && this.speed >= this.maxSpeed || this.speed <= this.minSpeed) {
            this.speed = Math.min(Math.max(this.speed, this.minSpeed), this.maxSpeed);
            if (this.deltaSpeedChange) {
                this.deltaSpeed = - this.deltaSpeed;
            }
        }
        this.degree = (this.degree + this.deltaDegree * f + 360.0f) % 360.0f;
        //Gdx.app.log("y", ""+f4 * f5 * f);
        this.actor.moveBy(f2 * f3 * f, f4 * f5 * f);
    }
}
