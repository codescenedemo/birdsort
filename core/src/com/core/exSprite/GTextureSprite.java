package com.core.exSprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class GTextureSprite extends Actor {
   private float skewX;
   private float skewY;
   private TextureAtlas.AtlasSprite sprite;

   public GTextureSprite(Texture texture) {
      this((Texture)texture, 0, 0, texture.getWidth(), texture.getHeight());
   }

   public GTextureSprite(Texture texture, int x, int y, int width, int height) {
      this(new TextureAtlas.AtlasRegion(texture, x, y, width, height));
   }

   public GTextureSprite(TextureAtlas.AtlasRegion atlasRegion) {
      this.sprite = new TextureAtlas.AtlasSprite(atlasRegion);
   }

   public GTextureSprite(TextureRegion textureRegion) {
      this((TextureRegion)textureRegion, 0, 0, textureRegion.getRegionWidth(), textureRegion.getRegionHeight());
   }

   public GTextureSprite(TextureRegion textureRegion, int x, int y, int width, int height) {
      this(textureRegion.getTexture(), x, y, width, height);
   }

   public void draw(Batch batch, float parentAlpha) {
      this.sprite.draw(batch, parentAlpha);
   }

   public void rotate(float amountInDegrees) {
      super.rotateBy(amountInDegrees);
      this.sprite.rotate(amountInDegrees);
   }

   public void scale(float scale) {
      super.scaleBy(scale);
      this.sprite.setScale(this.sprite.getScaleX() + scale, this.sprite.getScaleY() + scale);
   }

   public void setBounds(float x, float y, float width, float height) {
      super.setBounds(x, y, width, height);
      this.sprite.setBounds(x, y, width, height);
   }

   public void setColor(float r, float g, float b, float a) {
      super.setColor(r, g, b, a);
      this.sprite.setColor(r, g, b, a);
   }

   public void setColor(Color color) {
      super.setColor(color);
      this.sprite.setColor(color);
   }

   public void setHeight(float height) {
      super.setHeight(height);
      this.sprite.setSize(this.sprite.getWidth(), height);
   }

   public void setLeftBottom(float x, float y) {
      float[] var3 = this.sprite.getVertices();
      var3[5] += x;
      var3[6] += y;
   }

   public void setLeftTop(float x, float y) {
      float[] var3 = this.sprite.getVertices();
      var3[0] += x;
      var3[1] += y;
   }

   public void setOrigin(float originX, float originY) {
      super.setOrigin(originX, originY);
      this.sprite.setOrigin(originX, originY);
   }

   public void setOriginX(float originX) {
      super.setOriginX(originX);
      this.sprite.setOrigin(originX, this.getOriginY());
   }

   public void setOriginY(float originY) {
      super.setOriginY(originY);
      this.sprite.setOrigin(this.getOriginX(), originY);
   }

   public void setPosition(float x, float y) {
      super.setPosition(x, y);
      this.sprite.setPosition(x, y);
   }

   public void setRegion(TextureRegion textureRegion) {
      this.sprite.setRegion(textureRegion);
   }

   public void setRightBottom(float x, float y) {
      float[] var3 = this.sprite.getVertices();
      var3[10] += x;
      var3[11] += y;
   }

   public void setRightTop(float x, float y) {
      float[] var3 = this.sprite.getVertices();
      var3[15] += x;
      var3[16] += y;
   }

   public void setRotation(float degrees) {
      super.setRotation(degrees);
      this.sprite.setRotation(degrees);
   }

   public void setScale(float scaleX, float scaleY) {
      super.setScale(scaleX, scaleY);
      this.sprite.setScale(scaleX, scaleY);
   }

   public void setScaleX(float scaleX) {
      super.setScaleX(scaleX);
      this.sprite.setScale(scaleX, this.getScaleY());
   }

   public void setScaleY(float scaleY) {
      super.setScaleY(scaleY);
      this.sprite.setScale(this.getScaleX(), scaleY);
   }

   public void setSize(float width, float height) {
      super.setSize(width, height);
      this.sprite.setSize(width, height);
   }

   public void setSkew(float skewX, float knewY) {
      this.skewX = skewX;
      this.skewY = knewY;
      float[] vertices = this.sprite.getVertices();

      for(int i = 0; i < vertices.length; i += 5) {
         float var3 = vertices[i];
         float var4 = this.getX();
         float var5 = this.getOriginX();
         vertices[i] += (vertices[i + 1] - (this.getY() + this.getOriginY())) * skewX;
         int var7 = i + 1;
         vertices[var7] += (var3 - (var4 + var5)) * knewY;
      }

   }

   public void setWidth(float width) {
      super.setWidth(width);
      this.sprite.setSize(width, this.sprite.getHeight());
   }

   public void setX(float x) {
      super.setX(x);
      this.sprite.setX(x);
   }

   public void setY(float y) {
      super.setY(y);
      this.sprite.setY(y);
   }

   public void size(float size) {
      super.sizeBy(size);
      this.sprite.setSize(this.sprite.getWidth() + size, this.sprite.getHeight() + size);
   }

   public void translate(float x, float y) {
      super.moveBy(x, y);
      this.sprite.translate(x, y);
   }
}
