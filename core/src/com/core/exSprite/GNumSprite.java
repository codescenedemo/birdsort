package com.core.exSprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.core.util.GRes;

import java.util.HashMap;

public class GNumSprite extends Actor {
   public static final byte HCENTER_BOTTOM = 5;
   public static final byte HCENTER_TOP = 3;
   public static final byte HCENTER_VCENTER = 4;
   public static final byte LEFT_BOTTOM = 2;
   public static final byte LEFT_TOP = 0;
   public static final byte LEFT_VCENTER = 1;
   public static final byte RIGHT_BOTTOM = 8;
   public static final byte RIGHT_TOP = 6;
   public static final byte RIGHT_VCENTER = 7;
   private int anchor;
   private HashMap<Character, TextureAtlas.AtlasRegion> charMap;
   private String extraSymbol;
   private int num;
   private int numH;
   private String numStr;
   private int numW;
   private int spacing;
   private int type;

   public GNumSprite(TextureAtlas.AtlasRegion atlasRegion, int num, int spacing) {
      this(atlasRegion, num, (String)null, spacing, 0);
   }

   public GNumSprite(TextureAtlas.AtlasRegion atlasRegion, int num, int spacing, byte anchor) {
      this(atlasRegion, num, (String)null, spacing, anchor);
   }

   public GNumSprite(TextureAtlas.AtlasRegion atlasRegion, int num, String symbol, int spaceing, int anchor) {
      this.numStr = "0";
      this.charMap = new HashMap<>();
      this.type = 0;
      this.setAtlasRegion(atlasRegion, symbol);
      this.spacing = spaceing;
      this.anchor = anchor;
      this.setNum(num);
   }

   public GNumSprite(TextureAtlas.AtlasRegion atlasRegion, String numStr, String sysmbol, int spacing, int anchor) {
      this.numStr = "0";
      this.charMap = new HashMap<>();
      this.type = 1;
      this.setAtlasRegion(atlasRegion, sysmbol);
      this.spacing = spacing;
      this.anchor = anchor;
      this.setNum(numStr);
   }

   public void draw(Batch batch, float parentAlpha) {
      if(this.numStr != null) {
         int var13 = this.numStr.length();
         int var14 = this.numW;
         int var15 = this.spacing;
         int var16 = this.spacing;
         float var3 = this.getScaleX();
         float var4 = this.getScaleY();
         float var5 = this.getRotation();
         float var6 = MathUtils.sinDeg(var5);
         float var7 = MathUtils.cosDeg(var5);
         float var8 = (float)((var13 * (var14 + var15) - var16) * (-this.anchor / 3)) * var3 / 2.0F;
         float var9 = (float)(-this.anchor % 3 * this.numH / 2);

         for(var13 = 0; var13 < this.numStr.length(); ++var13) {
            TextureAtlas.AtlasRegion var17 = (TextureAtlas.AtlasRegion)this.charMap.get(this.numStr.charAt(var13));
            if(var17 != null) {
               float var10 = (float)((this.numW + this.spacing) * var13) * var3 + var8;
               float var11 = this.getX();
               float var12 = this.getY();
               Color var18 = this.getColor();
               batch.setColor(var18.r, var18.g, var18.b, var18.a * parentAlpha);
               batch.draw(var17, var11 + var10 * var7, var10 * var6 + var12 + var9, 0.0F, -var9, (float)this.numW, (float)this.numH, var3, var4, var5);
            }
         }
      }

   }

   public float getHeight() {
      return (float)this.numH;
   }

   public float getWidth() {
      return (float)(this.numStr.length() * (this.numW + this.spacing) - this.spacing);
   }

   public void setAnchor(int anchor) {
      this.anchor = anchor;
   }

   public void setAtlasRegion(TextureAtlas.AtlasRegion atlasRegion) {
      this.setAtlasRegion(atlasRegion, this.extraSymbol);
   }

   public void setAtlasRegion(TextureAtlas.AtlasRegion atlasRegion, String symbol) {
      int var4;
      if(symbol == null) {
         var4 = 10;
      } else {
         var4 = symbol.length() + 10;
      }

      this.numW = atlasRegion.getRegionWidth() / var4;
      this.numH = atlasRegion.getRegionHeight();
      this.setHeight((float)this.numH);
      this.charMap.clear();
      char var3 = 48;

      for(int i = 0; i < var4; ++i) {
         TextureAtlas.AtlasRegion var6 = GRes.createRegionFromAtlasRegion(atlasRegion, this.numW * i, 0, this.numW, this.numH);
         if(i < 10) {
            this.charMap.put(var3, var6);
            ++var3;
         } else {
            this.charMap.put(symbol.charAt(i - 10), var6);
         }
      }

   }

   public void setNum(int num) {
      if(this.num != num || this.type != 0) {
         this.type = 0;
         this.num = num;
         this.numStr = Integer.toString(num);
      }
   }

   public void setNum(String numStr) {
      this.type = 1;
      this.numStr = numStr;
   }

   public void setSpacing(int spacing) {
      this.spacing = spacing;
   }
}
