package com.core.exSprite;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Null;
import com.core.util.GStage;
import com.esotericsoftware.spine.AnimationState;
import com.esotericsoftware.spine.AnimationStateData;
import com.esotericsoftware.spine.Bone;
import com.esotericsoftware.spine.Skeleton;
import com.esotericsoftware.spine.SkeletonBounds;
import com.esotericsoftware.spine.SkeletonData;
import com.esotericsoftware.spine.SkeletonRenderer;
import com.esotericsoftware.spine.Slot;
import com.esotericsoftware.spine.attachments.Attachment;
import com.esotericsoftware.spine.attachments.BoundingBoxAttachment;
import com.esotericsoftware.spine.attachments.MeshAttachment;
import com.game.GMain;

public class GSpineSprite extends Actor {
  private static SkeletonRenderer skeletonRenderer = getSkeletonRenderer();

  public Skeleton skeleton;
  public AnimationState state;
  AnimationState.TrackEntry entry;
  boolean is_mesh;

  public String curAnimation = "";
  public String nextAnimation = "";
  public boolean nextLoop = false;
  public Rectangle bound = null;

  static SkeletonRenderer getSkeletonRenderer() {
    SkeletonRenderer s_r = new SkeletonRenderer();
    s_r.setPremultipliedAlpha(true);
    return s_r;
  }

  public GSpineSprite(String name) {
    SkeletonData data2D = GMain.getAssetManager().getSkeletonData(name);
    skeleton = new Skeleton(data2D);
    AnimationStateData stateData2D = new AnimationStateData(data2D);
    state = new AnimationState(stateData2D);
    Polygon poly = getBoundingBox("hit_area");
    if (poly != null)
      bound = getBoundingBox("hit_area").getBoundingRectangle();
  }

  public GSpineSprite(SkeletonData data2D) {
    skeleton = new Skeleton(data2D);
    AnimationStateData stateData2D = new AnimationStateData(data2D);
    state = new AnimationState(stateData2D);
    is_mesh = checkIsMesh();
    Polygon poly = getBoundingBox("hit_area");
    if (poly != null)
      bound = getBoundingBox("hit_area").getBoundingRectangle();
  }

  private void SetAnimation(int trackIndex, String animation, boolean loop, boolean force) {
    if (animation.equals(curAnimation) && !force)
      return;
    this.entry = state.setAnimation(trackIndex, animation, loop);
    curAnimation = animation;

  }

  public float getWidth() {
    return skeleton.getData().getWidth();
  }

  public float getHeight() {
    return skeleton.getData().getHeight();
  }

  @Override
  public void draw(Batch batch, float parentAlpha) {
    skeletonRenderer.draw(batch, skeleton);
    batch.setBlendFunction(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
  }

  @Override
  public void act(float delta) {
    state.update(delta);
    state.apply(skeleton);
    skeleton.updateWorldTransform();
    if (entry != null && entry.isComplete() && entry.getLoop() == false && !nextAnimation.equals("")) {
      SetAnimation(0, nextAnimation, nextLoop, false);
    }

    super.act(delta);
  }

  public Polygon getPolygon(String slot_name) {
    Slot slot = skeleton.findSlot(slot_name);
    if (slot == null)
      return null;
    BoundingBoxAttachment boundingBoxAttachment = (BoundingBoxAttachment) slot.getAttachment();

    SkeletonBounds bounds = new SkeletonBounds();
    bounds.update(skeleton, true);
    FloatArray transformedBoundingBox = bounds.getPolygon(boundingBoxAttachment);

    float[] f = transformedBoundingBox.toArray();
    // float[] f = boundingBoxAttachment.getVertices();//transformedBoundingBox.toArray();
    Polygon polygon = new Polygon(f);
    //Rectangle rec = polygon.getBoundingRectangle();
    //polygon.setOrigin(rec.x+rec.width/2,rec.y+rec.height/2);

    //Vector2 pos = localToStageCoordinates(new Vector2());
    //polygon.setPosition(this.getX(), this.getY());
    return polygon;
  }

  static FloatArray polygon = new FloatArray();// polygonPool.obtain();

  public Polygon getWorldBoundingBox(String slot_name) {       //get bounding box theo world position
    Slot slot = skeleton.findSlot(slot_name);
    if (slot == null)
      return null;

    BoundingBoxAttachment boundingBox = (BoundingBoxAttachment) slot.getAttachment();
    boundingBox.computeWorldVertices(slot, 0, boundingBox.getWorldVerticesLength(),
            polygon.setSize(boundingBox.getWorldVerticesLength()), 0, 2);

    return new Polygon(polygon.toArray());
  }

  public Polygon getBoundingBox(String slot_name) {
    Slot slot = skeleton.findSlot(slot_name);
    if (slot == null)
      return null;

    BoundingBoxAttachment boundingBox = (BoundingBoxAttachment) slot.getAttachment();
//        boundingBox.computeWorldVertices(slot, 0, boundingBox.getWorldVerticesLength(),
//                polygon.setSize(boundingBox.getWorldVerticesLength()), 0, 2);
//

    return new Polygon(boundingBox.getVertices());
  }

  public void drawBound(ShapeRenderer shapeRenderer, Batch batch) {
    shapeRenderer.setProjectionMatrix(GStage.getCamera().combined);
    shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
    shapeRenderer.setColor(Color.RED);
    batch.end();
    SkeletonBounds bounds = new SkeletonBounds();
    bounds.update(skeleton, true);
    shapeRenderer.rect(bounds.getMinX(), bounds.getMinY(), bounds.getWidth(), bounds.getHeight());
    Array<FloatArray> polygons = bounds.getPolygons();
    for (int i = 0, n = polygons.size; i < n; i++) {
      FloatArray polygon = polygons.get(i);
      shapeRenderer.polygon(polygon.items, 0, polygon.size);
    }

    shapeRenderer.end();
    batch.begin();
  }

  public Vector2 getBonePosition(Bone bone) {
    Vector2 pos = new Vector2(bone.getWorldX(), bone.getWorldY());
    pos = localToStageCoordinates(pos);
    return pos;
  }

  public float getBoneRotation(Bone bone) {
    float rotation = bone.getWorldRotationY() + 90;
//        if (rotation<0)
//            rotation=360-Math.abs(rotation);
    return rotation;
  }

  public void setBoneRotation(Bone bone, float angle) {
    float bone_world_rotation = bone.getWorldRotationX();
    if (bone_world_rotation < 0)
      bone_world_rotation = 360 - Math.abs(bone_world_rotation);
    float rotation = bone.getRotation() + angle - bone_world_rotation;
    bone.setRotation(rotation);
  }

  public void setColor(Color color) {
    for (int i = 0; i < skeleton.getSlots().size; i++) {
      Slot slot = skeleton.getSlots().get(i);
      slot.getColor().set(color);
    }
  }

  public void setDarkColor(Color color) {
    for (int i = 0; i < skeleton.getSlots().size; i++) {
      Slot slot = skeleton.getSlots().get(i);
      Color c = slot.getDarkColor();
      if (c != null)
        c.set(color);
    }
  }

  boolean checkIsMesh() {
    Array<Slot> drawOrder = skeleton.getDrawOrder();
    for (int i = 0, n = drawOrder.size; i < n; i++) {
      Slot slot = drawOrder.get(i);
      Attachment attachment = slot.getAttachment();
      if (attachment instanceof MeshAttachment) {
        return true;
      }
    }
    return false;
  }

  public void changeAnimation(String animation, boolean loop) {
    changeAnimation(animation, loop, false);
  }
  public void changeAnimation(String animation, boolean loop, boolean force) {
    this.nextAnimation = "";
    this.nextLoop = false;
    this.SetAnimation(0, animation, loop, force);
  }

  public void changeAnimation(String animation, String nextAnimation, boolean loop, boolean force) {
    this.nextAnimation = nextAnimation;
    this.nextLoop = loop;
    this.SetAnimation(0, animation, false, force);

  }

  public void setTimeScale(float scale) {
    state.setTimeScale(scale);
  }

  @Override
  public void setPosition(float x, float y) {
    super.setPosition(x, y);
    skeleton.setPosition(this.getX(), this.getY());
  }

  @Override
  public void setPosition(float x, float y, int alignment) {
    super.setPosition(x, y, alignment);
    skeleton.setPosition(x, y);
  }

  @Override
  public void setScale(float x, float y) {
    super.setScale(x, y);
    skeleton.setScale(x, y);
  }

  @Override
  public void setScale(float scalexy) {
    super.setScale(scalexy);
    skeleton.setScale(scalexy, scalexy);
  }

  @Override
  public void moveBy(float x, float y) {
    skeleton.setPosition(x, y);
  }

  @Override
  public void setRotation(float degrees) {
    super.setRotation(degrees);
    skeleton.getRootBone().setRotation(degrees);
  }

  @Override
  public @Null
  Actor hit(float x, float y, boolean touchable) {
    if (touchable && this.getTouchable() != Touchable.enabled) return null;
    if (!isVisible() || bound == null) return null;
    return x >= bound.x && x < bound.width && y >= bound.y && y < bound.height ? this : null;
  }

  public void setSkin(String name){
    skeleton.setSkin(name);
  }
}
