package com.core.exSprite;

import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.core.util.GAssetsManager;
import com.game.GMain;

public class GAnimationFrame extends Actor {
  Animation<TextureRegion> animation;
  float elapsedTime = 0;
  float PPM = 1;

  public void init(String atlasName, float frameDuration) {
    TextureAtlas atlas = GMain.getAssetManager().getTextureAtlas(atlasName);
    animation = new Animation<>(frameDuration, atlas.getRegions());
    elapsedTime = 0;

    setPPM(1);
  }

  public void init(Array<TextureRegion> regions, float frameDuration) {
    animation = new Animation<>(frameDuration, regions);
    elapsedTime = 0;

    setPPM(1);
  }

  public void setPPM(float ppm) {
    this.PPM = ppm;

    TextureRegion tr = animation.getKeyFrame(0);
    setSize(tr.getRegionWidth() / PPM, tr.getRegionHeight() / PPM);
    setOrigin(Align.center);
  }


  @Override
  public void draw(Batch batch, float parentAlpha) {
    TextureRegion tr = animation.getKeyFrame(elapsedTime, true);
    batch.draw(tr, this.getX(), this.getY(), this.getOriginX(), this.getOriginY(), getWidth(), getHeight(), this.getScaleX(), this.getScaleY(), this.getRotation());
  }

  @Override
  public void act(float delta) {
    elapsedTime += delta;
    super.act(delta);
  }

  public void resetElapsedTime() {
    elapsedTime = 0;
  }

  public boolean isCompleted() {
    return animation.isAnimationFinished(elapsedTime);
  }
}
