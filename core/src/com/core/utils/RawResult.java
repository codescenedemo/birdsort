package com.core.utils;

public interface RawResult {
  void onResult(String rawData);
}
