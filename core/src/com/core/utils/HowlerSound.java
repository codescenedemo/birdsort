package com.core.utils;

import com.badlogic.gdx.audio.Sound;

@SuppressWarnings("unused")
public class HowlerSound implements Sound {




    public static void loadSound(String name, String md5name, boolean isLoop, boolean autoPlay){
        loadHowlerSound(name, md5name, isLoop, autoPlay);
    }

    public static void pauseSound(String name){
        pauseHowlerSound(name);
    }

    public static void stopSound(String name){
        stopHowlerSound("sound/" + name);
    }

    public static void resumeSound(String name){
        playSound(name);
    }

    public static void setVolume(String name, float volume){
        setHowlerSoundVolume(name, volume);
    }

    public static void setLooping(String name, boolean loop){
        setHowlerSoundLooping(name, loop);
    }

    public static void playSound(String name){
        playHowlerSound("sound/" + name);
    }

    public static void playMusic(String name) {
        playHowlerSound(name);
    }

    private static native void setHowlerSoundVolume(String name , float volume)/*-{
            var sound = $wnd.sounds[name];
            if(sound)
                sound.volume(volume);
    }-*/;

    private static native void setHowlerSoundLooping(String name , boolean loop)/*-{
            var sound = $wnd.sounds[name];
            if(sound)
                sound.loop(loop);
    }-*/;

    private static native void pauseHowlerSound(String name )/*-{
            var sound = $wnd.sounds[name];

            if(sound)
                sound.pause();
    }-*/;

    private static native void stopHowlerSound(String name )/*-{
            var sound = $wnd.sounds[name];

            if(sound)
                sound.stop();
    }-*/;

    private static native void playHowlerSound(String name )/*-{
            var sound = $wnd.sounds[name];
            if(sound)
                sound.play();
    }-*/;

    public static native boolean isMusicPlaying(String name)/*-{
        var sound = $wnd.sounds[name];
        if (sound)
            return sound.playing();
        else
            return false;
}-*/;

    private static native void loadHowlerSound(String name, String md5name, boolean isLoop, boolean autoPlay)/*-{
            if(!$wnd.sounds){
                $wnd.sounds = [];
            }
            var sound = new $wnd.Howl({
              src: [md5name],
              autoplay: autoPlay,
              loop: isLoop
            });
            sound.once('load', function(){
              $wnd.console.log("sound " + md5name + " loaded");
              $wnd.sounds[name] = sound;
            });
    }-*/;



    private String name;
    public HowlerSound(String name) {
        this.name = name;
    }

    @Override
    public long play() {
        HowlerSound.pauseSound(name);
        return 0;
    }

    @Override
    public long play(float volume) {
        HowlerSound.playSound(name);
        return 0;
    }

    @Override
    public long play(float volume, float pitch, float pan) {
        return 0;
    }

    @Override
    public long loop() {
        return 0;
    }

    @Override
    public long loop(float volume) {
        return 0;
    }

    @Override
    public long loop(float volume, float pitch, float pan) {
        return 0;
    }

    @Override
    public void stop() {
        HowlerSound.stopSound(name);
    }

    @Override
    public void pause() {
    }

    @Override
    public void resume() {
    }

    @Override
    public void dispose() {

    }

    @Override
    public void stop(long soundId) {
        HowlerSound.stopSound(name);
    }

    @Override
    public void pause(long soundId) {
    }

    @Override
    public void resume(long soundId) {
    }

    @Override
    public void setLooping(long soundId, boolean looping) {

    }

    @Override
    public void setPitch(long soundId, float pitch) {

    }

    @Override
    public void setVolume(long soundId, float volume) {

    }

    @Override
    public void setPan(long soundId, float pan, float volume) {

    }
}