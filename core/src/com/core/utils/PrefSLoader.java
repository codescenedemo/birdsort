package com.core.utils;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Preferences;
import com.badlogic.gdx.utils.Json;
import com.core.model.Session;
import com.game.GMain;

public class PrefSLoader implements SessionLoader {
  protected Json serializer;
  protected Preferences prefs;
  protected Session session;
  protected String key;
  public PrefSLoader(String key) {
    serializer = new Json();
    this.serializer.setIgnoreUnknownFields(true);
    prefs = Gdx.app.getPreferences("def");
    session = Session.ofDefault();
    this.key = key;
  }
  @Override
  public void load(LoadResult res){
    String val = prefs.getString(key, "");

    try {
      session = serializer.fromJson(Session.class, val);
      session.reBalance();
      res.onResult(session);
    }
    catch (Exception e) {
      GMain.platform().log("[session] " + e.getMessage());
      session = Session.ofDefault();
      res.onResult(session);
    }
  }

  @Override
  public void sync(){
    String val = serializer.toJson(session, Session.class);
    prefs.putString(key, val);
    prefs.flush();
  }
}
