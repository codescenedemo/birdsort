package com.core.utils.inject;

/*
Representing [data]effect handling protocols:
  - ExtArgs: External Argument, use in case 4 integer number is not enough to express the context
  - [1,1,100,0]
    + First argument mean the operator: +,-,*,/ or random or any function like ax + b
    + Second argument represent the field of the object to store the value after calculation
    + Third and last argument will be the coefficient of the function or something like that's :)

This is the master interface (one interface to rule them all!!)
 */

import java.util.List;

@FunctionalInterface
@SuppressWarnings("unused")
public interface OperationHandler {
  String apply(final List<String> fmt);
}