package com.core.utils.inject;

import com.badlogic.gdx.utils.ObjectMap;

import java.util.List;

@SuppressWarnings("unused")
public class FlexOperation implements OperationHandler {
  private final ObjectMap<String, OperationHandler> subHandlers;

  public FlexOperation() {
    subHandlers = new ObjectMap<>();
  }

  public void addHandlers(String subField, OperationHandler subHandler) {
    subHandlers.put(subField, subHandler);
  }

  @Override
  public String apply(List<String> fmt) {
    return subHandlers.get(fmt.get(0)).apply(fmt);
  }
}