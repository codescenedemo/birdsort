package com.core.utils.inject;

import com.badlogic.gdx.utils.IntMap;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.reflect.ClassReflection;
import com.badlogic.gdx.utils.reflect.Field;

@SuppressWarnings("unchecked")
public class ReflectUtil {
  private static final ObjectMap<String, ObjectMap<String, Field>> fieldCache;
  static {
    fieldCache = new ObjectMap<>();
  }

  private static Field getField(Object target, String fieldName) throws Exception {
    String className = target.getClass().getSimpleName();

    ObjectMap<String, Field> cachedClass = fieldCache.get(className);
    if (cachedClass == null) {
      cachedClass = new ObjectMap<>();
      fieldCache.put(className, cachedClass);
    }

    Field cachedField = cachedClass.get(fieldName);
    if (cachedField == null) { // first time inject
      cachedField = ClassReflection.getField(target.getClass(), fieldName);
      cachedClass.put(fieldName, cachedField);
    }
    return cachedField;
  }

  public static void addInt(Object target, String fieldName, int value) {
    try {
      Field field = getField(target, fieldName);
      int oldValue = (int)(field.get(target));
      int newValue = oldValue + value;
      field.set(target, newValue);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  public static void addIntInt(Object target, String fieldName, int key, int value) {
    try {
      Field field = getField(target, fieldName);
      IntMap<Integer> cast = (IntMap<Integer>)(field.get(target));
      if (cast.containsKey(key)) {
        int oldValue = cast.get(key);
        cast.put(key, oldValue + value);
      }
      else
        cast.put(key, value);
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }
}