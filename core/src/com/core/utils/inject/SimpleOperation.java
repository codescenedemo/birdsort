package com.core.utils.inject;

import java.util.List;
/*
Support add integer, put int value into an IntMap

["target", "add", "field", "amount"]           : add "amount" into "field" of "target" (type int)
["target", "m.add", "field", "key", "amount"]  : add "amount" into "key" of IntMap "field" of "target"

Example:
["profile", "add", "money", "300"]              : add 300 into money field of profile object
["inventory", "m.add", "item", "1", "10"]       : add 10 item id 1 into IntMap item of inventory object
 */

@SuppressWarnings("unused")
public class SimpleOperation implements OperationHandler {
  Object target;
  public SimpleOperation(Object target) {
    this.target = target;
  }
  @Override
  public String apply(List<String> fmt) {
    String operation = fmt.get(1);
    switch (operation) {
      case "add" :
        int amount = Integer.parseInt(fmt.get(3));
        ReflectUtil.addInt(target, fmt.get(2), amount);
        return "ok";
      case "m.add":
        int key = Integer.parseInt(fmt.get(3));
        int value = Integer.parseInt(fmt.get(4));
        ReflectUtil.addIntInt(target, fmt.get(2), key, value);
        return "ok";
      default: return "unknownOperation";
    }
  }
}