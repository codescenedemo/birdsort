package com.core.utils;

import com.core.model.Session;

public interface SessionLoader {
  void load(LoadResult res);
  void sync();

  interface LoadResult {
    void onResult(Session session);
  }
}