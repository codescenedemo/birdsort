package com.core.utils.wheel;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.FrameBuffer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

@SuppressWarnings("deprecation")
public class Wheel extends Group {
  WheelConfig wc;
  Image wheelCircle;
  Vector2 cp;
  float acc = 0;
  EventListener listener;
  boolean muteDrawing = false;
  boolean lock = false;
  final HashMap<Integer, WheelItem> wheelItemMap = new HashMap<>();
  float WHEEL_ARC = 0;
  int PARTITION = 1;
  WheelRollAction rollAction;

  public Wheel(WheelConfig wc) {
    this.wc = wc;
  }

  public void init() {
    try {
      PARTITION = wc.wheelItems.size();

      if (!validateWheelSetting()) {
        muteDrawing = true;
        throw new IllegalStateException("wheel setting wrong");
      }

      wheelItemMap.clear();

      int totalPercent = 0;
      for (WheelItem item : wc.wheelItems)
        totalPercent += item.percent;

      if (totalPercent != wc.TOTAL_PERCENT)
        throw new IllegalStateException("wheel percent inconsistency: " + totalPercent);

      for (WheelItem item : wc.wheelItems)
        wheelItemMap.put(item.id, item);

      WHEEL_ARC = 360f / PARTITION;
      cp = new Vector2(wc.wheelTex.getRegionWidth()/2f, wc.wheelTex.getRegionHeight()/2f);
      combine();
      wheelCircle = new Image(wc.wheelTex);
      wheelCircle.setOrigin(wheelCircle.getWidth()/2, wheelCircle.getHeight()/2);

      addActor(wheelCircle);
      setWidth(wheelCircle.getWidth());
      setHeight(wheelCircle.getHeight());

      rollAction = new WheelRollAction(this);
    }
    catch (Exception e) {
      muteDrawing = true;
      Gdx.app.error("Wheel error", e.getMessage());
      if (listener != null) {
        listener.error(e.getMessage());
      }
    }
  }

  public void setWheelListener(EventListener listener) {
    this.listener = listener;
  }

  private boolean validateWheelSetting() {
    return  !(PARTITION <= 0                    ||
            wc.TEXT_SPACE <= 0                   ||
            wc.wheelTex == null                  ||
            wc.wheelText == null                 ||
            wc.wheelDot == null                  ||
            wc.lightDot == null                  ||
            wc.wheelItems == null                ||
            listener == null                     ||
            wc.wheelItems.size() != PARTITION    ||
            wc.wheelItems.size()%2 != 0          );
  }

  @Override
  public void act(float delta) {
    acc += delta;
    super.act(delta);
  }

  final private static Vector2 pos = new Vector2();
  @Override
  public void draw(Batch batch, float parentAlpha) {

    super.draw(batch, parentAlpha);

    if (muteDrawing)
      return;

    float               scl               = getScaleX();

    //render light dot
    for (int i = 0; i < PARTITION; i++) {
      if (acc < wc.FLASH_DURATION) {
        if (i %2 == 0) {
          float w         = wc.lightDot.getRegionWidth();
          float h         = wc.lightDot.getRegionHeight();
          float ox        = w/2f;
          float oy        = h/2f;
          pos .set(0, wc.wheelTex.getRegionHeight()* wc.DOT_FLOAT /2f)
                  .rotate(wheelCircle.getRotation() + i*WHEEL_ARC + WHEEL_ARC/2)
                  .add(cp).scl(scl).add(getX(), getY()).sub(w/2, h/2);
          batch.draw(wc.lightDot, pos.x, pos.y, ox, oy, w, h, scl, scl, 0);
        }
      }
      else if (acc > wc.FLASH_DURATION && acc < wc.FLASH_DURATION*2) {
        if (i%2 == 1) {
          float w         = wc.lightDot.getRegionWidth();
          float h         = wc.lightDot.getRegionHeight();
          float ox        = w/2f;
          float oy        = h/2f;
          pos .set(0, wc.wheelTex.getRegionHeight()* wc.DOT_FLOAT /2f)
                  .rotate(wheelCircle.getRotation() + i*WHEEL_ARC + WHEEL_ARC/2)
                  .add(cp).scl(scl).add(getX(), getY()).sub(w/2, h/2);
          batch.draw(wc.lightDot, pos.x, pos.y, ox, oy, w, h, scl, scl, 0);
        }
      }
      else {
        acc = 0;
      }
    }
  }

  private List<Tuple<Vector2, WheelItem>> calcItemPosition() {
    Vector2 roller = Vector2.Zero.setAngle(0).set(0, wc.wheelTex.getRegionHeight()*wc.ITEM_FLOAT/2);
    List<Tuple<Vector2, WheelItem>> result = new ArrayList<>();
    for (int i = 0; i < PARTITION; i++) {
      TextureRegion region = wc.wheelItems.get(i)/*!*/.tex;
      Vector2 pos = roller.cpy().rotate(i*-WHEEL_ARC).add(cp)
              .sub(region.getRegionWidth()/2f, region.getRegionHeight()/2f);
      result.add(new Tuple<>(pos, wc.wheelItems.get(i)));
    }
    return result;
  }

  private List<Vector2> calcDotPosition(float shiftRadian) {
    Vector2 roller = Vector2.Zero.setAngle(0).set(0, wc.wheelTex.getRegionHeight()* wc.DOT_FLOAT /2);
    List<Vector2> result = new ArrayList<>();
    for (int i = 0; i < PARTITION; i++) {
      result.add(roller.cpy().rotate(i*WHEEL_ARC + shiftRadian)
              .add(cp).sub(wc.wheelDot.getRegionWidth()/2f, wc.wheelDot.getRegionHeight()/2f));
    }
    return result;
  }

  private void combine() {
    int                               h             = wc.wheelTex.getRegionWidth();
    int                               w             = wc.wheelTex.getRegionHeight();
    Matrix4                           projector     = new Matrix4();
    Batch                             batch         = new SpriteBatch();
    List<Tuple<Vector2, WheelItem>>   items         = calcItemPosition();
    FrameBuffer fbo;

    projector.setToOrtho2D(0, 0, w, h);
    batch.setProjectionMatrix(projector);
    fbo     = new FrameBuffer(Pixmap.Format.RGBA8888, w, h, false);
    List<Vector2> dotPos = calcDotPosition(WHEEL_ARC / 2);

    fbo.begin();
    batch.begin();
    batch.draw(wc.wheelTex, 0, 0);

    //render qty text
    if (wc.RENDER_TEXT) {
      for (int i = 0; i < PARTITION; i++) {
        AngularSpriteFont sprite;
        if (wc.POLAR_TEXT_RENDER)
          sprite = new PolarSpriteFont(wc.wheelItems.get(i)./*!*/qtyText, wc.wheelText, wc);
        else
          sprite = new AngularSpriteFont(wc.wheelItems.get(i)./*!*/qtyText, wc.wheelText, wc);
        sprite.draw(batch,-i*WHEEL_ARC);
      }
    }

    //render items
    if(wc.RENDER_ITEM) {
      for (int i = 0; i < PARTITION; i++) {
        int     iw        = wc.wheelItems.get(i)./*!*/tex.getRegionWidth();
        int     ih        = wc.wheelItems.get(i).tex.getRegionHeight();
        float   ix        = items.get(i).position.x;
        float   iy        = items.get(i).position.y;
        float   ox        = iw/2f;
        float   oy        = ih/2f;
        float   scl       = wc.ITEM_SCALE;
        float   ang       = -i*WHEEL_ARC;
        batch.draw(items.get(i).wheelItem.tex, ix, iy, ox, oy, iw, ih, scl, scl, ang);

        //render dots
        iw    =  wc.wheelDot.getRegionWidth();
        ih    =  wc.wheelDot.getRegionHeight();
        ix    =  dotPos.get(i)./*!*/x;
        iy    =  dotPos.get(i).y;
        ox    =  iw/2f;
        oy    =  ih/2f;
        scl   =  1;
        ang   =  0;
        batch.draw(wc.wheelDot, ix, iy, ox, oy, iw, ih, scl, scl, ang);
      }
    }
    batch.end();
    fbo.end();
    batch.flush();
    batch.dispose();

    wc.wheelTex = new TextureRegion(fbo.getColorBufferTexture());

    wc.wheelTex.flip(false, !wc.Y_DOWN);
  }

  public void startRollWheel(){
    float vel = -wc.MAX_ANGULAR_VEL;
    int outcome = calcOutcome();
    rollWheel(vel, outcome);
  }


  public void rollWheel(float vel, int outcome) {
    if (outcome >= PARTITION) {
      if (listener != null)
        listener.error("outcome > partition");
    }
    else {
      if (!lock) {
        rollAction.init(vel, outcome);
        wheelCircle.addAction(rollAction);
        lock = true;
      }
    }
  }

  public void stopWheel(int outcome) {
    rollAction.stopRoll(outcome);
  }


  public int calcOutcome() {
    Random rand = new Random();
    int rate = MathUtils.random(0, wc.TOTAL_PERCENT - 1);
    int totalPer = 0;
    for (WheelItem wheelItem : wc.wheelItems) {
      totalPer += wheelItem.percent;
      if (rate < totalPer)
        return wheelItem.id;
    }
    throw new IllegalStateException("Wheel item percents inconsistency");
  }
}