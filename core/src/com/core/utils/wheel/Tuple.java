package com.core.utils.wheel;

@SuppressWarnings("unused")
public class Tuple<T,V> {
  public T position;
  public V wheelItem;

  public Tuple(T pos, V item) {
    position  = pos;
    wheelItem = item;
  }
}