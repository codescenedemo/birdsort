package com.core.utils.wheel;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;

import java.util.List;

public class WheelConfig {
  public  TextureRegion         wheelTex;       //Wheel circle Texture
  public  TextureRegion         wheelDot;       //Wheel dot texture
  public  TextureRegion         lightDot;       //Light dot texture
  public  Sound                 wheelTick;      //Wheel tick sound
  public  BitmapFont            wheelText;      //Wheel font
  public  List<WheelItem>       wheelItems;     //Wheel item information's

  public  float                 TEXT_SPACE          = 6;       //khoảng cách chữ
  public  boolean               Y_DOWN              = false;   //tọa độ y down hay y up
  public  boolean               INFINITE_ROLL       = false;   //true nghĩa là wheel quay vô tận, bên ngoài tự gọi stop tr
                                                               // false thì wheel sẽ tự tính cửa theo item percent và auto dừng

  public boolean                RENDER_ITEM         = true;              //có vẽ icon item hay ko
  public float                  ITEM_SCALE          = 0.4f;              //scale của wheel item
  public float                  ITEM_FLOAT          = 0.66f;             //vẽ item gần hay xa tâm tròn
  public float                  DOT_FLOAT           = 0.92f;             //vẽ dot gần hay xa tâm tròn
  public boolean                RENDER_TEXT         = true;              //có vẽ text số lượng hay ko
  public boolean                POLAR_TEXT_RENDER   = false;             //true: vẽ text phóng từ tâm, false: vẽ xung quanh rìa đường tròn
  public float                  ANGULAR_TEXT_FLOAT  = 0.88f;             //font số lượng nằm gần hay xa tâm tròn, TH vẽ xoay vòng
  public float                  POLAR_TEXT_FLOAT    = 0.44f;             //font số lượng nằm gần hay xa tâm tròn, TH vẽ phóng từ tâm

  public float                  FLASH_DURATION      = 0.4f;             // thời gian nhấp nháy đèn
  public int                    VELOCITY_SCALE      = 15;                // scale vận tốc xoay, càng cao càng xoay nhanh
  public int                    ROLLING_DURATION    = 6;                 // thời gian xoay tính bằng giây
  public int                    TOTAL_PERCENT       = 10000;             // tổng percent của tất cả item
  public int                    MAX_ANGULAR_VEL     = 3000;             // tổng percent của tất cả item
}