package com.core.utils.wheel;

import com.badlogic.gdx.graphics.g2d.TextureRegion;

@SuppressWarnings("unused")
public class WheelItem {
  int               id;
  int               itemId;
  TextureRegion     tex;
  int               qty;
  String            qtyText;
  int               percent;

  public static WheelItem of(TextureRegion tex, int id, int itemId, int qty, String qtyText, int percent) {
    WheelItem r = new WheelItem();
    r.id        = id;
    r.itemId    = itemId;
    r.qtyText   = qtyText;
    r.qty       = qty;
    r.percent   = percent;
    r.tex       = tex;
    return r;
  }

  @Override
  public String toString() {
    return "WheelItem{" +
        "id=" + id +
        ", itemId=" + itemId +
        ", tex=" + tex +
        ", qty=" + qty +
        ", qtyText='" + qtyText + '\'' +
        ", percent=" + percent +
        '}';
  }
  public TextureRegion getRegion(){
    return this.tex;
  }

  public String getQtyText() {
    return this.qtyText;
  }

  public int getItemId() {
    return itemId;
  }

  public int getQty() {
    return qty;
  }
}