package com.core.utils.wheel;

public interface EventListener {
  boolean start();
  void end(WheelItem item);
  void error(String msg);
}