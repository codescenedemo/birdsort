package com.core.utils.wheel;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

import java.util.ArrayList;
import java.util.List;

@SuppressWarnings("unused")
public class AngularSpriteFont {
  List<Sprite>                  composite;
  final private   BitmapFont    font;
  final private   String        text;
  final protected WheelConfig   config;

  private Sprite getGlyphSprite(char ch) {
    BitmapFont.Glyph glyph = font.getData().getGlyph(ch);
    Sprite s =  new Sprite(font.getRegion().getTexture(),
            glyph.srcX,glyph.srcY,glyph.width , glyph.height);

    s.setOrigin(glyph.width/2f, glyph.height/2f);
    return s;
  }

  AngularSpriteFont(String text, BitmapFont font, WheelConfig config) {
    this.text = text;
    this.font = font;
    this.config = config;
    this.composite = new ArrayList<>();
    make();
  }

  private void make() {
    for (char ch : text.toCharArray()) {
      composite.add(getGlyphSprite(ch));
    }
  }

  protected void draw(Batch batch, float angle) {
    int     w             = config.wheelTex.getRegionWidth();
    int     h             = config.wheelTex.getRegionHeight();
    Vector2 roller        = (new Vector2()).set(0, h * config.ANGULAR_TEXT_FLOAT /2);
    Vector2 cp            = Vector2 .Zero.set(w/2f, h/2f);
    float   align         = angle + ((composite.size() - 1)/2f)*config.TEXT_SPACE;

    for (Sprite sprite : composite) {
      Vector2 _pos = roller .cpy().rotate(align).add(cp)
              .sub(sprite.getRegionWidth()/2f, sprite.getRegionHeight()/2f);
      sprite.setX(_pos.x);
      sprite.setY(_pos.y);
      sprite.setRotation(align);
      align -= config.TEXT_SPACE;
      sprite.draw(batch);
    }
  }
}