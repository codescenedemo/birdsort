package com.core.utils.wheel;

import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;

public class WheelRollAction extends TemporalAction {
  float velocity        = 0;
  float acc             = 0;
  float coefficient     = 1;
  float initAngle       = 0;
  int outcome           = 0;
  boolean infiniteRoll  = true; //roll infinity and slow down when something happen;
  Wheel wheel;

  public WheelRollAction(Wheel wheel) {
    this.wheel = wheel;
  }

  void stopRoll(int outcome) {
    infiniteRoll = false;
    acc = 0;
    this.outcome    = outcome;
    this.initAngle  = outcome * wheel.WHEEL_ARC;
  }

  @Override
  protected void begin() {
    acc = 0;
    coefficient = velocity/wheel.WHEEL_ARC;
    this.setReverse(true);
    super.begin();
  }

  @Override
  public boolean act(float delta) {
    if (!infiniteRoll)
      return super.act(delta); //slowing down rolling
    else {
      _update(delta); //infinite rolling
      return false;
    }
  }

  protected void _update(float delta) { //infinity roll, waiting for signal
    this.acc += velocity;
    float _magicNumber = 2.5f; //don't ask me why, it just work T____T
    actor.setRotation(actor.getRotation() + delta*velocity/_magicNumber);
    //calc playing sound
    float magicNumber = 5;
    if (Math.abs(acc /(wheel.wc.VELOCITY_SCALE*coefficient)) >= magicNumber) {
      wheel.wc.wheelTick.play(1);
      this.acc = 0;
    }
  }

  @Override
  protected void update(float percent) { //slow down roll
    this.acc += velocity*percent*percent;
    actor.setRotation(initAngle - velocity*percent*percent);

    //calc playing sound
    float magicNumber = 2;
    if (Math.abs(acc /(wheel.wc.VELOCITY_SCALE*coefficient)) >= magicNumber) {
      wheel.wc.wheelTick.play(1);
      this.acc = 0;
    }
  }

  void init(float velocity, int outcome) {
    super.reset();
    this.velocity = velocity;
    this.outcome = outcome;
    setDuration((float)wheel.wc.ROLLING_DURATION);
    this.initAngle = outcome * wheel.WHEEL_ARC;
    this.infiniteRoll = wheel.wc.INFINITE_ROLL;//todo check this
    this.coefficient = 1;
  }

  @Override
  protected void end() {
    if (wheel.listener != null) {
      WheelItem outputItem = wheel.wheelItemMap.get(outcome);
      if (outputItem != null)
        wheel.listener.end(outputItem);
      else
        wheel.listener.error("Wheel Static Item Inconsistency");
    }

    wheel.lock = false;
    super.end();
  }
}