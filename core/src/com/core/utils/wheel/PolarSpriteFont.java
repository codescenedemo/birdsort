package com.core.utils.wheel;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.math.Vector2;

@SuppressWarnings("unused")
public class PolarSpriteFont extends AngularSpriteFont {
  PolarSpriteFont(String text, BitmapFont font, WheelConfig config) {
    super(text, font, config);
  }

  @Override
  protected void draw(Batch batch, float angle) {
    int     w             = config.wheelTex.getRegionWidth();
    int     h             = config.wheelTex.getRegionHeight();
    Vector2 roller        = (new Vector2()).set(0, h * config.POLAR_TEXT_FLOAT /2);
    Vector2 cp            = Vector2 .Zero.set(w/2f, h/2f);
    int     idx           = 0;
    float   magicNumber   = 4.2f;

    for (Sprite sprite : composite) {
      Vector2 _pos = roller .cpy().add(0,idx++*config.TEXT_SPACE*magicNumber)
              .rotate(angle).add(cp)
              .sub(sprite.getRegionWidth()/2f, sprite.getRegionHeight()/2f);
      sprite.setX(_pos.x);
      sprite.setY(_pos.y);
      sprite.setRotation(angle + 90);
      sprite.draw(batch);
    }
  }
}