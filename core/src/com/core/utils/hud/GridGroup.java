package com.core.utils.hud;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.ObjectMap;

import java.util.Arrays;
import java.util.List;

@SuppressWarnings("unused unchecked")
public class GridGroup extends ScrollPane {
  ObjectMap<String, Actor>  indexer;
  int                       nCol;
  float                     pad;
  Table                     innerTable;

  public GridGroup(float width, float height, int nCol, float pad, int al) {
    super(new Table());
    innerTable = (Table) getActor();
    innerTable.align(al);
    setSize(width, height);
    indexer   = new ObjectMap<>();
    this.nCol = nCol;
    this.pad  = pad;
  }

  public void addGrid(Actor child, String key) {
    if (child != null) {
      if (nCol > 1) {
        innerTable.add(child).pad(pad);
        indexer.put(key, child);
        if (indexer.size%nCol == 0)
          innerTable.row();
      }
      else if (nCol == 1) {
        innerTable.add(child).padTop(pad).row();
        indexer.put(key, child);
      }
      else if (nCol == 0) {
        innerTable.add(child).padRight(pad).padLeft(pad);
        indexer.put(key, child);
      }
    }
    innerTable.setSize(getPrefWidth(), getPrefHeight());
  }

  public void addGrid(Actor child, String key, int colspan) {
    if (child != null) {
      if (nCol > 1) {
        innerTable.add(child).pad(pad).colspan(colspan);
        indexer.put(key, child);
        if (indexer.size%nCol == 0)
          innerTable.row();
      }
      else if (nCol == 1) {
        innerTable.add(child).colspan(colspan).padTop(pad).row();
        indexer.put(key, child);
      }
      else if (nCol == 0) {
        innerTable.add(child).colspan(colspan).padRight(pad).padLeft(pad);
        indexer.put(key, child);
      }
    }
    innerTable.setSize(getPrefWidth(), getPrefHeight());
  }

  public void remove(String identity) {
    Actor child = indexer.get(identity);
    if (child != null) {
      indexer.remove(identity);
      innerTable.removeActor(child);
    }
    setSize(getPrefWidth(), getPrefHeight());
  }

  public void clearGridChildren() {
    innerTable.clearChildren();
    indexer.clear();
  }

  public<T extends Actor> T query(String path, Class<T> type) {
    List<String> split = Arrays.asList(path.split("/"));
    return query(split, type);
  }

  <T extends Actor> T query(List<String> path, Class<T> type) {
    if (path == null || path.size() == 0)
      return null;
    if (path.size() == 1)
      return (T)indexer.get(path.get(0));
    Actor innerGroup = indexer.get(path.get(0));
    if (innerGroup instanceof MapGroup)
      return ((MapGroup)innerGroup).query(path.subList(1, path.size()), type);
    return null;
  }
}