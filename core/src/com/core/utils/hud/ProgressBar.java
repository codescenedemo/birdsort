package com.core.utils.hud;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

@SuppressWarnings("unused")
public class ProgressBar extends Group {
  final private Image         progress;
  final private TextureRegion originTG;
  final private TextureRegion targetTG;
  private float               sliceSpeed = 0.2f;
  private float               percent = 0;

  public ProgressBar(TextureRegion bg, TextureRegion pg, float offSetX, float offSetY) {
    setSize(bg.getRegionWidth(), bg.getRegionHeight());
    Image _bg   = new Image(bg);
    progress    = new Image(pg);
    originTG    = pg;
    targetTG    = new TextureRegion(originTG);

    targetTG.setRegionWidth(0);
    progress.setDrawable(new TextureRegionDrawable(targetTG));
    progress.setWidth(0);
    progress.setPosition(offSetX, offSetY);

    addActor(_bg);
    addActor(progress);
  }

  public void reset() {
    targetTG.setRegionWidth(0);
    progress.setWidth(0);
  }

  public Image getPg(){
    return progress;
  }


  public void updateTG(String drawable){

  }

  public void setSliceSpeed(float sliceSpeed) {
    this.sliceSpeed = sliceSpeed;
  }

  private void sp(float percent, boolean slice) {
    if (percent > 1.0f)
      return;
    int w = (int)(originTG.getRegionWidth()*percent);
    int ow = targetTG.getRegionWidth();
    if (slice) {
      int dw = w - ow;
      addAction(new TemporalAction(sliceSpeed) {
        @Override
        protected void begin() {
          setInterpolation(Interpolation.fastSlow);
          super.begin();
        }

        @Override
        protected void update(float percent) {
          targetTG.setRegionWidth(ow + (int)(dw*percent));
          progress.setWidth(ow + dw*percent);
        }
      });
    }
    else {
      targetTG.setRegionWidth(w);
      progress.setWidth(w);
    }
  }

  //[1.0 -> 100.0]
  public void setPercent(float percent, boolean slice) {
    if (percent < 0.0)
      return;
    this.percent = percent;
    sp(this.percent*0.01f, slice);
  }

  public float getPercent() {
    return percent;
  }
}

