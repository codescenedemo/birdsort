package com.core.utils.hud.builders;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.core.utils.hud.GridGroup;

@SuppressWarnings("unused")
public class GGB extends AbstractActorBuilder<GridGroup> {
  public static GGB New() { return new GGB(); }
  public int col = 1;
  public float pad = 10;
  public boolean scrollX, scrollY;
  public int tbAlign = 1;

  public GGB col(int col) {
    this.col = col; return this;
  }

  public GGB pad(float pad) {
    this.pad = pad; return this;
  }

  public GGB disableScroll(boolean x, boolean y) {
    this.scrollX = x; this.scrollY = y; return this;
  }

  public GGB tbAlign(int align) {
    this.tbAlign = align; return this;
  }

  @Override
  public GridGroup build() {
    GridGroup res = new GridGroup(w, h, col, pad, tbAlign);
    applyCommonProps(res);
    res.setScrollingDisabled(scrollX, scrollY);
    for (AbstractActorBuilder<?> bd : childs) {
      bd.name(name + "/" + index);
      Actor out = bd.build();
      res.addGrid(out, bd.index);
      if (adapter.debug()) {
        out.addListener(new ClickListener() {
          @Override
          public void enter(InputEvent event, float x, float y, int pointer, Actor fromActor) {
            if (bd.index != null && !bd.index.equals("")) {
              out.setDebug(true);
              adapter.debugMsg(bd.name + "/" + bd.index);
            }
          }

          @Override
          public void exit(InputEvent event, float x, float y, int pointer, Actor toActor) {
            out.setDebug(false);
            adapter.debugMsg("");
          }
        });
      }
    }

    return res;
  }
}