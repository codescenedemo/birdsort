package com.core.utils.hud.builders;

import com.core.utils.hud.ProgressBar;

@SuppressWarnings("unused")
public class PBB extends AbstractActorBuilder<ProgressBar> {
  public PBB() {}
  public static PBB New() { return new PBB(); }

  public String frame;
  public String progress;
  public float  offSetX;
  public float  offSetY;
  public float  percent;

  public PBB bg(String frame) {
    this.frame = frame; return this;
  }

  public PBB fg(String progress) {
    this.progress = progress; return this;
  }

  public PBB offSet(float x, float y) {
    this.offSetX = x;
    this.offSetY = y;
    return this;
  }

  public PBB pc(float percent) {
    this.percent = percent; return this;
  }

  @Override
  public ProgressBar build() {
    ProgressBar pb = new ProgressBar(adapter.getRegion(frame), adapter.getRegion(progress), offSetX, offSetY);
    pb.setPercent(percent, false);
    applyCommonProps(pb);
    return pb;
  }
}