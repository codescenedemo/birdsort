package com.core.utils.hud.builders;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.core.util.GUtils;

@SuppressWarnings("unused")
public class IB extends AbstractActorBuilder<Image> {
  public IB() {
  }

  public static IB New() {
    return new IB();
  }

  public float alpha;
  public String c;
  public String drawable;
  public String ninepatch;
  public int top, bot, left, right;

  public IB drawable(String key) {
    this.drawable = key;
    return this;
  }

  public IB nPatch(String region, int left, int right, int top, int bot) {
    this.ninepatch = region;
    this.left = left;
    this.right = right;
    this.top = top;
    this.bot = bot;
    return this;
  }



  public IB overlay(float w, float h, float alpha) {
    return overlay(w, h, alpha, null);
  }

  public IB overlay(float w, float h, float alpha, String c) {
    this.w = w;
    this.h = h;
    this.alpha = alpha;
    this.c = c;
    return this;
  }

  @Override
  public Image build() {
    Image res;
    if (this.drawable != null) {
      res = new Image(adapter.getDrawable(this.drawable));
    } else if (ninepatch != null) {
      res = new Image(new NinePatch(adapter.getRegion(this.ninepatch), left, right, top, bot));
      res.setSize(w, h);
    } else if (c != null) res = GUtils.createOverlay(w, h, alpha, Color.valueOf(c));
    else res = GUtils.createOverlay(w, h, alpha);
    applyCommonProps(res);
    return res;
  }
}