package com.core.utils.hud.builders;

import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.core.utils.hud.ShaderImage;
import com.core.utils.hud.external.ApplyUniform;

@SuppressWarnings("unused")
public class ShaderIB extends AbstractActorBuilder<Image> {
  public ShaderIB() {
  }

  public static ShaderIB New() {
    return new ShaderIB();
  }

  String region;
  String ninePatch;
  String shader;
  ApplyUniform uniform;
  boolean active;

  public ShaderIB setRegion(String region) {
    ninePatch = null;
    this.region = region;
    return this;
  }

  public ShaderIB setShader(String shader) {
    this.shader = shader;
    return this;
  }

  public ShaderIB setActive(boolean active) {
    this.active = active;
    return this;
  }

  public ShaderIB setUniform(ApplyUniform uniform) {
    this.uniform = uniform;
    return this;
  }

  @Override
  public ShaderImage build() {
    ShaderImage img;
    if (region != null)
      img = new ShaderImage(adapter.getRegion(region));
    else
      return new ShaderImage();
    img.shader = adapter.getShaderProgram(shader);
    img.active = active;
    img.uniform = uniform;
    applyCommonProps(img);
    return img;
  }
}