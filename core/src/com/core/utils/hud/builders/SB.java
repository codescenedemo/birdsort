package com.core.utils.hud.builders;

import com.core.exSprite.GSpineSprite;

@SuppressWarnings("unused")
public class SB extends AbstractActorBuilder<GSpineSprite> {
  public SB() {
  }

  public static SB New() {
    return new SB();
  }

  String spineName = "";
  String animation = "";
  String skin = "";
  boolean loop;
  float timeScale = 1;

  public SB skin(String skin) {
    this.skin = skin;
    return this;
  }

  public SB spineName(String name) {
    this.spineName = name;
    return this;
  }

  public SB animation(String animation) {
    this.animation = animation;
    return this;
  }

  public SB loop(boolean loop) {
    this.loop = loop;
    return this;
  }

  public SB timeScl(float timeScl) {
    this.timeScale = timeScl;
    return this;
  }

  @Override
  public GSpineSprite build() {
    GSpineSprite res = new GSpineSprite(spineName);
    if (!skin.isEmpty())
      res.setSkin(skin);
    if (!animation.isEmpty())
      res.changeAnimation(animation, loop, false);

    res.setTimeScale(timeScale);

    applyCommonProps(res);
    return res;
  }
}