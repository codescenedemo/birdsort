package com.core.utils.hud;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.NinePatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.core.utils.hud.external.BuilderAdapter;
import com.game.GMain;

@SuppressWarnings("unused")
public class BuilderBridge implements BuilderAdapter {
  Label         debugMsg;
  HUD           hud;
  Drawable      nullTexture;
  Image         overlay;
  boolean       debug;

  public BuilderBridge(HUD hud) {
    this.hud   = hud;
    this.debug = false;
    Pixmap tex = new Pixmap(40, 40, Pixmap.Format.RGB888);
    tex.setColor(Color.BROWN);
    tex.fill();
    nullTexture = new TextureRegionDrawable(new Texture(tex));
    setupDebug();
  }

  @Override
  public Drawable getDrawable(String region) {
    TextureRegion res = GMain.getAssetManager().getAtlasRegion("ui", region);
    if (res == null)
      res = GMain.getAssetManager().getAtlasRegion("nottiny", region);
    if (res == null) {
      return nullTexture;
    }
    return new TextureRegionDrawable(res);
  }

  @Override
  public TextureRegion getRegion(String region) {
    TextureRegion res = GMain.getAssetManager().getAtlasRegion("ui", region);
    if (res == null)
      res = GMain.getAssetManager().getAtlasRegion("nottiny", region);
    if (res == null) {
      Pixmap pix = new Pixmap(40, 40, Pixmap.Format.RGB888);
      pix.setColor(Color.BROWN);
      pix.fill();
      Gdx.app.log("ERROR", "Null Texture Region: " + region);
      return new TextureRegion(new Texture(pix));
    }
    return res;
  }

  @Override
  public ShaderProgram getShaderProgram(String name){
    return GMain.getAssetManager().getShaderProgram(name);
  }

  @Override
  public NinePatch ninePatch(String key, int left, int right, int top, int bottom) {
    return new NinePatch(getRegion(key), left, right, top, bottom);
  }

  @Override
  public BitmapFont getBitmapFont(String key) {
    return GMain.getAssetManager().getBitmapFont(key + ".fnt");
  }

  @Override
  public MapGroup query(String path) {
    return hud.query(path, MapGroup.class);
  }

  @Override
  public void debugMsg(String msg) {
    String old = debugMsg.getText().toString();
    if (!old.contains(msg)) {
      debugMsg.setText(msg);
    }
  }

  @Override
  public boolean debug() {
    return this.debug;
  }

  @Override
  public float worldWidth() {
    return hud.getWidth();
  }

  @Override
  public float worldHeight() {
    return hud.getHeight();
  }

  public void showDebug() {
    debugMsg.setPosition(0,0);
    hud.addActor(overlay, AL.cb);
    hud.addActor(debugMsg, AL.cb);
  }

  private void setupDebug() {
    Label.LabelStyle ls = new Label.LabelStyle();
    ls.font = new BitmapFont();
    debugMsg = new Label("", ls);

    Pixmap labelColor = new Pixmap(720, 40, Pixmap.Format.RGB888);
    labelColor.setColor(Color.BROWN);
    labelColor.fill();
    overlay = new Image(new Texture(labelColor));
    overlay.setColor(0.5f, 0.5f, 0.5f, 0.5f);
  }
}