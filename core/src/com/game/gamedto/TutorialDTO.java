package com.game.gamedto;

import com.badlogic.gdx.utils.IntMap;
import com.game.config.RemoteConfig;

public class TutorialDTO {
  public int id, branchId;
  public float startX, startY, endX, endY;
  public String content;
  public float posY;
  public boolean removeLabel;
  public static TutorialDTO of(int id, float startX, float startY, int branchId){
    TutorialDTO dto = new TutorialDTO();
    dto.id = id;
    dto.startX = startX;
    dto.startY = startY;
    dto.branchId = branchId;
    dto.content = "";
    dto.posY = 0;
    dto.removeLabel = true;

    return dto;
  }

  public static TutorialDTO of(int id, float startX, float startY, int branchId, String content, float posY, boolean removeLabel){
    TutorialDTO dto = new TutorialDTO();
    dto.id = id;
    dto.startX = startX;
    dto.startY = startY;
    dto.branchId = branchId;
    dto.content = content;
    dto.posY = posY;
    dto.removeLabel = removeLabel;

    return dto;
  }

  public static IntMap<TutorialDTO> tuts;

  static {
    tuts = new IntMap<>();
    //lv1
    tuts.put(1, TutorialDTO.of(1, 50,  280 + 80 + RemoteConfig.heightPad(), 1, "Touch to select bird", 500 + RemoteConfig.heightPad(), true));
    tuts.put(2, TutorialDTO.of(2, 596, 330 + 80 + RemoteConfig.heightPad(), 2, "Touch to move bird",   500 + RemoteConfig.heightPad(), true));
    //lv2
    tuts.put(3, TutorialDTO.of(3, 220, 280+ 80 + RemoteConfig.heightPad(), 1, "Only bird with the same color can be moved", 650 + RemoteConfig.heightPad(), false));
    tuts.put(4, TutorialDTO.of(4, 0,   410+ 80 + RemoteConfig.heightPad(), 3, "", 650 + RemoteConfig.heightPad(), true));
    tuts.put(5, TutorialDTO.of(5, 396, 330+ 80+ RemoteConfig.heightPad(), 2, "",  650 + RemoteConfig.heightPad(), true));
    tuts.put(6, TutorialDTO.of(6, 50,  280+ 80+ RemoteConfig.heightPad(),  1, "", 650 + RemoteConfig.heightPad(), true));
    //lv4
    tuts.put(7, TutorialDTO.of(3, 220, 280+ 80+ RemoteConfig.heightPad(), 1, "You must wake the bird up before moving it", 650 + RemoteConfig.heightPad(), true));
    tuts.put(8, TutorialDTO.of(3, 596, 330+ 80+ RemoteConfig.heightPad(), 2, "", 650 + RemoteConfig.heightPad(), true));

  }

}
