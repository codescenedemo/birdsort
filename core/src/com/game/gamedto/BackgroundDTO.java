package com.game.gamedto;

import com.badlogic.gdx.utils.IntMap;

public class BackgroundDTO {
  public String drawable;
  public int id;
  public int levelUnlock;

  public static BackgroundDTO of(int id, int levelUnlock, String drawable){
    BackgroundDTO dto = new BackgroundDTO();
    dto.id = id;
    dto.levelUnlock = levelUnlock;
    dto.drawable = drawable;
    return dto;
  }

  public static IntMap<BackgroundDTO> backgrounds;

  static {
    backgrounds = new IntMap<>();
    backgrounds.put(1, BackgroundDTO.of(1,  10, "bg1"));
    backgrounds.put(2, BackgroundDTO.of(2,  10,"bg2"));
    backgrounds.put(3, BackgroundDTO.of(3,  30,"bg3"));
    backgrounds.put(4, BackgroundDTO.of(4,  50,"bg4"));
    backgrounds.put(5, BackgroundDTO.of(5,  70,"bg5"));
    backgrounds.put(6, BackgroundDTO.of(6,  80, "bg6"));
  }
}
