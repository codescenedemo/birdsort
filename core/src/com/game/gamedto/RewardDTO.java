package com.game.gamedto;

import com.badlogic.gdx.utils.IntMap;

public class RewardDTO {
  public int id;
  public int levelUnlock;
  public String drawable;
  public String type;

  public static RewardDTO of(int id, int levelUnlock, String drawable, String type) {
    RewardDTO dto = new RewardDTO();
    dto.id = id;
    dto.levelUnlock = levelUnlock;
    dto.drawable = drawable;
    dto.type = type;
    return dto;
  }

  public static IntMap<RewardDTO> rewards;

  static {
    rewards = new IntMap<>();

    rewards.put( 1, RewardDTO.of( 2,  15,  "avatar_bg2", "bg"));
    rewards.put( 2, RewardDTO.of( 2,  20,  "avatar_branch2", "branch"));
    rewards.put( 3, RewardDTO.of( 3,  25,  "avatar_bg3", "bg"));
    rewards.put( 4, RewardDTO.of( 3,  30,  "avatar_branch3", "branch"));
    rewards.put( 5, RewardDTO.of( 4,  35,  "avatar_bg4", "bg"));
    rewards.put( 6, RewardDTO.of( 4,  40,  "avatar_branch4", "branch"));
    rewards.put( 7, RewardDTO.of( 5,  45,  "avatar_bg5", "bg"));
    rewards.put (8, RewardDTO.of( 5,  50,  "avatar_branch5", "branch"));
    rewards.put (9, RewardDTO.of( 6,  55,  "avatar_bg5", "bg"));
    rewards.put (10, RewardDTO.of( 6,  60, "avatar_branch6", "branch"));

  }
}
