package com.game.gamedto;

import com.badlogic.gdx.utils.IntMap;

public class BranchDTO {
  public int id;
  public int levelUnlock;
  public String drawable;

  public static BranchDTO of(int id, int levelUnlock, String drawable){
    BranchDTO dto = new BranchDTO();
    dto.id = id;
    dto.levelUnlock = levelUnlock;
    dto.drawable = drawable;
    return dto;
  }
  public static IntMap<BranchDTO> branchs;

  static {
    branchs = new IntMap<>();
    branchs.put(1, BranchDTO.of(1,  20,  "branch1"));
    branchs.put(2, BranchDTO.of(2,  20,  "branch2"));
    branchs.put(3, BranchDTO.of(3,  40,  "branch3"));
    branchs.put(4, BranchDTO.of(4,  60,  "branch4"));
    branchs.put(5, BranchDTO.of(5,  80,  "branch5"));
    branchs.put(6, BranchDTO.of(6,  100, "branch6"));
  }
}
