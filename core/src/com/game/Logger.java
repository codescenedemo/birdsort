package com.game;

import com.badlogic.gdx.Application;
import com.badlogic.gdx.Gdx;
import com.strongjoshua.console.LogLevel;

public class Logger {

    public static native void h5Log(String str )/*-{
            $wnd.console.log(str);
        }-*/;

    public static native void h5Error(String str )/*-{
            $wnd.console.error(str);
        }-*/;

    public static void log(String str){
        if(Gdx.app.getType() == Application.ApplicationType.WebGL)
            h5Log(str);
        else
            Gdx.app.log("", str);
    }

    public static void error(String str){
        if(Gdx.app.getType() == Application.ApplicationType.WebGL)
            h5Error(str);
        else
            Gdx.app.error("", str);
    }

    public static void log(String str, LogLevel level){
        if(level == LogLevel.ERROR)
            error(str);
        log(str);
    }

    public static void consolelog(String str){
        log(str);
    }


    public static void consolelog(String str, LogLevel level){
        log(str);

        //Fishot.console.refresh(true);
    }

}
