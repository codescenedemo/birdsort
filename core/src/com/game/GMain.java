package com.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.core.model.Session;
import com.core.util.GAssetsManager;
import com.core.util.GDirectedGame;
import com.core.util.GScreen;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.SessionLoader;
import com.core.utils.hud.BuilderBridge;
import com.core.utils.hud.HUD;
import com.core.utils.hud.builders.AbstractActorBuilder;

import com.game.object.MyCmd;
import com.game.screens.LoadingScreen;
import com.game.screens.PlayScreen;
import com.gameservice.PlayService;
import com.platform.AdsClose;
import com.platform.IPlat;
import com.strongjoshua.console.Console;
import com.strongjoshua.console.GUIConsole;

public class GMain extends GDirectedGame {
  public static GMain me;
  public static float SCREEN_RATIO;
  private final IPlat platform;
  private GScreen loadingScreen;
  private GScreen homeScreen;
  private GScreen playScreen;
  private GScreen buildScreen;
  private HUD hud;
  private Session session;
  private GAssetsManager assetsManager;
  private SessionLoader sessionLoader;
  public PlayService playService;
  public static Console console;



  public GMain(IPlat plat) {
    platform = plat;
    me = this;
  }

  private void init() {

    float width = 720;
    float height = width * Gdx.graphics.getHeight() / Gdx.graphics.getWidth();

//    System.out.println("width: " + width + "height: " + height);

    SCREEN_RATIO = Gdx.graphics.getHeight() / height;
    GStage.init(width, height, 0, 0);
    initConsole();
    //GStage.initBox2D(10);
    hud = new HUD(width, height);
//    hud.setDebug(true);
//    System.out.println("WIDTH: " + width + "HEIGHT: " + height);
    AbstractActorBuilder.adapter = new BuilderBridge(hud);

    GSound.setIsUseHowler();

  }


  @Override
  public void render() {
    if (session == null)
      return;
    super.render();
    if (console != null)
      console.draw();
  }

  public static int firstWidth, firstHeight;

  @Override
  public void resize(int width, int height) {
    platform.onShow();
    float wh = Gdx.graphics.getHeight();
//    float ww = wh * width / height;
    if (firstWidth == 0)
      firstWidth = width;
    if (firstHeight == 0)
      firstHeight = height;
    SCREEN_RATIO = width / wh;
    console.setSize(width, height);
    super.resize(width, height);
  }

  private void initConsole() {
    console = new GUIConsole(new Skin(Gdx.files.internal("test_skin/uiskin.json")), false);
    console.setDisplayKeyID(Input.Keys.GRAVE);

    console.setConsoleStackTrace(true);
    console.setTitle("Console");
    console.setHoverColor(Color.WHITE);
    console.setNoHoverColor(Color.BLUE);
    console.setHoverAlpha(1f);
    console.setNoHoverAlpha(1f);

    InputMultiplexer im1 = new InputMultiplexer();
    im1.addProcessor(GStage.getStage());

    Gdx.input.setInputProcessor(im1);
    console.setMaxEntries(100);

    console.resetInputProcessing();
    // console already present, logged to consoles
    console.resetInputProcessing();

    console.setSizePercent(100, 60);
    console.setPositionPercent(0, 100);
    console.setCommandExecutor(new MyCmd());
    console.enableSubmitButton(true);

    console.setSubmitText("Fire!");
    console.setVisible(false);
  }

  public void create() {
    this.init();
//    FileHandle levels = Gdx.files.local("levelRotate/");
//    VisUI.load();
//    FileChooser.setDefaultPrefsName("javax.swing.JFileChooser");
//    saveFileChooser = new FileChooser(FileChooser.Mode.SAVE);
//    saveFileChooser.setScale(1);
//    saveFileChooser.setDirectory("levelRotate/");
//    saveFileChooser.setOrigin(Align.center);

    assetsManager = new GAssetsManager();
    sessionLoader = platform.getSessionLoader();
    sessionLoader.load(s -> {
      session = s;
      GSound.setMusicSilence(!s.setting.isMusicOn);
      GSound.setSoundSilence(!s.setting.isSoundOn);
//      ((LoadingScreen) loadingScreen).initStage1();
      this.setScreen(new LoadingScreen());
      System.gc();

    });
    playService = platform.getPlayService();
    playService.setHandler(new PlayServiceHandler());
  }

  public void dispose() {
    super.dispose();
  }

  public static HUD hud() {
    return ((GMain) Gdx.app.getApplicationListener()).hud;
  }

  public static GScreen curScreen() {
    return ((GMain) Gdx.app.getApplicationListener()).getCurrScreen();
  }

  public static GAssetsManager getAssetManager() {
    return ((GMain) Gdx.app.getApplicationListener()).assetsManager;
  }

  public static IPlat platform() {
    return ((GMain) Gdx.app.getApplicationListener()).platform;
  }

  public static LoadingScreen loadingScreen() {
    return (LoadingScreen) ((GMain) Gdx.app.getApplicationListener()).loadingScreen;
  }

  public static PlayScreen playScreen() {
    if (((GMain) Gdx.app.getApplicationListener()).playScreen == null)
      ((GMain) Gdx.app.getApplicationListener()).playScreen = new PlayScreen();
    return (PlayScreen) ((GMain) Gdx.app.getApplicationListener()).playScreen;
  }



//
//  public static BuildLevelScreen buidScreen() {
//    if (((GMain) Gdx.app.getApplicationListener()).homeScreen == null)
//      ((GMain) Gdx.app.getApplicationListener()).homeScreen = new HomeScreen();
//    return (BuildLevelScreen) ((GMain) Gdx.app.getApplicationListener()).buildScreen;
//  }

  public static void syncSession() {
    SessionLoader pLoader = ((GMain) Gdx.app.getApplicationListener()).sessionLoader;
    if (pLoader != null)
      pLoader.sync();
  }

  public static Session session() {
    return ((GMain) Gdx.app.getApplicationListener()).session;
  }



  @Override
  public void pause() {
    super.pause();
  }

  public static void showVideoReward(AdsClose ondone) {
//    GSound.setMusicSilence(true);
    platform().ShowVideoReward(callback -> {
//      GSound.setMusicSilence(false);
      ondone.OnEvent(callback);
    });
  }

  public static void showFullScreen(Runnable runOnShow, Runnable runOnDone) {
    AdsClose onShow = b -> {
//      GSound.setMusicSilence(true);
      if (runOnShow != null)
        Gdx.app.postRunnable(runOnShow);
    };
    AdsClose onDone = b -> {
//      GSound.setMusicSilence(false);
      if (runOnDone != null)
        Gdx.app.postRunnable(runOnDone);
    };
    if (GMain.platform().isFullScreenReady()) {
      GMain.platform().ShowFullscreen(onShow, onDone);
    } else {
      onShow.OnEvent(true);
      onDone.OnEvent(true);
    }
  }

}