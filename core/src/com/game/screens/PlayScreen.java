package com.game.screens;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.core.action.exAction.GSimpleAction;
import com.core.util.GLayer;
import com.core.util.GScreen;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.hud.HUD;
import com.game.GMain;
import com.game.config.RemoteConfig;
import com.game.config.SoundConfig;
import com.game.object.BoardGame;
import com.game.ui.ClaimPopup;
import com.game.ui.FooterUI;
import com.game.ui.HeaderUI;
import com.game.ui.RewardPopUp;
import com.game.ui.ShopUI;
import com.game.ui.WinPopUp;


public class PlayScreen extends GScreen {

  HUD hud;
  public BoardGame board;


  @Override
  public void dispose() {

  }

  @Override
  public void init() {

    GMain.platform().ShowBanner(RemoteConfig.isShowBanner());

    GStage.clearStage();
    hud = GMain.hud();

    GStage.addToLayer(GLayer.ui, hud);
    hud.clear();
    hud.setTouchable(Touchable.childrenOnly);

    hud.addAction(GSimpleAction.simpleAction((d, a) -> {
      boolean isMusicPlaying = GSound.isMusicPlaying();
      if (!isMusicPlaying)
        GSound.playMusic(SoundConfig.BGMUSIC);
      System.out.println("check music is playing: " + isMusicPlaying);
      return isMusicPlaying;
    }));

    board = new BoardGame(720, GStage.getWorldHeight());
    board.intit();

    initComponents();

    GMain.hud().addActor(GMain.hud().query("footer", Actor.class));
    GMain.hud().addActor(GMain.hud().query("header", Actor.class));
    GMain.hud().addActor(GMain.hud().query("winUI", Actor.class));
    if (GMain.session().inventory.unlockedIdx < 10) {
      new RewardPopUp();
      GMain.hud().addActor(GMain.hud().query("reward", Actor.class));
    }
    GMain.hud().addActor(GMain.hud().query("claim", Actor.class));
    GMain.hud().addActor(GMain.hud().query("shop", Actor.class));


    if (board.boardArray.size >= 12) {
      GMain.hud().query("footer/root/branch", Actor.class).setVisible(false);
    }

  }

  public void initComponents() {
    new FooterUI(board);
    new HeaderUI();
    new WinPopUp();
    new ClaimPopup();
    new ShopUI();
  }

  @Override
  public void run() {

  }


}
