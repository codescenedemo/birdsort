package com.game.screens;


import com.core.exSprite.particle.GParticleSystem;
import com.core.util.GLayer;
import com.core.util.GScreen;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.HUD;
import com.core.utils.hud.ProgressBar;

import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.PBB;
import com.game.GMain;
import com.game.Logger;
import com.game.config.SoundConfig;

public class LoadingScreen extends GScreen {
  int step = 0;

  ProgressBar bar;
  HUD hud;

  @Override
  public void dispose() {

  }

  @Override
  public void init() {
    step = 0;
    hud = GMain.hud();
    GStage.addToLayer(GLayer.ui, hud);
    IB.New().drawable("loading").pos(0, 0).align(AL.c).parent(hud).build();
    IB.New().drawable("logo").pos(0, 150).align(AL.ct).parent(hud).build();
    createSlider();
  }

  public void initTextureAtlas() {
//    GMain.getAssetManager().loadTextureAtlas("particle");
    GMain.getAssetManager().loadTextureAtlas("ui");
  }

  public void initPartical() {

    new GParticleSystem("hammer_hit.p", "particle", 1, 10);

  }

  public void initFont() {
//        GMain.getAssetManager().loadBitmapFont("font");
//        GMain.getAssetManager().loadBitmapFont("moriafont20");
  }

  public void initSound1() {

    GMain.getAssetManager().loadSound(SoundConfig.BIRD_FLY);
    GMain.getAssetManager().loadSound(SoundConfig.CHIRP_HIGH);
    GMain.getAssetManager().loadMusic(SoundConfig.BGMUSIC);
    GMain.getAssetManager().loadSound(SoundConfig.BTNCLICK);
    GMain.getAssetManager().loadSound(SoundConfig.SWOOSH);
    GMain.getAssetManager().loadSound(SoundConfig.CRACK);
    GMain.getAssetManager().loadSound(SoundConfig.ALARM);
    GMain.getAssetManager().loadSound(SoundConfig.WIN);
  }

  public void initSound2() {
    GSound.initSound(SoundConfig.BIRD_FLY);
    GSound.initSound(SoundConfig.CHIRP_HIGH);
    GSound.initMusic(SoundConfig.BGMUSIC);
    GSound.initSound(SoundConfig.BTNCLICK);
    GSound.initSound(SoundConfig.SWOOSH);
    GSound.initSound(SoundConfig.CRACK);
    GSound.initSound(SoundConfig.ALARM);
    GSound.initSound(SoundConfig.WIN);

  }


  public void loadSkeletonData(){
    GMain.getAssetManager().loadSkeletonData("bird");

  }

  public void createSlider() {
    bar =  PBB.New().bg("progress_out").fg("progress_in").offSet(0, 0).pc(0).pos(0, 150).align(AL.cb).parent(hud).touchable(false).build();
    GMain.platform().ShowBanner(false);
  }


  @Override
  public void run() {

    switch (step) {
      case 0:
        initTextureAtlas();
        initPartical();
        initFont();
        initSound1(); //load
        loadSkeletonData();

        step = 1;

        break;
      case 1:
        GMain.getAssetManager().update();
        Logger.log("Assets loading : " + GMain.getAssetManager().getProgress() + "%");
        float percent = GMain.getAssetManager().getProgress();
        bar.setPercent(percent, true);
        if (GMain.getAssetManager().isFinished()) {
          step = 2;
        }
        break;
      case 2:

        initSound2(); //init
        GStage.addToLayer(GLayer.ui, GMain.hud());
        setScreen(new PlayScreen());

        break;
      default:
        break;
    }
  }
}
