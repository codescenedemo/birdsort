//package com.game.screens;
//
//import com.badlogic.gdx.math.Vector2;
//import com.badlogic.gdx.scenes.scene2d.Group;
//import com.badlogic.gdx.scenes.scene2d.InputEvent;
//import com.badlogic.gdx.scenes.scene2d.ui.Image;
//import com.badlogic.gdx.scenes.scene2d.ui.Label;
//import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
//import com.badlogic.gdx.utils.Align;
//import com.core.util.GLayer;
//import com.core.util.GStage;
//import com.core.utils.hud.AL;
//import com.core.utils.hud.builders.LB;
//import com.game.GMain;
//import com.game.controller.HookController;
//
//
//public class GamePlay {
//  public static Character character;
//  public static Hook hook;
//  public static Image rope;
//  public static Image lHook;
//  public static Image ropeRay;
//  public static HookController hookControl;
//  public static Label debugLabel;
//  public static Label debugLabel2;
//  public static Group touchArea;
//  public static State state;
//
//  public static Vector2 currPosRope = new Vector2();
//  public static Vector2 currPosHook = new Vector2();
//
//
//  public enum State{
//    ROTATION,
//    MOVE,
//    REWIND,
//    CATCH,
//    NONE
//  }
//
//  public static void init(){
//    hookControl = new HookController();
//    setUpCharacter();
//    setRopeRay();
//    state = State.ROTATION;
//  }
//
//  public static void setUpCharacter(){
//    //character
//    character = new Character();
//    GStage.addToLayer(GLayer.top, character);
//    character.setPosition(GStage.getWorldWidth() / 2, GStage.getWorldHeight() - 100);
//    //rope
//    rope = new Image(GMain.getAssetManager().getAtlasRegion("ui", "play_rope"));
//    rope.setAlign(AL.t);
//    rope.setOrigin(AL.ct);
//    rope.setScale(0.7f);
//    rope.setPosition(GStage.getWorldWidth() / 2 + character.getWidth() / 2, character.getY() - character.getHeight() + 10 );
////    currPosReset.x = rope.getX();
////    currPosReset.y = rope.getY();
//    currPosRope = (new Vector2(rope.getX(Align.center), rope.getY(Align.top)));
//    GStage.addToLayer(GLayer.top, rope);
//    //hook
//    hook = new Hook();
//    hook.setOrigin(AL.c);
//    hook.setScale(0.7f);
//    GStage.addToLayer(GLayer.top, hook);
//    hook.setPosition(rope.getX() - hook.getWidth() / 2 + 4 , rope.getY() - hook.getHeight() - 20 );
//
//    setupLhook();
//
//    debugLabel = LB.New().font("font_normal").fontScale(0.4f).text("ROTATE: ").pos(-200, - 200 ).parent(character).build();
//    debugLabel2 = LB.New().font("font_normal").fontScale(0.4f).text("ROTATE: ").pos(-200, - 100 ).parent(character).build();
//    touchArea = new Group();
//    touchArea.setSize(GStage.getWorldWidth(), GStage.getWorldHeight());
//    GStage.addToLayer(GLayer.top, touchArea);
//    touchArea.addListener(new ClickListener(){
//      @Override
//      public void clicked(InputEvent event, float x, float y) {
//        super.clicked(event, x, y);
//        state = State.MOVE;
//      }
//    });
//  }
//
//  public static void setupLhook(){
//
//    if(lHook == null){
//      lHook = new Image(GMain.getAssetManager().getAtlasRegion("ui", "hookleft"));
//      lHook.setOrigin(Align.center);
//      lHook.setZIndex(1);
//      GStage.addToLayer(GLayer.top, lHook);
//    }
//
//    state = State.ROTATION;
//    lHook.setVisible(false);
//  }
//
//
//  public static float calculateRopeLength(){
//    float deltaX ;
//    float deltaY ;
//    rope.setOrigin(Align.bottom);
//    if(rope.getY() >= currPosRope.y)
//      deltaY = hook.getY()- currPosRope.y;
//    else
//      deltaY = currPosRope.y - hook.getY();
//
//    if(rope.getX() > currPosRope.x)
//    {
//      deltaX = hook.getX() - currPosRope.x;
//
//    }
//    else
//      deltaX =  currPosRope.x - hook.getX();
//
//
//    if(hook.getX() - currPosHook.x >= 100)
//    {
//      return (float) Math.sqrt((deltaX*deltaX) + (deltaY * deltaY)) + (hook.getHeight() /1.8f);
//    }
//
//    return  (float) Math.sqrt((deltaX*deltaX) + (deltaY * deltaY));
//  }
//
//  public static void update(){
//
//    switch (state){
//      case ROTATION:
//        hookControl.rostateCrane(rope,hook,lHook,ropeRay);
//        break;
//      case MOVE:
//
//        hookControl.moveCrane(rope,hook,lHook, calculateRopeLength(), currPosRope);
//        break;
//    }
//    debugLabel.setText("HOOK Y: " + hook.getY());
//    debugLabel2.setText("ROPE SIZE: " + rope.getHeight());
//
////    character.addAction(GSimpleAction.simpleAction(new GSimpleAction.ActInterface() {
////      @Override
////      public boolean act(float dt, Actor actor) {
////        hookControl.rostateCrane(rope,hook,hook.hook,ropeRay);
////        return false;
////      }
////    }));
//
//  }
//
//  public static void setRopeRay(){
//    //create raycast
//    ropeRay = new Image(GMain.getAssetManager().getAtlasRegion("ui", "play_rope"));
//    ropeRay.setHeight(GStage.getHeight());
//    ropeRay.setOrigin(Align.top);
//    ropeRay.setPosition(GStage.getWorldWidth() / 2 + character.getWidth() / 2, character.getY() - character.getHeight() + 10);
////    ropeRay.setPosition(hook.getWidth()/2 - ropeRay.getWidth()/2, rope.getY() - hook.getHeight() - ropeRay.getHeight());
//    GStage.addToLayer(GLayer.top, ropeRay);
////    ropeRay.setDebug();
//    ropeRay.setVisible(false);
//  }
//
//
//}
