package com.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import com.core.exSprite.GSpineSprite;
import com.core.util.GLayer;
import com.core.util.GScreen;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.GridGroup;
import com.core.utils.hud.HUD;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.GGB;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.builders.SB;
import com.game.GMain;
import com.game.config.PositionConfig;
import com.game.gamelogic.actions.AbsPosition;
import com.game.gamemodel.GameBallModel;
import com.game.gamemodel.GameBottleModel;
import com.game.gameview.Bird;
import com.game.gameview.Branch;



public class BuildLevelScreen extends GScreen {

  HUD hud;
  public Array<GameBallModel> birds;
  public Array<GameBottleModel> bottles;
  public static BuildLevelScreen inst;
  GridGroup birdGrid;
  public static MapGroup ui;
  public static int birdId;
  public static int lv;
  FileHandle fileHandle;
  public static int state;
  public static int bombIdx;
  public static int eggIdx;
  public static int hammerIdx;

  @Override
  public void dispose() {

  }

  @Override
  public void init() {
    hud = GMain.hud();
    state = 1;
    GStage.addToLayer(GLayer.ui, hud);
    hud.clear();
    birds = new Array<>();
    bottles = new Array<>();
    inst = this;
    ui = MGB.New().size(GStage.getWorldWidth(), GStage.getWorldHeight()).pos(0, 0).parent(hud).align(AL.bl).build();


//    initBranch();
    initUI();

    birdGrid = GGB.New().col(0).pad(2).size(GStage.getWorldWidth(), 120).pos(0, 0).align(AL.tl).parent(hud).build();
    birdGrid.setDebug(true);

    for (int i = 0; i < 5; i++) {
      Image birdImg = IB.New().drawable(i + "").build();
      birdGrid.addGrid(birdImg, i + "");
      int finalI = i;

      birdImg.addListener(new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
          super.clicked(event, x, y);
          birdId = finalI;
        }
      });
    }


  }

  @Override
  public void run() {

  }

  public void initBranch() {
    ui.clear();
    float x, y;
    for (int i = 0; i < bottles.size; i++) {
      GameBottleModel bottle = bottles.get(i);
      Branch branch = new Branch(bottle.id);
//      branchView.add(branch);
//      branch.setDebug(true, true);
      if (branch.getId() % 2 == 0) {
        x = 720;
        branch.a.setOriginX(AL.br);
      } else {
        branch.a.setOriginX(AL.bl);

        x = -10;
      }
      y = i * 70 + 150;

      ui.addActor(branch);

      branch.setPosition(x, y);

      for (int j = 0; j < bottle.balls.size; j++) {
        GameBallModel ball = bottle.balls.get(j);
        Bird bird = new Bird(ball.idColor, ball.hasClock, ball.isSleep, ball.bombIdx, ball.eggId, ball.hammerIdx);
        branch.birds.add(bird);
      }

      branch.initBirdBuild();

    }
  }

  public void initUI() {

    //bird grid group

    //add branch button
    Button branch = BB.New().bg("board_item").label("add branch", "birdsort_eff60", 0, 6, AL.c).fontScale(0.5f).pos(0, 0).align(AL.bl).parent(hud).build();
    branch.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
//        GameBottleModel bottle = new GameBottleModel();
//        bottle.id = bottles.size + 1;
//        bottles.add(bottle);
        addBranch();
        System.out.println("SIZE: " + bottles.size);

      }
    });

    //save button
    Button save = BB.New().bg("board_item").label("save", "birdsort_eff60", 0, 6, AL.c).fontScale(0.5f).pos(0, 0).align(AL.cb).parent(hud).build();
    save.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        saveLv();
      }
    });


    //draw clock button
    MapGroup clock = MGB.New().size(100, 100).childs(
        LB.New().font("birdsort_eff60").fontScale(0.5f).text("CLOCK").pos(0, 0).align(AL.c)
    ).pos(0, 120).align(AL.tr).parent(ui).debug(true).build();

    clock.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 2;
      }
    });


    MapGroup sleep = MGB.New().size(100, 100).childs(
        LB.New().font("birdsort_eff60").fontScale(0.5f).text("SLEEP").pos(0, 0).align(AL.c)
    ).pos(150, 120).align(AL.tr).parent(ui).debug(true).build();

    sleep.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 3;
      }
    });


    MapGroup dlt = MGB.New().size(100, 100).childs(
        LB.New().font("font_normal").fontScale(0.5f).text("delete").pos(0, 0).align(AL.c)
    ).pos(300, 120).align(AL.tr).parent(ui).debug(true).build();

    dlt.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 1;
        bottles.clear();
        ui.clear();
        initUI();
      }
    });


    MapGroup bomb = MGB.New().size(100, 100).childs(
        LB.New().font("font_normal").fontScale(0.5f).text("bomb").pos(0, 0).align(AL.c)
    ).pos(450, 120).align(AL.tr).parent(ui).debug(true).build();

    bomb.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 4;
      }
    });


    MapGroup egg1 = MGB.New().size(100, 100).childs(
      IB.New().drawable("egg1").pos(0, 0).align(AL.c)
    ).pos(0, 250).align(AL.tr).parent(ui).debug(true).build();

    egg1.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 5;
        eggIdx = 1;
      }
    });

    MapGroup egg2 = MGB.New().size(100, 100).childs(
        IB.New().drawable("egg2").pos(0, 0).align(AL.c)
    ).pos(150, 250).align(AL.tr).parent(ui).debug(true).build();

    egg2.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 5;
        eggIdx = 2;
      }
    });


    MapGroup hammer1 = MGB.New().size(100, 100).childs(
        LB.New().font("font_normal").fontScale(0.5f).text("hammer 1").pos(0, 0).align(AL.c)
    ).pos(300, 250).align(AL.tr).parent(ui).debug(true).build();

    hammer1.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 6;
        hammerIdx = 1;
      }
    });



    MapGroup hammer2 = MGB.New().size(100, 100).childs(
        LB.New().font("font_normal").fontScale(0.5f).text("hammer 2").pos(0, 0).align(AL.c)
    ).pos(450, 250).align(AL.tr).parent(ui).debug(true).build();

    hammer2.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        state = 6;
        hammerIdx = 2;
      }
    });

  }


  public void saveLv() {
    fileHandle = Gdx.files.local("level/lv" + lv + ".json");

    String str = "{ \"idLv\":" + lv + ",\"bottles\": [";
    for (int i = 0; i < bottles.size; i++) {
      //ball write
      str += "{\"balls\": [";
      for (int j = 0; j < bottles.get(i).balls.size; j++) {
        if (j != bottles.get(i).balls.size - 1) {
          str += bottles.get(i).balls.get(j).idColor + ",";
        } else {
          str += bottles.get(i).balls.get(j).idColor;
        }
      }
      if (i != bottles.size - 1) {
        str += "], ";
      } else {
        str += "],";
      }


      //clock write
      str += "\"clock\": [";
      for (int j = 0; j < bottles.get(i).balls.size; j++) {
        String tmp;
        if (bottles.get(i).balls.get(j).hasClock) {
          tmp = 1 + "";
        } else {
          tmp = 0 + "";
        }

        if (j != bottles.get(i).balls.size - 1) {
          str += tmp + ",";
        } else {
          str += tmp;
        }
      }
      if (i != bottles.size - 1) {
        str += "], ";
      } else {
        str += "],";
      }

      //sleep write
      str += "\"sleep\": [";
      for (int j = 0; j < bottles.get(i).balls.size; j++) {
        String tmp;
        if (bottles.get(i).balls.get(j).isSleep) {
          tmp = 1 + "";
        } else {
          tmp = 0 + "";
        }

        if (j != bottles.get(i).balls.size - 1) {
          str += tmp + ",";
        } else {
          str += tmp;
        }
      }
      if (i != bottles.size - 1) {
        str += "], ";
      } else {
        str += "],";
      }

      //eggWrite

      str += "\"egg\": [";
      for (int j = 0; j < bottles.get(i).balls.size; j++) {
        String tmp;
        GameBallModel ball = bottles.get(i).balls.get(j);

        tmp = ball.eggId +"";

        if (j != bottles.get(i).balls.size - 1) {
          str += tmp + ",";
        } else {
          str += tmp;
        }
      }
      if (i != bottles.size - 1) {
        str += "], ";
      } else {
        str += "],";
      }
      // hammer write
      str += "\"hammer\": [";
      for (int j = 0; j < bottles.get(i).balls.size; j++) {
        String tmp;
        GameBallModel ball = bottles.get(i).balls.get(j);

        tmp = ball.hammerIdx +"";

        if (j != bottles.get(i).balls.size - 1) {
          str += tmp + ",";
        } else {
          str += tmp;
        }
      }
      if (i != bottles.size - 1) {
        str += "], ";
      } else {
        str += "],";
      }


      //bomb write

      str += "\"bomb\": [";

      for (int j = 0; j < bottles.get(i).balls.size; j++){
        String tmp =  -1 + "";
        if (bottles.get(i).balls.get(j).bombIdx != -1){
          tmp = bottles.get(i).balls.get(j).bombIdx + "";
        }

        if (j != bottles.get(i).balls.size - 1) {
          str += tmp + ",";
        } else {
          str += tmp;
        }


      }
      if (i != bottles.size - 1) {
        str += "]}, ";
      } else {
        str += "]}";
      }


    }
    str += "]}";

    System.out.println(str);
    fileHandle.writeString(str, false);


  }

  public void addBranch() {
    float x, y;

    GameBottleModel bottle = new GameBottleModel();
    bottle.balls = new Array<>();
    bottle.id = bottles.size + 1;
    System.out.println("branch id: " + bottle.id);

    bottles.add(bottle);

    Branch branch = new Branch(bottle.id);
//      branchView.add(branch);
//      branch.setDebug(true, true);
    if (branch.getId() % 2 == 0) {
      x = 720;
      branch.a.setOriginX(AL.br);
    } else {
      branch.a.setOriginX(AL.bl);

      x = -10;
    }
    y = bottle.id * 70 + 150;

    ui.addActor(branch);

    branch.setPosition(x, y);


    branch.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        super.clicked(event, x, y);
        if (state == 1) { //draw

          if (bottles.get(bottle.id - 1).balls.size >= 4) {
            return;
          }
          GameBallModel bird = new GameBallModel(birdId);
          bottles.get(bottle.id - 1).balls.add(bird);

          Image birdImg = IB.New().drawable(birdId + "").scale(0.5f, 0.5f).build();
          birdImg.setDebug(true);
          float desX, desY;
          desY = AbsPosition.absPos(branch, AL.bl).y;
          if (branch.getId() % 2 == 0) {
            desX = PositionConfig.rightPos.get(bottles.get(bottle.id - 1).balls.size - 1);
          } else {
            desX = PositionConfig.leftPos.get(bottles.get(bottle.id - 1).balls.size - 1);
          }
          ui.addActor(birdImg);
          birdImg.setPosition(desX, desY);
          birdImg.addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
              super.clicked(event, x, y);

              float desX, desY;
              desX = AbsPosition.absPos(birdImg, AL.bl).x;
              desY = AbsPosition.absPos(birdImg, AL.bl).y;

              if (state == 2) { //clock

                Image clock = IB.New().drawable("clock").scale(0.4f, 0.4f).build();
                ui.addActor(clock);
                clock.setPosition(desX - 30, desY);
                clock.setOrigin(AL.c);
                bird.hasClock = true;
              }

              if (state == 3) { //sleep
                Label sleep = LB.New().font("font_normal").fontScale(0.3f).text("sleep").build();
                ui.addActor(sleep);
                sleep.setPosition(desX, desY);
                sleep.setOrigin(AL.c);
                bird.isSleep = true;
              }

              if (state == 4) { //drawBomb
                bird.bombIdx = bombIdx;
                Label sleep = LB.New().font("font_normal").fontScale(0.2f).text("bomb: " + bombIdx).build();
                sleep.setTouchable(Touchable.disabled);
                ui.addActor(sleep);
                sleep.setPosition(desX, desY);
                sleep.setOrigin(AL.c);
              }

              if (state == 5){ //draw egg
                birdImg.setDrawable(IB.New().drawable("egg"+eggIdx).build().getDrawable());
                bird.eggId = eggIdx;
              }

              if (state == 6){ // hammer
                bird.hammerIdx = hammerIdx;
                Label hammer = LB.New().font("font_normal").fontScale(0.4f).text("ham: " + hammerIdx).build();
                hammer.setTouchable(Touchable.disabled);
                ui.addActor(hammer);
                hammer.setPosition(desX, desY);
                hammer.setOrigin(AL.c);
              }
            }
          });
        }

      }
    });


  }


}
