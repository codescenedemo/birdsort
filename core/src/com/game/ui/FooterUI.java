package com.game.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.external.EventHandler;
import com.game.GMain;
import com.game.config.RemoteConfig;
import com.game.object.BoardGame;

public class FooterUI extends BaseUI implements EventHandler {

  BoardGame boardGame;
  boolean isAdded;

  public FooterUI(BoardGame boardGame) {
    isAdded = false;
    this.setTouchable(Touchable.childrenOnly);
    this.setSize(720, GStage.getWorldHeight());
    this.boardGame = boardGame;

    GMain.hud().regisHandler("footerEvent", this);
    GMain.hud().index("footer", this);

    MGB.New().size(720, 100).debug(false).childs(
        BB.New().bg("btn_undo").fontScale(0.6f).pos(140, 0).align(AL.cr).idx("undo").childs(
            BB.New().bg("btn_green").label(GMain.session().inventory.undo + "", "birdsort_eff60", 0, 1, AL.c).fontScale(0.5f).idx("quantity").pos(0, 0).align(AL.br),
            BB.New().bg("btn_ads").fontScale(0.3f).idx("ads").pos(0, 0).visible(false).align(AL.br)
        ),
        BB.New().bg("btn_branch").pos(250, 0).align(AL.cr).idx("branch").childs(
            BB.New().bg("btn_green").label(GMain.session().inventory.branch + "", "birdsort_eff60", 0, 1, AL.c).fontScale(0.3f).idx("quantity").pos(0, 0).align(AL.br),
            BB.New().bg("btn_ads").fontScale(0.3f).idx("ads").pos(0, 0).visible(false).align(AL.br)

        ),
        BB.New().bg("btn_next").pos(30, 0).align(AL.cr).idx("next").child(
            BB.New().bg("btn_ads").fontScale(0.3f).idx("ads").pos(0, 0).visible(true).align(AL.br)
        ),
        BB.New().bg("btn_shop").pos(20, 0).align(AL.cl).idx("shop")
    ).pos(0, 140).align(AL.cb).parent(this).idx("root").build();


    GMain.hud().clickConnect("footer/root/undo", "footerEvent", "undo", 0, null);
    GMain.hud().clickConnect("footer/root/branch", "footerEvent", "addBranch", 0, null);
    GMain.hud().clickConnect("footer/root/next", "footerEvent", "next", 0, null);
    GMain.hud().clickConnect("footer/root/shop", "shopEvent", "show", 0, null);

    updateQuantity();
  }


  @Override
  public void handleEvent(Actor actor, String action, int intParam, Object objParam) {
    switch (action) {

      case "show":
        updateQuantity();
        this.query("root/branch", Button.class).setVisible(true);
        break;
      case "undo":

        if (GMain.session().inventory.undo > 0) {
          if (BoardGame.boardGame.undoStacks.isEmpty()) {
            return;
          }

          GMain.session().inventory.undo--;
          boardGame.undo();
          updateQuantity();
        } else {
          GMain.showVideoReward(success -> {
            if (success) {
              BoardGame.trackEventVideoAds("undo");
              GMain.session().inventory.undo += RemoteConfig.getUndoAds();
              updateQuantity();
            } else {
              showToast("VIDEO NOT READY");
            }
          });
        }
        break;
      case "addBranch":
        if (GMain.session().inventory.branch > 0){
          boardGame.addBranch();
          GMain.session().inventory.branch--;
          GMain.syncSession();
          isAdded = true;
          this.query("root/branch", Button.class).setVisible(false);

          return;
        }

        GMain.showVideoReward(success -> {
          if (success) {
            BoardGame.trackEventVideoAds("add_branch");

            boardGame.addBranch();
            isAdded = true;
            this.query("root/branch", Button.class).setVisible(false);

          } else {
            showToast("VIDEO NOT READY");
          }
        });

        break;
      case "next":
        GMain.showVideoReward(success -> {
          if (success) {
            BoardGame.trackEventVideoAds("skip_level");
            GMain.session().profile.level++;
            BoardGame.boardGame.newG();
          } else {
            showToast("VIDEO NOT READY");
          }
        });

        break;
      case "updateText":
        Button undo = this.query("root/undo", Button.class);
        undo.setText("Undo: " + intParam);
        break;
    }
  }

  @Override
  void initHUDHandler() {

  }

  @Override
  protected void initUI() {

  }

  @Override
  protected void initEvent() {

  }

  public void updateQuantity() {
    Button quantityBranch = this.query("root/branch/quantity", Button.class);
    quantityBranch.setText(GMain.session().inventory.branch + "");
    Button quantityUndo = this.query("root/undo/quantity", Button.class);
    quantityUndo.setText(GMain.session().inventory.undo + "");
    if (GMain.session().inventory.undo <= 0) {
      this.query("root/undo/ads", Button.class).setVisible(true);
      quantityUndo.setVisible(false);
    }else {
      this.query("root/undo/ads", Button.class).setVisible(false);
      quantityUndo.setVisible(true);

    }

    if (GMain.session().inventory.branch <= 0) {
      this.query("root/branch/ads", Button.class).setVisible(true);
      quantityBranch.setVisible(false);
    }else {
      this.query("root/branch/ads", Button.class).setVisible(false);
      quantityBranch.setVisible(true);
    }


  }
}
