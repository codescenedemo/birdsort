package com.game.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.CBB;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.external.EventHandler;
import com.game.GMain;
import com.game.config.RemoteConfig;
import com.game.object.BoardGame;

public class HeaderUI extends BaseUI implements EventHandler {


  boolean isSetting;

  public HeaderUI() {
    this.setTouchable(Touchable.childrenOnly);
    GMain.hud().index("header", this);
    GMain.hud().regisHandler("headerEvent", this);
    this.setSize(720, GStage.getWorldHeight());
    MGB.New().size(720, 200).pos(0, 0).align(AL.tl).idx("root").childs(
        LB.New().font("birdsort_eff60").text("Lv: " + GMain.session().profile.level).pos(0, 0).align(AL.c).idx("lv"),
        BB.New().bg("btn_replay").pos(50, 0).align(AL.cr).idx("resetBtn")
    ).parent(this).build();

    MGB.New().size(70, 300).pos(50, 50).align(AL.tl).idx("settingGr").parent(this).childs(
        IB.New().drawable("setting_bar").pos(0, -30).align(AL.c).idx("panel").visible(false),
        BB.New().bg("btn_setting").pos(0, 0).align(AL.ct).idx("btnSetting"),
        BB.New().bg("btn_sound").pos(0, 0).align(AL.ct).idx("btnSound").visible(false),
        BB.New().bg("btn_music").pos(0, 0).align(AL.ct).idx("btnMusic").visible(false),
        BB.New().bg("btn_vibrate").pos(0, 0).align(AL.ct).idx("btnVibrate").visible(false)
    ).build();


    Button st = this.query("settingGr/btnSetting", Button.class);
    st.setZIndex(100);





    isSetting = false;
    loadUI();
    GMain.hud().clickConnect("header/root/resetBtn", "headerEvent", "resetLv", 0, null);
    GMain.hud().clickConnect("header/settingGr/btnSetting", "headerEvent", "setting", 0, null);
    GMain.hud().clickConnect("header/settingGr/btnVibrate", "headerEvent", "vibrate", 0, null);
    GMain.hud().clickConnect("header/settingGr/btnSound", "headerEvent", "sound", 0, null);
    GMain.hud().clickConnect("header/settingGr/btnMusic", "headerEvent", "music", 0, null);
  }

  @Override
  public void handleEvent(Actor actor, String action, int intParam, Object objParam) {
    switch (action) {
      case "show":
        this.query("root/lv", Label.class).setText("Lv: " + GMain.session().profile.level);
        break;
      case "resetLv":
        GMain.showFullScreen(() -> {
        }, () -> {
          GMain.platform().TrackCustomEvent("show_full_screen");
          BoardGame.boardGame.newG();
        });

        break;
      case "setting":
        Button btnVibrate = GMain.hud().query("header/settingGr/btnVibrate", Button.class);
        Button btnSound = GMain.hud().query("header/settingGr/btnSound", Button.class);
        Button btnMusic = GMain.hud().query("header/settingGr/btnMusic", Button.class);
        Button btnSetting = GMain.hud().query("header/settingGr/btnSetting", Button.class);

        if (!isSetting) {
          this.addAction(Actions.sequence(
              Actions.run(() -> {
                btnSetting.setTouchable(Touchable.disabled);
              }),
              Actions.run(() -> {
                this.query("settingGr/panel", Image.class).setVisible(true);
                btnVibrate.setVisible(true);
                btnVibrate.addAction(Actions.moveBy(0, -110, 0.3f, Interpolation.swingOut));

                btnSound.setVisible(true);
                btnSound.addAction(Actions.moveBy(0, -180, 0.3f, Interpolation.swingOut));

                btnMusic.setVisible(true);
                btnMusic.addAction(Actions.moveBy(0, -250, 0.3f, Interpolation.swingOut));
                isSetting = true;
              }),
              Actions.delay(0.3f, Actions.run(() -> {
                btnSetting.setTouchable(Touchable.enabled);
              }))
          ));

        }
        else {
          isSetting = false;
          this.query("settingGr/panel", Image.class).setVisible(false);


          btnSetting.setTouchable(Touchable.disabled);
          btnVibrate.addAction(Actions.sequence(

              Actions.moveBy(0, 110, 0.2f, Interpolation.slowFast),
              Actions.run(() -> {
                btnVibrate.setVisible(false);

              })
          ));
          btnSound.addAction(Actions.sequence(
              Actions.moveBy(0, 180, 0.2f, Interpolation.slowFast),
              Actions.run(() -> {
                btnSound.setVisible(false);
              })
          ));
          btnMusic.addAction(Actions.sequence(
              Actions.moveBy(0, 250, 0.2f, Interpolation.slowFast),
              Actions.run(() -> {
                btnMusic.setVisible(false);

                btnSetting.setZIndex(100);
                btnSetting.setTouchable(Touchable.enabled);
              })
          ));
        }
        break;
      case "vibrate":
        vibrateClick();
        break;
      case "sound":
        soundClick();
        break;
      case "music":
        musicClick();
        break;
      case "fullScreen":
        GMain.hud().fireEvent("rewardEvent", "show", 0, null);
        if (GMain.session().profile.level > RemoteConfig.getFullScreenLevel()){
          this.addAction(Actions.delay(0.5f, Actions.run(() -> {
            GMain.showFullScreen(() -> {
            }, () -> {

              GMain.platform().TrackCustomEvent("show_full_screen");

            });
          })));
        }

        break;
    }
  }

  public void vibrateClick(){

    boolean isVibrate = GMain.session().setting.isVibrate;
    Button vibrateBtn = this.query("settingGr/btnVibrate", Button.class);
    if (isVibrate){
      GMain.session().setting.isVibrate = false;
      vibrateBtn.setBg(IB.New().drawable("btn_vibrateoff").build());
    }else {
      GMain.session().setting.isVibrate = true;
      vibrateBtn.setBg(IB.New().drawable("btn_vibrate").build());
    }

    GMain.syncSession();
  }

  public void soundClick(){

    boolean isSound = GMain.session().setting.isSoundOn;
    Button soundBtn = this.query("settingGr/btnSound", Button.class);
    if (isSound){
      GMain.session().setting.isSoundOn = false;
      soundBtn.setBg(IB.New().drawable("btn_soundoff").build());
      GSound.setSoundSilence(true);
    }else {
      GMain.session().setting.isSoundOn = true;
      soundBtn.setBg(IB.New().drawable("btn_sound").build());
      GSound.setSoundSilence(false);

    }
    GMain.syncSession();

  }

  public void musicClick(){

    boolean isMusic = GMain.session().setting.isMusicOn;
    Button musicBtn = this.query("settingGr/btnMusic", Button.class);
    if (isMusic){
      GMain.session().setting.isMusicOn = false;
      musicBtn.setBg(IB.New().drawable("btn_musicoff").build());
      GSound.setMusicSilence(true);
    }else {
      GMain.session().setting.isMusicOn = true;
      musicBtn.setBg(IB.New().drawable("btn_music").build());
      GSound.setMusicSilence(false);

    }
    GMain.syncSession();

  }



  public void loadUI(){
    Button vibrateBtn = this.query("settingGr/btnVibrate", Button.class);
//    Button vibrateBtn = this.query("settingGr/btnVibrate", Button.class);
    Button soundBtn = this.query("settingGr/btnSound", Button.class);
    Button musicBtn = this.query("settingGr/btnMusic", Button.class);

    if (GMain.session().setting.isVibrate){
      vibrateBtn.setBg(IB.New().drawable("btn_vibrate").build());

    }else {
      vibrateBtn.setBg(IB.New().drawable("btn_vibrateoff").build());
    }

    if (GMain.session().setting.isSoundOn){
      soundBtn.setBg(IB.New().drawable("btn_sound").build());
    }else {
      soundBtn.setBg(IB.New().drawable("btn_soundoff").build());
    }

    if (GMain.session().setting.isMusicOn){
      musicBtn.setBg(IB.New().drawable("btn_music").build());
    }else {
      musicBtn.setBg(IB.New().drawable("btn_musicoff").build());
    }
  }

  @Override
  void initHUDHandler() {

  }

  @Override
  protected void initUI() {

  }

  @Override
  protected void initEvent() {

  }
}
