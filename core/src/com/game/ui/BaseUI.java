package com.game.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.DragListener;
import com.badlogic.gdx.utils.Align;
import com.core.exSprite.particle.GParticleSprite;
import com.core.exSprite.particle.GParticleSystem;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.MGB;
import com.game.GMain;
import com.platform.AdsClose;

public abstract class BaseUI extends MapGroup {
  Image ov;

  public BaseUI() {
    super(GStage.getWorldWidth(), GStage.getWorldHeight());
//    ov = IB.New().solid("FFFFFF", 0.8f).size(GStage.getWorldWidth(), GStage.getWorldHeight() * 3).build();
  }

  abstract void initHUDHandler();

  public void actionShow() {
    actionShow(0, 0);
  }

  public void actionShow(float padX, float padY) {
    this.setY(GStage.getWorldHeight());
    this.addAction(Actions.moveToAligned(GStage.getWorldWidth() / 2 + padX, GStage.getWorldHeight() / 2 + padY, AL.c, 0.6f, Interpolation.swingOut));
  }

  static MapGroup toast;

  public static void showToast(String content) {
    if (toast != null)
      toast.remove();
    System.out.println("TOAST");
    toast = MGB.New().size(600, 150).pos(0, -150).align(AL.ct).parent(GMain.hud()).childs(
        IB.New().nPatch("level", 50, 50, 50, 50).size(600, 150).align(AL.c),
        LB.New().text(content).font("font_normal").fontColor("b8763b").fontScale(.7f).wrap(true, 500).pos(0, 10).idx("title").align(AL.c)
    ).build();
    toast.addAction(Actions.sequence(
        Actions.moveTo(toast.getX(), GMain.hud().getHeight() - 170, 0.2f, Interpolation.swingOut),
        Actions.delay(2),
        Actions.moveTo(toast.getX(), GMain.hud().getHeight(), 0.2f, Interpolation.swingIn),
        Actions.removeActor()
    ));
    toast.addListener(new DragListener() {
      float startY;

      @Override
      public void dragStart(InputEvent event, float x, float y, int pointer) {
        startY = y;
        super.dragStart(event, x, y, pointer);
      }

      @Override
      public void dragStop(InputEvent event, float x, float y, int pointer) {
        float dy = Math.abs(y - startY);
        if(y > startY && dy > 20){
          toast.clearActions();
          toast.addAction(Actions.sequence(
              Actions.moveTo(toast.getX(), GMain.hud().getHeight(), 0.2f, Interpolation.swingIn),
              Actions.removeActor()
          ));
        }
        super.dragStop(event, x, y, pointer);
      }
    });
  }

  public static void moneyAnimBridge(int nCoin, float sx, float sy, float ex, float ey, Runnable afterEff) {
    Vector2 src = new Vector2(sx, sy);
    Vector2 dest = new Vector2(ex, ey);
    float time;
    float duration = 0;
    float scl = 1;
    float maxLen = 0;
    float maxDur = 0;
    for (int i = 0; i < nCoin; i++) {
      Image sprite = new Image(GMain.getAssetManager().getTextureRegion("ui", "coin"));
      float dx = MathUtils.random(-60 * scl, 60 * scl);
      float dy = MathUtils.random(-60 * scl, 60 * scl);
      float len = src.dst(src.cpy().set(src.x + dx, src.y + dy));
      time = len / 200;
      duration = src.dst(dest) / 1000;
      if (len > maxLen)
        maxLen = len;
      if (duration > maxDur)
        maxDur = duration;
      sprite.setPosition(src.x, src.y);
      sprite.setOrigin(Align.center);
      sprite.setScale(scl);
      sprite.addAction(Actions.sequence(
          Actions.moveTo(src.x + dx, src.y + dy, time),
          Actions.delay(time),
          Actions.moveTo(dest.x, dest.y, duration, Interpolation.swingIn),
          Actions.run(() -> {
            sprite.remove();
            GParticleSprite eff = GParticleSystem.getGParticleSystem("money_tick").create(GMain.hud(), (int) ex + 22, (int) ey + 22);
            eff.setLoop(false);
          })
      ));
      GMain.hud().addActor(sprite);
    }
//    GSound.playSound(SoundsConfig.FLYCOIN);
    GMain.hud().addAction(Actions.delay(maxDur + maxLen / 200 + 0.5f, Actions.run(() -> {
      if (afterEff != null)
        afterEff.run();
    })));

//    GMain.hud().addAction(Actions.delay(duration + maxLen / 200, Actions.run(() -> GSound.playSound(SoundsConfig.COLLECTGEMS))));
  }

  private static final Vector2 temp = new Vector2();

  public static Vector2 absPos(Actor c, float x, float y, int align) {
    return c.localToStageCoordinates(temp.cpy().set(c.getX(align) + x - c.getX(), c.getY(align) + y - c.getY()));
  }

  public static Vector2 absPos(Actor c, int align) {
    return c.localToStageCoordinates(temp.cpy().set(c.getX(align) - c.getX(), c.getY(align) - c.getY()));
  }

  protected abstract void initUI();
  protected abstract void initEvent();
}
