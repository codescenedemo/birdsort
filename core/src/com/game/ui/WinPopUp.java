package com.game.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.core.util.GScreen;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.external.EventHandler;
import com.game.GMain;

public class WinPopUp extends BaseUI implements EventHandler {


  Image ovl;

  public WinPopUp(){
    this.setTouchable(Touchable.childrenOnly);
    GMain.hud().index("winUI", this);
    ovl = IB.New().overlay(GStage.getWorldWidth(), GStage.getWorldHeight(), 0.7f).pos(0, 0).align(AL.bl).parent(this).build();
    GMain.hud().regisHandler("winEvent", this);
    MGB.New().size(500, 500).pos(0, 0).align(AL.c).parent(this).childs(

    ).idx("root").build();
    this.setVisible(false);
  }

  @Override
  public void handleEvent(Actor actor, String action, int intParam, Object objParam) {
    switch (action){
      case "show":
        GMain.showFullScreen(() -> {
        }, () -> {
          GMain.platform().TrackCustomEvent("show_full_screen");
          GMain.me.setScreen(GMain.curScreen());
        });

        break;
    }
  }

  @Override
  void initHUDHandler() {

  }

  @Override
  protected void initUI() {

  }

  @Override
  protected void initEvent() {

  }
}
