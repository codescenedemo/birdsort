package com.game.ui;

import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.ProgressBar;
import com.core.utils.hud.builders.AbstractActorBuilder;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.builders.PBB;
import com.core.utils.hud.external.BuilderAdapter;
import com.core.utils.hud.external.EventHandler;
import com.game.GMain;
import com.game.config.RemoteConfig;
import com.game.gamedto.RewardDTO;
import com.game.object.BoardGame;

public class RewardPopUp extends BaseUI implements EventHandler {

  Image ovl;
  MapGroup root;

  public RewardPopUp() {
    int rewardIdx = GMain.session().inventory.rewardIdx;

    System.out.println("reward IDX: " + rewardIdx);
    this.setTouchable(Touchable.childrenOnly);
    this.setVisible(false);
    GMain.hud().index("reward", this);
    GMain.hud().regisHandler("rewardEvent", this);

    this.setOrigin(AL.c);

    ovl = IB.New().overlay(GStage.getWorldWidth(), GStage.getWorldHeight(), 0.75f).parent(this).pos(0, 0).align(AL.bl).build();
    ovl.setScale(1);

    root = MGB.New().size(465, 738).pos(50, 120).align(AL.c).parent(this).idx("root").childs(
        IB.New().drawable("popup_complete").pos(0, 0).align(AL.bl),
        BB.New().bg("tittle").label("COMPLETE", "birdsort_eff60", 0, 0, AL.c).pos(0, 150).align(AL.ct),
        LB.New().font("birdsort_eff60").text("A").pos(0, 0).align(AL.c).idx("text").visible(false),
        PBB.New().bg("progress_out").fg("progress_in").offSet(0, 0).pc(10 * GMain.session().profile.progress).idx("bar").pos(-10, 20).align(AL.cb).touchable(false),
        PBB.New().bg("box").fg("avatar_bg1").offSet(0, 0).pc(10 * GMain.session().profile.progress).idx("barReward").pos(-25, -85).align(AL.c).touchable(false),
        LB.New().font("birdsort_eff60").text(GMain.session().profile.progress * 10 + "%").fontScale(0.7f).pos(-10, 20).align(AL.cb).idx("txtBar"),

        BB.New().bg("btn_nextlevel").label("Next Level", "birdsort_eff60", 0, 5, AL.c).fontScale(0.75f).pos(-28, -140).align(AL.cb).idx("confirm")
    ).build();



    root.setScale(0);

    GMain.hud().clickConnect("reward/root/confirm", "rewardEvent", "confirm", 0, null);
  }


  public void changeReward(String drawable){
//    root.query("barReward", ProgressBar.class).setDebug(true, true);
//    root.query("barReward", ProgressBar.class).getPg().setDrawable(AbstractActorBuilder.adapter.getDrawable(drawable));
//    root.query("barReward", ProgressBar.class).getPg().setPosition(, 0, AL.bl);
    root.query("barReward", ProgressBar.class).remove();
    PBB.New().bg("box").fg(drawable).offSet(0, 0).pc(10 * GMain.session().profile.progress).idx("barReward").pos(-25, -85).align(AL.c).touchable(false).parent(root).build();

  }


  @Override
  public void handleEvent(Actor actor, String action, int intParam, Object objParam) {
    switch (action) {
      case "hide":
        this.setVisible(false);
        this.query("root/bar", ProgressBar.class).setPercent(0, false);
        this.query("root/barReward", ProgressBar.class).setPercent(0, false);
        break;
      case "show":


        if (GMain.session().inventory.rewardIdx <= 10) {
          RewardDTO dto = RewardDTO.rewards.get(GMain.session().inventory.rewardIdx);
          changeReward(dto.drawable);
          this.query("root/text", Label.class).setText(dto.drawable);
          changeReward(dto.drawable);

          GMain.session().profile.progress++;
          GMain.syncSession();
          this.setVisible(true);


          Button next = this.query("root/confirm", Button.class);
          next.clearActions();
          next.setScale(0);
          root.addAction(Actions.sequence(
              Actions.scaleTo(1, 1, 0.35f, Interpolation.swingOut),
              Actions.run(() -> {
                this.query("root/bar", ProgressBar.class).setPercent(GMain.session().profile.progress * 10, true);
                this.query("root/barReward", ProgressBar.class).setPercent(GMain.session().profile.progress * 10, true);
                this.query("root/txtBar", Label.class).setText(GMain.session().profile.progress * 10 + "%");
              }),
              Actions.delay(0.15f),
              Actions.run(() -> {
                if (GMain.session().profile.progress != 10) {
                  next.addAction(Actions.sequence(
                      Actions.scaleTo(1, 1, 0.4f),
                      Actions.forever(Actions.sequence(
                          Actions.scaleTo(0.95f, 0.95f, 0.7f),
                          Actions.scaleTo(1, 1, 0.7f)
                      ))
                  ));
                } else {
                  next.setScale(0);
                }

              })

          ));
//          this.query("root/bar", ProgressBar.class).setPercent(GMain.session().profile.progress * 20, true);
//          this.query("root/txtBar", Label.class).setText(GMain.session().profile.progress * 20+"%");
          if (GMain.session().profile.progress == 10) {
            this.addAction(Actions.delay(0.8f, Actions.run(() -> {
              GMain.session().inventory.unlockedIdx = GMain.session().inventory.rewardIdx;

              switch (RewardDTO.rewards.get(GMain.session().inventory.rewardIdx).type) {
                case "bg":
                  GMain.hud().fireEvent("claimEvent", "claim", dto.id, "bg");
                  break;
                case "branch":
                  GMain.hud().fireEvent("claimEvent", "claim", dto.id, "branch");
                  break;
              }

              GMain.session().inventory.rewardIdx++;
              GMain.session().profile.progress = 0;
              GMain.syncSession();

            })));
          }
        } else {
          GMain.session().profile.level++;

          BoardGame.boardGame.newG();

        }
        break;
      case "confirm":
        this.setVisible(false);
        GMain.session().profile.level++;
        BoardGame.boardGame.newG();
        break;
    }
  }

  @Override
  void initHUDHandler() {
  }

  @Override
  protected void initUI() {

  }

  @Override
  protected void initEvent() {

  }
}
