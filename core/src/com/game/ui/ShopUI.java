package com.game.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.GridGroup;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.GGB;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.external.EventHandler;
import com.game.GMain;
import com.game.gamedto.BackgroundDTO;
import com.game.gamedto.BranchDTO;
import com.game.gameview.Branch;
import com.game.object.BoardGame;

public class ShopUI extends BaseUI implements EventHandler {


  Image ovl;
  GridGroup branchGrid;
  GridGroup themeGrid;

  Button branch, themes;

  public ShopUI() {
    this.setTouchable(Touchable.childrenOnly);
    this.setVisible(false);
    GMain.hud().index("shop", this);
    GMain.hud().regisHandler("shopEvent", this);
    ovl = IB.New().overlay(GStage.getWorldWidth(), GStage.getWorldHeight(), 0.6f).pos(0, 0).align(AL.bl).parent(this).build();
    MGB.New().size(642, 762).pos(0, -100).align(AL.c).debug(false).childs(
        IB.New().drawable("popup_shop").pos(0, 0).align(AL.bl),
        BB.New().bg("tab_branch1").pos(-90, -175).align(AL.ct).idx("branch"),
        BB.New().bg("tab_themes1").pos(130, -175).align(AL.ct).idx("themes"),
        IB.New().drawable("roof_shop").pos(0, -85).align(AL.ct),
        BB.New().bg("btn_exit_shop").pos(-50, -50).align(AL.tr).idx("exitBtn"),
        LB.New().font("birdsort_eff60").text("MY THEMES").wrap(true, 365).fontScale(0.8f).pos(0, 124).align(AL.ct).idx("text"),


        GGB.New().pad(2).col(3).size(562, 490).pos(0, -60).align(AL.c).idx("branchCol"),
        GGB.New().pad(2).col(3).size(562, 490).pos(0, -60).align(AL.c).idx("theme")
    ).idx("root").parent(this).build();

    branchGrid = this.query("root/branchCol", GridGroup.class);
    themeGrid = this.query("root/theme", GridGroup.class);
    themeGrid.setVisible(false);

    branch = this.query("root/branch", Button.class);
    themes = this.query("root/themes", Button.class);

    GMain.hud().clickConnect("shop/root/exitBtn", "shopEvent", "exit", 0, null);
    GMain.hud().clickConnect("shop/root/branch", "shopEvent", "branch", 0, null);
    GMain.hud().clickConnect("shop/root/themes", "shopEvent", "theme", 0, null);

  }

  @Override
  public void handleEvent(Actor actor, String action, int intParam, Object objParam) {
    switch (action) {
      case "show":
        if (GMain.session().profile.level <= 4){
          showToast("UNLOCK AT LEVEL 5");
          return;
        }
        loadTheme();
        this.setVisible(true);
        break;
      case "exit":
        this.setVisible(false);
      case "branch":
        loadBranch();
        break;
      case "theme":
        loadTheme();
        break;

    }
  }

  public void loadBranch() {



//    themes.clearActions();
//    branch.clearActions();


//    branch.addAction(Actions.sequence(
//        Actions.moveBy(0, 30f),
//        Actions.forever(Actions.sequence(
//            Actions.moveBy(0, 10, 0.5f),
//            Actions.moveBy(0, -10, 0.5f)
//        ))
//    ));

    themes.setBg(IB.New().drawable("tab_themes2").build());
    branch.setBg(IB.New().drawable("tab_branch1").build());
    this.query("root/text", Label.class).setText("MY BRANCHES");

    branchGrid.setVisible(true);
    themeGrid.setVisible(false);
    branchGrid.clearGridChildren();




    for (int i = 1; i <= BranchDTO.branchs.size; i++) {
      BranchDTO dto = BranchDTO.branchs.get(i);
      Image br = IB.New().drawable("avatar_branch"+dto.id).pos(0, 0).align(AL.c).build();
      br.setOrigin(AL.c);
      Image tick = IB.New().drawable("check").pos(0, 0).align(AL.c).idx("tick").build();
      Image ovl = IB.New().overlay(187, 236, 0.68f).pos(0, 0).align(AL.bl).build();
      Label lb = LB.New().font("birdsort_eff60").fontScale(0.4f).text("UNLOCK AT LV " + dto.levelUnlock).wrap(true, 175).touchable(false).build();
      MapGroup gr = MGB.New().size(180, 230).childs(
          IB.New().drawable("shop_paper").pos(0, 0).align(AL.c)
//          LB.New().font("birdsort_eff60").text(dto.drawable).fontScale(0.4f).pos(0, 0).align(AL.c)
      ).build();
      gr.addActor(br, AL.c);
      gr.addActor(tick, AL.c);
      gr.addActor(ovl);
      gr.addActor(lb, AL.c);

      ovl.setPosition(0, 0, AL.bl);
      branchGrid.addGrid(gr, i + "");
//      gr.setDebug(true);
      tick.setVisible(false);
      if (i == GMain.session().profile.choosenBranchIdx) {
        tick.setVisible(true);
        tick.setVisible(true);
        for (int j = 0; j < BoardGame.boardGame.branchView.size; j++){
          Branch branch = BoardGame.boardGame.branchView.get(j);
          branch.a.setDrawable(IB.New().drawable("branch"+GMain.session().profile.choosenBranchIdx).build().getDrawable());
        }
      }
      int idx = GMain.session().inventory.unlockBranchId.get(i, -2);


      if (idx != -2) {
        gr.addListener(new ClickListener() {
          @Override
          public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            branchClick(idx);


          }
        });
        ovl.setVisible(false);
        lb.setVisible(false);
      }

      if (idx == -1) {
        Image ads = IB.New().drawable("btn_ads").build();
        gr.addActor(ads, AL.cb);
        ovl.setVisible(true);
        lb.setVisible(false);

        int finalI = i;

        ovl.addListener(new ClickListener() {
          @Override
          public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            System.out.println("CLICK");
            GMain.showVideoReward(sucess -> {
              BoardGame.trackEventVideoAds("claimSkin");
              if (sucess) {
                GMain.session().inventory.unlockBranchId.put(finalI, finalI);
                GMain.session().profile.choosenBranchIdx = finalI;
                loadBranch();
                for (int i = 0; i < BoardGame.boardGame.branchView.size; i++){
                  Branch branch = BoardGame.boardGame.branchView.get(i);
                  branch.a.setDrawable(IB.New().drawable("branch"+GMain.session().profile.choosenBranchIdx).build().getDrawable());
                }

                GMain.syncSession();
              } else {
                showToast("VIDEO NOT READY");
              }
            });
          }
        });
      }
    }
  }

  public void themeClick(int idx) {




    GMain.session().profile.choosenBgIdx = idx;

    loadTheme();
    GMain.syncSession();
  }

  public void branchClick(int idx) {
    GMain.session().profile.choosenBranchIdx = idx;

    loadBranch();

    GMain.syncSession();
  }

  public void loadTheme() {


    themes.setBg(IB.New().drawable("tab_themes1").build());
    branch.setBg(IB.New().drawable("tab_branch2").build());
    this.query("root/text", Label.class).setText("MY THEMES");




    branchGrid.setVisible(false);
    themeGrid.setVisible(true);
    themeGrid.clearGridChildren();

    //init grid
    for (int i = 1; i <= BackgroundDTO.backgrounds.size; i++) {
      BackgroundDTO dto = BackgroundDTO.backgrounds.get(i);
      Image bg = IB.New().drawable("avatar_bg"+dto.id).size(150, 200).pos(0, 0).align(AL.bl).build();
      Image tick = IB.New().drawable("check").pos(0, 0).align(AL.c).idx("tick").build();
      Image ovl = IB.New().overlay(180, 230, 0.68f).pos(0, 0).align(AL.bl).build();
      Label lb = LB.New().font("birdsort_eff60").fontScale(0.4f).text("UNLOCK AT LV " + dto.levelUnlock).wrap(true, 175).touchable(false).build();

      MapGroup gr = MGB.New().size(180, 230).childs(
          IB.New().drawable("shop_paper").pos(0, 0).align(AL.c),
          LB.New().font("birdsort_eff60").text(dto.drawable).fontScale(0.4f).pos(0, 0).align(AL.c)
      ).build();
      gr.addActor(bg, AL.c);
      gr.addActor(ovl);
      gr.addActor(lb, AL.c);
      ovl.setPosition(0, 0, AL.bl);
      gr.addActor(tick, AL.c);
      tick.setVisible(false);

      if (i == GMain.session().profile.choosenBgIdx) {
        tick.setVisible(true);
        BoardGame.boardGame.bg.setDrawable(IB.New().drawable("bg"+GMain.session().profile.choosenBgIdx).build().getDrawable());

      }


      themeGrid.addGrid(gr, i + "");
//      gr.setDebug(true`);


      int idx = GMain.session().inventory.unlockBgId.get(i, -2);


      if (idx != -2) {
        gr.addListener(new ClickListener() {
          @Override
          public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            themeClick(idx);
          }
        });
        ovl.setVisible(false);
        lb.setVisible(false);
      }

      if (idx == -1) {
        Image ads = IB.New().drawable("btn_ads").build();
        gr.addActor(ads, AL.cb);
        ovl.setVisible(true);
        lb.setVisible(false);
        int finalI = i;

        ovl.addListener(new ClickListener() {
          @Override
          public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            System.out.println("CLICK");
            GMain.showVideoReward(sucess -> {
              if (sucess) {
                GMain.session().inventory.unlockBgId.put(finalI, finalI);
                GMain.session().profile.choosenBgIdx = finalI;
                BoardGame.boardGame.bg.setDrawable(IB.New().drawable("bg"+GMain.session().profile.choosenBgIdx).build().getDrawable());

                loadTheme();
                GMain.syncSession();
              } else {
                showToast("VIDEO NOT READY");
              }
            });
          }
        });
      }
    }


  }

  @Override
  void initHUDHandler() {

  }

  @Override
  protected void initUI() {

  }


  @Override
  protected void initEvent() {

  }
}
