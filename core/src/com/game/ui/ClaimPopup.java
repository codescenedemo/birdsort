package com.game.ui;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.external.EventHandler;
import com.game.GMain;
import com.game.gamedto.BackgroundDTO;
import com.game.gamedto.BranchDTO;
import com.game.object.BoardGame;

public class ClaimPopup extends BaseUI implements EventHandler {

  Image ovl;
  String type;
  int param;
  MapGroup root;

  public ClaimPopup() {
    this.setVisible(false);
    this.setTouchable(Touchable.childrenOnly);
    GMain.hud().index("claim", this);
    GMain.hud().regisHandler("claimEvent", this);
    ovl = IB.New().overlay(GStage.getWorldWidth(), GStage.getWorldHeight(), 0.8f).pos(0, 0).align(AL.bl).parent(this).build();
    root = MGB.New().size(500, 600).pos(0, 0).align(AL.c).childs(
        BB.New().bg("tittle").label("CONGRATULATIONS", "birdsort_eff60", 0, 0, AL.c).fontScale(0.67f).pos(0, -150).align(AL.ct),
        IB.New().drawable("light").pos(0, 40).align(AL.c).idx("light"),
        IB.New().drawable("reward_bg1").pos(0, 40).align(AL.c).idx("rewardImage"),
//        IB.New().drawable("")
        LB.New().font("font_normal").text("REWARD").pos(0, 85).align(AL.c).idx("reward").visible(false),
        BB.New().bg("btn_confirm").label("Next Level", "birdsort_eff60", 10, 10, AL.c).fontScale(0.65f).pos(0, -70).align(AL.cb).idx("btnConfirm"),
        BB.New().bg("btn_claim").label("Claim", "birdsort_eff60", 20, 10, AL.c).fontScale(0.65f).pos(0, -180).align(AL.c).idx("claim")
    ).idx("root").parent(this).build();

    root.setScale(0);

    Image light = this.query("root/light", Image.class);
    light.setOrigin(AL.c);
    light.addAction(Actions.forever(Actions.rotateBy(360, 5.5f)));


    GMain.hud().clickConnect("claim/root/btnConfirm", "claimEvent", "confirm", 0, 0);
  }

  @Override
  public void handleEvent(Actor actor, String action, int intParam, Object objParam) {
    switch (action) {
      case "confirm":
        if (type.equals("bg")) {
          GMain.session().inventory.unlockBgId.put(param, -1);
        } else {
          GMain.session().inventory.unlockBranchId.put(param, -1);
        }
        GMain.syncSession();
        this.setVisible(false);
        GMain.hud().fireEvent("rewardEvent", "hide", 0, null);
        GMain.session().profile.level++;
        BoardGame.boardGame.newG();
        GMain.syncSession();

        break;
      case "claim":

        this.setVisible(true);
        GMain.hud().fireEvent("rewardEvent", "hide", 0, null);
        this.query("root/rewardImage", Image.class).setDrawable(IB.New().drawable("avatar_" +(String) objParam + intParam).build().getDrawable());
        ovl.setScale(1);

        root.addAction(Actions.sequence(
            Actions.scaleTo(1, 1, 0.3f)
        ));

        Button nextBtn = this.query("root/btnConfirm", Button.class);
        nextBtn.setScale(0);
        nextBtn.addAction(Actions.delay(0.3f, Actions.scaleTo(1, 1, 0.6f)));

        Button claim = this.query("root/claim", Button.class);
        claim.clearActions();
        claim.addAction(Actions.forever(
            Actions.sequence(
                Actions.scaleTo(0.9f, 0.9f, 1f),
                Actions.scaleTo(1f, 1, 1f)
            )
        ));


        String cases = (String) objParam;
        type = cases;
        String drawable = "";
        param = intParam;
        if (cases.equals("bg")) {
          BackgroundDTO bg = BackgroundDTO.backgrounds.get(intParam);
          drawable = bg.drawable;
        } else {
          BranchDTO br = BranchDTO.branchs.get(intParam);
          drawable = br.drawable;
        }

        this.query("root/reward", Label.class).setText(drawable);
        GMain.hud().clickConnect("claim/root/claim", "claimEvent", "watchAd", intParam, cases);

        break;
      case "watchAd":
        GMain.showVideoReward(success -> {
          if (success){
            BoardGame.trackEventVideoAds("claimSkin");
            String c = (String) objParam;
            if (c.equals("bg")) {
              GMain.session().inventory.unlockBgId.put(intParam, intParam);
            } else {
              GMain.session().inventory.unlockBranchId.put(intParam, intParam);
              GMain.session().inventory.unlockBgId.put(intParam, intParam);

            }
            GMain.syncSession();
            GMain.session().profile.level++;
            BoardGame.boardGame.newG();
            this.setVisible(false);

          }else {
            showToast("VIDEO NOT READY");

          }
        });

        break;
    }
  }

  @Override
  void initHUDHandler() {

  }

  @Override
  protected void initUI() {

  }

  @Override
  protected void initEvent() {

  }
}
