package com.game.gamemodel;

public interface ICommand {
  public void exec();
  public void undo();
}
