package com.game.gamemodel;



public class GameBallModel {
  public int idColor;
  public boolean hasClock;
  public boolean isSleep;
  public int bombIdx = -1;
  public int eggId;
  public int hammerIdx;

  public GameBallModel(int id){
    this.idColor = id;
    this.hasClock = false;
    this.isSleep = false;
    this.bombIdx = -1;
    this.eggId = -1;
    this.hammerIdx = -1;
  }

  public void reBalance(){
  }
}
