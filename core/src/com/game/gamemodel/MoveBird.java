package com.game.gamemodel;

import com.badlogic.gdx.utils.Array;
import com.game.gameview.Branch;
import com.game.object.BoardGame;

public class MoveBird implements ICommand {





  public Array<GameBallModel> birds;
  public GameBottleModel oldBranch, newBranch;

  public MoveBird(Array<GameBallModel> birds,  GameBottleModel oldBranch, GameBottleModel newBranch){
    this.birds = birds;
    this.oldBranch = oldBranch;
    this.newBranch = newBranch;
  }

  @Override
  public void exec() {

  }

  @Override
  public void undo() {


    BoardGame boardGame = BoardGame.boardGame;
    boardGame.idBranchClicked = oldBranch.id;
    Branch desBranch = boardGame.branchView.get(oldBranch.id - 1);
    Branch startBranch = boardGame.branchView.get(newBranch.id - 1);

    boardGame.tmpBird.clear();
    boardGame.idBranchClicked = startBranch.getId();


    for (int i = this.birds.size - 1 ; i >= 0; i--){
      boardGame.tmpBird.add(startBranch.birds.get(i));
    }


    boardGame.actionMoveBirds(desBranch);
    for (int i = boardGame.tmpBird.size - 1; i >= 0; i--){ // model update
      boardGame.boardArray.get(oldBranch.id - 1).balls.add(boardGame.boardArray.get(newBranch.id - 1).balls.pop());
    }

    boardGame.idBranchClicked = - 1;
    boardGame.choosenColorId = -1;
  }
}
