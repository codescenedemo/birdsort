package com.game.gamemodel;

import com.badlogic.gdx.utils.Array;


public class GameBottleModel {
  public int id;
  public Array<GameBallModel> balls;

  public static GameBottleModel ofDefault(){
    GameBottleModel b = new GameBottleModel();
    b.balls = new Array<>();
    return b;
  }

  public void reBalance(){
    if(balls == null){
      balls = new Array<>();
    }
  }

  public void setId(int id){
    this.id = id;
  }
}
