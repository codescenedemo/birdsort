package com.game.config;

import com.core.util.GStage;
import com.game.GMain;

public class RemoteConfig {

  public static float getMoveSpeed(){
    return Float.parseFloat( GMain.platform().GetConfigStringValue("moveSpeed", "0.3"));
  }


  public static float getLandingSpeed(){
    return Float.parseFloat( GMain.platform().GetConfigStringValue("landingSpeed", "0.3"));
  }

  public static float getBranchPadding(){
    return Float.parseFloat( GMain.platform().GetConfigStringValue("branch_padding", "70"));
  }

  public static int getChirpRate(){
    return GMain.platform().GetConfigIntValue("chirp_rate", 3);
  }

  public static int getFullScreenLevel(){
    return GMain.platform().GetConfigIntValue("fullscreen_level", 3);
  }


  public static float getBranchYIndex(){
    return Float.parseFloat( GMain.platform().GetConfigStringValue("branch_y", "150"));
  }

  public static float heightPad(){
    return GStage.getWorldHeight() - 1280;
  }

  public static int getUndoAds(){
    return GMain.platform().GetConfigIntValue("undoAds", 5);
  }


  public static int getMaxLevel(){
    return GMain.platform().GetConfigIntValue("max_level", 399);
  }


  public static boolean isShowBanner(){


    if (GMain.platform().GetConfigIntValue("isShowBanner", 1) == 1){
      return true;
    }else {
      return false;
    }
  }





}
