package com.game.config;

public class SoundConfig {
  public static final String BIRD_FLY = "birdfly.mp3";
  public static final String CHIRP_HIGH = "chirp_high.mp3";
  public static final String BGMUSIC = "bg.mp3";
  public static final String BTNCLICK = "click.mp3";
  public static final String SWOOSH = "swoosh.mp3";
  public static final String CRACK = "crack.mp3";
  public static final String ALARM = "alarm.mp3";
  public static final String WIN = "win.mp3";
}
