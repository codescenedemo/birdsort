package com.game.config;

import com.badlogic.gdx.utils.IntMap;

public class PositionConfig {
  public static IntMap<Float> leftPos;
  public static IntMap<Float>  rightPos;
  public static float rightEnd = 900f;
  static{
    leftPos = new IntMap<>();
    rightPos = new IntMap<>();
    leftPos.put(0, -31f);
    leftPos.put(1, 40f );
    leftPos.put(2, 129f);
    leftPos.put(3, 219f);


    rightPos.put(0, 621f);
    rightPos.put(1, 553f);
    rightPos.put(2, 475f);
    rightPos.put(3, 398f);
  }
}
