package com.game.gameview;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.core.exSprite.GSpineSprite;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.builders.BB;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.core.utils.hud.builders.SB;

public class Bird extends MapGroup {


  int id;

  public GSpineSprite spine;
  public GSpineSprite choosen_spine;

  public GSpineSprite egg;
  public Image hammer;

  public Image clock;
  public GSpineSprite bomb;
  public Button count;

  public float scaleX;
  public boolean isLeft;
  public float curX, curY;
  public boolean isMoving;
  public boolean hasClock;
  public boolean isSleep;

  public int bombIdx;
  public int eggIdx;
  public int hammerIdx;

  public Bird(int id, boolean hasClock, boolean isSleep, int bombIdx, int eggIdx, int hammerIdx){
    super(120, 120);
    this.id = id;
    setTouchable(Touchable.disabled);
    setOrigin(AL.c);
    scaleX = 1;
    isLeft = true;
    isMoving = false;
    this.hasClock = hasClock;
    this.isSleep = isSleep;
    this.bombIdx = bombIdx;
    this.eggIdx = eggIdx;
    this.hammerIdx = hammerIdx;
    initUI();
    setScale(0.9f, 0.9f);

  }

  void initUI(){

    choosen_spine = SB.New().spineName("bird").animation("idle").loop(true).pos(2, 0).align(AL.c).visible(false).parent(this).build();
    choosen_spine.setSkin(id+1+"");
    choosen_spine.setVisible(false);

    spine = SB.New().spineName("bird").animation("idle_sd").loop(true).pos(0, 0).align(AL.c).parent(this).build();
    spine.setSkin(id+1+"");

    this.setSize(120, 120);

    if (hasClock){
      clock = IB.New().drawable("clock").pos(10, -10).scale(1, 1).align(AL.cb).parent(this).build();
    }
    if (isSleep){
      spine.changeAnimation("sleep", true, false);
    }
    if (bombIdx != -1){
      bomb = SB.New().spineName("boom").animation("flame").loop(true).parent(this).pos(0, -30, AL.c).build();
      count = BB.New().bg("ring").label(bombIdx+"", "birdsort_eff60", 0, 0, AL.c).fontScale(0.4f).align(AL.c).pos(-4, -36).parent(this).align(AL.c).build();
    }





    if (eggIdx != -1){
      egg =  SB.New().spineName("eggs").skin(""+eggIdx).animation("idle").loop(true).pos(-20, 0).align(AL.c).parent(this).build();
      egg.setOrigin(AL.c);
      egg.setTimeScale(0.5f);
      choosen_spine.setVisible(false);
      spine.setVisible(false);
      egg.setVisible(true);

    }
    if (hammerIdx != - 1){
      hammer =  IB.New().drawable("hammer_"+hammerIdx).scale(1, 1).pos(20, -20).align(AL.cb).parent(this).build();
    }

  }

  public int getColorId(){
    return this.id;
  }

  public void setCurPos(float x, float y){
    this.curX = x;
    this.curY = y;
  }

  public void changeFlyAnim(){
    spine.setVisible(true);
    choosen_spine.setVisible(false);
    spine.changeAnimation("fly", true, false);
    choosen_spine.changeAnimation("fly", true, false);

    if (eggIdx != -1){
      choosen_spine.setVisible(false);
      spine.setVisible(false);
      egg.setVisible(true);
    }
  }

  public void changeAnimIdle(){
    spine.setVisible(true);
    choosen_spine.setVisible(false);
    spine.changeAnimation("idle", true, true);
    choosen_spine.changeAnimation("idle_sd", true, true);

    if (eggIdx != -1){
      choosen_spine.setVisible(false);
      spine.setVisible(false);
      egg.setVisible(true);
    }
  }

  public void changeAnimSleep(){
    spine.setVisible(true);
    choosen_spine.setVisible(false);
    spine.changeAnimation("sleep", true);
    choosen_spine.changeAnimation("sleep", true);
  }

  public void changeAnimChoose(){
    spine.setVisible(true);
    choosen_spine.setVisible(true);

  }


  public void flip(int dir){


    setScaleX(dir);
    if (count != null){
      count.setScaleX(dir);

    }
  }

  @Override
  public void act(float delta) {
    super.act(delta);
  }
}
