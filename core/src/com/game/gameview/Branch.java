package com.game.gameview;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.Group;

import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.core.exSprite.GShapeSprite;
import com.core.utils.hud.AL;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.MGB;
import com.core.utils.hud.builders.SB;
import com.game.GMain;
import com.game.config.PositionConfig;
import com.game.gamelogic.actions.AbsPosition;
import com.game.object.BoardGame;
import com.game.screens.BuildLevelScreen;

public class Branch extends Group {
  private int id;

  public Image a;

  public Array<Bird> birds;
  public Rectangle body;

  public Branch(int id) {
    this.id = id;
    intitUI();
    birds = new Array<>();
    body = new Rectangle();
    body = new Rectangle(this.getX(), this.getY(), 341, 120);

//    GShapeSprite shapeSprite = new GShapeSprite();
//    shapeSprite.createRectangle(false, this.getX(), this.getY(), 341, 120);
//    addActor(shapeSprite);

    if (id % 2 != 0){
      MGB.New().size(341, 120).pos(0, 0).parent(this).debug(false).build();
    }else {
      MGB.New().size(341, 120).pos(-341, 0).parent(this).debug(false).build();
    }

  }

  public void intitUI() {

    int directionX = 1;
    if (id % 2 == 0) {
      directionX = -1;

    }
    a = IB.New().drawable("branch"+ GMain.session().profile.choosenBranchIdx).parent(this).pos(0, 0, AL.c).scale(directionX, 1).build();
    addActor(a);

    if (id % 2 == 0){
      a.setOrigin(AL.br);
    }else {
      a.setOrigin(AL.bl);
    }



  }

  public void initBird() {


    float padX = 0;
    if (BoardGame.boardGame.boardArray.size >= 13 && this.id % 2 == 0){
      padX = 150;
    }



//    BoardGame.boardGame.setTouchable(Touchable.disabled);
    float x, y, startX, startY;

    y = AbsPosition.absPos(this, AL.c).y;

    System.out.println("bird y: " + y);
    startY = y + 300;
    for (int i = 0; i < birds.size; i++) {
      Bird bird = birds.get(i);
      int scaleX = 1;
      if (this.getId() % 2 == 0) {
        x = PositionConfig.rightPos.get(i);
        startX = 900;
        bird.isLeft = false;

      } else {
        x = PositionConfig.leftPos.get(i);
        startX = -300;
        scaleX = -1;
        bird.isLeft = true;
      }

      bird.changeFlyAnim();
//      bird.setScaleX(scaleX);
      bird.flip(scaleX);

      bird.setPosition(startX, -100);
      bird.addAction(Actions.sequence(
          Actions.delay(i * 0.1f),
          Actions.moveTo(x + padX, y + 80 - 150, 1),
          Actions.run(() -> {
            bird.spine.changeAnimation("landing", true);
          }),
          Actions.moveBy(0, -50, 0.8f),
          Actions.run(() -> {
            bird.changeAnimIdle();

            if (bird.isSleep){
              bird.spine.changeAnimation("sleep", true, false);
            }
          }),
          Actions.delay(MathUtils.random(0, 1.5f), Actions.run(() -> {
            if (!bird.isSleep){
              bird.changeAnimIdle();

            }
            BoardGame.boardGame.setTouchable(Touchable.enabled);
          }))

      ));
      bird.setCurPos(x, y+30);
      bird.setTouchable(Touchable.disabled);
      bird.setOrigin(AL.c);
//      this.setDebug(true);



    }


    this.addAction(Actions.delay(2, Actions.run(() -> {
      shake();
    })));

    for (int i = birds.size - 1; i >= 0; i--) {
      Bird bird = birds.get(i);
      BoardGame.boardGame.addActor(bird);
    }
  }


  public void initBirdBuild() {


    float x, y, startX, startY;

    y = AbsPosition.absPos(this, AL.c).y;
    startY = y + 300;
    for (int i = 0; i < birds.size; i++) {
      Bird bird = birds.get(i);

      if (this.getId() % 2 == 0) {
        x = PositionConfig.rightPos.get(i);
        startX = 900;
        bird.isLeft = false;
      } else {
        x = PositionConfig.leftPos.get(i);
        startX = -300;
        bird.isLeft = true;
      }
      bird.setPosition(startX, startY, AL.c);
      bird.addAction(Actions.moveTo(x, y + 30, 0.8f));
      bird.setCurPos(x, y+30);

      bird.setTouchable(Touchable.disabled);
    }



    for (int i = birds.size - 1; i >= 0; i--) {
      Bird bird = birds.get(i);
      BuildLevelScreen.inst.ui.addActor(bird);
    }
  }





  public void shake(){

    if (birds.size <= 0){
      System.out.println("RETURN");
      return;
    }

    int mul = 1;
    if (id % 2 == 0){
      mul = -1;
    }

    this.a.addAction(Actions.repeat(2, Actions.sequence(
        Actions.rotateBy(2f * mul, 0.2f),
        Actions.rotateBy(-2f * mul, 0.2f)
    )));

    for (int i = 0; i < birds.size; i++){
      Bird bird = birds.get(i);
      bird.addAction(Actions.sequence(
          Actions.moveBy(0,   5f, 0.2f),
          Actions.moveBy(0,  -5f, 0.2f),
          Actions.moveBy(0,   5f, 0.2f),
          Actions.moveBy(0,  -5f, 0.2f)
      ));
    }
  }

  public int getId() {
    return this.id;
  }

  public void setId(int id) {
    this.id = id;
  }

}


