package com.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.JsonValue;
import com.game.GMain;
import com.gameservice.PlayHandler;
import com.gameservice.PlayService;

public class PlayServiceHandler implements PlayHandler {
  PlayService playService;

  public PlayServiceHandler() {
    playService = GMain.me.playService;
    playService.loginWithDeviceID();
  }

  @Override
  public void handle(JsonValue resp) {
    String cmd = resp.getString("cmd");
    switch (cmd) {
      case "LoginWithAndroidDeviceID":
        if (playService.getDisplayName().equals("")) {
          playService.updateDisplayName(GMain.session().profile.name);
        }
        break;
      case "UpdateUserTitleDisplayName":
        Gdx.app.log("a", "change name success");
        break;
      case "GetLeaderboard":
          GMain.hud().fireEvent("rankingEvent", "load", 0, resp.get("data").get("Leaderboard"));
        break;
      default:
        break;
    }
  }
}
