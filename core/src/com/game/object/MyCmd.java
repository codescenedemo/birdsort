package com.game.object;



import com.core.util.GStage;
import com.game.GMain;
import com.game.controller.LevelLoader;
import com.game.screens.BuildLevelScreen;
import com.game.screens.PlayScreen;
import com.strongjoshua.console.CommandExecutor;



public class MyCmd extends CommandExecutor {

  public void addUndo(){
    GMain.session().inventory.undo = 3;
    GMain.syncSession();
  }

  public void addBranch(){
    BoardGame.boardGame.addBranch();
  }


  public void resetReward(){

    GMain.session().inventory.rewardIdx = 1;
    GMain.session().profile.progress = 0;
    GMain.session().inventory.unlockedIdx = 0;
    GMain.session().inventory.unlockBgId.clear();
    GMain.session().inventory.unlockBgId.put(1, 1);
    GMain.session().inventory.unlockBranchId.clear();
    GMain.session().inventory.unlockBranchId.put(1, 1);
    GMain.syncSession();





  }

  public void loadLevel(int lv){
    BuildLevelScreen.inst.bottles = LevelLoader.getLv(lv);
    BuildLevelScreen.inst.initBranch();
  }

  public void setLv(int lv){
    BuildLevelScreen.lv = lv;
  }

  public void setBomb(int move){
    BuildLevelScreen.bombIdx = move;
  }

  public void buildMode(){
    GMain.me.setScreen(new BuildLevelScreen());
  }

  public void p(int lv){
    GMain.session().profile.level = lv;
    GMain.me.setScreen(new PlayScreen());
  }

  public void newG(){
    BoardGame.boardGame = null;
    BoardGame.boardGame = new BoardGame(720, GStage.getWorldHeight());
    BoardGame.boardGame.intit();
  }

  public void setPer(){
    GMain.session().profile.progress = 9;
  }

}
