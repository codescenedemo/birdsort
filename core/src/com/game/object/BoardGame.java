package com.game.object;


import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Array;

import com.badlogic.gdx.utils.ObjectMap;
import com.core.action.exAction.GArcMoveToAction;
import com.core.exSprite.particle.GParticleSystem;
import com.core.util.GLayer;
import com.core.util.GSound;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.Button;
import com.core.utils.hud.MapGroup;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.MGB;
import com.game.GMain;
import com.game.config.PositionConfig;
import com.game.config.RemoteConfig;
import com.game.config.SoundConfig;
import com.game.controller.LevelLoader;
import com.game.controller.TutorialController;
import com.game.gamelogic.actions.AbsPosition;
import com.game.gamelogic.actions.HandMoveAction;
import com.game.gamemodel.GameBallModel;
import com.game.gamemodel.GameBottleModel;
import com.game.gamemodel.ICommand;
import com.game.gamemodel.MoveBird;
import com.game.gameview.Bird;
import com.game.gameview.Branch;



import java.util.Stack;


public class BoardGame extends MapGroup {
  public Array<GameBottleModel> boardArray;
  public Array<Branch> branchView;
  public static BoardGame boardGame;


  public int choosenColorId;
  public int idBranchClicked;

  public Stack<ICommand> undoStacks;
  public Array<Bird> tmpBird;
  public Array<GameBallModel> tmpBirdModel;
  public int colorIdRemove;
  public Bird bombBird;
  public TutorialController tutController;
  public float padX;
  public float padY;
  public Image bg;



  public BoardGame(float w, float h) {
    super(w, h);

  }


  public void intit() {
    if (boardGame != null){
      boardGame = null;
    }
    boardGame = this;
    choosenColorId = -1;
    idBranchClicked = -1;
    GLayer.sprite.getGroup().clear();
    GLayer.top.getGroup().clear();
    GStage.addToLayer(GLayer.sprite, this);
    this.clearActions();
    this.clear();
    MapGroup a = MGB.New().size(GStage.getWorldWidth(), GStage.getWorldHeight()).pos(0, 0).align(AL.bl).parent(GLayer.bottom.getGroup()).build();
    bg = IB.New().drawable("bg" + GMain.session().profile.choosenBgIdx).pos(0, 0).align(AL.c).touchable(false).build();
    a.addActor(bg, AL.c);
    a.setScale(2);

//    bg.setScale(2);
    newGame();



  }


  public void scaleGame(){

    float div = 0.8f;
    padX = 0;
    padY = 0;
    if (boardArray.size >= 13){
      padX = 150;
      padY = 30;
      this.setScale(div);
      for (int i = 0; i < branchView.size; i++){
        Branch view = branchView.get(i);
        if (view.getId() % 2 == 0){
          view.addAction(Actions.moveBy(150, 0));
        }
      }
    }
  }

  public void newGame() {

    this.setY(150, AL.bl);
    this.setScale(1f);

    this.clear();
    this.clearChildren();
    this.clearActions();

    boardArray = new Array<>();


    tmpBird = new Array<>();
    tmpBirdModel = new Array<>();
    branchView = new Array<>();
    boardArray.clear();
    branchView.clear();
    undoStacks = new Stack<>();
    loadLevel();
    initBranch();



    boardGame.setTouchable(Touchable.disabled);

    this.addAction(Actions.delay(2.5f, Actions.run(() -> {
      boardGame.setTouchable(Touchable.enabled);

      BoardGame.boardGame.tutController.startTut();
      intitBranchEvent();

    })));

    undoStacks = new Stack<>();
    trackEventStart();
    tutController = new TutorialController(GMain.session().profile.level);


    scaleGame();
  }

  public void loadLevel() {
    boardArray.clear();
    boardArray = LevelLoader.getLv(GMain.session().profile.level);
//    boardArray = LevelLoader.getLv(502);


  }

  public void initBranch() {
    float x, y;
    for (int i = 0; i < boardArray.size; i++) {
      GameBottleModel bottle = boardArray.get(i);
      Branch branch = new Branch(bottle.id);
      System.out.println("branch id: " + branch.getId());
      branchView.add(branch);
//      branch.setDebug(true, true);
      if (branch.getId() % 2 == 0) {
        x = 720;
        branch.a.setOriginX(AL.br);
      } else {
        branch.a.setOriginX(AL.bl);

        x = -10;
      }



      y = i * 70 + RemoteConfig.getBranchYIndex() ;
      if (boardArray.size <= 8){
        y = i * 70 + 150;
      }

      if (boardArray.size >= 11){
        y = i * 70 + 100 ;
      }



      y += RemoteConfig.heightPad();

      System.out.println("height pad: " + RemoteConfig.heightPad());

      addActor(branch);

      branch.setPosition(x, y);


      for (int j = 0; j < bottle.balls.size; j++) {
        GameBallModel ball = bottle.balls.get(j);
        Bird bird = new Bird(ball.idColor, ball.hasClock, ball.isSleep, ball.bombIdx, ball.eggId, ball.hammerIdx);
        if (bird.bombIdx != -1) {
          bombBird = bird;
        }
        branch.birds.add(bird);
      }

      branch.initBird();
    }





  }

  public void intitBranchEvent() {
    for (int i = 0; i < branchView.size; i++) {
      Branch branch = branchView.get(i);
      branch.setTouchable(Touchable.enabled);
      branch.clearListeners();
      branch.addListener(new ClickListener() {
        @Override
        public void clicked(InputEvent event, float x, float y) {
          branchClick(branch);
          super.clicked(event, x, y);
        }
      });
    }
  }


  public boolean checkColor(Branch branch) {
    if (branch.birds.isEmpty()) {
      return true;
    }
    Bird bird = branch.birds.get(branch.birds.size - 1);
    if (bird.eggIdx != -1) {
      return true;
    }
    return bird.getColorId() == choosenColorId && branch.birds.size < 4;
  }


  public void checkBranch(GameBottleModel branch) {
    if (branch.balls.size < 4) {
      return;
    }


    for (int i = 0; i < branch.balls.size - 1; i++) {
      GameBallModel ball = branch.balls.get(i);
      GameBallModel ball2 = branch.balls.get(i + 1);
      if (ball.idColor != ball2.idColor) {
        return;
      }
      if (ball.eggId != -1 || ball.isSleep) {
        return;
      }

    }


    Branch rs = boardGame.branchView.get(branch.id - 1);
    rs.setTouchable(Touchable.disabled);

    this.addAction(Actions.sequence(

        Actions.delay(1.2f),

        Actions.run(() -> {
          for (int i = 0; i < rs.birds.size; i++) {
            Bird bird = rs.birds.get(i);


            if (bird.hasClock) {

              branch.balls.get(i).hasClock = false;
              this.addActor(bird.clock);
              bird.clock.setPosition(bird.getX(), bird.getY());
              bird.clock.addAction(Actions.sequence(
                  Actions.parallel(
                      Actions.scaleTo(1.5f, 1.5f, 0.5f),
                      Actions.moveTo(GStage.getWorldWidth() / 2 - 45, GStage.getWorldHeight() / 2, 0.5f)
                  ),
                  Actions.run(() -> GSound.playSound(SoundConfig.ALARM)),
                  Actions.repeat(15, Actions.sequence(
                      Actions.rotateBy(10, 0.05f),
                      Actions.rotateBy(-10, 0.05f)
                  )),
                  Actions.delay(0, Actions.run(() -> {
                    wakeUp();
                    bird.clock.remove();
                  }))
              ));
            }

            if (bird.hammerIdx != -1) {
              bird.hammer.clearActions();
              hammerAction(bird, bird.hammerIdx);
            }

            colorIdRemove = rs.birds.get(i).getColorId();
            removeUndoStack();
            bird.isMoving = true;
            float desX = -200;
            if (branch.id % 2 != 0) {
              desX = PositionConfig.rightEnd;
            }
            GSound.playSound(SoundConfig.BIRD_FLY);
            bird.spine.changeAnimation("fly", true, false);
            bird.spine.setTimeScale(1.5f);
            if (GMain.session().setting.isVibrate) {
              Gdx.input.vibrate(50);
            }
            int finalI = i;
            bird.addAction(Actions.sequence(
                Actions.parallel(
                    Actions.delay(0.2f * i, Actions.moveTo(desX, 900, 1f)),
                    Actions.run(() -> {

                      if (bird.bombIdx != -1) {
                        float x, y;
                        x= AbsPosition.absPos(bird.bomb, AL.c).x;
                        y = AbsPosition.absPos(bird.bomb, AL.bl).y - 130;
                        float  xB;
                        if (bird.isLeft){
                              xB = GStage.getWorldWidth() / 2 - 100 ;
                        }else {
                          xB = GStage.getWorldWidth() / 2 + 100;
                        }
                        this.addActor(bird.bomb);
                        bird.bomb.setPosition(x, y);
                        bird.bomb.changeAnimation("off", true);
                        bird.count.remove();
                        bird.bomb.addAction(Actions.delay(0.3f * finalI, GArcMoveToAction.arcMoveTo(GStage.getWorldWidth() / 2, -GStage.getWorldHeight() / 2, xB, bird.getY() + 100, 1.2f, null)
));
                      }
                    })
                ),

                Actions.removeActor()
            ));


          }
        }),
        Actions.delay(1.5f, Actions.run(() -> {
          branch.balls.clear();
          rs.birds.clear();
          rs.setTouchable(Touchable.enabled);

          if (checkWin()) {
            GSound.playSound(SoundConfig.WIN);
            trackEventCompleted();
            GMain.hud().fireEvent("headerEvent", "fullScreen", 0, null);

          }

        }))
    ));


  }


  public boolean checkWin() {
    for (int i = 0; i < boardArray.size; i++) {
      if (!boardArray.get(i).balls.isEmpty()) {
        return false;
      }
    }
    return true;
  }

  public void branchClick(Branch branch) {

    if (idBranchClicked == branch.getId()) { //unselect
      removeChoosenAction();
      tmpBird.clear();
      tmpBirdModel.clear();
      idBranchClicked = -1;
      choosenColorId = 0;

    } else if (idBranchClicked == -1) { //start choose

      if (branch.birds.isEmpty()) { //sellecting empty branch
        return;
      }

      idBranchClicked = branch.getId();

      tmpBird.clear();
      tmpBirdModel.clear();
      GameBottleModel choosenBranchModel = boardArray.get(branch.getId() - 1);
      Bird choosenBird = branch.birds.peek();
      if (choosenBird.isMoving || choosenBird.isSleep || choosenBird.eggIdx != -1) {
        if (choosenBird.isSleep) {
          addAction(Actions.sequence(
              Actions.run(() -> {
                choosenBird.choosen_spine.setVisible(true);
                choosenBird.choosen_spine.changeAnimation("sleep_sd_r", false);
              }),
              Actions.delay(0.5f, Actions.run(choosenBird::changeAnimSleep))
          ));

        }
        return;
      }
      GameBallModel choosenBirdModel = choosenBranchModel.balls.peek();
      GSound.playSound(SoundConfig.CHIRP_HIGH);
      tmpBird.add(choosenBird);
//      choosenBird.spine.changeAnimation("idle_sd", true);
      choosenBird.changeAnimChoose();
      tmpBirdModel.add(choosenBirdModel);
      for (int i = branch.birds.size - 2; i >= 0; i--) {
        Bird b = branch.birds.get(i);

        GameBallModel bModel = choosenBranchModel.balls.get(i);
        if (b.getColorId() != choosenBird.getColorId() || b.isSleep || b.eggIdx != -1) {
          break;
        }

        if (b.getColorId() == choosenBird.getColorId()) {
          tmpBird.add(b);
//          b.spine.changeAnimation("idle_sd", true);
          b.changeAnimChoose();
          tmpBirdModel.add(bModel);
        }
      }

//      choosenBirdAction();


      choosenColorId = branch.birds.get(branch.birds.size - 1).getColorId();

    } else { //destination choose
      if (checkColor(branch)) {
        if (tmpBird.size <= 0 || tmpBirdModel.size <= 0) {
          return;
        }


        actionMoveBirds(branch); // view update


        int tempIdx = idBranchClicked;
        for (int i = 0; i < tmpBird.size; i++) { // model update
          boardArray.get(branch.getId() - 1).balls.add(boardArray.get(tempIdx - 1).balls.pop());
        }

        if (bombBird != null) {
          bombBird.bombIdx--;
          bombBird.count.setText(bombBird.bombIdx + "");
          if (bombBird.bombIdx == 0) {
            System.out.println("loose");
            trackEventFailed();
          }

        }

        Array<GameBallModel> tmp = new Array<>();
        while (!tmpBirdModel.isEmpty()) {
          tmp.add(tmpBirdModel.pop());

        }

        MoveBird moveBird = new MoveBird(tmp, boardArray.get(idBranchClicked - 1), boardArray.get(branch.getId() - 1));
        undoStacks.push(moveBird);

        idBranchClicked = -1;
        choosenColorId = -1;


        checkBranch(boardArray.get(branch.getId() - 1));


      } else { // choose different color id
        if (branch.birds.isEmpty()) { //sellecting empty branch
          return;
        }
        removeChoosenAction();

        idBranchClicked = branch.getId();

        tmpBird.clear();
        tmpBirdModel.clear();
        GameBottleModel choosenBranchModel = boardArray.get(branch.getId() - 1);
        Bird choosenBird = branch.birds.peek();
        if (choosenBird.isSleep || choosenBird.eggIdx != -1) {
          return;
        }

        GSound.playSound(SoundConfig.CHIRP_HIGH);


        GameBallModel choosenBirdModel = choosenBranchModel.balls.peek();
        choosenBird.changeAnimChoose();
        tmpBird.add(choosenBird);
        tmpBirdModel.add(choosenBirdModel);
        for (int i = branch.birds.size - 2; i >= 0; i--) {
          Bird b = branch.birds.get(i);

          GameBallModel bModel = choosenBranchModel.balls.get(i);
          if (b.getColorId() != choosenBird.getColorId() || b.isSleep || b.eggIdx != -1) {
            break;
          }

          if (b.getColorId() == choosenBird.getColorId()) {
            tmpBird.add(b);
//            b.spine.changeAnimation("idle_sd", true);

            b.changeAnimChoose();
            tmpBirdModel.add(bModel);

          }
        }

//        choosenBirdAction();
        choosenColorId = branch.birds.get(branch.birds.size - 1).getColorId();
//        debugColorId.setText(choosenColorId + "");


      }
    }

  }


  public void actionMoveBirds(Branch branch) {

    Button undoBtn = GMain.hud().query("footer/root/undo", Button.class);
    Button addBranch = GMain.hud().query("footer/root/branch", Button.class);

    //disable button
    undoBtn.clearActions();
    undoBtn.getBg().setColor(1, 1, 1, 0.6f);
    undoBtn.setTouchable(Touchable.disabled);

    addBranch.clearActions();
    addBranch.getBg().setColor(1, 1, 1, 0.6f);
    addBranch.setTouchable(Touchable.disabled);
    float desX, desY;
    int index = 0;


    int numAvail = 4 - branch.birds.size;

    desY = branch.getY();
    removeChoosenAction();





    while (tmpBird.size > numAvail) {
      tmpBird.pop();
      tmpBirdModel.pop();
    }




    for (int i = 0; i < tmpBird.size; i++) {

      Bird b = branchView.get(idBranchClicked - 1).birds.pop();




      b.isMoving = true;
//      b.scaleX = 1;
      if (!branch.birds.isEmpty()) {
        index = branch.birds.size;
      }

      if (branch.getId() % 2 == 0) {
        desX = PositionConfig.rightPos.get(index) + padX;
        if (b.isLeft) {
          b.isLeft = false;
        }
      } else {
        desX = PositionConfig.leftPos.get(index);
        if (!b.isLeft) {
          b.isLeft = true;
        }
      }

      branch.birds.add(b);

      float distance = Vector2.dst(b.getX(), b.getY(), desX, desY + 30);


      float mul = distance / 170;
      mul = Math.min(mul, 1.4f);
      if (distance < 312){
        mul = 1;
      }

      System.out.println("speed: " +RemoteConfig.getMoveSpeed() * mul);


      setZIndexBranch();




      b.setRotation(0);
      b.clearActions();


      float finalDesX = desX;
      b.addAction(Actions.sequence(
          Actions.run(() -> {


            if (b.curX < finalDesX) {
//              b.setScaleX(-1);
              b.flip(-1);
            } else if (b.curX > finalDesX) {
              b.flip(1);

            }
            b.changeFlyAnim();
          }),

          Actions.delay(i * 0.2f),
          Actions.moveTo(desX, desY + 80, RemoteConfig.getMoveSpeed() * mul),
          Actions.run(() -> {
            b.spine.changeAnimation("landing", false, false);
            if (b.isLeft) {
              b.flip(-1);

            } else {
              b.flip(1);
            }

          }),


          Actions.moveTo(desX, desY + 30, RemoteConfig.getLandingSpeed()),

          Actions.run(() -> {
            b.setCurPos(finalDesX, desY + 30);
            b.isMoving = false;
            b.changeAnimIdle();

          })

      ));

      //enable button
      undoBtn.addAction(Actions.delay(RemoteConfig.getLandingSpeed() + RemoteConfig.getMoveSpeed() + 0.2f, Actions.run(() -> {
        undoBtn.getBg().setColor(1, 1, 1, 1f);
        undoBtn.setTouchable(Touchable.enabled);

        addBranch.getBg().setColor(1, 1, 1, 1f);
        addBranch.setTouchable(Touchable.enabled);

      })));
    }

    GSound.playSound(SoundConfig.BIRD_FLY);

    branch.clearActions();
    branch.addAction(Actions.delay(RemoteConfig.getLandingSpeed() + RemoteConfig.getMoveSpeed(), Actions.run(branch::shake)));

  }


  public void removeChoosenAction() {
//    System.out.println("tmpBird sie: " + tmpBird.size);
    for (int i = 0; i < tmpBird.size; i++) {
      Bird b = tmpBird.get(i);
      b.choosen_spine.setVisible(false);
    }
  }

  public void undo() {
    if (undoStacks.isEmpty()) {
      return;
    }
    MoveBird bird = (MoveBird) undoStacks.pop();
    bird.undo();
    if (bombBird != null) {
      bombBird.bombIdx++;
      bombBird.count.setText(bombBird.bombIdx + "");
    }
  }


  public void removeUndoStack() {

    for (int i = 0; i < undoStacks.size(); i++) {

      MoveBird bird = (MoveBird) undoStacks.get(i);
      if (bird == null) {
        return;
      }
      if (bird.birds.get(0).idColor == colorIdRemove) {
        undoStacks.remove(bird);
      }
    }
  }

  public void addBranch() {
    //model
    GameBottleModel botlle = new GameBottleModel();
    botlle.balls = new Array<>();
    botlle.setId(boardArray.size + 1);

    boardArray.add(botlle);
    //view
    float x, y;
    Branch newBranch = new Branch(branchView.size + 1);
    newBranch.birds = new Array<>();

    branchView.add(newBranch);


    newBranch.addListener(new ClickListener() {
      @Override
      public void clicked(InputEvent event, float x, float y) {
        branchClick(newBranch);
        super.clicked(event, x, y);
      }
    });


    if (newBranch.getId() % 2 == 0) {
      x = 720 + padX;
      newBranch.a.setOriginX(AL.br);
    } else {
      x = -10;
    }
    y = RemoteConfig.getBranchYIndex();
    if (boardArray.size <= 8){
      y = 150;
    }

    if (boardArray.size >= 11){
      y = 100;
    }

    y += RemoteConfig.heightPad();

    newBranch.setPosition(x, y);
    addActor(newBranch);
    newBranch.initBird();

    for (int i = 0; i < branchView.size - 1; i++) {
      Branch branch = branchView.get(i);
      branch.addAction(Actions.moveBy(0, RemoteConfig.getBranchPadding() + padY, 0.5f));
      for (int j = 0; j < branch.birds.size; j++) {
        branch.birds.get(j).addAction(Actions.moveBy(0, RemoteConfig.getBranchPadding() + padY, 0.5f));
      }
    }


  }


  public void setZIndexBranch() {
    for (int i = branchView.size - 1; i >= 0; i--) {
      for (int j = branchView.get(i).birds.size - 1; j >= 0; j--) {
        Bird bird = branchView.get(i).birds.get(j);
        bird.setZIndex(100 + j + i);
      }
    }
  }

  public void wakeUp() {

    GSound.stopSound(SoundConfig.ALARM);

    //model
    for (int i = 0; i < boardArray.size; i++) {
      GameBottleModel branch = boardArray.get(i);
      for (int j = 0; j < branch.balls.size; j++) {
        GameBallModel bird = branch.balls.get(j);
        bird.isSleep = false;
      }
    }

    //view
    for (int i = 0; i < branchView.size; i++) {
      Branch branch = branchView.get(i);
      for (int j = 0; j < branch.birds.size; j++) {
        Bird bird = branch.birds.get(j);
        if (bird.isSleep) {
          bird.addAction(Actions.sequence(
              Actions.moveBy(0, 18f, 0.1f),
              Actions.moveBy(0, -18f, 0.1f)
          ));
          bird.changeAnimIdle();
          bird.isSleep = false;
        }
      }
    }


    for (int k = 0; k < boardArray.size; k++) {
      GameBottleModel b = boardArray.get(k);
      checkBranch(b);
    }

  }

  public void hammerAction(Bird hammerBird, int hammerIdx) {
    //model
    for (int i = 0; i < boardArray.size; i++) {
      GameBottleModel branch = boardArray.get(i);
      for (int j = 0; j < branch.balls.size; j++) {
        GameBallModel bird = branch.balls.get(j);
        if (bird.eggId == hammerIdx) {
          bird.eggId = -1;
          break;
        }
      }
    }
    GSound.playSound(SoundConfig.SWOOSH);

    //view
    for (int i = 0; i < branchView.size; i++) {

      Branch branch = branchView.get(i);
      for (int j = 0; j < branch.birds.size; j++) {
        Bird bird = branch.birds.get(j);
        if (bird.eggIdx != -1) {
          bird.spine.setScale(0);

          bird.addAction(Actions.sequence(
              Actions.run(() -> {
                float tmpX, tmpY;
                tmpX = AbsPosition.absPos(hammerBird, AL.c).x;
                tmpY = AbsPosition.absPos(hammerBird, AL.c).y - 100;
                this.addActor(hammerBird.hammer);
                hammerBird.hammer.setPosition(tmpX, tmpY, AL.bl);
                hammerBird.hammer.addAction(Actions.sequence(
                    Actions.parallel(
                        Actions.moveTo(AbsPosition.absPos(bird, AL.c).x, bird.getY(), 0.6f),
                        Actions.rotateBy(360 * 2, 0.6f)
                    ),
                    Actions.delay(0.6f, Actions.removeActor())
                ));




              }),

              Actions.delay(0.7f, Actions.run(() -> {
                GSound.playSound(SoundConfig.CRACK);
                GParticleSystem.getGParticleSystem("hammer_hit.p").create(this, bird.getX() + 70, bird.getY() + 60);
                bird.spine.addAction(Actions.scaleTo(1, 1, 0.8f, Interpolation.fastSlow));
                bird.egg.setTimeScale(0.7f);
                bird.egg.changeAnimation("hatch", false);
              })),

              Actions.run(() -> {
                for (int k = 0; k < boardArray.size; k++) {
                  GameBottleModel b = boardArray.get(k);
                  checkBranch(b);
                }
              }),

              Actions.delay(0.7f),
              Actions.run(() -> {
                bird.egg.setVisible(false);
                bird.eggIdx = -1;
                bird.spine.setVisible(true);
              })
          ));
        }
      }
    }

  }

  float count = 0;

  @Override
  public void act(float delta) {
    super.act(delta);
    count += delta;
    if (count >= RemoteConfig.getChirpRate()) {
      GSound.playSound(SoundConfig.CHIRP_HIGH);
      count = 0;
    }
  }


  public static void trackEventCompleted() {
    ObjectMap<String, Object> data = new ObjectMap<>();
    data.put("level", GMain.session().profile.level);
    GMain.platform().TrackLevelInfo("level_completed", data);
    GMain.platform().TrackCustomEvent("level_completed_" + GMain.session().profile.level);
  }

  public static void trackEventStart() {
    ObjectMap<String, Object> data = new ObjectMap<>();
    data.put("level", GMain.session().profile.level);
    GMain.platform().TrackLevelInfo("level_start", data);
    GMain.platform().TrackCustomEvent("level_start_" + GMain.session().profile.level);
  }

  public static void trackEventFailed() {
    ObjectMap<String, Object> data = new ObjectMap<>();
    data.put("level", GMain.session().profile.level);
    GMain.platform().TrackLevelInfo("level_failed", data);
    GMain.platform().TrackCustomEvent("level_failed_" + GMain.session().profile.level);
  }

  public static void trackEventVideoAds(String type) {
    ObjectMap<String, Object> data = new ObjectMap<>();
    data.put("level", GMain.session().profile.level);
    data.put("type", type);
    GMain.platform().TrackLevelInfo("videoads", data);
  }


  public void newG(){

    if (GMain.session().profile.level > RemoteConfig.getMaxLevel()){
      GMain.session().profile.level = RemoteConfig.getMaxLevel();
    }

    if (tutController.res != null){
      for (int i = 0; i < tutController.res.getActions().size; i++){
        if (tutController.res.getActions().get(i) instanceof  HandMoveAction){
          HandMoveAction a = (HandMoveAction) tutController.res.getActions().get(i);
          a.finish();
        }
      }
    }

    newGame();
    GMain.hud().fireEvent("headerEvent", "show" ,0, null);
    GMain.hud().fireEvent("footerEvent", "show" ,0, null);
//    this.setY(0, AL.bl);

  }

}





