package com.game.controller;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import com.game.gamemodel.GameBallModel;

public class BirdLoader {
  public static Array<GameBallModel> loadBird(JsonValue list){
    Array<GameBallModel> res = new Array<>();
    if (list == null)
      return null;
    for (int i = 0; i < list.size; i++){
      GameBallModel ball = new GameBallModel(i);
      for (int j = 0; j < list.get(i).size; j++){
        ball.idColor = list.get(i).getInt(j);
      }
      res.add(ball);
    }
    return res;
  }
}
