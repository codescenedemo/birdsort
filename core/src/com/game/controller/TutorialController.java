package com.game.controller;

import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.SequenceAction;

import com.game.gamelogic.actions.HandMoveAction;
import com.game.object.BoardGame;

public class TutorialController {
  int lv;
  public SequenceAction res;

  public TutorialController(int lv){
    this.lv = lv;
  }

  public void startTut(){

    switch (lv){

      case 1:
        res = Actions.sequence(
          HandMoveAction.getAction(1),
          HandMoveAction.getAction(2)
        );
        break;
      case 2:
        res = Actions.sequence(
            HandMoveAction.getAction(3),
            HandMoveAction.getAction(4),
            HandMoveAction.getAction(5),
            HandMoveAction.getAction(6)
        );
        break;

      case 4:
        res = Actions.sequence(
            HandMoveAction.getAction(7),
            HandMoveAction.getAction(8)

        );
        break;
    }
    if (res != null){
      BoardGame.boardGame.addAction(res);
    }

  }
}
