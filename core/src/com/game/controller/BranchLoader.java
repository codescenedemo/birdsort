package com.game.controller;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonValue;
import com.game.gamemodel.BottleModel;
import com.game.gamemodel.GameBallModel;
import com.game.gamemodel.GameBottleModel;
import com.game.gamemodel.LevelModel;

public class BranchLoader {
  public static Array<GameBottleModel> loadBottle(JsonValue list) {
    Array<GameBottleModel> res = new Array<>();
    if (list == null) {
      System.out.println("BOTTLE LIST IS NULL");
      return null;
    }


    for (int i = 0; i < list.size; i++) {
      GameBottleModel bottle = new GameBottleModel();
      bottle.balls = new Array<>();
      bottle.id = i + 1;

      int[] arr = list.get(i).get("balls").asIntArray();
      for (int j = 0; j < arr.length; j++) {
        GameBallModel ball = new GameBallModel(arr[j]);
        bottle.balls.add(ball);
      }

      //clock init
      if (list.get(i).get("clock") != null) {
        int[] clockArr = list.get(i).get("clock").asIntArray();
        for (int k = 0; k < clockArr.length; k++) {
          if (clockArr[k] == 1){
            bottle.balls.get(k).hasClock = true;
          }
        }
      }

      //sleeping init
      if (list.get(i).get("sleep") != null){
        int[] sleepArr = list.get(i).get("sleep").asIntArray();
        for (int k = 0; k < sleepArr.length; k++){
          if (sleepArr[k] == 1){
            bottle.balls.get(k).isSleep = true;
          }
        }
      }

      //bomb init
      if (list.get(i).get("bomb") != null){
        int[] bombs = list.get(i).get("bomb").asIntArray();
        for (int k = 0; k < bombs.length; k++){
          if (bombs[k] != - 1){
            bottle.balls.get(k).bombIdx = bombs[k];
          }
        }
      }
      //egg init
      if (list.get(i).get("egg") != null){
        int[] eggs = list.get(i).get("egg").asIntArray();
        for (int k = 0; k < eggs.length; k++){
          if (eggs[k] != - 1){
            bottle.balls.get(k).eggId = eggs[k];
          }
        }
      }

      //hammer
      if (list.get(i).get("hammer") != null){
        int[] hammers = list.get(i).get("hammer").asIntArray();
        for (int k = 0; k < hammers.length; k++){
          if (hammers[k] != - 1){
            bottle.balls.get(k).hammerIdx = hammers[k];
          }
        }
      }


      res.add(bottle);
    }
    return res;
  }
  public static Array<GameBottleModel> loadBottle(LevelModel model) {
    Array<GameBottleModel> res = new Array<>();
    if (model.bottles == null || model.bottles.isEmpty()) {
      System.out.println("BOTTLE LIST IS NULL");
      return null;
    }


    for (int i = 0; i < model.bottles.size; i++) {
      BottleModel bottleModel = model.bottles.get(i);

      GameBottleModel bottle = new GameBottleModel();
      bottle.balls = new Array<>();
      bottle.id = i + 1;


      int[] arr = bottleModel.balls;
      for (int j = 0; j < arr.length; j++) {
        GameBallModel ball = new GameBallModel(arr[j]);
        bottle.balls.add(ball);
      }

      //clock init
      if (bottleModel.clock != null) {
        int[] clockArr = bottleModel.clock;
        for (int k = 0; k < clockArr.length; k++) {
          if (clockArr[k] == 1){
            bottle.balls.get(k).hasClock = true;
          }
        }
      }

      //sleeping init
      if (bottleModel.sleep != null){
        int[] sleepArr = bottleModel.sleep;
        for (int k = 0; k < sleepArr.length; k++){
          if (sleepArr[k] == 1){
            bottle.balls.get(k).isSleep = true;
          }
        }
      }

      //bomb init
      if (bottleModel.bomb != null){
        int[] bombs = bottleModel.bomb;
        for (int k = 0; k < bombs.length; k++){
          if (bombs[k] != - 1){
            bottle.balls.get(k).bombIdx = bombs[k];
          }
        }
      }
      //egg init
      if (bottleModel.egg != null){
        int[] eggs = bottleModel.egg;
        for (int k = 0; k < eggs.length; k++){
          if (eggs[k] != - 1){
            bottle.balls.get(k).eggId = eggs[k];
          }
        }
      }

      //hammer
      if (bottleModel.hammer != null){
        int[] hammers = bottleModel.hammer;
        for (int k = 0; k < hammers.length; k++){
          if (hammers[k] != - 1){
            bottle.balls.get(k).hammerIdx = hammers[k];
          }
        }
      }


      res.add(bottle);
    }
    return res;
  }
}
