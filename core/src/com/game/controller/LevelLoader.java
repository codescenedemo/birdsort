package com.game.controller;

import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.JsonReader;
import com.badlogic.gdx.utils.JsonValue;
import com.game.GMain;
import com.game.gamemodel.GameBottleModel;
import com.game.gamemodel.LevelModel;

public class LevelLoader {
  static Array<GameBottleModel> bottleModels;

  public static Array<GameBottleModel> getLv(int lv){
    FileHandle file;
    JsonReader reader = new JsonReader();
    String levelJon = GMain.platform().GetConfigStringValue("level"+lv, "null");
    if (levelJon.equals("null")){
      //read from file
      LevelModel model = GMain.getAssetManager().getLevelData("lv" + lv);
      bottleModels = BranchLoader.loadBottle(model);
    }else {
      try {
        JsonValue value = reader.parse(levelJon);
        bottleModels = BranchLoader.loadBottle(value.get("bottles"));
        if (bottleModels == null){
          LevelModel model = GMain.getAssetManager().getLevelData("lv" + lv);
          bottleModels = BranchLoader.loadBottle(model);
        }
      }catch (Exception e){
        e.printStackTrace();
        LevelModel model = GMain.getAssetManager().getLevelData("lv" + lv);
        bottleModels = BranchLoader.loadBottle(model);
      }
    }

    return bottleModels;
  }


}
