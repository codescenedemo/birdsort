package com.game.gamelogic.actions;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.Actor;

public class AbsPosition {
  private static final Vector2 temp = new Vector2();

  public static Vector2 absPos(Actor c, float x, float y, int align) {
    return c.localToStageCoordinates(temp.cpy().set(c.getX(align) + x - c.getX(), c.getY(align) + y - c.getY()));
  }

  public static Vector2 absPos(Actor c, int align) {
    return c.localToStageCoordinates(temp.cpy().set(c.getX(align) - c.getX(), c.getY(align) - c.getY()));
  }
}
