package com.game.gamelogic.actions;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.TemporalAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.core.util.GLayer;
import com.core.util.GStage;
import com.core.utils.hud.AL;
import com.core.utils.hud.builders.IB;
import com.core.utils.hud.builders.LB;
import com.game.controller.TutorialController;
import com.game.gamedto.TutorialDTO;
import com.game.gameview.Branch;
import com.game.object.BoardGame;

public class HandMoveAction extends TemporalAction {

  int id;
  TutorialDTO dto;
  Image hand;
  String content;
  Label tutLabel;
  public static HandMoveAction getAction(int id){
    HandMoveAction action = new HandMoveAction();


    action.id = id;
    action.dto = TutorialDTO.tuts.get(id);
    System.out.println("Y: " + action.dto.startY);
    action.hand = IB.New().drawable("hand").pos(action.dto.startX, action.dto.startY).parent(GLayer.top.getGroup()).build();
    action.hand.setPosition(action.dto.startX, action.dto.startY);
    action.hand.setTouchable(Touchable.disabled);
    action.hand.setVisible(false);
    action.setDuration(2000000);
    action.content = action.dto.content;
    action.tutLabel = LB.New().font("birdsort_eff60").text(action.content).wrap(true, 700).pos(0, action.dto.posY).align(AL.cb).parent(BoardGame.boardGame).build();
    action.tutLabel.setVisible(false);
    action.hand.setZIndex(90000);


    return action;
  }

  @Override
  protected void begin() {
    System.out.println("Begin phase: " + dto.id);
    super.begin();
    hand.setVisible(true);
    tutLabel.setVisible(true);
    hand.addAction(Actions.forever(
        Actions.sequence(
            Actions.scaleTo(0.8f, 0.8f, 0.3f),
            Actions.scaleTo(1, 1, 0.3f)
        )
    ));

    for (int i = 0; i < BoardGame.boardGame.branchView.size; i++){
      Branch branch = BoardGame.boardGame.branchView.get(i);
      if (branch.getId() != dto.branchId){
        branch.setTouchable(Touchable.disabled);
      }
      else {
        branch.clearListeners();
        branch.addListener(new ClickListener(){
          @Override
          public void clicked(InputEvent event, float x, float y) {
            super.clicked(event, x, y);
            BoardGame.boardGame.branchClick(branch);
            finish();
          }
        });
      }
    }

  }

  @Override
  public void finish() {
    System.out.println("FINISH PHASE: " + dto.id);
    super.finish();
    BoardGame.boardGame.intitBranchEvent();
    hand.remove();
    if (dto.removeLabel){
      tutLabel.remove();
    }
  }

  @Override
  public boolean act(float delta) {

    return super.act(delta);
  }

  @Override
  protected void update(float percent) {

  }
}
