package com.platform;

import com.badlogic.gdx.utils.ObjectMap;
import com.core.utils.SessionLoader;
import com.gameservice.PlayService;

public interface IPlat {
  void log(String str);

  String GetDefaultLanguage();

  boolean isVideoRewardReady();

  void ShowVideoReward(AdsClose callback);

  void ShowRewardInterstitial(AdsClose callback);

  void ShowFullscreen(AdsClose onshow, AdsClose ondone);

  void ShowUIFullscreen();

  void ShowNative(boolean visible);

  boolean IsNativeAdvancedReady();

  boolean isFullScreenReady();

  void ShowBanner(boolean visible);

  int GetConfigIntValue(String name, int defaultValue);

  String GetConfigStringValue(String name, String defaultValue);


  void CrashKey(String key, String value);

  void CrashLog(String log);

  int GetBannerHeight();

  void OpenStore();

  void TrackScreen(String screen);

  void TrackNotification(int notifyId, int dailyNotificationCount);

  void TrackCustomEvent(String event);

  void TrackGameStart(int serverVersion);

  void TrackEvaluate(String eventname, int hitCount, String evaluate);

  void TrackLevelInfo(String event, int level, float time);

  void TrackLevelInfo(String event, ObjectMap<String, Object> data);

  void TrackPlayerInfo(String event, int mode, int level);

  void TrackPlaneInfo(String event, int planeid, int level);

  void TrackVideoReward(String type);

  void TrackPlayerDead(String event, int mode, int difficult, int level, int parentModel, int shooterModel, boolean isBoss);

  int GetNotifyId();

  void SetDailyNotification(int id, String header, String content, int days, int hours);

  void CancelDailyNotification(int id);

  void TrackRating(int rateid, int rateFromLevel);

  void Restart();

  void ShowAdsTestSuit();

  void onShow();

  int dayDiff(long t1, long t2);

  String getMd5PathName(String filename);

  void preloadAssets(String assetTxt, PreloaderCallback callback);

  SessionLoader getSessionLoader();

  String getPlatform();

  public static interface PreloaderCallback {

    public void update(float percent, boolean isDone);

    public void error (String file);

  }

  PlayService getPlayService();
}
